/*
    Author : Ben Baik
    Name : OppTrigger
    Date : Prior to 14 June 2019
    Description: Watches for Opportunity Events
    Modification History :
    *************************************************************
    Version             Author          Date                        Description
    1                   Brad            17 June 2019                Refactoring best practices
	1.1					  Ben				 28 June 2019			   Added IsInitialSync flag check
*/
trigger OppTrigger on Opportunity (before update, after update, before insert, after insert) {
    Metadata__c initialSyncFlag = Metadata__c.getValues('IsInitialSync');
    
    if (initialSyncFlag != null && initialSyncFlag.Value__c == 'false') {
        OppTriggerHandler oppTriggerHandler = new OppTriggerHandler();
        oppTriggerHandler.handleTrigger(Trigger.new, Trigger.oldMap, Trigger.operationType);   
    }
}