/*
    Author : Vikram
    Name : RestServiceToUpsertGroupRecords
    Date : 26th March, 2019
    Description: Rest service to upsert Group records
    Modification History :
    *************************************************************
    Vesion               Author             Date                      Description
	1.1					 Vikram				18 June 2019			  Added logging functionality and few comments where necessary
*/

@RestResource(urlMapping='/UpsertGroupRecords/*')
global with sharing class RestServiceToUpsertGroupRecords{
    
    // class that represents SagittaGroup model
    global class SagittaGrp {		
        global String ItemCode { get; set; }
        global String ItemCodeHash { get; set; }
        global String Name { get; set; }
        global String Status { get; set; }
        global Boolean IsHidden { get; set; }
        global String EntityId { get; set; }
        global String EntityName { get; set; }        
    }
    
    // class represents response to caller
    global class ResponseHandler {
        public Boolean Success {get; set;}
        public String Status {get; set;}
        public String Message {get;set;}
    }

	// Deserialize the request body and insert the data
	// @param - No Param
	// @return - ResponseHandler    
    @HttpPost
    global static ResponseHandler doPost() {
        ResponseHandler obj = new ResponseHandler();
        RestRequest req = RestContext.request;
        String failedRecordIds = '';
        try {
            String postBody = req.requestBody.toString();
            // Deserialize the request body and converts into List<SagittaGrp>
            List<SagittaGrp> grps = (List<SagittaGrp>)JSON.deserialize(postBody, List<SagittaGrp>.class);
            List<Sagitta_Group__c> grpsToUpsert = new List<Sagitta_Group__c>();
            // For loop to iterate through all the incoming Group records
            for (SagittaGrp grp : grps) {
                Sagitta_Group__c newGrp = new Sagitta_Group__c();
                newGrp.Name = grp.Name;
                newGrp.ItemCode__c = grp.ItemCodeHash;
                newGrp.Sagitta_isHidden__c = grp.IsHidden;
                grpsToUpsert.add(newGrp);
            }
            Schema.SObjectField extIdField = Sagitta_Group__c.Fields.ItemCode__c;
            // To insert the list of records
            Database.UpsertResult[] results = Database.upsert(grpsToUpsert, extIdField, false);
            for (Database.UpsertResult result : results) {
                if (!result.isSuccess()) {
                    Database.Error[] upsertErrors = result.getErrors();
                    for (Database.Error upsertError : upsertErrors) {
                        failedRecordIds = failedRecordIds + upsertError.getMessage() + upsertError.getFields();
                    }
                }
            }
            // Response handler initialization once the insert is successful/failure
            // Insert log into Application Log object for request and response json
            if (String.isNotBlank(failedRecordIds)) {
                obj.success = false;
                obj.status='ERROR';
                obj.message= failedRecordIds;
                ApplicationLogger.logWebServiceMessage('RestServiceToUpsertGroupRecords', 'doPost', failedRecordIds, 
                                                   ApplicationLogger.ERROR, req.requestBody.toString(), JSON.serialize(obj));
            } else {
                obj.success = true;
                obj.status='SUCCESS';
                obj.message= 'SUCCESS';
                ApplicationLogger.logWebServiceMessage('RestServiceToUpsertGroupRecords', 'doPost', 'SUCCESS', 
                                                   ApplicationLogger.INFO, req.requestBody.toString(), JSON.serialize(obj));
            }
            
            return obj;
        } catch(Exception ex) {
            // Response handler initialization on exception
            RestContext.response.statusCode = 400;
            obj.success = false;
            obj.status='Error';
            obj.message=ex.getMessage();
            String message = 'Error: ' + ex.getMessage() + '; Stack Trace:' + ex.getStackTraceString();            
            // Insert log into Application Log object for request, response and error json
            ApplicationLogger.logWebServiceMessage('RestServiceToUpsertGroupRecords', 'doPost', message, 
                                                   ApplicationLogger.ERROR, req.requestBody.toString(), JSON.serialize(obj));
            return obj;
        }        
    }

}