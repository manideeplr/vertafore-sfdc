/*
    Author : Vikram
    Name : TestDataFactoryUtil
    Date : 14 June 2019
    Description: To help create unit test data.
    Modification History :
    *************************************************************
    Version               Author             Date                      Description
	1					  Vikram			 14 June 2019     		   Original
*/
@isTest
public class TestDataFactoryUtil {
	// Summary:
    // Creates Salesforce Standard User for unit tests
    // 
    // Details:
    // @params N/A
    // @return User - Standard User that was created
    public static User createStandardUser() {
        Profile p = [SELECT Id FROM Profile WHERE Name='Standard User']; 
        User user = new User(Alias = 'standt', 
                             Email='standarduser@testorg.com', 
            				 EmailEncodingKey='UTF-8',
                             LastName='Testing',
                             LanguageLocaleKey='en_US', 
            				 LocaleSidKey='en_US',
                             ProfileId = p.Id, 
            				 TimeZoneSidKey='America/Los_Angeles',
                             UserName='vert-test@vertafore.com');
        insert user;
        return user;
    }
    
    // Summary:
    // Creates Sagitta Division for unit tests
    // 
    // Details:
    // @params Integer noOfDivisions - the number of division to be created
    // @return List<_Sagitta_Division__c> - list of division that were created
    public static List<Sagitta_Division__c> createSagittaDivisions(Integer noOfDivisions) {
        List<Sagitta_Division__c> divList = new List<Sagitta_Division__c>();
        
        Sagitta_Division__c div;
        for(Integer i = 0; i < noOfDivisions; i++) {
            div = new Sagitta_Division__c(Name = 'Division'+ i);
            divList.add(div);
        }
        insert divList;
        return divList;
    }
    
    // Summary:
    // Creates Sagitta Branches for unit tests
    // 
    // Details:
    // @params Integer noOfBrs - the number of branches to be created
    // @return List<Sagitta_Branch__c> - list of branches that were created
    public static List<Sagitta_Branch__c> createSagittaBranches(Integer noOfBrs) {
        List<Sagitta_Branch__c> brList = new List<Sagitta_Branch__c>();
        
        Sagitta_Branch__c branch;
        for(Integer i = 0; i < noOfBrs; i++) {
            branch = new Sagitta_Branch__c(Name = 'Branch'+ i);
            brList.add(branch);
        }
        insert brList;
        return brList;
    }
    
    // Summary:
    // Creates divBr junctions for unit tests
    // 
    // Details:
    // @params Integer noOfDivBrJun - the number of divBr junctions to be created
    // @return List<DivBrJunction__c> - list of divBr junctions that were created
    public static List<DivBrJunction__c> createSagittaDivBrJunction(Integer noOfDivBrJun) {
        List<DivBrJunction__c> divBrJnList = new List<DivBrJunction__c>();
        List<Sagitta_Division__c> divList = TestDataFactoryUtil.createSagittaDivisions(noOfDivBrJun);
        List<Sagitta_Branch__c> brList = TestDataFactoryUtil.createSagittaBranches(noOfDivBrJun);
        
        DivBrJunction__c divBrJn;
        for(Integer i = 0; i < noOfDivBrJun; i++) {
            divBrJn = new DivBrJunction__c(
                Name = 'DivBrJn'+ i, 
                Sagitta_Division__c = divList[i].Id,
                Sagitta_Branch__c = brList[i].Id
            );
            divBrJnList.add(divBrJn);
        }
        insert divBrJnList;
        return divBrJnList;
    }
}