// 
// (c) 2019 Vertaore, Inc.
// Delete the Application Logs older than 30 Days
// 14 June 2019     Vikram       Original
//

global class DeleteApplicationLogsBatch Implements Database.batchable<sobject>{
    
    global Database.QueryLocator start(Database.BatchableContext BC){
        date d=system.today().adddays(-30);
        string query = 'SELECT Id FROM Application_Log__c where CreatedDate <=:d';
        return Database.getQueryLocator(String.escapeSingleQuotes(query));
    }
    
    global  void execute(Database.BatchableContext BC,List<SObject> scope){
        delete scope;
    }
    
    global void finish(Database.BatchableContext BC){
    }
}