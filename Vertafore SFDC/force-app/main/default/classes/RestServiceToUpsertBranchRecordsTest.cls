@isTest
public class RestServiceToUpsertBranchRecordsTest {

  	@isTest
	static void test1(){
		List<RestServiceToUpsertBranchRecords.SagittaBrn> itmToInsert = new List<RestServiceToUpsertBranchRecords.SagittaBrn>();
		RestServiceToUpsertBranchRecords.SagittaBrn itm = new RestServiceToUpsertBranchRecords.SagittaBrn();
        itm.ItemCode = '1';
        itm.ItemCodeHash = '033041071';
        itm.Name = 'Test';
        itm.Status = 'A';
        itm.IsHidden = true;
        itm.EntityId = '162';
        itm.EntityName = 'SagittaBranchEntity';
        itmToInsert.add(itm);
        String myJSON = JSON.serialize(itmToInsert);
		

        RestRequest request = new RestRequest();
        request.requestUri ='https://test.salesforce.com/services/apexrest/UpsertBranchRecords';
        request.httpMethod = 'POST';
		request.requestBody = Blob.valueof(myJSON);
		
        RestContext.request = request;
		RestServiceToUpsertBranchRecords.ResponseHandler res = RestServiceToUpsertBranchRecords.doPost();
		
        List<Sagitta_Branch__c> divs = [SELECT Id FROM Sagitta_Branch__c];
        system.assertEquals(1, divs.size());		
	}
    
    @isTest
	static void test2(){
		RestServiceToUpsertBranchRecords.SagittaBrn itm = new RestServiceToUpsertBranchRecords.SagittaBrn();
        itm.ItemCode = '1';
        itm.ItemCodeHash = '033041071';
        itm.Name = 'Test';
        itm.Status = 'A';
        itm.IsHidden = true;
        itm.EntityId = '162';
        itm.EntityName = 'SagittaBranchEntity';
        String myJSON = JSON.serialize(itm);
		

        RestRequest request = new RestRequest();
        request.requestUri ='https://test.salesforce.com/services/apexrest/UpsertBranchRecords';
        request.httpMethod = 'POST';
		request.requestBody = Blob.valueof(myJSON);
        
        RestResponse response = new RestResponse();
        response.statusCode = 400;
		
        RestContext.request = request;
        RestContext.response = response;
		RestServiceToUpsertBranchRecords.ResponseHandler res = RestServiceToUpsertBranchRecords.doPost();
		
        List<Sagitta_Branch__c> divs = [SELECT Id FROM Sagitta_Branch__c];
        system.assertEquals(0, divs.size());		
	}
}