/*
    Author : Vikram
    Name : MergedAccountsSyncBatch
    Date : 23 July, 2019
    Description: Used to sync Accounts to Sagitta after the AccountMergeJob is done.
    Modification History :
    *************************************************************
    Version             Author          Date                        Description
*/

global class MergedAccountsSyncBatch implements Database.Batchable<sObject>,Database.AllowsCallouts{
    
    //Set of Ids of merged-updated accounts
    
    Set<Id> MainAccounts = new Set<Id>();
    
    global MergedAccountsSyncBatch(Set<Id> listMainAccounts)
    {
        mainAccounts = listMainAccounts;
    }
    
    global Database.QueryLocator start(Database.BatchableContext BC)
    {
        return DataBase.getQueryLocator([SELECT Id FROM account WHERE Id IN : mainAccounts]);
    }
    
    // Summary:
    // Calls the HttpClient class to send notification or callout to Sagitta for the updates made by user.
    //
    // @param context - 
    // @param scope -
    global void execute(Database.BatchableContext BC, List<sObject> scopeAcc)
    {
        
        for (Account acc : (List<Account>)scopeAcc){
            try {
				string xmlBody = SFAMessenger.createXMLBody(acc.Id);                
                HttpClient client = new HttpClient();
                client.SendXMLRequest(xmlBody);
            }
            catch (Exception ex) {         
                String message = 'Error syncing accounts. ;' + 
                'Error: ' + ex.getMessage() + '; Stack Trace:' + ex.getStackTraceString();
            	ApplicationLogger.logMessage('MergedAccountsSyncBatch', 'execute', message, ApplicationLogger.ERROR);        
            }
        }
    }
    
    global void finish(Database.BatchableContext BC)
    {
    }    
}