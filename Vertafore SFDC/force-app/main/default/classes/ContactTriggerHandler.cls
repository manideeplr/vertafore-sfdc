/*
    Author : 
    Name : ContactTriggerHandler
    Date : Prior to 14 June 2019
    Description: Validates conditions to send message to sync completed Tasks
                 Ensures related Account is synced
                 If contact is reassigned to new, synced SF Account, updates
                 Sagitta Contact to the new Customer
    Modification History :
    *************************************************************
    Version             Author          Date                        Description
    1                   Brad            17 June 2019                Refactoring
*/
public class ContactTriggerHandler {
    // Summary:
    // Handles Contact trigger behavior
    // 
    // Details:
    // @param workingRecords - List of all Contact records that are being inserted or updated.
    // @param triggerEvent - AFTER_INSERT or AFTER_UPDATE
    // @return TriggerHandlerStatus - MESSAGES_SENT or NO_MESSAGES_SENT
	public TriggerHandlerStatus handleTrigger(List<Contact> newRecords, Map<Id, Contact> oldRecords, System.TriggerOperation triggerEvent) { 
        try {
            if (triggerEvent == TriggerOperation.AFTER_INSERT || triggerEvent == TriggerOperation.AFTER_UPDATE) {
            Metadata__c intFlag = Metadata__c.getValues('IntegrationFlag');
            for (Contact contact : newRecords){
                // Get contact's parent account ID
                if (String.isBlank(contact.AccountId)) {
                    return TriggerHandlerStatus.NO_MESSAGES_SENT;
                }
                
                //Get Sagitta customer ID from parent account
                Account newAccount = [SELECT Sagitta_Customer_ID__c FROM Account WHERE Id =: contact.AccountId];
                
                // if parent account is synced with Sagitta
                if (String.isNotBlank(newAccount.Sagitta_Customer_ID__c) && (intFlag != null && string.isNotBlank(intFlag.Value__c) && intFlag.Value__c == 'true')){
                    String xmlBody = SFAMessenger.createXMLBody(contact.Id);
                    Id jobId = SFAMessenger.createAndSendJob(xmlBody);
					return TriggerHandlerStatus.MESSAGES_SENT;
                }
                // if newly assigned account is not synced and there are old records
                else if (String.isBlank(newAccount.Sagitta_Customer_ID__c) && oldRecords != null) {
                    // get old contact and old account
                    Contact oldContact = oldRecords.get(contact.Id);
                    Account oldAccount = [SELECT Sagitta_Customer_ID__c FROM Account WHERE Id =: oldContact.AccountId];
                    // if old account was synced
                    if (String.isNotBlank(oldAccount.Sagitta_Customer_ID__c) && (intFlag != null && string.isNotBlank(intFlag.Value__c) && intFlag.Value__c == 'true')) {
                        string integrationId = 'INTEGRATION-ID12345';
                        String xmlBody = SFAMessenger.createXMLBody(contact.Id);
                        Id jobId = SFAMessenger.createAndSendJob(xmlBody);
                        return TriggerHandlerStatus.MESSAGES_SENT;
                    }
                }
                else {
                    return TriggerHandlerStatus.NO_MESSAGES_SENT;
                }
            }
        }
    } catch (Exception ex) {
        String message = 'There was a problem validating Contact information. ; ' +
            'Error: ' + ex.getMessage() + '; Stack Trace: ' + ex.getStackTraceString();
    //    ApplicationLogger.logMessage('ContactTriggerHandler', 'handleTrigger', message, ApplicationLogger.ERROR);
        throw ex;
    }
        
        return TriggerHandlerStatus.NO_MESSAGES_SENT;
    }
}