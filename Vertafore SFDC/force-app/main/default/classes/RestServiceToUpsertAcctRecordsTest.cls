@isTest
public class RestServiceToUpsertAcctRecordsTest {
	@testSetup static void TestSetup() {
        createIntegrationFlag('true');
        createIsInitialSyncFlag('false');
        
        Sagitta_Employee__c exec = new Sagitta_Employee__c(
        	Name = 'Jim Bond',
            Sagitta_Employee_Code__c = '!!4',
            Sagitta_Is_Executive__c = true,
            Sagitta_Is_Representative__c = false,
            Sagitta_Entity_ID__c = '54321',
            Sagitta_Employee_Status__c = 'Active'
        );
        insert exec;
        
        Sagitta_Employee__c rep = new Sagitta_Employee__c(
        	Name = 'Nichelle Foster',
            Sagitta_Employee_Code__c = '!\"2',
            Sagitta_Is_Executive__c = false,
            Sagitta_Is_Representative__c = true,
            Sagitta_Entity_ID__c = '12345',
            Sagitta_Employee_Status__c = 'Active'
        );
        insert rep;
        
        Sagitta_Activity_Action_Type__c activityActionType = new Sagitta_Activity_Action_Type__c();
        activityActionType.Sagitta_Activity_Action_Id__c = '26544';
        Sagitta_Issuing_Carrier__c issuingCarrier = new Sagitta_Issuing_Carrier__c(
            Sagitta_Entity_ID__c= 'ID text', 
            Sagitta_Carrier_Type__c= 'N',
            Sagitta_IsHidden__c = false,
            Sagitta_Issuing_Carrier_Code__c= 'text'
        );
        insert activityActionType;
        insert issuingCarrier;
        
        Sagitta_Agency_Settings__c agencySettings1 = new Sagitta_Agency_Settings__c(
            Sagitta_Agency_Number__c='text', 
            Default_Activity_Action_Type__c= [SELECT Id FROM Sagitta_Activity_Action_Type__c LIMIT 1].Id,
            Default_Bill_Method__c= 'A', 
            Default_Issuing_Carrier__c= [SELECT Id FROM Sagitta_Issuing_Carrier__c LIMIT 1].Id, 
            Default_Policy_Number__c='text', 
            Integration_User_Id__c='text', 
            Vertafore_Id__c='text',
            Default_Agency_Admin__c = [SELECT Id FROM Sagitta_Employee__c WHERE Name =: exec.Name].Id
        );
        insert agencySettings1;
        

        
        Sagitta_Business_Entity__c businessEntity = new Sagitta_Business_Entity__c(
        	Name = 'Association',
            Sagitta_Business_Entity_Id__c = '1111'
        );
        insert businessEntity;
        
        Sagitta_Division__c SagittaDivision = new Sagitta_Division__c(
        	Name = 'Division One',
            ItemCode__c = '$$$'
        );
        insert SagittaDivision;
        
        Sagitta_Branch__c SagittaBranch = new Sagitta_Branch__c(
        	Name = 'Branch One',
            ItemCode__c = '***'
        );
        insert SagittaBranch;
        
        Sagitta_Department__c SagittaDepartment = new Sagitta_Department__c(
        	Name = 'Department One',
            ItemCode__c = '+++'
        );
        insert SagittaDepartment;
        
        Sagitta_Group__c SagittaGroup = new Sagitta_Group__c(
        	Name = 'Group One',
            ItemCode__c = '&&&'
        );
        insert SagittaGroup;
        
        DivBrJunction__c divBrJunc = new DivBrJunction__c(Sagitta_Division__c = SagittaDivision.Id, Sagitta_Branch__c = SagittaBranch.Id);
        BrDepJunction__c brDepJunc = new BrDepJunction__c(Sagitta_Branch__c = SagittaBranch.Id, Sagitta_Department__c = SagittaDepartment.Id);
        DepGrpJunction__c depGrpJunc = new DepGrpJunction__c(Sagitta_Department__c = SagittaDepartment.Id, Sagitta_Group__c = SagittaGroup.Id);
        insert divBrJunc;
        insert brDepJunc;
        insert depGrpJunc;
        
        Sagitta_Mapping__c mapping = new Sagitta_Mapping__c(
        	Name = 'MyMapping',
            Sagitta_Assigned_To__c = UserInfo.getUserId(),
            Sagitta_Division__c = SagittaDivision.Id,
            Sagitta_Branch__c = SagittaBranch.Id,
            Sagitta_Department__c = SagittaDepartment.Id,
            Sagitta_Group__c = SagittaGroup.Id,
            Sagitta_Executive__c = exec.Id,
            Sagitta_Representative__c = rep.Id
        );
        insert mapping;
    }
    
    @isTest
    static void doPost_ShouldUpsertAcct_WhenDeliveredInJsonList(){
        List<RestServiceToUpsertAcctRecords.SagittaCustomer> itmToInsert = new List<RestServiceToUpsertAcctRecords.SagittaCustomer>();
        RestServiceToUpsertAcctRecords.SagittaCustomer itm = new RestServiceToUpsertAcctRecords.SagittaCustomer();
		itm.CustomerId = 'c5b25a06-f102-47b2-98cb-0f5c2a30c653';
        itm.CustomerNumber = '35';
        itm.CustomerType = 'C';
        itm.FirmName = 'Craig';
        itm.IsActive = 'Active';
        itm.BusinessEntity = 'Association';
        itm.EMail = '';
        itm.DoingBusinessAs = 'Customers 35';
        itm.FirstName = 'Craig';
        itm.MiddleName = '';
        itm.Last = 'Taylor';
        itm.CustomerWebAddress = '';
        itm.AddressLine1 = '555 S Main St';
        itm.AddressLine2 = 'Ste 500';
        itm.City = 'Denver';
        itm.State = 'CO';
        itm.ZipCode = '80202';
        itm.HomeAreaCode = '303';
        itm.HomePhone = '5555555';
        itm.HomeExtension = '123';
        itm.BusinessAreaCode = '720';
        itm.BusinessPhone = '4444444';
        itm.BusinessExtension = '321';
        itm.IsPersonalCust = true;
        itm.IsCommercialCustomer = false;
        itm.IsBenefitCustomer = false;
        itm.IsFinancialCustomer = false;
        itm.IsHealthCustomer = false;
        itm.IsLifeCustomer = false;
        itm.IsNonPropertyAndCasualtyCustomer = false;
        itm.GLDivisionCode = '$$$';
        itm.GLBranchCode = '***';
        itm.GLDepartmentCode = '+++';
        itm.GLGroupCode = '&&&';
        itm.AccountExecCode = '!!4';
        itm.AccountRepCode = '!\"2';
        itmToInsert.add(itm);
        String myJSON = JSON.serialize(itmToInsert);
        
        RestRequest request = new RestRequest();
        request.requestUri ='https://test.salesforce.com/services/apexrest/SIV/UpsertAcctRecords';
        request.httpMethod = 'POST';
        request.requestBody = Blob.valueof(myJSON);
        
        RestContext.request = request;
        RestServiceToUpsertAcctRecords.ResponseHandler res = RestServiceToUpsertAcctRecords.doPost();
        
        List<Account> accounts = [SELECT Id, Sagitta_Customer_ID__c FROM Account WHERE Sagitta_Customer_ID__c =: itm.CustomerId];
        system.assertEquals(1, accounts.size());    
    }
    
    @isTest
    static void doPost_ShouldNotUpsertAcct_WhenNotDeliveredInJsonList(){
        RestServiceToUpsertAcctRecords.SagittaCustomer itm = new RestServiceToUpsertAcctRecords.SagittaCustomer();
		itm.CustomerId = 'c5b25a06-f102-47b2-98cb-0f5c2a30c653';
        itm.CustomerNumber = '35';
        itm.CustomerType = 'C';
        itm.FirmName = 'Craig';
        itm.IsActive = 'Active';
        itm.BusinessEntity = 'Association';
        itm.EMail = '';
        itm.DoingBusinessAs = 'Customers 35';
        itm.FirstName = 'Craig';
        itm.MiddleName = '';
        itm.Last = 'Taylor';
        itm.CustomerWebAddress = '';
        itm.AddressLine1 = '555 S Main St';
        itm.AddressLine2 = 'Ste 500';
        itm.City = 'Denver';
        itm.State = 'CO';
        itm.ZipCode = '80202';
        itm.HomeAreaCode = '303';
        itm.HomePhone = '5555555';
        itm.HomeExtension = '123';
        itm.BusinessAreaCode = '720';
        itm.BusinessPhone = '4444444';
        itm.BusinessExtension = '321';
        itm.IsPersonalCust = true;
        itm.IsCommercialCustomer = false;
        itm.IsBenefitCustomer = false;
        itm.IsFinancialCustomer = false;
        itm.IsHealthCustomer = false;
        itm.IsLifeCustomer = false;
        itm.IsNonPropertyAndCasualtyCustomer = false;
        itm.GLDivisionCode = '$$$';
        itm.GLBranchCode = '***';
        itm.GLDepartmentCode = '+++';
        itm.GLGroupCode = '&&&';
        itm.AccountExecCode = '!!4';
        itm.AccountRepCode = '!\"2';
        String myJSON = JSON.serialize(itm);
        
        RestRequest request = new RestRequest();
        request.requestUri ='https://test.salesforce.com/services/apexrest/SIV/UpsertAcctRecords';
        request.httpMethod = 'POST';
        request.requestBody = Blob.valueof(myJSON);
        RestContext.request = request;
        
		RestResponse response = new RestResponse();
        response.statusCode = 400;
		RestContext.response = response;
        
        RestServiceToUpsertAcctRecords.ResponseHandler res = RestServiceToUpsertAcctRecords.doPost();
        
        List<Account> accounts = [SELECT Id FROM Account];
        system.assertEquals(0, accounts.size());    
    }
    
    @isTest
    static void doPost_ShouldUpdateAcct_IfAcctAlreadyExists(){
        Test.startTest();  
        List<Sagitta_Employee__c> execs = [SELECT Id FROM Sagitta_Employee__c WHERE Sagitta_Employee_Code__c =: '!!4'];
        Sagitta_Employee__c exec = execs.get(0);
        List<Sagitta_Employee__c> reps = [SELECT Id FROM Sagitta_Employee__c WHERE Sagitta_Employee_Code__c =: '!\"2'];
        Sagitta_Employee__c rep = reps.get(0);
        
        Account existingAcct = new Account();
		existingAcct.Name = 'Test Account';
        existingAcct.Sagitta_Customer_ID__c = 'c5b25a06-f102-47b2-98cb-0f5c2a30c653';
        existingAcct.Sagitta_Employee_Account_Exec__c = exec.Id;
        existingAcct.Sagitta_Employee_Account_Rep__c = rep.Id;
        
        insert existingAcct;
        Test.stopTest();
        
        
        RestServiceToUpsertAcctRecords.SagittaCustomer itm = new RestServiceToUpsertAcctRecords.SagittaCustomer();
		itm.CustomerId = 'c5b25a06-f102-47b2-98cb-0f5c2a30c653';
        itm.CustomerNumber = '35';
        itm.CustomerType = 'C';
        itm.FirmName = 'Craig';
        itm.IsActive = 'Active';
        itm.BusinessEntity = 'Association';
        itm.EMail = '';
        itm.DoingBusinessAs = 'Customers 35';
        itm.FirstName = 'Craig';
        itm.MiddleName = '';
        itm.Last = 'Taylor';
        itm.CustomerWebAddress = '';
        itm.AddressLine1 = '555 S Main St';
        itm.AddressLine2 = 'Ste 500';
        itm.City = 'Denver';
        itm.State = 'CO';
        itm.ZipCode = '80202';
        itm.HomeAreaCode = '303';
        itm.HomePhone = '5555555';
        itm.HomeExtension = '123';
        itm.BusinessAreaCode = '720';
        itm.BusinessPhone = '4444444';
        itm.BusinessExtension = '321';
        itm.IsPersonalCust = true;
        itm.IsCommercialCustomer = false;
        itm.IsBenefitCustomer = false;
        itm.IsFinancialCustomer = false;
        itm.IsHealthCustomer = false;
        itm.IsLifeCustomer = false;
        itm.IsNonPropertyAndCasualtyCustomer = false;
        itm.GLDivisionCode = '$$$';
        itm.GLBranchCode = '***';
        itm.GLDepartmentCode = '+++';
        itm.GLGroupCode = '&&&';
        itm.AccountExecCode = '!!4';
        itm.AccountRepCode = '!\"2';
        String myJSON = JSON.serialize(itm);
        
        RestRequest request = new RestRequest();
        request.requestUri ='https://test.salesforce.com/services/apexrest/SIV/UpsertAcctRecords';
        request.httpMethod = 'POST';
        request.requestBody = Blob.valueof(myJSON);
        
        RestResponse response = new RestResponse();
        response.statusCode = 400;
        
        RestContext.request = request;
        RestContext.response = response;
        RestServiceToUpsertAcctRecords.ResponseHandler res = RestServiceToUpsertAcctRecords.doPost();
        
        List<Account> accounts = [SELECT Id FROM Account];
        system.assertEquals(1, accounts.size());    
    }
    
    @isTest
    static void doPost_ShouldUpsertAcct_WithBusinessPhone_IfIsPersonalCustIsFalse() {
        List<RestServiceToUpsertAcctRecords.SagittaCustomer> itmToInsert = new List<RestServiceToUpsertAcctRecords.SagittaCustomer>();
        RestServiceToUpsertAcctRecords.SagittaCustomer itm = new RestServiceToUpsertAcctRecords.SagittaCustomer();
		itm.CustomerId = 'c5b25a06-f102-47b2-98cb-0f5c2a30c653';
        itm.CustomerNumber = '35';
        itm.CustomerType = 'C';
        itm.FirmName = 'Craig';
        itm.IsActive = 'Active';
        itm.BusinessEntity = 'Association';
        itm.EMail = '';
        itm.DoingBusinessAs = 'Customers 35';
        itm.FirstName = 'Craig';
        itm.MiddleName = '';
        itm.Last = 'Taylor';
        itm.CustomerWebAddress = '';
        itm.AddressLine1 = '555 S Main St';
        itm.AddressLine2 = 'Ste 500';
        itm.City = 'Denver';
        itm.State = 'CO';
        itm.ZipCode = '80202';
        itm.HomeAreaCode = '303';
        itm.HomePhone = '5555555';
        itm.HomeExtension = '123';
        itm.BusinessAreaCode = '720';
        itm.BusinessPhone = '4444444';
        itm.BusinessExtension = '321';
        itm.IsPersonalCust = false;
        itm.IsCommercialCustomer = false;
        itm.IsBenefitCustomer = false;
        itm.IsFinancialCustomer = false;
        itm.IsHealthCustomer = false;
        itm.IsLifeCustomer = false;
        itm.IsNonPropertyAndCasualtyCustomer = false;
        itm.GLDivisionCode = '$$$';
        itm.GLBranchCode = '***';
        itm.GLDepartmentCode = '+++';
        itm.GLGroupCode = '&&&';
        itm.AccountExecCode = '!!4';
        itm.AccountRepCode = '!\"2';
        itmToInsert.add(itm);
        String myJSON = JSON.serialize(itmToInsert);
        
        RestRequest request = new RestRequest();
        request.requestUri ='https://test.salesforce.com/services/apexrest/SIV/UpsertAcctRecords';
        request.httpMethod = 'POST';
        request.requestBody = Blob.valueof(myJSON);
        
        RestContext.request = request;
        RestServiceToUpsertAcctRecords.ResponseHandler res = RestServiceToUpsertAcctRecords.doPost();

        List<Account> accounts = [SELECT Id, Sagitta_Customer_ID__c, Phone FROM Account WHERE Sagitta_Customer_ID__c =: itm.CustomerId];      
        String expectedBusinessPhoneNumber = '(720) 444-4444 ext 321';
        System.assertEquals(1, accounts.size());
        if (accounts.size() == 1) {
            Account account = accounts.get(0);
        	System.assertEquals(account.Phone, expectedBusinessPhoneNumber);   
        }
    }
    
    @isTest
    static void doPost_ShouldUpsertAcct_WithMergedAddress_InSalesforce() {
        List<RestServiceToUpsertAcctRecords.SagittaCustomer> itmToInsert = new List<RestServiceToUpsertAcctRecords.SagittaCustomer>();
        RestServiceToUpsertAcctRecords.SagittaCustomer itm = new RestServiceToUpsertAcctRecords.SagittaCustomer();
		itm.CustomerId = 'c5b25a06-f102-47b2-98cb-0f5c2a30c653';
        itm.CustomerNumber = '35';
        itm.CustomerType = 'C';
        itm.FirmName = 'Craig';
        itm.IsActive = 'Active';
        itm.BusinessEntity = 'Association';
        itm.EMail = '';
        itm.DoingBusinessAs = 'Customers 35';
        itm.FirstName = 'Craig';
        itm.MiddleName = '';
        itm.Last = 'Taylor';
        itm.CustomerWebAddress = '';
        itm.AddressLine1 = '555 S Main St';
        itm.AddressLine2 = 'Ste 500';
        itm.City = 'Denver';
        itm.State = 'CO';
        itm.ZipCode = '80202';
        itm.HomeAreaCode = '303';
        itm.HomePhone = '5555555';
        itm.HomeExtension = '123';
        itm.BusinessAreaCode = '720';
        itm.BusinessPhone = '4444444';
        itm.BusinessExtension = '321';
        itm.IsPersonalCust = false;
        itm.IsCommercialCustomer = false;
        itm.IsBenefitCustomer = false;
        itm.IsFinancialCustomer = false;
        itm.IsHealthCustomer = false;
        itm.IsLifeCustomer = false;
        itm.IsNonPropertyAndCasualtyCustomer = false;
        itm.GLDivisionCode = '$$$';
        itm.GLBranchCode = '***';
        itm.GLDepartmentCode = '+++';
        itm.GLGroupCode = '&&&';
        itm.AccountExecCode = '!!4';
        itm.AccountRepCode = '!\"2';
        itmToInsert.add(itm);
        String myJSON = JSON.serialize(itmToInsert);
        
        RestRequest request = new RestRequest();
        request.requestUri ='https://test.salesforce.com/services/apexrest/SIV/UpsertAcctRecords';
        request.httpMethod = 'POST';
        request.requestBody = Blob.valueof(myJSON);
        
        RestContext.request = request;
        RestServiceToUpsertAcctRecords.ResponseHandler res = RestServiceToUpsertAcctRecords.doPost();

        List<Account> accounts = [SELECT Id, BillingStreet FROM Account WHERE Sagitta_Customer_ID__c =: itm.CustomerId];      
        String expectedBillingStreet = '555 S Main St\nSte 500';
        System.assertEquals(1, accounts.size());
        if (accounts.size() == 1) {
            Account account = accounts.get(0);
        	System.assertEquals(account.BillingStreet, expectedBillingStreet);   
        }
    }
    
    private static void createIntegrationFlag(String flag) {
        Metadata__c intFlag = new Metadata__c();
        intFlag.Name = 'IntegrationFlag';
        intFlag.Value__c = flag;
        insert intFlag;
    }
    
    private static void createIsInitialSyncFlag(String flag) {
        Metadata__c isInitialSyncFlag = new Metadata__c();
        isInitialSyncFlag.Name = 'IsInitialSync';
        isInitialSyncFlag.Value__c = flag;
        insert isInitialSyncFlag;
    }
}