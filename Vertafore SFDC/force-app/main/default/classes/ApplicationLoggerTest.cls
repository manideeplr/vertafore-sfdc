@isTest
public class ApplicationLoggerTest {

    public class ResponseHandler {
        public Boolean Success {get; set;}
        public String Status {get; set;}
        public String Message {get;set;}
    }
    
    @isTest
    public static void myTest1() {
        Map<String, String> metaMap = new Map<String, String>();
        metaMap.put('key1', 'value1');
        metaMap.put('key2', 'value2');
        
        String myJSON = JSON.serialize(metaMap);
        
        ResponseHandler obj = new ResponseHandler();
        obj.Success = true;
        obj.Status = 'Success';
        obj.Message = 'Success';
        
        ApplicationLogger.logWebServiceMessage('ClassName', 'MethodName', 'Message - For Success or Failure', 
                                                   ApplicationLogger.INFO, myJSON, JSON.serialize(obj));
        List<Application_Log__c> logList = [SELECT Id FROM Application_Log__c];
        system.assertEquals(1, logList.size());
    }
    
    @isTest
    public static void myTest2() {        
        ApplicationLogger.logMessage('ClassName', 'MethodName', 'Message - For Success or Failure', ApplicationLogger.INFO);
        List<Application_Log__c> logList = [SELECT Id FROM Application_Log__c];
        system.assertEquals(1, logList.size());
    }
}