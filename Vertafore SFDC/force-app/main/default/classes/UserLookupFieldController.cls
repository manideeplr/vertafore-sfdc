/*
    Author : Jay Farnsworth
    Name : UserLookupFieldController
    Date : Prior to 17 June 2019
    Description: Adds functionality to LookUp field(s) on custom Aura components
    Modification History :
    *************************************************************
    Version             Author          Date                        Description
    1                   Brad            17 June 2019                Refactoring, logging
*/
public class UserLookupFieldController {
	@AuraEnabled
    // Summary:
    // Duplicates normal LookUp functions on custom LookUp fileds
    // 
    // Details:
    // @param searchString - User entered info in LookUp
    // @return List<String>
    public static List<String> searchForUser(String searchString) {
        try {
            List<String> result = new List<String>();
            if (!String.isBlank(searchString)) {
                searchString = '%' + searchString + '%';
            } else {
                return result;
            }
            List<User> users = [SELECT Name
                                FROM User
                                WHERE Name
                                LIKE: searchString ORDER
                                BY Name DESC LIMIT 5];
            for (User user: users) {
                result.add(user.Name);
            }
            result.sort();
            return result;
        } catch (Exception ex) {
            String message = 'Error looking up user; ' +
                'Error: ' + ex.getMessage() + '; Stack Trace: ' + ex.getStackTraceString();
            ApplicationLogger.logMessage('UserLookupFieldController', 'searchForUser', message, ApplicationLogger.ERROR);
            throw new AuraHandledException('There was an error looking up the user: ' + ex.getMessage()); 
        }
    }
}