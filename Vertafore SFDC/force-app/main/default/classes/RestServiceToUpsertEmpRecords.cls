/*
    Author : Vikram
    Name : RestServiceToUpsertEmpRecords
    Date : 26th March, 2019
    Description: Rest Service to upsert employee records
    Modification History :
    *************************************************************
    Vesion               Author             Date                      Description
	1.1					 Vikram				18 June 2019			  Added logging functionality and few comments where necessary
*/

@RestResource(urlMapping='/UpsertEmpRecords/*')
global with sharing class RestServiceToUpsertEmpRecords{
    
    // class that represents Sagitta Employee model
    global class SagittaEmp {
        global String EmployeeCode { get; set; }
        global String EmployeeCodeHash { get; set; }
        global String FirstName { get; set; }
        global String MiddleName { get; set; }
        global String LastName { get; set; }
        global String EmployeeStatus { get; set; }
        global Boolean Executive { get; set; }
        global Boolean Representative { get; set; }
        global String VimId { get; set; }
        global String EMail { get; set; }
        global String EntityId { get; set; }
        global String EntityName { get; set; }        
    }
    
    // class represents response to caller
    global class ResponseHandler {
        public Boolean Success {get; set;}
        public String Status {get; set;}
        public String Message {get;set;}
    }
    
    // Deserialize the request body and insert the data
	// @param - No Param
	// @return - ResponseHandler
    @HttpPost
    global static ResponseHandler doPost() {
        ResponseHandler obj = new ResponseHandler();
        RestRequest req = RestContext.request;
        String failedRecordIds = '';
        try {
            // This map converts single character received from Sagitta into readable string
            Map<string, string> empStatus = new Map<string, string>();
            empStatus.put('A', 'Active');
            empStatus.put('I', 'Inactive');
            empStatus.put('R', 'Retired');
            empStatus.put('D', 'Deleted');
            
            String postBody = req.requestBody.toString();
            // Deserialize the request body and converts into List<SagittaEmp>
            List<SagittaEmp> emps = (List<SagittaEmp>)JSON.deserialize(postBody, List<SagittaEmp>.class);
            List<Sagitta_Employee__c> empsToUpsert = new List<Sagitta_Employee__c>();
            // For loop to iterate through all the incoming Employee records
            for (SagittaEmp emp : emps) {
                Sagitta_Employee__c newEmp = new Sagitta_Employee__c();
                newEmp.Name = emp.FirstName + ' ' + emp.LastName;
                newEmp.Sagitta_Entity_ID__c = emp.EmployeeCodeHash;
                newEmp.Sagitta_Employee_Code__c = emp.EmployeeCodeHash;
                newEmp.Sagitta_Is_Executive__c = emp.Executive;
                newEmp.Sagitta_Is_Representative__c = emp.Representative;
                newEmp.Sagitta_Employee_Status__c = empStatus.get(emp.EmployeeStatus);
                empsToUpsert.add(newEmp);
            }
            Schema.SObjectField extIdfield = Sagitta_Employee__c.Fields.Sagitta_Entity_ID__c;
            // To insert the list of records
            Database.UpsertResult[] results = Database.upsert(empsToUpsert, extIdfield, false);
            for (Database.UpsertResult result : results) {
                if (!result.isSuccess()) {
                    Database.Error[] upsertErrors = result.getErrors();
                    for (Database.Error upsertError : upsertErrors) {
                        failedRecordIds = failedRecordIds + upsertError.getMessage() + upsertError.getFields();
                    }
                }
            }
            // Response handler initialization once the insert is successful/failure
            // Insert log into Application Log object for request and response json
            if (String.isNotBlank(failedRecordIds)) {
                obj.success = false;
                obj.status='ERROR';
                obj.message= failedRecordIds;
                ApplicationLogger.logWebServiceMessage('RestServiceToUpsertEmpRecords', 'doPost', failedRecordIds, 
                                                   ApplicationLogger.ERROR, req.requestBody.toString(), JSON.serialize(obj));
            } else {
                obj.success = true;
                obj.status='SUCCESS';
                obj.message= 'SUCCESS';
                ApplicationLogger.logWebServiceMessage('RestServiceToUpsertEmpRecords', 'doPost', 'SUCCESS', 
                                                   ApplicationLogger.INFO, req.requestBody.toString(), JSON.serialize(obj));
            }
            
            return obj;
        } catch(Exception ex) {
            // Response handler initialization on exception
            RestContext.response.statusCode = 400;
            obj.success = false;
            obj.status='Error';
            obj.message=ex.getMessage();
            String message = 'Error: ' + ex.getMessage() + '; Stack Trace:' + ex.getStackTraceString();            
            // Insert log into Application Log object for request, response and error json
            ApplicationLogger.logWebServiceMessage('RestServiceToUpsertEmpRecords', 'doPost', message, 
                                                   ApplicationLogger.ERROR, req.requestBody.toString(), JSON.serialize(obj));
            return obj;
        }        
    }

}