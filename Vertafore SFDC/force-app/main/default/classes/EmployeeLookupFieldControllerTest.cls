@isTest
public class EmployeeLookupFieldControllerTest {
    @testSetup
    public static void setup() {
        Sagitta_Employee__c amsExecOne = new Sagitta_Employee__c(Name = 'John', 
                                                                         Sagitta_Is_Executive__c = true, 
                                                                         Sagitta_Employee_Code__c = '654321',
                                                                         Sagitta_Entity_ID__c = '5235633',
                                                                         Sagitta_Employee_Status__c = 'Active');
        Sagitta_Employee__c amsExecTwo = new Sagitta_Employee__c(Name = 'Executive', 
                                                                         Sagitta_Is_Executive__c = true, 
                                                                         Sagitta_Employee_Code__c = 'ABCDEFG',
                                                                         Sagitta_Entity_ID__c = '5235637',
                                                                         Sagitta_Employee_Status__c = 'Active');
        Sagitta_Employee__c amsRepOne = new Sagitta_Employee__c(Name = 'Jane', 
                                                                        Sagitta_Is_Representative__c = true, 
                                                                        Sagitta_Employee_Code__c = '123456',
                                                                        Sagitta_Entity_ID__c = '5235625',
                                                                        Sagitta_Employee_Status__c = 'Active');
        Sagitta_Employee__c amsRepTwo = new Sagitta_Employee__c(Name = 'Representative', 
                                                                        Sagitta_Is_Representative__c = true, 
                                                                        Sagitta_Employee_Code__c = 'GFEDCBA',
                                                                        Sagitta_Entity_ID__c = '52354633',
                                                                        Sagitta_Employee_Status__c = 'Active');
        insert amsExecOne;
        insert amsExecTwo;
        insert amsRepOne;
        insert amsRepTwo;
    }
	@isTest
    public static void searchForExecs_ShouldReturn_ListOfExecs_WhenGivenSearchString() {
        string searchString = 'e';
        List<string> expected = new List<string> { 'Executive' };
        
        Test.startTest();
        List<string> actual = EmployeeLookupFieldController.searchForExecs(searchString);
        Test.stopTest();
        
        System.assertEquals(expected, actual);
    }
    
    @isTest
    public static void searchForReps_ShouldReturn_ListOfReps_WhenGivenSearchString() {
        string searchString = 'r';
        List<string> expected = new List<string> { 'Representative' };
        
        Test.startTest();
        List<string> actual = EmployeeLookupFieldController.searchForReps(searchString);
        Test.stopTest();
        
        System.assertEquals(expected, actual);
    }
}