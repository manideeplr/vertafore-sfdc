@isTest
public class RestServiceToUpsertVssoUserConfigTest {
	@testSetup static void TestSetup() {
        Sagitta_Employee__c exec = new Sagitta_Employee__c(
        	Name = 'Exec A',
            Sagitta_Employee_Code__c = '!!1',
            Sagitta_Is_Executive__c = true,
            Sagitta_Is_Representative__c = false,
            Sagitta_Entity_ID__c = '11111',
            Sagitta_Employee_Status__c = 'Active'
        );
        insert exec;
        
        exec = new Sagitta_Employee__c(
        	Name = 'Exec B',
            Sagitta_Employee_Code__c = '!!2',
            Sagitta_Is_Executive__c = true,
            Sagitta_Is_Representative__c = false,
            Sagitta_Entity_ID__c = '22222',
            Sagitta_Employee_Status__c = 'Active'
        );
        insert exec;
        
        exec = new Sagitta_Employee__c(
        	Name = 'Exec C',
            Sagitta_Employee_Code__c = '!!3',
            Sagitta_Is_Executive__c = true,
            Sagitta_Is_Representative__c = false,
            Sagitta_Entity_ID__c = '3333',
            Sagitta_Employee_Status__c = 'Active'
        );
        insert exec;
        
        Profile p = [SELECT Id FROM Profile WHERE Name='Standard User']; 
        User user = new User(Alias = 'standt', 
                             Email='standarduser@testorg.com', 
            				 EmailEncodingKey='UTF-8',
                             LastName='User A',
                             LanguageLocaleKey='en_US', 
            				 LocaleSidKey='en_US',
                             ProfileId = p.Id, 
            				 TimeZoneSidKey='America/Los_Angeles',
                             UserName='vert-testA@vertafore.com');
        insert user;
        
        user = new User(Alias = 'standt', 
                        Email='standarduser@testorg.com', 
            			EmailEncodingKey='UTF-8',
                        LastName='User B',
                        LanguageLocaleKey='en_US', 
            			LocaleSidKey='en_US',
                        ProfileId = p.Id, 
            			TimeZoneSidKey='America/Los_Angeles',
                        UserName='vert-testB@vertafore.com');
        insert user;
    }
    
    @isTest
    static void assignFreshUserToVimId(){
        
        User user = [SELECT Id, Vim_ID__c FROM User WHERE LastName = 'User A' LIMIT 1];
        List<Sagitta_Employee__c> employees = [SELECT Id, Name FROM Sagitta_Employee__c WHERE Name in ('Exec A', 'Exec B') ORDER BY Name];
        
        String executiveIds = '';
        for(Sagitta_Employee__c employee : employees){
            executiveIds = executiveIds + employee.Id + ',';
        }
        
        RestServiceToUpsertVssoUserConfig.VssoUserConfig vssoUserConfig = new RestServiceToUpsertVssoUserConfig.VssoUserConfig();
        vssoUserConfig.UserId = user.Id;
        vssoUserConfig.VimId = 'vimId123';
        vssoUserConfig.ExecutiveIds = executiveIds.substring(0, executiveIds.length() - 1);
        
        String myJSON = JSON.serialize(vssoUserConfig);
        
        RestRequest request = new RestRequest();
        request.requestUri ='https://test.salesforce.com/services/apexrest/SIV/UpsertVssoUserConfig';
        request.httpMethod = 'POST';
        request.requestBody = Blob.valueof(myJSON);
        RestContext.request = request;
        
        RestResponse response = new RestResponse();
        RestContext.response = response;                
        
        // Need Test.startTest and Test.stopTest to run @future method syncronously
        Test.startTest();    
        RestServiceToUpsertVssoUserConfig.ResponseHandler res = RestServiceToUpsertVssoUserConfig.doPost();
        Test.stopTest();
        
        user = [SELECT Id, Vim_ID__c FROM User WHERE Id =: vssoUserConfig.UserId LIMIT 1];
        system.assertEquals(vssoUserConfig.VimId, user.Vim_ID__c);  
        
        employees = [SELECT Id, Name FROM Sagitta_Employee__c WHERE Sagitta_Assign_To__c =: vssoUserConfig.UserId ORDER BY Name];
        system.assertEquals('Exec A', employees[0].Name);    
        system.assertEquals('Exec B', employees[1].Name);    
    }
    
    @isTest
    static void removeOneExecAndAssignAnotherToAUser(){
        
        //Set up user with Exec A and Exec B assigned
        User user = [SELECT Id, Vim_ID__c FROM User WHERE LastName = 'User A' LIMIT 1];
        List<Sagitta_Employee__c> employees = [SELECT Id, Name FROM Sagitta_Employee__c WHERE Name in ('Exec A', 'Exec B') ORDER BY Name];
        
        String executiveIds = '';
        for(Sagitta_Employee__c employee : employees){
            executiveIds = executiveIds + employee.Id + ',';
        }
        
        RestServiceToUpsertVssoUserConfig.VssoUserConfig vssoUserConfig = new RestServiceToUpsertVssoUserConfig.VssoUserConfig();
        vssoUserConfig.UserId = user.Id;
        vssoUserConfig.VimId = 'vimId123';
        vssoUserConfig.ExecutiveIds = executiveIds.substring(0, executiveIds.length() - 1);
        
        String myJSON = JSON.serialize(vssoUserConfig);
        
        RestRequest request = new RestRequest();
        request.requestUri ='https://test.salesforce.com/services/apexrest/SIV/UpsertVssoUserConfig';
        request.httpMethod = 'POST';
        request.requestBody = Blob.valueof(myJSON);
        RestContext.request = request;
        
        RestResponse response = new RestResponse();
        RestContext.response = response;     
        
        // Need Test.startTest and Test.stopTest to run @future method syncronously
        Test.startTest();    
        RestServiceToUpsertVssoUserConfig.ResponseHandler res = RestServiceToUpsertVssoUserConfig.doPost();
        
        
        //Change from Exec A and Exec B over to Exec A and Exec C
        employees = [SELECT Id, Name FROM Sagitta_Employee__c WHERE Name in ('Exec A', 'Exec C') ORDER BY Name];
        
        executiveIds = '';
        for(Sagitta_Employee__c employee : employees){
            executiveIds = executiveIds + employee.Id + ',';
        }
        
        vssoUserConfig = new RestServiceToUpsertVssoUserConfig.VssoUserConfig();
        vssoUserConfig.UserId = user.Id;
        vssoUserConfig.VimId = 'vimId123';
        vssoUserConfig.ExecutiveIds = executiveIds.substring(0, executiveIds.length() - 1);
        
        myJSON = JSON.serialize(vssoUserConfig);
        
        request = new RestRequest();
        request.requestUri ='https://test.salesforce.com/services/apexrest/SIV/UpsertVssoUserConfig';
        request.httpMethod = 'POST';
        request.requestBody = Blob.valueof(myJSON);
        RestContext.request = request;
        
        response = new RestResponse();
        RestContext.response = response;                
        
        res = RestServiceToUpsertVssoUserConfig.doPost();
        Test.stopTest();
        
        
        
        user = [SELECT Id, Vim_ID__c FROM User WHERE Id =: vssoUserConfig.UserId LIMIT 1];
        system.assertEquals(vssoUserConfig.VimId, user.Vim_ID__c);  
        
        employees = [SELECT Id, Name FROM Sagitta_Employee__c WHERE Sagitta_Assign_To__c =: vssoUserConfig.UserId ORDER BY Name];
        system.assertEquals('Exec A', employees[0].Name);    
        system.assertEquals('Exec C', employees[1].Name);    
    }
    
	@isTest
    static void reassignVimFromOneUserToAnother(){
        
        User user = [SELECT Id, Vim_ID__c FROM User WHERE LastName = 'User A' LIMIT 1];
        
        RestServiceToUpsertVssoUserConfig.VssoUserConfig vssoUserConfig = new RestServiceToUpsertVssoUserConfig.VssoUserConfig();
        vssoUserConfig.UserId = user.Id;
        vssoUserConfig.VimId = 'vimId123';
        vssoUserConfig.ExecutiveIds = '';
        
        String myJSON = JSON.serialize(vssoUserConfig);
        
        RestRequest request = new RestRequest();
        request.requestUri ='https://test.salesforce.com/services/apexrest/SIV/UpsertVssoUserConfig';
        request.httpMethod = 'POST';
        request.requestBody = Blob.valueof(myJSON);
        RestContext.request = request;
        
        RestResponse response = new RestResponse();
        RestContext.response = response;  
        
        // Need Test.startTest and Test.stopTest to run @future method syncronously
        Test.startTest();    
        RestServiceToUpsertVssoUserConfig.ResponseHandler res = RestServiceToUpsertVssoUserConfig.doPost();
        
        
        //Reassign Vim to User B
        user = [SELECT Id, Vim_ID__c FROM User WHERE LastName = 'User B' LIMIT 1];
        
        vssoUserConfig = new RestServiceToUpsertVssoUserConfig.VssoUserConfig();
        vssoUserConfig.UserId = user.Id;
        vssoUserConfig.VimId = 'vimId123';
        vssoUserConfig.ExecutiveIds = '';
        
        myJSON = JSON.serialize(vssoUserConfig);
        
        request = new RestRequest();
        request.requestUri ='https://test.salesforce.com/services/apexrest/SIV/UpsertVssoUserConfig';
        request.httpMethod = 'POST';
        request.requestBody = Blob.valueof(myJSON);
        RestContext.request = request;
        
        response = new RestResponse();
        RestContext.response = response;  
        
        res = RestServiceToUpsertVssoUserConfig.doPost();
        
        Test.stopTest();
        
        user = [SELECT Id, Vim_ID__c FROM User WHERE LastName = 'User A' LIMIT 1];
        system.assertEquals(null, user.Vim_ID__c);  
        
        user = [SELECT Id, Vim_ID__c FROM User WHERE LastName = 'User B' LIMIT 1];
        system.assertEquals(vssoUserConfig.VimId, user.Vim_ID__c);  
    }
    
}