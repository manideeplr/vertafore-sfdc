@isTest
public class RestServiceToUpsertBrnDeptJnRecordsTest {

  	@isTest
	static void test1(){        
        Sagitta_Branch__c brn = new Sagitta_Branch__c();
        brn.ItemCode__c = '1';
        brn.Name = 'Test Branch';
        insert brn;
        
        Sagitta_Department__c dept = new Sagitta_Department__c();
        dept.ItemCode__c = '1';
        dept.Name = 'Test Dept';
        insert dept;
        
        List<RestServiceToUpsertBrnDeptJnRecords.SagittaDept> deptToInsert = new List<RestServiceToUpsertBrnDeptJnRecords.SagittaDept>();
		List<RestServiceToUpsertBrnDeptJnRecords.SagittaBrn> brnsToInsert = new List<RestServiceToUpsertBrnDeptJnRecords.SagittaBrn>();
        RestServiceToUpsertBrnDeptJnRecords.SagittaDept deptJn = new RestServiceToUpsertBrnDeptJnRecords.SagittaDept();
        deptJn.ItemCodeHash = '1';
        deptToInsert.add(deptJn);
        RestServiceToUpsertBrnDeptJnRecords.SagittaBrn brnJn = new RestServiceToUpsertBrnDeptJnRecords.SagittaBrn();
        brnJn.ItemCodeHash = '1';
        brnJn.Departments = deptToInsert;
        brnsToInsert.add(brnJn);
        String myJSON = JSON.serialize(brnsToInsert);

        RestRequest request = new RestRequest();
        request.requestUri ='https://test.salesforce.com/services/apexrest/UpsertBrnDeptJnRecords';
        request.httpMethod = 'POST';
		request.requestBody = Blob.valueof(myJSON);
		
        RestContext.request = request;
		RestServiceToUpsertBrnDeptJnRecords.ResponseHandler res = RestServiceToUpsertBrnDeptJnRecords.doPost();
		
        List<BrDepJunction__c> brns = [SELECT Id FROM BrDepJunction__c];
        system.assertEquals(1, brns.size());		
	}
    
    @isTest
	static void test2(){
        Sagitta_Branch__c brn = new Sagitta_Branch__c();
        brn.ItemCode__c = '1';
        brn.Name = 'Test Branch';
        insert brn;
        
        Sagitta_Department__c dept = new Sagitta_Department__c();
        dept.ItemCode__c = '1';
        dept.Name = 'Test Department';
        insert dept;
        
        Sagitta_Department__c dept1 = new Sagitta_Department__c();
        dept1.ItemCode__c = '2';
        dept1.Name = 'Test Department';
        insert dept1;
        
        BrDepJunction__c brnDeptJn = new BrDepJunction__c();
        brnDeptJn.Sagitta_Department__c = dept.Id;
        brnDeptJn.Sagitta_Branch__c = brn.Id;
        insert brnDeptJn;
        
        List<RestServiceToUpsertBrnDeptJnRecords.SagittaBrn> brnsToInsert = new List<RestServiceToUpsertBrnDeptJnRecords.SagittaBrn>();
		List<RestServiceToUpsertBrnDeptJnRecords.SagittaDept> deptsToInsert = new List<RestServiceToUpsertBrnDeptJnRecords.SagittaDept>();
        RestServiceToUpsertBrnDeptJnRecords.SagittaDept deptJn = new RestServiceToUpsertBrnDeptJnRecords.SagittaDept();
        deptJn.ItemCodeHash = '1';
        deptsToInsert.add(deptJn);
        RestServiceToUpsertBrnDeptJnRecords.SagittaDept deptJn2 = new RestServiceToUpsertBrnDeptJnRecords.SagittaDept();
        deptJn2.ItemCodeHash = '2';
        deptsToInsert.add(deptJn2);
		RestServiceToUpsertBrnDeptJnRecords.SagittaBrn brnJn = new RestServiceToUpsertBrnDeptJnRecords.SagittaBrn();
        brnJn.ItemCodeHash = '1';
        brnJn.Departments = deptsToInsert;
        brnsToInsert.add(brnJn);
        String myJSON = JSON.serialize(brnsToInsert);
		

        RestRequest request = new RestRequest();
        request.requestUri ='https://test.salesforce.com/services/apexrest/UpsertBrnDeptJnRecords';
        request.httpMethod = 'POST';
		request.requestBody = Blob.valueof(myJSON);
		
        RestContext.request = request;
		RestServiceToUpsertBrnDeptJnRecords.ResponseHandler res = RestServiceToUpsertBrnDeptJnRecords.doPost();
		
        List<BrDepJunction__c> brns = [SELECT Id FROM BrDepJunction__c];
        system.assertEquals(2, brns.size());		
	}
    
    @isTest
    static void test3(){
        
		List<RestServiceToUpsertBrnDeptJnRecords.SagittaDept> deptsToInsert = new List<RestServiceToUpsertBrnDeptJnRecords.SagittaDept>();
        RestServiceToUpsertBrnDeptJnRecords.SagittaDept deptJn = new RestServiceToUpsertBrnDeptJnRecords.SagittaDept();
        deptJn.ItemCodeHash = '1';
        deptsToInsert.add(deptJn);
		RestServiceToUpsertBrnDeptJnRecords.SagittaBrn brnJn = new RestServiceToUpsertBrnDeptJnRecords.SagittaBrn();
        brnJn.ItemCodeHash = '1';
        brnJn.Departments = deptsToInsert;
        String myJSON = JSON.serialize(brnJn);
		

        RestRequest request = new RestRequest();
        request.requestUri ='https://test.salesforce.com/services/apexrest/UpsertDivBrnJnRecords';
        request.httpMethod = 'POST';
		request.requestBody = Blob.valueof(myJSON);
        
        RestResponse response = new RestResponse();
        response.statusCode = 400;
		
        RestContext.request = request;
        RestContext.response = response;
		RestServiceToUpsertBrnDeptJnRecords.ResponseHandler res = RestServiceToUpsertBrnDeptJnRecords.doPost();
		
        List<BrDepJunction__c> brns = [SELECT Id FROM BrDepJunction__c];
        system.assertEquals(0, brns.size());		
	}
}