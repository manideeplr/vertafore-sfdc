@isTest
public class TaskTriggerHandlerTest {
    @testSetup
    public static void setup() {
        createIsInitialSyncFlag('false');
        
        Profile p = [SELECT Id FROM Profile WHERE Name='Standard User']; 
        User user = new User(Alias = 'standt', 
                             Email='standarduser@testorg.com', 
            				 EmailEncodingKey='UTF-8',
                             LastName='Testing',
                             LanguageLocaleKey='en_US', 
            				 LocaleSidKey='en_US',
                             ProfileId = p.Id, 
            				 TimeZoneSidKey='America/Los_Angeles',
                             UserName='vert-test@vertafore.com');
        insert user;
        
        Sagitta_Employee__c amsExecutive = new Sagitta_Employee__c(
            Name = 'Executive',
            Sagitta_Is_Executive__c = true,
            Sagitta_Employee_Code__c = 'ABCDEFG',
            Sagitta_Entity_ID__c = '5235633',
            Sagitta_Employee_Status__c	 = 'Active'
        );
        Sagitta_Employee__c amsRepresentative = new Sagitta_Employee__c(
            Name = 'Representative',
            Sagitta_Is_Representative__c = true,
            Sagitta_Employee_Code__c = '123456',
            Sagitta_Entity_ID__c = '5239633',
        	Sagitta_Employee_Status__c = 'Active'
        );
        insert amsExecutive;
        insert amsRepresentative;
        
        Sagitta_Activity_Action_Type__c activityActionType = new Sagitta_Activity_Action_Type__c(
        	Sagitta_Activity_Action_Id__c = '26544'
        );
        Sagitta_Issuing_Carrier__c issuingCarrier = new Sagitta_Issuing_Carrier__c(
            Sagitta_Entity_ID__c= 'ID text', 
            Sagitta_Carrier_Type__c= 'N',
            Sagitta_IsHidden__c = false,
            Sagitta_Issuing_Carrier_Code__c= 'text'
        );
        insert activityActionType;
        insert issuingCarrier;
        
        Sagitta_Agency_Settings__c agencySettings1 = new Sagitta_Agency_Settings__c(
            Sagitta_Agency_Number__c='text', 
            Default_Activity_Action_Type__c= [SELECT Id FROM Sagitta_Activity_Action_Type__c LIMIT 1].Id,
            Default_Bill_Method__c= 'A', 
            Default_Issuing_Carrier__c= [SELECT Id FROM Sagitta_Issuing_Carrier__c LIMIT 1].Id, 
            Default_Policy_Number__c='text', 
            Integration_User_Id__c='text', 
            Vertafore_Id__c='text',
            Default_Agency_Admin__c = [SELECT Id FROM Sagitta_Employee__c WHERE Name =: amsExecutive.Name].Id
        );
        insert agencySettings1;
        
        Sagitta_Division__c SagittaDivision = new Sagitta_Division__c(
        	Name = 'Division One',
            ItemCode__c = '$$$'
        );
        insert SagittaDivision;
        
        Sagitta_Branch__c SagittaBranch = new Sagitta_Branch__c(
        	Name = 'Branch One',
            ItemCode__c = '***'
        );
        insert SagittaBranch;
        
        Sagitta_Department__c SagittaDepartment = new Sagitta_Department__c(
        	Name = 'Department One',
            ItemCode__c = '+++'
        );
        insert SagittaDepartment;
        
        Sagitta_Group__c SagittaGroup = new Sagitta_Group__c(
        	Name = 'Group One',
            ItemCode__c = '&&&'
        );
        insert SagittaGroup;

        DivBrJunction__c divBrJunc = new DivBrJunction__c(Sagitta_Division__c = SagittaDivision.Id, Sagitta_Branch__c = SagittaBranch.Id);
        BrDepJunction__c brDepJunc = new BrDepJunction__c(Sagitta_Branch__c = SagittaBranch.Id, Sagitta_Department__c = SagittaDepartment.Id);
        DepGrpJunction__c depGrpJunc = new DepGrpJunction__c(Sagitta_Department__c = SagittaDepartment.Id, Sagitta_Group__c = SagittaGroup.Id);
        insert divBrJunc;
        insert brDepJunc;
        insert depGrpJunc;
        
        Sagitta_Mapping__c mapping = new Sagitta_Mapping__c(
        	Name = 'MyMapping',
            Sagitta_Assigned_To__c = UserInfo.getUserId(),
            Sagitta_Division__c = SagittaDivision.Id,
            Sagitta_Branch__c = SagittaBranch.Id,
            Sagitta_Department__c = SagittaDepartment.Id,
            Sagitta_Group__c = SagittaGroup.Id,
            Sagitta_Executive__c = amsExecutive.Id,
            Sagitta_Representative__c = amsRepresentative.Id
        );
        insert mapping;
        
        Account syncedAccount = new Account(
            Name = 'Synced',
			Sagitta_Customer_ID__c = 'SagittaCUSTOMERID',
            Sagitta_Employee_Account_Exec__c = amsExecutive.Id,
            Sagitta_Employee_Account_Rep__c = amsRepresentative.Id
        );
        insert syncedAccount;
        
        Account notSyncedAccount = new Account(
            Name = 'Not Synced',
            Sagitta_Employee_Account_Exec__c = amsExecutive.Id,
            Sagitta_Employee_Account_Rep__c = amsRepresentative.Id
        );
        insert notSyncedAccount;
        
        Contact syncedContact = new Contact(FirstName = 'Contact', LastName = 'Synced', AccountId = syncedAccount.Id, Sagitta_Customer_Contact_ID__c = '1234');
        insert syncedContact;
        
        Contact notSyncedContact = new Contact(FirstName = 'Contact', LastName = 'Not Synced', AccountId = notSyncedAccount.Id);
        insert notSyncedContact;
        
        Task syncedTask = new Task(Subject = 'Synced', Status = 'Completed', Priority = 'Normal', WhatId = syncedAccount.Id, Sagitta_Action__c = activityActionType.Id);
        insert syncedTask;
        
        Task syncedTask2 = new Task(Subject = 'Synced2', Status = 'Completed', Priority = 'Normal', WhoId = syncedContact.Id, Sagitta_Action__c = activityActionType.Id);
        insert syncedTask2;
        
        Task notSyncedTask = new Task(Subject = 'Not Synced', Status = 'Not Started', Priority = 'Normal', WhoId = notSyncedContact.Id, Sagitta_Action__c = activityActionType.Id);
        insert notSyncedTask;
    }
    
    @isTest
    public static void HandleTrigger_ShouldReturnNO_MESSAGES_SENT_WhenPassedListOfTasksAndBEFORE_INSERT() {
        TaskTriggerHandler taskTriggerHandler = new TaskTriggerHandler();
        Task task = [SELECT Id, Subject, Status, Priority, WhoId, WhatId, Description FROM Task WHERE Subject = 'Not Synced'];
        List<Task> listOfTasks = new List<Task>();
        listOfTasks.add(task);
        TriggerHandlerStatus expected = TriggerHandlerStatus.NO_MESSAGES_SENT;
        
        TriggerHandlerStatus actual = taskTriggerHandler.handleTrigger(listOfTasks, TriggerOperation.BEFORE_INSERT);
        
        System.assertEquals(expected, actual);
    }
    
    @isTest
    public static void HandleTrigger_ShouldReturnNO_MESSAGES_SENT_WhenPassedListOfTasksAndAFTER_INSERT_WhereTaskIsNotCompleteAndAccountIsNotSynced() {
        TaskTriggerHandler taskTriggerHandler = new TaskTriggerHandler();
        Contact notSyncedContact = [SELECT Id, FirstName, LastName, AccountId FROM Contact WHERE LastName = 'Not Synced'];
        Task notSyncedTask = [SELECT Subject, Status, Priority, WhoId, WhatId FROM Task WHERE WhoId = :notSyncedContact.Id];
        List<Task> listOfTasks = new List<Task>();
        createIntegrationFlag('true');
        listOfTasks.add(notSyncedTask);
        TriggerHandlerStatus expected = TriggerHandlerStatus.NO_MESSAGES_SENT;
        
        TriggerHandlerStatus actual = taskTriggerHandler.handleTrigger(listOfTasks, TriggerOperation.AFTER_INSERT);
        
        System.assertEquals(expected, actual);
    }
    
    @isTest
    public static void HandleTrigger_ShouldReturnMESSAGES_SENT_WhenPassedListOfTasksAndAFTER_INSERT_WhereTaskIsCompleteAndAccountIsSynced() {
        TaskTriggerHandler taskTriggerHandler = new TaskTriggerHandler();
        Account syncedAccount = [SELECT Id FROM Account WHERE Name = 'Synced'];
        Task syncedTask = [SELECT Subject, Status, Priority, WhoId, WhatId FROM Task WHERE WhatId = :syncedAccount.Id AND WhoId = null];
        List<Task> listOfTasks = new List<Task>();
        createIntegrationFlag('true');
        listOfTasks.add(syncedTask);
        TriggerHandlerStatus expected = TriggerHandlerStatus.MESSAGES_SENT;
        
        TriggerHandlerStatus actual = taskTriggerHandler.handleTrigger(listOfTasks, TriggerOperation.AFTER_INSERT);
        
        System.assertEquals(expected, actual);
    }
    
    @isTest
    public static void HandleTrigger_ShouldReturnMESSAGES_SENT_WhenPassedListOfTasksAndAFTER_INSERT_WhereTaskIsCompleteAndContactIsSynced() {
        TaskTriggerHandler taskTriggerHandler = new TaskTriggerHandler();
        Contact syncedContact = [SELECT Id, FirstName, LastName, AccountId FROM Contact WHERE LastName = 'Synced'];
        Task syncedTask = [SELECT Subject, Status, Priority, WhoId, WhatId FROM Task WHERE WhoId = :syncedContact.Id];
        List<Task> listOfTasks = new List<Task>();
        createIntegrationFlag('true');
        listOfTasks.add(syncedTask);
        TriggerHandlerStatus expected = TriggerHandlerStatus.MESSAGES_SENT;
        
        TriggerHandlerStatus actual = taskTriggerHandler.handleTrigger(listOfTasks, TriggerOperation.AFTER_INSERT);
        
        System.assertEquals(expected, actual);
    }
    
    private static void createIntegrationFlag(String flag) {
        Metadata__c intFlag = new Metadata__c();
        intFlag.Name = 'IntegrationFlag';
        intFlag.Value__c = flag;
        insert intFlag;
    }
    
    private static void createIsInitialSyncFlag(String flag) {
        Metadata__c isInitialSyncFlag = new Metadata__c();
        isInitialSyncFlag.Name = 'IsInitialSync';
        isInitialSyncFlag.Value__c = flag;
        insert isInitialSyncFlag;
    }
}