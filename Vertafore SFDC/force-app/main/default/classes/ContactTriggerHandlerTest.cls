@isTest
public class ContactTriggerHandlerTest {
	@testSetup
    public static void setup() {
        createIntegrationFlag('true');
        createIsInitialSyncFlag('false');
        
        Profile p = [SELECT Id FROM Profile WHERE Name='Standard User']; 
        User user = new User(Alias = 'standt', 
                             Email='standarduser@testorg.com', 
            				 EmailEncodingKey='UTF-8',
                             LastName='Testing',
                             LanguageLocaleKey='en_US', 
            				 LocaleSidKey='en_US',
                             ProfileId = p.Id, 
            				 TimeZoneSidKey='America/Los_Angeles',
                             UserName='vert-test@vertafore.com');
        insert user;

        Sagitta_Employee__c emp = new Sagitta_Employee__c(
            Name = 'Employee',
            Sagitta_Entity_ID__c = '55555',
            Sagitta_Is_Executive__c = true,
            Sagitta_Is_Representative__c = true,
        	Sagitta_Employee_Code__c = 'zzz',
        	Sagitta_Employee_Status__c = 'Active'
        );
        insert emp;
        
        Sagitta_Activity_Action_Type__c activityActionType = new Sagitta_Activity_Action_Type__c(
        	Sagitta_Activity_Action_Id__c = '26544'
        );
        Sagitta_Issuing_Carrier__c issuingCarrier = new Sagitta_Issuing_Carrier__c(
            Sagitta_Entity_ID__c= 'ID text', 
            Sagitta_Carrier_Type__c= 'N',
            Sagitta_IsHidden__c = false,
            Sagitta_Issuing_Carrier_Code__c= 'text'
        );
        insert activityActionType;
        insert issuingCarrier;
        
        Sagitta_Agency_Settings__c agencySettings1 = new Sagitta_Agency_Settings__c(
            Sagitta_Agency_Number__c='text', 
            Default_Activity_Action_Type__c= [SELECT Id FROM Sagitta_Activity_Action_Type__c LIMIT 1].Id,
            Default_Bill_Method__c= 'A', 
            Default_Issuing_Carrier__c= [SELECT Id FROM Sagitta_Issuing_Carrier__c LIMIT 1].Id, 
            Default_Policy_Number__c='text', 
            Integration_User_Id__c='text', 
            Vertafore_Id__c='text',
        	Default_Agency_Admin__c = [SELECT Id FROM Sagitta_Employee__c WHERE Name =: emp.Name].Id
        );
        insert agencySettings1;
        
		Sagitta_Division__c div1 = new Sagitta_Division__c(Name = 'Division 1');
        Sagitta_Department__c dept1 = new Sagitta_Department__c(Name = 'Department 1');
        Sagitta_Branch__c branch1 = new Sagitta_Branch__c(Name = 'Branch 1');
        Sagitta_Group__c grp1 = new Sagitta_Group__c(Name = 'Group 1');
        insert div1;
        insert dept1;
        insert branch1;
        insert grp1;
        
        DivBrJunction__c divBrJunc = new DivBrJunction__c(Sagitta_Division__c = div1.Id, Sagitta_Branch__c = branch1.Id);
        BrDepJunction__c brDepJunc = new BrDepJunction__c(Sagitta_Branch__c = branch1.Id, Sagitta_Department__c = dept1.Id);
        DepGrpJunction__c depGrpJunc = new DepGrpJunction__c(Sagitta_Department__c = dept1.Id, Sagitta_Group__c = grp1.Id);
        insert divBrJunc;
        insert brDepJunc;
        insert depGrpJunc;
        
        Sagitta_Mapping__c mapping = new Sagitta_Mapping__c(
        	Name = 'MyMapping',
            Sagitta_Assigned_To__c = UserInfo.getUserId(),
            Sagitta_Division__c = div1.Id,
            Sagitta_Branch__c = branch1.Id,
            Sagitta_Department__c = dept1.Id,
            Sagitta_Group__c = grp1.Id,
            Sagitta_Executive__c = emp.Id,
            Sagitta_Representative__c = emp.Id
        );
        insert mapping;
        
        // Setup accounts
        Account acctSynced = new Account(Name = 'AccountSynced', Sagitta_Customer_ID__c='abcdefg');
        Account acctNotSynced = new Account(Name = 'AccountNotSynced');
        insert acctSynced;
        insert acctNotSynced;
        
        // Setup contacts
        Contact contactSynced = new Contact(LastName = 'ContactSynced', AccountId = acctSynced.Id);
        Contact contactNotSynced = new Contact(LastName = 'ContactNotSynced', AccountId = acctNotSynced.Id);
        Contact contactNoAccount = new Contact(LastName = 'ContactNoAccount');
        insert contactSynced;
        insert contactNotSynced;
        insert contactNoAccount;
        
        Contact oldContactSynced = new Contact(LastName = 'OldContactSynced', AccountId = acctSynced.Id);
        Contact oldContactNotSynced = new Contact(LastName = 'OldContactNotSynced', AccountId = acctNotSynced.Id);
        Contact newContactSynced = new Contact(LastName = 'NewContactSynced', AccountId = acctSynced.Id);
        Contact newContactNotSynced = new Contact(LastName = 'NewContactNotSynced', AccountId = acctNotSynced.Id);
        insert oldContactSynced;
        insert oldContactNotSynced;
        insert newContactSynced;
        insert newContactNotSynced;
    }
    
    // Insert tests
    @isTest
    public static void HandleTrigger_ShouldReturn_MESSAGES_SENT_WhenInsertingContact(){
        Contact contactSynced = [SELECT Name, AccountId FROM Contact WHERE LastName = 'ContactSynced'];
        List<Contact> newRecords = new List<Contact>();
        newRecords.add(contactSynced);
        createIntegrationFlag('true');
        
        ContactTriggerHandler contactTriggerHandler = new ContactTriggerHandler();
        TriggerHandlerStatus actual = contactTriggerHandler.handleTrigger(newRecords, null, TriggerOperation.AFTER_INSERT);        
        TriggerHandlerStatus expected = TriggerHandlerStatus.MESSAGES_SENT;
        
        System.assertEquals(expected, actual);
    }
    
    @isTest
    public static void HandleTrigger_ShouldReturn_NO_MESSAGES_SENT_WhenInsertingContact(){
        Contact contactNotSynced = [SELECT Name, AccountId FROM Contact WHERE LastName = 'ContactNotSynced'];
        List<Contact> newRecords = new List<Contact>();
        newRecords.add(contactNotSynced);
        
        ContactTriggerHandler contactTriggerHandler = new ContactTriggerHandler();
        TriggerHandlerStatus actual = contactTriggerHandler.handleTrigger(newRecords, null, TriggerOperation.AFTER_INSERT);        
        TriggerHandlerStatus expected = TriggerHandlerStatus.NO_MESSAGES_SENT;
        
        System.assertEquals(expected, actual);
    }
    
    @isTest
    public static void HandleTrigger_ShouldReturn_NO_MESSAGES_SENT_WhenInsertingContactWithNoAccount(){
        Contact contactNoAccount = [SELECT Name, AccountId FROM Contact WHERE LastName = 'ContactNoAccount'];
        List<Contact> newRecords = new List<Contact>();
        newRecords.add(contactNoAccount);
		
		ContactTriggerHandler contactTriggerHandler = new ContactTriggerHandler();        
        TriggerHandlerStatus actual = contactTriggerHandler.handleTrigger(newRecords, null, TriggerOperation.AFTER_INSERT);        
        TriggerHandlerStatus expected = TriggerHandlerStatus.NO_MESSAGES_SENT;
        
        System.assertEquals(expected, actual);
    }
    
    // Update tests
    @isTest
    public static void HandleTrigger_ShouldReturn_MESSAGES_SENT_WhenUpdatingContact(){
        createIntegrationFlag('true');
        // create old contact record
        Contact oldContactSynced = [SELECT Id, Name, AccountId FROM Contact WHERE LastName = 'OldContactSynced'];
        Map<Id, Contact> oldRecords = new Map<Id, Contact>();
        
        // create new contact record
		Contact contactSynced = [SELECT Name, AccountId FROM Contact WHERE LastName = 'ContactSynced'];
        List<Contact> newRecords = new List<Contact>();
        
        // create new record list and old record map
        oldContactSynced.Id = contactSynced.Id;
        newRecords.add(contactSynced);
        oldRecords.put(contactSynced.Id, oldContactSynced);
        
        ContactTriggerHandler contactTriggerHandler = new ContactTriggerHandler();
        TriggerHandlerStatus actual = contactTriggerHandler.handleTrigger(newRecords, oldRecords, TriggerOperation.AFTER_UPDATE);        
        TriggerHandlerStatus expected = TriggerHandlerStatus.MESSAGES_SENT;
        
        System.assertEquals(expected, actual);
    }
    
    @isTest
    public static void HandleTrigger_ShouldReturn_NO_MESSAGES_SENT_WhenUpdatingContact(){
        // create old contact record
        Contact oldContactNotSynced = [SELECT Id, Name, AccountId FROM Contact WHERE LastName = 'OldContactNotSynced'];
        Map<Id, Contact> oldRecords = new Map<Id, Contact>();
        
        // create new contact record
        Contact contactNotSynced = [SELECT Id, Name, AccountId FROM Contact WHERE LastName = 'ContactNotSynced'];
        List<Contact> newRecords = new List<Contact>();
        
        // create new record list and old record map
        oldContactNotSynced.Id = contactNotSynced.Id;
        newRecords.add(contactNotSynced);
        oldRecords.put(contactNotSynced.Id, oldContactNotSynced);
        
        ContactTriggerHandler contactTriggerHandler = new ContactTriggerHandler();
        TriggerHandlerStatus actual = contactTriggerHandler.handleTrigger(newRecords, oldRecords, TriggerOperation.AFTER_UPDATE);        
        TriggerHandlerStatus expected = TriggerHandlerStatus.NO_MESSAGES_SENT;
        
        System.assertEquals(expected, actual);
    }
    
    @isTest
    public static void HandleTriggerAfterUpdate_ShouldReturn_MESSAGES_SENT_WhenReassigningContactFromSyncedToNotSyncedAccount() {
	    // create old contact record
	    createIntegrationFlag('true');
        Contact oldContactSynced = [SELECT Name, AccountId FROM Contact WHERE LastName = 'ContactSynced'];
        List<Contact> newRecords = new List<Contact>();
        
        // create new contact record
        Contact newContactNotSynced = [SELECT Id, Name, AccountId FROM Contact WHERE LastName = 'NewContactNotSynced'];
        Map<Id, Contact> oldRecords = new Map<Id, Contact>();
        
        // create new record list and old record map
        oldContactSynced.Id = newContactNotSynced.Id;
        oldRecords.put(newContactNotSynced.Id, oldContactSynced);
        newRecords.add(newContactNotSynced);
        
        ContactTriggerHandler contactTriggerHandler = new ContactTriggerHandler();
        TriggerHandlerStatus actual = contactTriggerHandler.handleTrigger(newRecords, oldRecords, TriggerOperation.AFTER_UPDATE); 
        TriggerHandlerStatus expected = TriggerHandlerStatus.MESSAGES_SENT;
        
        System.assertEquals(expected, actual);
    }
    
    @isTest
    public static void HandleTriggerAfterUpdate_ShouldReturn_MESSAGES_SENT_WhenReassigningContactFromNotSyncedToSyncedAccount() {
        // create old contact record
        Contact oldContactNotSynced = [SELECT Id, Name, AccountId FROM Contact WHERE LastName = 'NewContactNotSynced'];
        Map<Id, Contact> oldRecords = new Map<Id, Contact>();
        createIntegrationFlag('true');
	    // create new contact record
        Contact newContactSynced = [SELECT Name, AccountId FROM Contact WHERE LastName = 'ContactSynced'];
        List<Contact> newRecords = new List<Contact>();
        
        // create new record list and old record map
        oldContactNotSynced.Id = newContactSynced.Id;
        oldRecords.put(newContactSynced.Id, oldContactNotSynced);
        newRecords.add(newContactSynced);
        
        ContactTriggerHandler contactTriggerHandler = new ContactTriggerHandler();
        TriggerHandlerStatus actual = contactTriggerHandler.handleTrigger(newRecords, oldRecords, TriggerOperation.AFTER_UPDATE); 
        TriggerHandlerStatus expected = TriggerHandlerStatus.MESSAGES_SENT;
        
        System.assertEquals(expected, actual);
    }
    
    @isTest
    public static void HandleTriggerAfterUpdate_ShouldReturnNO_MESSAGES_SENT_WhenReassigningContactFromNotSyncedToSyncedAccountAndFlagFalse() {
        // create old contact record
        Contact oldContactNotSynced = [SELECT Id, Name, AccountId FROM Contact WHERE LastName = 'NewContactNotSynced'];
        Map<Id, Contact> oldRecords = new Map<Id, Contact>();
        createIntegrationFlag('false');
	    // create new contact record
        Contact newContactSynced = [SELECT Name, AccountId FROM Contact WHERE LastName = 'ContactSynced'];
        List<Contact> newRecords = new List<Contact>();
        
        // create new record list and old record map
        oldContactNotSynced.Id = newContactSynced.Id;
        oldRecords.put(newContactSynced.Id, oldContactNotSynced);
        newRecords.add(newContactSynced);
        
        ContactTriggerHandler contactTriggerHandler = new ContactTriggerHandler();
        TriggerHandlerStatus actual = contactTriggerHandler.handleTrigger(newRecords, oldRecords, TriggerOperation.AFTER_UPDATE); 
        TriggerHandlerStatus expected = TriggerHandlerStatus.NO_MESSAGES_SENT;
        
        System.assertEquals(expected, actual);
    }
    
    @isTest
    public static void HandleTriggerAfterUpdate_ShouldReturnNO_MESSAGES_SENT_WhenReassigningContactFromNotSyncedToSyncedAccountAndNoIntegrationFlagData() {
        createIsInitialSyncFlag('false');
        createIntegrationFlag('');
        // create old contact record
        Contact oldContactNotSynced = [SELECT Id, Name, AccountId FROM Contact WHERE LastName = 'NewContactNotSynced'];
        Map<Id, Contact> oldRecords = new Map<Id, Contact>();
	    // create new contact record
        Contact newContactSynced = [SELECT Name, AccountId FROM Contact WHERE LastName = 'ContactSynced'];
        List<Contact> newRecords = new List<Contact>();
        
        // create new record list and old record map
        oldContactNotSynced.Id = newContactSynced.Id;
        oldRecords.put(newContactSynced.Id, oldContactNotSynced);
        newRecords.add(newContactSynced);
        
        ContactTriggerHandler contactTriggerHandler = new ContactTriggerHandler();
        TriggerHandlerStatus actual = contactTriggerHandler.handleTrigger(newRecords, oldRecords, TriggerOperation.AFTER_UPDATE); 
        TriggerHandlerStatus expected = TriggerHandlerStatus.NO_MESSAGES_SENT;
        
        System.assertEquals(expected, actual);
    }
    
    private static void createIntegrationFlag(String flag) {
        Metadata__c intFlag = new Metadata__c();
        intFlag.Name = 'IntegrationFlag';
        intFlag.Value__c = flag;
        insert intFlag;
    }
    
    private static void createIsInitialSyncFlag(String flag) {
        Metadata__c isInitialSyncFlag = new Metadata__c();
        isInitialSyncFlag.Name = 'IsInitialSync';
        isInitialSyncFlag.Value__c = flag;
        insert isInitialSyncFlag;
    }
}