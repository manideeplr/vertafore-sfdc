/*
    Author : Jay Farnsworth
    Name : BusinessUnitSaver
    Date : Prior to 18 June 2019
    Description: 
    Modification History :
    *************************************************************
    Version             Author          Date                        Description
    1                   Brad            18 June 2019                Refactoring, logging
*/
public class BusinessUnitSaver {
    @AuraEnabled
    public static boolean saveBusinessUnits(string divName, string branchName, string deptName, string grpName, string sObjectId, string recordType) {
        Sagitta_Division__c div;
        Sagitta_Branch__c branch;
        Sagitta_Department__c dept;
        Sagitta_Group__c grp;

        // Get each object by name
        try {
            div = [SELECT Id FROM Sagitta_Division__c WHERE Name =: divName][0];
            branch = [SELECT Id FROM Sagitta_Branch__c WHERE Name =: branchName][0];
            dept = [SELECT Id FROM Sagitta_Department__c WHERE Name =: deptName][0];
            grp = [SELECT Id FROM Sagitta_Group__c WHERE Name =: grpName][0];
        } catch (Exception ex) {
            String message = 'Error finding Division, Branch, Department, or Group; ' +
                'Error: ' + ex.getMessage() + '; Stack Trace: ' + ex.getStackTraceString();
           // ApplicationLogger.logMessage('BusinessUnitSaver', 'saveBusinessUnits', message, ApplicationLogger.ERROR);
            // string in constructor is client side message, string in .setMessage is server side message
            AuraHandledException auraExc = new AuraHandledException('Could not find Division, Branch, Department, or Group.');
            auraExc.setMessage('Could not find Division, Branch, Department, or Group.');
            throw auraExc;
        }
        
        // Make sure selection is valid by finding matching junction objects
        try {
            DivBrJunction__c divBrJunc = [SELECT Id FROM DivBrJunction__c WHERE Sagitta_Division__c =: div.Id AND Sagitta_Branch__c =: branch.Id][0];
            BrDepJunction__c branchDepJunc = [SELECT Id FROM BrDepJunction__c WHERE Sagitta_Branch__c =: branch.Id AND Sagitta_Department__c =: dept.Id][0];
            DepGrpJunction__c depGrpJunc = [SELECT Id FROM DepGrpJunction__c WHERE Sagitta_Department__c =: dept.Id AND Sagitta_Group__c =: grp.Id][0];
        } catch (Exception ex) {
            String message = 'Invalid combination of Division, Branch, Department, or Group; ' +
                'Error: ' + ex.getMessage() + '; Stack Trace: ' + ex.getStackTraceString();
           // ApplicationLogger.logMessage('BusinessUnitSaver', 'saveBusinessUnits', message, ApplicationLogger.ERROR);
            // string in constructor is client side message, string in .setMessage is server side message
            AuraHandledException auraExc = new AuraHandledException('Invalid combination of Division, Branch, Department, or Group.');
            auraExc.setMessage('Invalid combination of Division, Branch, Department, or Group.');
            throw auraExc;
        }
        
        // Get user's Sagitta Mapping record
        List<Sagitta_Mapping__c> mappingList = [SELECT Id FROM Sagitta_Mapping__c WHERE Sagitta_Assigned_To__c =: UserInfo.getUserId()];
        if (recordType == 'lead') {
            // Get the record that we're going to copy the Business Units values to
            List<Lead> leadList = [SELECT Id, Sagitta_Employee_Account_Exec__c, Sagitta_Employee_Account_Rep__c FROM Lead WHERE Id =: sObjectId];
            if (leadList.size() == 0) {
                String message = 'Could not find Lead';
              //  ApplicationLogger.logMessage('BusinessUnitSaver', 'saveBusinessUnits', message, ApplicationLogger.ERROR);
                // string in constructor is client side message, string in .setMessage is server side message
                AuraHandledException auraExc = new AuraHandledException('Could not find Lead.');
                auraExc.setMessage('Could not find Lead.');
                throw auraExc;
            }
            Lead lead = leadList[0];
            
            // Copy Domain Settings values to the current object
            lead.Sagitta_Division__c = div.Id;
            lead.Sagitta_Branch__c = branch.Id;
            lead.Sagitta_Department__c = dept.Id;
            lead.Sagitta_Group__c = grp.Id;
            update lead;
        }
        else if (recordType == 'account') {
            // Get the record that we're going to copy the Business Units values to
            List<Account> accountList = [SELECT Id,  Sagitta_Employee_Account_Exec__c, Sagitta_Employee_Account_Rep__c FROM Account WHERE Id =: sObjectId];
            if (accountList.size() == 0) {
                String message = 'Could not find Account';
           //     ApplicationLogger.logMessage('BusinessUnitSaver', 'saveBusinessUnits', message, ApplicationLogger.ERROR);
                // string in constructor is client side message, string in .setMessage is server side message
                AuraHandledException auraExc = new AuraHandledException('Could not find Account.');
                auraExc.setMessage('Could not find Account.');
                throw auraExc;
            }
            Account account = accountList[0];
            
            // Copy Domain Settings values to the current object
            account.Sagitta_Division__c = div.Id;
            account.Sagitta_Branch__c = branch.Id;
            account.Sagitta_Department__c = dept.Id;
            account.Sagitta_Group__c = grp.Id;
            update account;
        }
        else if (recordType == 'opportunity') {
            // Get the record that we're going to copy the Business Units values to
            List<Opportunity> oppList = [SELECT Id, Sagitta_Opportunity_Exec__c, Sagitta_Opportunity_Rep__c FROM Opportunity WHERE Id =: sObjectId];
            if (oppList.size() == 0) {
                String message = 'Could not find Opportunity';
             //   ApplicationLogger.logMessage('BusinessUnitSaver', 'saveBusinessUnits', message, ApplicationLogger.ERROR);
                // string in constructor is client side message, string in .setMessage is server side message
                AuraHandledException auraExc = new AuraHandledException('Could not find Opportunity.');
                auraExc.setMessage('Could not find Opportunity.');
                throw auraExc;
            }
            Opportunity opp = oppList[0];
            
            // Copy Domain Settings values to the current object
            opp.Sagitta_Division__c = div.Id;
            opp.Sagitta_Branch__c = branch.Id;
            opp.Sagitta_Department__c = dept.Id;
            opp.Sagitta_Group__c = grp.Id;
            update opp;
        }
        else if (recordType == 'Sagitta_Mapping__c'){
            List<Sagitta_Mapping__c> mapList = [SELECT Id, Sagitta_Representative__c, Sagitta_Executive__c FROM Sagitta_Mapping__c WHERE Id =: sObjectId];
            if (mapList.size() == 0) {
                String message = 'Could not find mapping';
        //        ApplicationLogger.logMessage('BusinessUnitSaver', 'saveBusinessUnits', message, ApplicationLogger.ERROR);
                // string in constructor is client side message, string in .setMessage is server side message
                AuraHandledException auraExc = new AuraHandledException('Could not find Mapping.');
                auraExc.setMessage('Could not find Mapping.');
                throw auraExc;
            }
            Sagitta_Mapping__c mapping = mapList[0];
            
			mapping.Sagitta_Division__c = div.Id;
            mapping.Sagitta_Branch__c = branch.Id;
            mapping.Sagitta_Department__c = dept.Id;
            mapping.Sagitta_Group__c = grp.Id;
            update mapping;
        }
        else {
            String message = 'Invalid record type';
           // ApplicationLogger.logMessage('BusinessUnitSaver', 'saveBusinessUnits', message, ApplicationLogger.ERROR);
            // string in constructor is client side message, string in .setMessage is server side message
            AuraHandledException auraExc = new AuraHandledException('Invalid record type');
            auraExc.setMessage('Invalid record type');
            throw auraExc;
        }
        
        return true;
    }
    @AuraEnabled
    public static String[] getRecord(string recordId, string type){
        try {
            String[] businessUnitNames;
            switch on type {
            when 'lead' {
                 List<lead> records = [SELECT Id,  Sagitta_Division__c, Sagitta_Branch__c, Sagitta_Department__c, Sagitta_Group__c  FROM Lead WHERE Id =: recordId];
                 string divId = records[0].Sagitta_Division__c;
        		 string divName = [SELECT Name FROM Sagitta_Division__c WHERE id =: divId LIMIT 1].Name;
        	 	 string branchId = records[0].Sagitta_Branch__c;
        		 string branchName = [SELECT Name FROM Sagitta_Branch__c WHERE id =: branchId LIMIT 1].Name;
        		 string depId = records[0].Sagitta_Department__c;
        		 string depName = [SELECT Name FROM Sagitta_Department__c WHERE id =: depId LIMIT 1].Name;
        		 string groupId = records[0].Sagitta_Group__c;
        		 string groupName = [SELECT Name FROM Sagitta_Group__c WHERE id =: groupId LIMIT 1].Name;
                 businessUnitNames = new String[]{divName, branchName, depName, groupName};
            }
            when 'account' {
                 List<account> records = [SELECT Id,  Sagitta_Division__c, Sagitta_Branch__c, Sagitta_Department__c, Sagitta_Group__c  FROM Account WHERE Id =: recordId];
                 string divId = records[0].Sagitta_Division__c;
        		 string divName = [SELECT Name FROM Sagitta_Division__c WHERE id =: divId LIMIT 1].Name;
        	 	 string branchId = records[0].Sagitta_Branch__c;
        		 string branchName = [SELECT Name FROM Sagitta_Branch__c WHERE id =: branchId LIMIT 1].Name;
        		 string depId = records[0].Sagitta_Department__c;
        		 string depName = [SELECT Name FROM Sagitta_Department__c WHERE id =: depId LIMIT 1].Name;
        		 string groupId = records[0].Sagitta_Group__c;
        		 string groupName = [SELECT Name FROM Sagitta_Group__c WHERE id =: groupId LIMIT 1].Name;
                 businessUnitNames = new String[]{divName, branchName, depName, groupName};
            }
            when 'opportunity' {
                 List<opportunity> records = [SELECT Id,  Sagitta_Division__c, Sagitta_Branch__c, Sagitta_Department__c, Sagitta_Group__c  FROM Opportunity WHERE Id =: recordId];
                 string divId = records[0].Sagitta_Division__c;
        		 string divName = [SELECT Name FROM Sagitta_Division__c WHERE id =: divId LIMIT 1].Name;
        	 	 string branchId = records[0].Sagitta_Branch__c;
        		 string branchName = [SELECT Name FROM Sagitta_Branch__c WHERE id =: branchId LIMIT 1].Name;
        		 string depId = records[0].Sagitta_Department__c;
        		 string depName = [SELECT Name FROM Sagitta_Department__c WHERE id =: depId LIMIT 1].Name;
        		 string groupId = records[0].Sagitta_Group__c;
        		 string groupName = [SELECT Name FROM Sagitta_Group__c WHERE id =: groupId LIMIT 1].Name;
                 businessUnitNames = new String[]{divName, branchName, depName, groupName};
            }
            when 'Sagitta_Mapping__c' {
                 List<Sagitta_Mapping__c> records = [SELECT Id,  Sagitta_Division__c, Sagitta_Branch__c, Sagitta_Department__c, Sagitta_Group__c  FROM Sagitta_Mapping__c WHERE Id =: recordId];
                 string divId = records[0].Sagitta_Division__c;
        		 string divName = [SELECT Name FROM Sagitta_Division__c WHERE id =: divId LIMIT 1].Name;
        	 	 string branchId = records[0].Sagitta_Branch__c;
        		 string branchName = [SELECT Name FROM Sagitta_Branch__c WHERE id =: branchId LIMIT 1].Name;
        		 string depId = records[0].Sagitta_Department__c;
        		 string depName = [SELECT Name FROM Sagitta_Department__c WHERE id =: depId LIMIT 1].Name;
        		 string groupId = records[0].Sagitta_Group__c;
        		 string groupName = [SELECT Name FROM Sagitta_Group__c WHERE id =: groupId LIMIT 1].Name;
                 businessUnitNames = new String[]{divName, branchName, depName, groupName};
            }
            when else {
                // string in constructor is client side message, string in .setMessage is server side message
                AuraHandledException auraExc = new AuraHandledException('Invalid record type');
                auraExc.setMessage('Invalid record type');
                throw auraExc;
            }
        }
            
        return businessUnitNames;
        } catch (Exception ex) {
            String message = 'Invalid record type; ' +
                'Error: ' + ex.getMessage() + '; Stack Trace: ' + ex.getStackTraceString();
         //   ApplicationLogger.logMessage('BusinessUnitSaver', 'getRecord', message, ApplicationLogger.ERROR);
            throw new AuraHandledException('Invalid record type: ' + ex.getMessage());
        }
    }
    
    @AuraEnabled
    //@AuraEnabled
    public static List<String> initDivisionList() {
        try {
            List<String> result = new List<String>();
            List<Sagitta_Division__c> divs = [SELECT Name, Id, Sagitta_isHidden__c FROM Sagitta_Division__c WHERE Sagitta_IsHidden__c = false];
            for(Sagitta_Division__c div : divs)
            {
            	result.add(div.Name);                
            }
            result.sort();
            return result;
        } catch(Exception ex) {
            String message = 'Error initiating Division List; ' +
                'Error: ' + ex.getMessage() + '; Stack Trace: ' + ex.getStackTraceString();
          //  ApplicationLogger.logMessage('BusinessUnitSaver', 'initDivisionList', message, ApplicationLogger.ERROR);
            throw new AuraHandledException('Error initiating Division List: ' + ex.getMessage());
        }
    }
     
    // Generates list from Junction Object, based on passed parameters
    @AuraEnabled
    public static List<String> getBUList(String selectInput, String topLevel, String lowLevel, String junction)
    {
        try {
            String query = 'SELECT Name, Sagitta_isHidden__c FROM '+ lowLevel + ' WHERE Id IN (SELECT ' + lowLevel + ' FROM ' + junction + ' WHERE ' + topLevel + '.Name LIKE \'' + String.escapeSingleQuotes(selectInput) + '\')';
            sObject[] objList = Database.query(query);
            List<String> objNames = new List<String>();
            for (sObject obj : objList ) {
                if (obj.get('Sagitta_isHidden__c') == false) {
                    objNames.add((String)obj.get('Name'));
                }
            }
            objNames.sort();
            return objNames;
        } catch (Exception ex) {
            String message = 'Error initiating Business Unit List; ' +
                'Error: ' + ex.getMessage() + '; Stack Trace: ' + ex.getStackTraceString();
        //    ApplicationLogger.logMessage('BusinessUnitSaver', 'getBUList', message, ApplicationLogger.ERROR);
            throw new AuraHandledException('Error initiating Business Unit List: ' + ex.getMessage());
        }
    }
}