/*
    Author : Vikram
    Name : AccountMergeBatch
    Date : Prior to 18 June 2019
    Description: Used to merge Accounts in a manageable batch size
    Modification History :
    *************************************************************
    Version             Author          Date                        Description
    1                   Brad            18 June 2019                Refactoring, logging
	1.1                 Vikram          10 July 2019				Code changes to make acct with CustId as masterAcct
	1.2                 Vikram          19 July 2019				Code changes to fix continues looping of batch class
	1.3					Brad			22 July 2019				Merge fields from Child to Master if necessary
	1.4                 Vikram          24 July 2019                Removed code changes version 1.2
	1.5 				Vikram			31 July 2019 				code changes to fix limit on findDuplicates method and SyncFlag check
*/
global class AccountMergeBatch implements Database.Batchable<SObject> {
    
    global Set<Id> mergedIds = new Set<id>();
    global Set<String> mergedRecordIds = new Set<String>();
    
    global Database.QueryLocator start(Database.BatchableContext context) {
        return Database.getQueryLocator('SELECT Id FROM Account ORDER BY Name');
    }
    
    // Summary:
    // Checks for duplicate Account records, collates all data between them and merges the two Accounts in Salesforce
    //
    // @param context - 
    // @param scope - 
    global void execute(Database.BatchableContext context, List<Account> scope) {
        
        try {
            // Set IntegrationFlag to false during merge to stop multiple Tasks and Object updates back to Sagitta
            Metadata__c intflag = Metadata__c.getInstance('IntegrationFlag');
            intflag.Value__c = 'false';
            update intflag;
            Metadata__c syncFlag = Metadata__c.getInstance('IsInitialSync');
            syncFlag.Value__c = 'true';
            update syncFlag;
            
            DescribeSObjectResult describeResult = Account.getSObjectType().getDescribe();
            List<String> fieldNames = new List<String>(describeResult.fields.getMap().keySet());
            String query ='SELECT ' + String.join( fieldNames, ',' ) + ' FROM ' + describeResult.getName();
            Map<String, Account> accMap = new Map<String, Account>();
            for(Sobject sobjAcc : Database.query(query)) {
                Account acc = (Account)sobjAcc;
                accMap.put(acc.Id, acc);
            }
            //v1.5 start
            List<Account> acctListMain = scope;
            List<List<Account>> wrapList = new List<List<Account>>();
            List<Account> tempList;
            for (integer i=0; i < (acctListMain.size()/50)+1; i++){
                tempList = new List<Account>();
                for (integer j=(i*50); (j<(i*50)+50) && j<acctListMain.size() ; j++) {
                    tempList.add(acctListMain[j]);
                }
                wrapList.add(tempList);
            }
            Map<String, List<Account>> accKeyMap = new Map<String, List<Account>>();
            for (integer i=0;i<wrapList.size();i++) {
                if(wrapList[i].size() > 0) {
                    Datacloud.FindDuplicatesResult[] results = Datacloud.FindDuplicates.findDuplicates(wrapList[i]);
                    Set<Id> acctIds = new Set<Id>();
                    for (Datacloud.FindDuplicatesResult findDupeResult : results) {
                        for (Datacloud.DuplicateResult dupeResult : findDupeResult.getDuplicateResults()) {                
                            for (Datacloud.MatchResult matchResult : dupeResult.getMatchResults()) {
                                for (Datacloud.MatchRecord matchRecord : matchResult.getMatchRecords()) {
                                    List<String> keys = new List<String>();
                                    List<Datacloud.FieldDiff> fieldDiffs = matchRecord.getFieldDiffs();
                                    for (Datacloud.FieldDiff diff : fieldDiffs) {
                                        if(diff.getDifference() == 'Same') {
                                            keys.add(diff.getName());
                                        }                                
                                    }
                                    Account acc = (Account)matchRecord.getRecord();
                                    Account accToCheck = accMap.get(acc.Id);
                                    if (!acctIds.contains(acc.Id)) {
                                        acctIds.add(acc.Id);
                                        String key = '';
                                        for (String keyname : keys){
                                            key = key + accToCheck.get(keyname);
                                        }
                                        if (accKeyMap.containsKey(key)) {
                                            accKeyMap.get(key).add(accToCheck);                            
                                        } else {
                                            List<Account> accList = new List<Account>();
                                            accList.add(accToCheck);
                                            accKeyMap.put(key, accList);
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
            
            for(List<Account> accList : accKeyMap.Values()) {
                List<Account> childAccts = new List<Account>();
                Account masterAcct;
                for(Account accToMerge : accList) {
                    if(String.isNotBlank(accToMerge.Sagitta_Customer_ID__c)){
                        masterAcct = accToMerge;
                    }
                    else {
                        childAccts.add(accToMerge);
                    }
                }
                if(masterAcct != null) {
                    for (Account childAcct : childAccts) {
                        if (!mergedRecordIds.contains(childAcct.Id) && masterAcct.Id != childAcct.Id) {
                            mapAccountFields(masterAcct, childAcct);
                            Database.MergeResult res = Database.Merge(new Account(Id=masterAcct.Id), new Account(Id=childAcct.Id));
                            if (res.isSuccess()) {
                                mergedIds.add(res.getId());
                                List<String> deletedRecordIds = res.getMergedRecordIds();
                                mergedRecordIds.add(deletedRecordIds[0]);
                            }
                        }
                    }
                } else {
                    masterAcct = childAccts[0];
                    for (integer i=1; i<childAccts.size(); i++) {
                        Account childAcct = childAccts[i];
                        if (!mergedRecordIds.contains(childAcct.Id) && masterAcct.Id != childAcct.Id) {
                            mapAccountFields(masterAcct, childAcct);
                            Database.MergeResult res = Database.Merge(new Account(Id=masterAcct.Id), new Account(Id=childAccts[i].Id));
                            if (res.isSuccess()) {
                                mergedIds.add(res.getId());
                                List<String> deletedRecordIds = res.getMergedRecordIds();
                                mergedRecordIds.add(deletedRecordIds[0]);
                            } 
                        }                       
                    }
                }
            }
        } catch (Exception ex) {
            String message = 'Error merging accounts. ;' + 
                'Error: ' + ex.getMessage() + '; Stack Trace:' + ex.getStackTraceString();
            ApplicationLogger.logMessage('AccountMergeBatch', 'execute', message, ApplicationLogger.ERROR);
        }
        finally{
            // Reset IntegrationFlag back to true
            Metadata__c intflag = Metadata__c.getInstance('IntegrationFlag');
            intflag.Value__c = 'true';
            update intflag;
            Metadata__c syncFlag = Metadata__c.getInstance('IsInitialSync');
            syncFlag.Value__c = 'false';
            update syncFlag;
        }
    }
    
    global void finish(Database.BatchableContext context) {
        //Call the batch to udpate merged accounts
        // TODO Future feature may require this call to be overridden
    	Database.executeBatch(new MergedAccountsSyncBatch(mergedIds));
    }
    
    private void mapAccountFields(Account master, Account child) {
        if (master.AccountNumber == null && child.AccountNumber != null) {
            master.AccountNumber = child.AccountNumber;
        }
        if (master.Site == null && child.Site != null) {
            master.Site = child.Site;
        }
        if (master.AccountSource == null && child.Site != null) {
            master.AccountSource = child.AccountSource;
        }
        if (master.Sagitta_Account_Status__c == null && child.Sagitta_Account_Status__c != null) {
            master.Sagitta_Account_Status__c = child.Sagitta_Account_Status__c;
        }
        if (master.Sagitta_Business_Entity__c == null && child.Sagitta_Business_Entity__c != null) {
            master.Sagitta_Business_Entity__c = child.Sagitta_Business_Entity__c;
        }
        if (master.AnnualRevenue == null && child.AnnualRevenue != null) {
            master.AnnualRevenue = child.AnnualRevenue;
        }
        if (master.Sagitta_TOB_IsBenefits__c == false && child.Sagitta_TOB_IsBenefits__c == true) {
            master.Sagitta_TOB_IsBenefits__c = true;
        }
        if (master.BillingStreet == null && child.BillingStreet != null) {
            master.BillingStreet = child.BillingStreet;
        }
        if (master.BillingCity == null && child.BillingCity != null) {
            master.BillingCity = child.BillingCity;
        }
        if (master.BillingState == null && child.BillingState != null) {
            master.BillingState = child.BillingState;
        }
        if (master.BillingPostalCode == null && child.BillingPostalCode != null) {
            master.BillingPostalCode = child.BillingPostalCode;
        }
        if (master.Sagitta_Client_Type__c == null && child.Sagitta_Client_Type__c != null) {
            master.Sagitta_Client_Type__c = child.Sagitta_Client_Type__c;
        }
        if (master.Sagitta_TOB_IsCommercial__c == false && child.Sagitta_TOB_IsCommercial__c == true) {
            master.Sagitta_TOB_IsCommercial__c = true;
        }
        if (master.Sagitta_Company_Email_Address__c == null && child.Sagitta_Company_Email_Address__c != null) {
            master.Sagitta_Company_Email_Address__c = child.Sagitta_Company_Email_Address__c;
        }
        if (master.Sagitta_Customer_Priority__c == null && child.Sagitta_Customer_Priority__c != null) {
            master.Sagitta_Customer_Priority__c = child.Sagitta_Customer_Priority__c;
        }
        if (master.Jigsaw == null && child.Jigsaw != null) {
            master.Jigsaw = child.Jigsaw;
        }
        if (master.Description == null && child.Description != null) {
            master.Description = child.Description;
        }
        if (master.Sagitta_DBA__c == null && child.Sagitta_DBA__c != null) {
            master.Sagitta_DBA__c = child.Sagitta_DBA__c;
        }
        if (master.NumberOfEmployees == null && child.NumberOfEmployees != null) {
            master.NumberOfEmployees = child.NumberOfEmployees;
        }
        if (master.Fax == null && child.Fax != null) {
            master.Fax = child.Fax;
        }
        if (master.Sagitta_TOB_IsFinancial__c == false && child.Sagitta_TOB_IsFinancial__c == true) {
            master.Sagitta_TOB_IsFinancial__c = true;
        }
        if (master.Sagitta_First_Name__c == null && child.Sagitta_First_Name__c != null) {
            master.Sagitta_First_Name__c = child.Sagitta_First_Name__c;
        }
        if (master.Sagitta_TOB_IsHealth__c == false && child.Sagitta_TOB_IsHealth__c == true) {
            master.Sagitta_TOB_IsHealth__c = true;
        }
        if (master.Industry == null && child.Industry != null) {
            master.Industry = child.Industry;
        }
        if (master.Sagitta_Last_Name__c == null && child.Sagitta_Last_Name__c != null) {
            master.Sagitta_Last_Name__c = child.Sagitta_Last_Name__c;
        }
        if (master.Sagitta_TOB_IsLife__c == false && child.Sagitta_TOB_IsLife__c == true) {
            master.Sagitta_TOB_IsLife__c = true;
        }
        if (master.Sagitta_Middle_Name__c == null && child.Sagitta_Middle_Name__c != null) {
            master.Sagitta_Middle_Name__c = child.Sagitta_Middle_Name__c;
        }
        if (master.Sagitta_TOB_IsNon_P_C__c == false && child.Sagitta_TOB_IsNon_P_C__c == true) {
            master.Sagitta_TOB_IsNon_P_C__c = true;
        }
        if (master.Ownership == null && child.Ownership != null) {
            master.Ownership = child.Ownership;
        }
        if (master.ParentId == null && child.ParentId != null) {
            master.ParentId = child.ParentId;
        }
        if (master.Sagitta_TOB_IsPersonal__c == false && child.Sagitta_TOB_IsPersonal__c == true) {
            master.Sagitta_TOB_IsPersonal__c = true;
        }
        if (master.Phone == null && child.Phone != null) {
            master.Phone = child.Phone;
        }
        if (master.Rating == null && child.Rating != null) {
            master.Rating = child.Rating;
        }
        if (master.ShippingStreet == null && child.ShippingStreet != null) {
            master.ShippingStreet = child.ShippingStreet;
        }
        if (master.ShippingCity == null && child.ShippingCity != null) {
            master.ShippingCity = child.ShippingCity;
        }
        if (master.ShippingState == null && child.ShippingState != null) {
            master.ShippingState = child.ShippingState;
        }
        if (master.ShippingPostalCode == null && child.ShippingPostalCode != null) {
            master.ShippingPostalCode = child.ShippingPostalCode;
        }
        if (master.Sic == null && child.Sic != null) {
            master.Sic = child.Sic;
        }
        if (master.SicDesc == null && child.SicDesc != null) {
            master.SicDesc = child.SicDesc;
        }
        if (master.TickerSymbol == null && child.TickerSymbol != null) {
            master.TickerSymbol = child.TickerSymbol;
        }
        if (master.Type == null && child.Type != null) {
            master.Type = child.Type;
        }
        if (master.Sagitta_Type_of_Business__c == null && child.Sagitta_Type_of_Business__c != null) {
            master.Sagitta_Type_of_Business__c = child.Sagitta_Type_of_Business__c;
        }
        if (master.Website == null && child.Website != null) {
            master.Website = child.Website;
        }
        
        /*  Following fields fail to install, but may need to be implemented later
        if (master.CleanStatus == null && child.CleanStatus != null) {
            master.CleanStatus = child.CleanStatus;
        }
        if (master.DandbCompanyId == null && child.DandbCompanyId != null) {
            master.DandbCompanyId = child.DandbCompanyId;
        }
        if (master.DunsNumber == null && child.DunsNumber != null) {
            master.DunsNumber = child.DunsNumber;
        }
        if (master.NaicsCode == null && child.NaicsCode != null) {
            master.NaicsCode = child.NaicsCode;
        }
        if (master.NaicsDesc == null && child.NaicsDesc != null) {
            master.NaicsDesc = child.NaicsDesc;
        }
        if (master.OperatingHoursId == null && child.OperatingHoursId != null) {
            master.OperatingHoursId = child.OperatingHoursId;
        }
        if (master.Tradestyle == null && child.Tradestyle != null) {
            master.Tradestyle = child.Tradestyle;
        }
        if (master.YearStarted == null && child.YearStarted != null) {
            master.YearStarted = child.YearStarted;
        }
        */
        
        update master;
    }
}