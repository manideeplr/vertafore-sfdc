/*
    Author : Jay Farnsworth
    Name : BusinessUnitListMaker
    Date : Prior to 18 June 2019
    Description: Creates a list of Business Units(div, branch, dept, group) and their relationships
                 used in the Business Units Aura Component, enabling cascade of BU
    Modification History :
    *************************************************************
    Version             Author          Date                        Description
    1                   Brad            18 June 2019                Refactoring, logging
*/
public class BusinessUnitListMaker {
    // Generates list of Division names queried from database
    // Summary
    //
    // Details:
    // Generates the Division List for the top level of the cascading BU settings
    // @return List<String> of all active Divisions
    @AuraEnabled
    public static List<String> initDivisionList() {
        try{
            List<String> result = new List<String>();
            List<Sagitta_Division__c> divs = [SELECT Name, Sagitta_IsHidden__c, Id FROM Sagitta_Division__c WHERE Sagitta_isHidden__c = false];
            for(Sagitta_Division__c div : divs)
            {
                result.add(div.Name);
            }
            result.sort();
            return result;
        } catch (Exception ex) {
            String message = 'Error generating Division List; ' +
                'Error: ' + ex.getMessage() + '; Stack Trace: ' + ex.getStackTraceString();
            ApplicationLogger.logMessage('BusinessUnitListMaker', 'initDivisionList', message, ApplicationLogger.ERROR);
            throw new AuraHandledException('Error generating Division List: ' + ex.getMessage());}
        
    }

    // Summary:
    // Generates list from a Junction Object, based on passed parameters
    //
    // Details:
    // Generates a List for the next level of the cascading BU settings
    // @param selectInput - value of the lookup selection
    // @param topLevel - object type of the value previously selected
    // @param lowLevel - object type of the next layer in the cascade
    // @param juntion - the junction object used to find relationship between topLevel selection
    //                  and the lowLevel choices
    // @return List<String> of valid Business Units
    @AuraEnabled
    public static List<String> getBUList(String selectInput, String topLevel, String lowLevel, String junction)
    {
        try {
            String query = 'SELECT Name, Sagitta_IsHidden__c FROM '+ lowLevel + ' WHERE Sagitta_isHidden__c = false AND Id IN (SELECT ' + lowLevel + ' FROM ' + junction + ' WHERE ' + topLevel + '.Name LIKE \'' + String.escapeSingleQuotes(selectInput) + '\')';
            sObject[] objList = Database.query(query);
            List<String> objNames = new List<String>();
            for (sObject obj : objList ) {
                objNames.add((String)obj.get('Name'));
            }
            objNames.sort();
            return objNames;
        } catch (Exception ex) {
            String message = 'Error generating Business Unit List; ' +
                'Error: ' + ex.getMessage() + '; Stack Trace: ' + ex.getStackTraceString();
            ApplicationLogger.logMessage('BusinessUnitListMaker', 'getBUList', message, ApplicationLogger.ERROR);
            throw new AuraHandledException('Error generating Business Unit List: ' + ex.getMessage());
        }
        
    }
}