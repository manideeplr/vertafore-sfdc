@isTest
public class RestServicePolicyContextTest {
	@testSetup
    private static void testSetup() {
        createIntegrationFlag('false');
        createIsInitialSyncFlag('true');
        
        Sagitta_Employee__c relevantEmployee = new Sagitta_Employee__c();
        relevantEmployee.Name = 'Relevant Employee';
		relevantEmployee.Sagitta_Is_Executive__c = true;
        relevantEmployee.Sagitta_Is_Representative__c = true;
		relevantEmployee.Sagitta_Employee_Code__c = '064033034';
		relevantEmployee.Sagitta_Entity_ID__c = '@!"';
		relevantEmployee.Sagitta_Employee_Status__c = 'Active';
        
        Sagitta_Division__c relevantDivision = new Sagitta_Division__c();
        relevantDivision.Name = 'Relevant Division';
        relevantDivision.Sagitta_IsHidden__c = false;
        relevantDivision.ItemCode__c = '041041042';
        
        Sagitta_Branch__c relevantBranch = new Sagitta_Branch__c();
        relevantBranch.Name = 'Relevant Branch';
        relevantBranch.Sagitta_IsHidden__c = false;
        relevantBranch.ItemCode__c = '119033104';
        
        Sagitta_Department__c relevantDept = new Sagitta_Department__c();
        relevantDept.Name = 'Relevant Dept';
        relevantDept.Sagitta_IsHidden__c = false;
        relevantDept.ItemCode__c = '051120034';
        
        Sagitta_Group__c relevantGroup = new Sagitta_Group__c();
        relevantGroup.Name = 'Relevant Group';
        relevantGroup.Sagitta_IsHidden__c = false;
        relevantGroup.ItemCode__c = '111043036';
        
        Sagitta_Issuing_Carrier__c relevantIssuingCarrier = new Sagitta_Issuing_Carrier__c();
        relevantIssuingCarrier.Name = 'Relevant Issuing Carrier';
        relevantIssuingCarrier.Sagitta_Entity_ID__c = '!!!';
        relevantIssuingCarrier.Sagitta_Carrier_Type__c = 'S';
        relevantIssuingCarrier.Sagitta_Issuing_Carrier_Code__c = '033033033';
        relevantIssuingCarrier.Sagitta_IsHidden__c = false;
        
        Sagitta_Line_of_Business__c relevantLobOne = new Sagitta_Line_of_Business__c();
        relevantLobOne.Name = 'Health';
        relevantLobOne.Sagitta_Entity_ID__c = 'd8amm291-10sa-49f2-a2f4-c0de99';
        relevantLobOne.Sagitta_LOB_Code__c = 'HLTH';
        relevantLobOne.Sagitta_LOB_Permanent_Flag__c = true;
        relevantLobOne.Sagitta_Type_of_Business__c = '1';
        relevantLobOne.Sagitta_IsHidden__c = false;
        
        Sagitta_Line_of_Business__c relevantLobTwo = new Sagitta_Line_of_Business__c();
        relevantLobTwo.Name = 'Life';
        relevantLobTwo.Sagitta_Entity_ID__c = 'c9830642-feb2-442f-a2f4-666666';
        relevantLobTwo.Sagitta_LOB_Code__c = 'LIFE';
        relevantLobTwo.Sagitta_LOB_Permanent_Flag__c = true;
        relevantLobTwo.Sagitta_Type_of_Business__c = '1';
        relevantLobTwo.Sagitta_IsHidden__c = false;
        
        insert relevantEmployee;
        insert relevantDivision;
        insert relevantBranch;
        insert relevantDept;
        insert relevantGroup;
        insert relevantIssuingCarrier;
        insert relevantLobOne;
        insert relevantLobTwo;
        
        Sagitta_Employee__c irrelevantEmployee = new Sagitta_Employee__c();
        irrelevantEmployee.Name = 'Irrelevant Employee';
		irrelevantEmployee.Sagitta_Is_Executive__c = true;
        irrelevantEmployee.Sagitta_Is_Representative__c = true;
		irrelevantEmployee.Sagitta_Employee_Code__c = '040040037';
		irrelevantEmployee.Sagitta_Entity_ID__c = '((%';
		irrelevantEmployee.Sagitta_Employee_Status__c = 'Active';
        
        Sagitta_Division__c irrelevantDivision = new Sagitta_Division__c();
        irrelevantDivision.Name = 'Irrelevant Division';
        irrelevantDivision.Sagitta_IsHidden__c = false;
        irrelevantDivision.ItemCode__c = '047042033';
        
        Sagitta_Branch__c irrelevantBranch = new Sagitta_Branch__c();
        irrelevantBranch.Name = 'Irrelevant Branch';
        irrelevantBranch.Sagitta_IsHidden__c = false;
        irrelevantBranch.ItemCode__c = '034421047';
        
        Sagitta_Department__c irrelevantDept = new Sagitta_Department__c();
        irrelevantDept.Name = 'Irrelevant Dept';
        irrelevantDept.Sagitta_IsHidden__c = false;
        irrelevantDept.ItemCode__c = '042033098';
        
		Sagitta_Group__c irrelevantGroup = new Sagitta_Group__c();
        irrelevantGroup.Name = 'Irrelevant Group';
        irrelevantGroup.Sagitta_IsHidden__c = false;
        irrelevantGroup.ItemCode__c = '045110034';
        
        Sagitta_Issuing_Carrier__c irrelevantIssuingCarrier = new Sagitta_Issuing_Carrier__c();
        irrelevantIssuingCarrier.Name = 'Irrelevant Issuing Carrier';
        irrelevantIssuingCarrier.Sagitta_Entity_ID__c = '"""';
        irrelevantIssuingCarrier.Sagitta_Carrier_Type__c = 'S';
        irrelevantIssuingCarrier.Sagitta_Issuing_Carrier_Code__c = '034034034';
        irrelevantIssuingCarrier.Sagitta_IsHidden__c = false;
        
        Sagitta_Line_of_Business__c irrelevantLobOne = new Sagitta_Line_of_Business__c();
        irrelevantLobOne.Name = 'Auto';
        irrelevantLobOne.Sagitta_Entity_ID__c = 'b617118e-fca5-473c-adf4-5bca9b891e3b';
        irrelevantLobOne.Sagitta_LOB_Code__c = 'AUTO';
        irrelevantLobOne.Sagitta_LOB_Permanent_Flag__c = true;
        irrelevantLobOne.Sagitta_Type_of_Business__c = '1';
        irrelevantLobOne.Sagitta_IsHidden__c = false;
        
        Sagitta_Line_of_Business__c irrelevantLobTwo = new Sagitta_Line_of_Business__c();
        irrelevantLobTwo.Name = 'Home';
        irrelevantLobTwo.Sagitta_Entity_ID__c = '244055f6-6c04-4803-81f6-e0d09256a009';
        irrelevantLobTwo.Sagitta_LOB_Code__c = 'HOME';
        irrelevantLobTwo.Sagitta_LOB_Permanent_Flag__c = true;
        irrelevantLobTwo.Sagitta_Type_of_Business__c = '1';
        irrelevantLobTwo.Sagitta_IsHidden__c = false;
        
        insert irrelevantEmployee;
        insert irrelevantDivision;
        insert irrelevantBranch;
        insert irrelevantDept;
        insert irrelevantGroup;
        insert irrelevantIssuingCarrier;
        insert irrelevantLobOne;
        insert irrelevantLobTwo;
        
        Account relevantAccount = new Account();
        relevantAccount.Name = 'Relevant Account';
        relevantAccount.Sagitta_Customer_ID__c = 'c5b25a06-f102-47b2-98cb-0f5c2a30c653';
        relevantAccount.Sagitta_Employee_Account_Rep__c = relevantEmployee.Id;
        relevantAccount.Sagitta_Employee_Account_Exec__c = relevantEmployee.Id;
        relevantAccount.Sagitta_Division__c = relevantDivision.Id;
        relevantAccount.Sagitta_Branch__c = relevantBranch.Id;
        relevantAccount.Sagitta_Department__c = relevantDept.Id;
        relevantAccount.Sagitta_Group__c = relevantGroup.Id;
        
        insert relevantAccount;
        
		Sagitta_Policy__c relevantPolicy = new Sagitta_Policy__c();
        relevantPolicy.Sagitta_Division__c = relevantDivision.Id;
        relevantPolicy.Sagitta_Branch__c = relevantBranch.Id;
        relevantPolicy.Sagitta_Department__c = relevantDept.Id;
        relevantPolicy.Sagitta_Group__c = relevantGroup.Id;
        relevantPolicy.Sagitta_Employee_Policy_Exec__c = relevantEmployee.Id;
        relevantPolicy.Sagitta_Employee_Policy_Rep__c = relevantEmployee.Id;
        relevantPolicy.Sagitta_Account_Name__c = relevantAccount.Id;
        relevantPolicy.Sagitta_Bill_Method__c = 'P';
        relevantPolicy.Sagitta_Customer_ID__c = 'c5b25a06-f102-47b2-98cb-0f5c2a30c653';
        relevantPolicy.Sagitta_Description__c = 'This place is awesome - said no one ever';
        relevantPolicy.Sagitta_Effective_Date__c = Date.newInstance(2019, 1, 1);
        relevantPolicy.Sagitta_Expiration_Date__c = Date.newInstance(2019, 1, 1).addYears(1);
        relevantPolicy.Sagitta_Policy_ID__c = 'e4bd851a-9aa6-479d-b930-a371beda5ae9';
        relevantPolicy.Name = 'RELEVANT POLICY';
        relevantPolicy.Sagitta_Policy_Status__c = 'A';
        relevantPolicy.Sagitta_Type_of_Business__c = '1';
        relevantPolicy.Sagitta_Annual_Premium__c = 1200.00;
        relevantPolicy.Sagitta_Writing_Company__c = relevantIssuingCarrier.Id;
        relevantPolicy.Sagitta_LOB__c = 'Health, Life';
        
        insert relevantPolicy;
        
        Account irrelevantAccount = new Account();
        irrelevantAccount.Name = 'Irrelevant Account';
        irrelevantAccount.Sagitta_Customer_ID__c = 'd32a8d88-s2oq-11az-4f3i-ddg5a73um8ci';
        irrelevantAccount.Sagitta_Employee_Account_Rep__c = irrelevantEmployee.Id;
        irrelevantAccount.Sagitta_Employee_Account_Exec__c = irrelevantEmployee.Id;
        irrelevantAccount.Sagitta_Division__c = irrelevantDivision.Id;
        irrelevantAccount.Sagitta_Branch__c = irrelevantBranch.Id;
        irrelevantAccount.Sagitta_Department__c = irrelevantDept.Id;
        irrelevantAccount.Sagitta_Group__c = irrelevantGroup.Id;
        
        insert irrelevantAccount;
        
		Sagitta_Policy__c irrelevantPolicy = new Sagitta_Policy__c();
        irrelevantPolicy.Sagitta_Division__c = irrelevantDivision.Id;
        irrelevantPolicy.Sagitta_Branch__c = irrelevantBranch.Id;
        irrelevantPolicy.Sagitta_Department__c = irrelevantDept.Id;
        irrelevantPolicy.Sagitta_Group__c = irrelevantGroup.Id;
        irrelevantPolicy.Sagitta_Employee_Policy_Exec__c = irrelevantEmployee.Id;
        irrelevantPolicy.Sagitta_Employee_Policy_Rep__c = irrelevantEmployee.Id;
        irrelevantPolicy.Sagitta_Account_Name__c = irrelevantAccount.Id;
        irrelevantPolicy.Sagitta_Bill_Method__c = 'P';
        irrelevantPolicy.Sagitta_Customer_ID__c = 'd32a8d88-s2oq-11az-4f3i-ddg5a73um8ci';
        irrelevantPolicy.Sagitta_Description__c = 'This place is awesome - said no one ever';
        irrelevantPolicy.Sagitta_Effective_Date__c = Date.newInstance(2019, 1, 1);
        irrelevantPolicy.Sagitta_Expiration_Date__c = Date.newInstance(2019, 1, 1).addYears(1);
        irrelevantPolicy.Sagitta_Policy_ID__c = 'kd771kd0-4hb3-6dda-3k9f-1da63dh8sa44';
        irrelevantPolicy.Name = 'IRRELEVANT POLICY';
        irrelevantPolicy.Sagitta_Policy_Status__c = 'A';
        irrelevantPolicy.Sagitta_Type_of_Business__c = '1';
        irrelevantPolicy.Sagitta_Annual_Premium__c = 2400.00;
        irrelevantPolicy.Sagitta_Writing_Company__c = irrelevantIssuingCarrier.Id;
        irrelevantPolicy.Sagitta_LOB__c = 'Auto, Home';
        
        insert irrelevantPolicy;
        
		Sagitta_Policy__c nullFieldsPolicy = new Sagitta_Policy__c();
        nullFieldsPolicy.Sagitta_Division__c = null;
        nullFieldsPolicy.Sagitta_Branch__c = null;
        nullFieldsPolicy.Sagitta_Department__c = null;
        nullFieldsPolicy.Sagitta_Group__c = null;
        nullFieldsPolicy.Sagitta_Employee_Policy_Exec__c = relevantEmployee.Id;
        nullFieldsPolicy.Sagitta_Employee_Policy_Rep__c = relevantEmployee.Id;
        nullFieldsPolicy.Sagitta_Account_Name__c = null;
        nullFieldsPolicy.Sagitta_Bill_Method__c = 'P';
        nullFieldsPolicy.Sagitta_Customer_ID__c = null;
        nullFieldsPolicy.Sagitta_Description__c = null;
        nullFieldsPolicy.Sagitta_Effective_Date__c = Date.newInstance(2035, 2, 28);
        nullFieldsPolicy.Sagitta_Expiration_Date__c = null;
        nullFieldsPolicy.Sagitta_Policy_ID__c = '6d515c62-22ed-4cfd-a59b-04533789ba41';
        nullFieldsPolicy.Name = 'POLICY NULL FIELDS';
        nullFieldsPolicy.Sagitta_Policy_Status__c = null;
        nullFieldsPolicy.Sagitta_Type_of_Business__c = '1';
        nullFieldsPolicy.Sagitta_Annual_Premium__c = null;
        nullFieldsPolicy.Sagitta_Writing_Company__c = null;
        nullFieldsPolicy.Sagitta_LOB__c = null;
        
        insert nullFieldsPolicy;
    }
    
    @isTest
    private static void getLobNamesAsString_ShouldReturnListOfRelevantLobNames_AsString() {
        List<RestServiceToUpsertPolicyRecords.SagittaPolicy> SagittaPolicies = createSagittaPolicies();
        RestServiceToUpsertPolicyRecords.SagittaPolicy policyOne = SagittaPolicies[0];
        RestServicePolicyContext policyContext = new RestServicePolicyContext(SagittaPolicies);
        
        String actual = policyContext.getLobNamesAsString(policyOne.LineOfBusinessIDs);
        
        System.assertEquals('Health, Life', actual);
    }
    
    @isTest
    private static void findEmployee_ShouldOnlyGetRelevantEmployees() {
        List<RestServiceToUpsertPolicyRecords.SagittaPolicy> SagittaPolicies = createSagittaPolicies();
        RestServiceToUpsertPolicyRecords.SagittaPolicy policyOne = SagittaPolicies[0];
        RestServicePolicyContext policyContext = new RestServicePolicyContext(SagittaPolicies);
        Sagitta_Employee__c irrelevantEmployee = [SELECT Id, Name, Sagitta_Employee_Code__c FROM Sagitta_Employee__c WHERE Name = 'Irrelevant Employee'];
        
        Sagitta_Employee__c actualOne = policyContext.findEmployee(policyOne.ExecCode);
        Sagitta_Employee__c actualTwo = policyContext.findEmployee(policyOne.CsrCode);
        Sagitta_Employee__c actualThree = policyContext.findEmployee(irrelevantEmployee.Sagitta_Employee_Code__c);
        
        System.assertEquals('Relevant Employee', actualOne.Name);
        System.assertEquals('Relevant Employee', actualTwo.Name);
        System.assertEquals(null, actualThree);
    }

	@isTest
    private static void findDivision_ShouldOnlyGetRelevantDivisions() {
        List<RestServiceToUpsertPolicyRecords.SagittaPolicy> SagittaPolicies = createSagittaPolicies();
        RestServiceToUpsertPolicyRecords.SagittaPolicy policyOne = SagittaPolicies[0];
        RestServicePolicyContext policyContext = new RestServicePolicyContext(SagittaPolicies);
        Sagitta_Division__c irrelevantDivision = [SELECT Id, Name, ItemCode__c FROM Sagitta_Division__c WHERE Name = 'Irrelevant Division'];
        
        Sagitta_Division__c actualOne = policyContext.findDivision(policyOne.GLDivisionCode);
        Sagitta_Division__c actualTwo = policyContext.findDivision(policyOne.GLDivisionCode);
        Sagitta_Division__c actualThree = policyContext.findDivision(irrelevantDivision.ItemCode__c);
        
        System.assertEquals('Relevant Division', actualOne.Name);
        System.assertEquals('Relevant Division', actualTwo.Name);
        System.assertEquals(null, actualThree);
    }

	@isTest
    private static void findBranch_ShouldOnlyGetRelevantBranches() {
        List<RestServiceToUpsertPolicyRecords.SagittaPolicy> SagittaPolicies = createSagittaPolicies();
        RestServiceToUpsertPolicyRecords.SagittaPolicy policyOne = SagittaPolicies[0];
        RestServicePolicyContext policyContext = new RestServicePolicyContext(SagittaPolicies);
        Sagitta_Branch__c irrelevantBranch = [SELECT Id, Name, ItemCode__c FROM Sagitta_Branch__c WHERE Name = 'Irrelevant Branch'];
        
        Sagitta_Branch__c actualOne = policyContext.findBranch(policyOne.GLBranchCode);
        Sagitta_Branch__c actualTwo = policyContext.findBranch(policyOne.GLBranchCode);
        Sagitta_Branch__c actualThree = policyContext.findBranch(irrelevantBranch.ItemCode__c);
        
        System.assertEquals('Relevant Branch', actualOne.Name);
        System.assertEquals('Relevant Branch', actualTwo.Name);
        System.assertEquals(null, actualThree);
    }
    
	@isTest
    private static void findDept_ShouldOnlyGetRelevantDepts() {
        List<RestServiceToUpsertPolicyRecords.SagittaPolicy> SagittaPolicies = createSagittaPolicies();
        RestServiceToUpsertPolicyRecords.SagittaPolicy policyOne = SagittaPolicies[0];
        RestServicePolicyContext policyContext = new RestServicePolicyContext(SagittaPolicies);
        Sagitta_Department__c irrelevantDept = [SELECT Id, Name, ItemCode__c FROM Sagitta_Department__c WHERE Name = 'Irrelevant Dept'];
        
        Sagitta_Department__c actualOne = policyContext.findDept(policyOne.GLDepartmentCode);
        Sagitta_Department__c actualTwo = policyContext.findDept(policyOne.GLDepartmentCode);
        Sagitta_Department__c actualThree = policyContext.findDept(irrelevantDept.ItemCode__c);
        
        System.assertEquals('Relevant Dept', actualOne.Name);
        System.assertEquals('Relevant Dept', actualTwo.Name);
        System.assertEquals(null, actualThree);
    }
    
	@isTest
    private static void findGroup_ShouldOnlyGetRelevantGroups() {
        List<RestServiceToUpsertPolicyRecords.SagittaPolicy> SagittaPolicies = createSagittaPolicies();
        RestServiceToUpsertPolicyRecords.SagittaPolicy policyOne = SagittaPolicies[0];
        RestServicePolicyContext policyContext = new RestServicePolicyContext(SagittaPolicies);
        Sagitta_Group__c irrelevantGroup = [SELECT Id, Name, ItemCode__c FROM Sagitta_Group__c WHERE Name = 'Irrelevant Group'];
        
        Sagitta_Group__c actualOne = policyContext.findGroup(policyOne.GLGroupCode);
        Sagitta_Group__c actualTwo = policyContext.findGroup(policyOne.GLGroupCode);
        Sagitta_Group__c actualThree = policyContext.findGroup(irrelevantGroup.ItemCode__c);
        
        System.assertEquals('Relevant Group', actualOne.Name);
        System.assertEquals('Relevant Group', actualTwo.Name);
        System.assertEquals(null, actualThree);
    }
    
	@isTest
    private static void findIssuingCarrier_ShouldOnlyGetRelevantIssuingCarriers() {
        List<RestServiceToUpsertPolicyRecords.SagittaPolicy> SagittaPolicies = createSagittaPolicies();
        RestServiceToUpsertPolicyRecords.SagittaPolicy policyOne = SagittaPolicies[0];
        RestServicePolicyContext policyContext = new RestServicePolicyContext(SagittaPolicies);
        Sagitta_Issuing_Carrier__c irrelevantIssuingCarrier = [SELECT Id, Name, Sagitta_Issuing_Carrier_Code__c FROM Sagitta_Issuing_Carrier__c WHERE Name = 'Irrelevant Issuing Carrier'];
        
        Sagitta_Issuing_Carrier__c actualOne = policyContext.findIssuingCarrier(policyOne.WritingCompanyCode);
        Sagitta_Issuing_Carrier__c actualTwo = policyContext.findIssuingCarrier(policyOne.WritingCompanyCode);
        Sagitta_Issuing_Carrier__c actualThree = policyContext.findIssuingCarrier(irrelevantIssuingCarrier.Sagitta_Issuing_Carrier_Code__c);
        
        System.assertEquals('Relevant Issuing Carrier', actualOne.Name);
        System.assertEquals('Relevant Issuing Carrier', actualTwo.Name);
        System.assertEquals(null, actualThree);
    }
    
	@isTest
    private static void findAccount_ShouldOnlyGetRelevantAccounts() {
        List<RestServiceToUpsertPolicyRecords.SagittaPolicy> SagittaPolicies = createSagittaPolicies();
        RestServiceToUpsertPolicyRecords.SagittaPolicy policyOne = SagittaPolicies[0];
        RestServicePolicyContext policyContext = new RestServicePolicyContext(SagittaPolicies);
        Account irrelevantAccount = [SELECT Id, Name, Sagitta_Customer_ID__c FROM Account WHERE Name = 'Irrelevant Account'];
        
        Account actualOne = policyContext.findAccount(policyOne.CustomerId);
        Account actualTwo = policyContext.findAccount(policyOne.CustomerId);
        Account actualThree = policyContext.findAccount(irrelevantAccount.Sagitta_Customer_ID__c);
        
        System.assertEquals('Relevant Account', actualOne.Name);
        System.assertEquals('Relevant Account', actualTwo.Name);
        System.assertEquals(null, actualThree);
    }
    
	@isTest
    private static void findPolicy_ShouldOnlyGetRelevantPolicies() {
        List<RestServiceToUpsertPolicyRecords.SagittaPolicy> SagittaPolicies = createSagittaPolicies();
        RestServiceToUpsertPolicyRecords.SagittaPolicy policyOne = SagittaPolicies[0];
        RestServicePolicyContext policyContext = new RestServicePolicyContext(SagittaPolicies);
        Sagitta_Policy__c irrelevantPolicy = [SELECT Id, Name, Sagitta_Policy_ID__c FROM Sagitta_Policy__c WHERE Name = 'IRRELEVANT POLICY'];
        
        Sagitta_Policy__c actualOne = policyContext.findPolicy(policyOne.PolicyId);
        Sagitta_Policy__c actualTwo = policyContext.findPolicy(policyOne.PolicyId);
        Sagitta_Policy__c actualThree = policyContext.findPolicy(irrelevantPolicy.Sagitta_Policy_ID__c);
        
        System.assertEquals('RELEVANT POLICY', actualOne.Name);
        System.assertEquals('RELEVANT POLICY', actualTwo.Name);
        System.assertEquals(new Sagitta_Policy__c(), actualThree);
    }
    
	@isTest
    private static void findPolicy_ShouldReturnPolicies_WithEmptyStrings_InsteadOfNull() {
        List<RestServiceToUpsertPolicyRecords.SagittaPolicy> SagittaPolicies = createSagittaPolicyWithNullValues();
        RestServiceToUpsertPolicyRecords.SagittaPolicy policyOne = SagittaPolicies[0];
        RestServicePolicyContext policyContext = new RestServicePolicyContext(SagittaPolicies);
        
        Sagitta_Policy__c relevant = policyContext.findPolicy(policyOne.PolicyId);
    
        System.assertEquals('', relevant.Sagitta_Customer_ID__c);
        System.assertEquals('', relevant.Sagitta_Description__c);
        System.assertEquals('', relevant.Sagitta_Division__c);
        System.assertEquals('', relevant.Sagitta_Branch__c);
        System.assertEquals('', relevant.Sagitta_Department__c);
        System.assertEquals('', relevant.Sagitta_Group__c);
        System.assertEquals('', relevant.Sagitta_LOB__c);
        System.assertEquals('', relevant.Sagitta_Policy_Status__c);
        System.assertEquals('', relevant.Sagitta_Writing_Company__c);
    }
    
    private static void createIntegrationFlag(String flag) {
        Metadata__c intFlag = new Metadata__c();
        intFlag.Name = 'IntegrationFlag';
        intFlag.Value__c = flag;
        insert intFlag;
    }
    
    private static void createIsInitialSyncFlag(String flag) {
        Metadata__c isInitialSyncFlag = new Metadata__c();
        isInitialSyncFlag.Name = 'IsInitialSync';
        isInitialSyncFlag.Value__c = flag;
        insert isInitialSyncFlag;
    }
    
    private static List<RestServiceToUpsertPolicyRecords.SagittaPolicy> createSagittaPolicies() {
        List<RestServiceToUpsertPolicyRecords.SagittaPolicy> policies = new List<RestServiceToUpsertPolicyRecords.SagittaPolicy>();
        
        RestServiceToUpsertPolicyRecords.SagittaPolicy policyOne = new RestServiceToUpsertPolicyRecords.SagittaPolicy();
        policyOne.BillMethod = 'P';
        policyOne.CsrCode = '064033034';
        policyOne.CustomerId = 'c5b25a06-f102-47b2-98cb-0f5c2a30c653';
        policyOne.Description = 'This place is awesome - said no one ever';
        policyOne.EffectiveDate = '2019-09-28T00:00:00Z';
        policyOne.ExecCode = '064033034';
        policyOne.ExpiryDate = '2035-02-28T00:00:00Z';
        policyOne.FullTermPremium = 1200.00;
        policyOne.GLDivisionCode = '041041042';
        policyOne.GLBranchCode = '119033104';
        policyOne.GLDepartmentCode = '051120034';
        policyOne.GLGroupCode = '111043036';
        policyOne.LineOfBusinessIDs = 'd8amm291-10sa-49f2-a2f4-c0de99, c9830642-feb2-442f-a2f4-666666';
        policyOne.PolicyId = 'e4bd851a-9aa6-479d-b930-a371beda5ae9';
        policyOne.PolicyNumber = 'RELEVANT POLICY';
		policyOne.RenewalReportFlag = 'A';
        policyOne.TypeOfBusiness = '1';
        policyOne.WritingCompanyCode = '033033033';
        
        policies.add(policyOne);
        
        return policies;
    }
    
    private static List<RestServiceToUpsertPolicyRecords.SagittaPolicy> createSagittaPolicyWithNullValues() {
        List<RestServiceToUpsertPolicyRecords.SagittaPolicy> policies = new List<RestServiceToUpsertPolicyRecords.SagittaPolicy>();
        
        RestServiceToUpsertPolicyRecords.SagittaPolicy policyOne = new RestServiceToUpsertPolicyRecords.SagittaPolicy();
        // some fields are required
        policyOne.BillMethod = 'P';
        policyOne.CsrCode = '040040037';
        policyOne.CustomerId = '';
        policyOne.Description = '';
        policyOne.EffectiveDate = '2035-02-28T00:00:00Z';
        policyOne.ExecCode = '040040037';
        policyOne.ExpiryDate = '';
        // not nullable in mappers
        policyOne.FullTermPremium = 0;
        policyOne.GLDivisionCode = '';
        policyOne.GLBranchCode = '';
        policyOne.GLDepartmentCode = '';
        policyOne.GLGroupCode = '';
        policyOne.LineOfBusinessIDs = '';
        policyOne.PolicyId = '6d515c62-22ed-4cfd-a59b-04533789ba41';
        policyOne.PolicyNumber = 'POLICY NULL FIELDS';
		policyOne.RenewalReportFlag = '';
        policyOne.TypeOfBusiness = '1';
        policyOne.WritingCompanyCode = '';
        
        policies.add(policyOne);
        
        return policies;
    }
}