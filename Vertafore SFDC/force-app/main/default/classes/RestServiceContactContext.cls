/*
    Author : Ben Baik
    Name : RestServiceContactContext
	Date : 19 July 2019
	Description : Gets relevant Salesforce objects for Sagitta Contacts (Contact and Account).
				  Finds object by ID or code in relevant data.
					
	Modification History :
	*************************************************************
	Version               Author             Date                      Description
	1                     Ben Baik           19 July 2019              Original
*/
public class RestServiceContactContext {
    private List<Account> accounts;
    private List<Contact> contacts;
    
    // Summary:
    // Gets all relevant data for Sagitta Customers
    // 
    // Details:
    // @param List<RestServiceToUpsertAcctRecords.SagittaContacts> contacts - Apex representation of Sagitta Contacts
    // @return RestServiceContactContext instance w/ relevant data
    public RestServiceContactContext(List<RestServiceToUpsertContactRecords.SagittaContact> SagittaContacts) {
        Set<String> customerIdSet = new Set<String>();
        Set<String> customerContactIdSet = new Set<String>();
        
        for (RestServiceToUpsertContactRecords.SagittaContact SagittaContact : SagittaContacts) {
            customerIdSet.add(SagittaContact.CustomerId);
            customerContactIdSet.add(SagittaContact.CCntId);
        }
        
		accounts = getRelevantAccounts(customerIdSet);
        contacts = getRelevantContacts(customerContactIdSet);
    } 
    
    // Summary:
    // Find Salesforce Account by Sagitta Customer ID
    // 
    // Details:
    // @param String customerId - Sagitta Customer ID
    // @return Account - Account with matching Sagitta Customer ID
    public Account findAccount(String customerId) {
        for (Account account : accounts) {
            if (account.Sagitta_Customer_Id__c == customerId) {
                return account;
            }
        }
        return null;
    }
    
    // Summary:
    // Find Salesforce Contact by Sagitta Customer ID
    // 
    // Details:
    // @param String customerContactId - Sagitta Customer Contact ID
    // @return Contact - Contact with matching Sagitta Customer Contact ID
    public Contact findContact(String customerContactId) {
        for (Contact contact : contacts) {
            if (contact.Sagitta_Customer_Contact_ID__c == customerContactId) {
                return contact;
            }
        }
        return new Contact();
    }
    
    // Summary:
    // Get all relevant Salesforce Account objects.
    // 
    // Details:
    // @param Set<String> customerIds - All relevant Sagitta Customer ID's
    // @return List<Account> - Relevant Accounts with matching Sagitta Customer ID's
    private List<Account> getRelevantAccounts(Set<String> customerIds) {
        return [SELECT Id,
                       Name,
                       Sagitta_Customer_ID__c 
                FROM Account
                WHERE Sagitta_Customer_ID__c
                IN :customerIds];
    }
    
    // Summary:
    // Get all relevant Salesforce Contact objects.
    // 
    // Details:
    // @param Set<String> customerContactIds - All relevant Sagitta Customer Contact ID's
    // @return List<Contact> - Relevant Contacts with matching Sagitta Customer Contact ID's
    private List<Contact> getRelevantContacts(Set<String> customerContactIds) {
        List<Contact> contactsWithNullFields = new List<Contact>();
        
        contactsWithNullFields = [SELECT Id,
                                         Sagitta_Customer_Contact_ID__c,
                                         Sagitta_Customer_ID__c,
                                         AccountId,
                                         FirstName,
                                         LastName,
                                         Email,
                                         Phone,
                                         MobilePhone,
                                         HomePhone,
                                         Fax,
                                         MailingStreet,
                                         MailingCity,
                                         MailingState,
                                         MailingPostalCode                            
                 				  FROM Contact
                 				  WHERE Sagitta_Customer_Contact_ID__c
                 				  IN :customerContactIds];
        
        return nullFieldsToEmptyString(contactsWithNullFields);
    }
    
    // Summary:
    // Convert all null string fields into empty string. SOQL returns null for empty string fields.
    // JSON deserializer returns '' for empty sring fields. This causes issues when fields are compared in RestServiceToUpsertContactRecords.mapFields.
    // 
    // Details:
    // @param List<Contact> contacts - List of relevant Contacts
    // @return List<Contact> - Relevant Contact list with empty string fields set to '' instead of null.
    private List<Contact> nullFieldsToEmptyString(List<Contact> contacts) {
		for (Contact contact : contacts) {
            if(contact.Sagitta_Customer_Contact_ID__c == null){
                contact.Sagitta_Customer_Contact_ID__c = '';
            }
            
            if(contact.Sagitta_Customer_ID__c == null){
                contact.Sagitta_Customer_ID__c = '';
            }
            
            if(contact.AccountId == null){
                contact.put('AccountId', '');
            }
            
            if(contact.FirstName == null){
                contact.FirstName = '';
            }
            
            if(contact.LastName == null){
                contact.LastName = '';
            }
            
            if(contact.Email == null){
                contact.Email = '';
            }
            
            if(contact.Phone == null){
                contact.Phone = '';
            }
                                    
            if(contact.MobilePhone == null){
                contact.MobilePhone = '';
            }
                                  
            if(contact.HomePhone == null){
                contact.HomePhone = '';
            }                       
            
            if(contact.Fax == null){
                contact.Fax = '';
            }
                        
            if(contact.MailingStreet == null){
                contact.MailingStreet = '';
            }
                        
            if(contact.MailingCity == null){
                contact.MailingCity = '';
            }
                        
            if(contact.MailingState == null){
                contact.MailingState = '';
            }
                        
            if(contact.MailingPostalCode == null){
                contact.MailingPostalCode = '';
            }
        }
        
        return contacts;
    }
}