/*
    Author : 
    Name : TaskTriggerHandler
    Date : Prior to 14 June 2019
    Description: Validates conditions to send message to sync completed Tasks
                 Adds SF message prefix to tasks synced to Sagitta
                 Sends message to integration if Task meets criteria
                 -- Task is related to a synced Account
    Modification History :
    *************************************************************
    Version             Author          Date                        Description
    1                                   Prior to 14 June 2019     
    2                   Brad            14 June 2019                Refactoring, logging
*/
public class TaskTriggerHandler {
    private string COMPLETED = 'Completed';
    private string DESCRIPTION_PREFIX = 'Created by Salesforce App - ';
    
    public TaskTriggerHandler() {}
    
    // Summary:
    // Handles Task Trigger behavior
    //
    // Details:
    // @param workingRecords - List of all Task records that are being inserted or updated.
    // @param triggerEvent - BEFORE_INSERT, BEFORE_UPDATE, AFTER_INSERT, or AFTER_UPDATE
    // @return TriggerHandlerStatus
    public TriggerHandlerStatus handleTrigger(List<Task> workingRecords, TriggerOperation triggerEvent) {
        Metadata__c intFlag = Metadata__c.getValues('IntegrationFlag');
        switch on triggerEvent {
            when BEFORE_INSERT, BEFORE_UPDATE {
                try {
                    
                    for (Task task : workingRecords) {
                        if (taskHasRelatedContact(task) && !taskHasRelatedAccount(task)) {
                            List<Contact> contacts = [SELECT Id, AccountId FROM Contact WHERE Id = :task.WhoId LIMIT 1];
                            if(!contacts.isEmpty())
                                task.WhatId = contacts[0].AccountId;
                            else
                                task.WhatId = null;
                            //getAccountFromContactAndAssignToTask(task);
                        }
                        insertPrefxInDescription(task);
                    }
                    return TriggerHandlerStatus.NO_MESSAGES_SENT;
                } catch (Exception ex) {
                    String message = 'BEFORE_INSERT/BEFORE_UPDATE; ' +
                        'Error: ' + ex.getMessage() + '; Stack Trace: ' + ex.getStackTraceString();
                    ApplicationLogger.logMessage('TaskTriggerHandler', 'handleTrigger', message, ApplicationLogger.ERROR);
                    throw ex;
                }
                    
            }
            
            when AFTER_INSERT, AFTER_UPDATE {
                try {
                    for (Task task: workingRecords) {
                        if (task.WhatId != null && task.WhoId == null && task.WhatId.getsObjectType() == Account.sObjectType) {
                            List<Account> accounts = [SELECT Id, Sagitta_Customer_Id__c FROM Account WHERE Id = :task.WhatId];
                            if (!accounts.isEmpty() && !String.isBlank(accounts[0].Sagitta_Customer_Id__c) && task.Status == COMPLETED && 
                                (intFlag != null && string.isNotBlank(intFlag.Value__c) && intFlag.Value__c == 'true')) {
                                String xmlBody = SFAMessenger.createXMLBody(task.Id);
                                Id jobId = SFAMessenger.createAndSendJob(xmlBody);
                                return TriggerHandlerStatus.MESSAGES_SENT;
                            }
                            else {
                                return TriggerHandlerStatus.NO_MESSAGES_SENT;
                            }
                        } else if (task.WhatId != null && task.WhoId == null && task.WhatId.getsObjectType() == Opportunity.sObjectType) {
                            List<Opportunity> opptys = [SELECT Id FROM Opportunity WHERE Id = :task.WhatId];
                            if (!opptys.isEmpty() && !String.isBlank(opptys[0].Id) && task.Status == COMPLETED && 
                                (intFlag != null && string.isNotBlank(intFlag.Value__c) && intFlag.Value__c == 'true')) {
                                String xmlBody = SFAMessenger.createXMLBody(task.Id);
                                Id jobId = SFAMessenger.createAndSendJob(xmlBody);
                                return TriggerHandlerStatus.MESSAGES_SENT;
                            }
                            else {
                                return TriggerHandlerStatus.NO_MESSAGES_SENT;
                            }
                        } else if (task.WhoId != null && task.WhoId.getsObjectType() == Contact.sObjectType) {
                            List<Contact> conts = [SELECT Id, Sagitta_Customer_Contact_ID__c FROM Contact WHERE Id = :task.WhoId];
                            if (!conts.isEmpty() && !String.isBlank(conts[0].Sagitta_Customer_Contact_ID__c) && task.Status == COMPLETED && 
                                (intFlag != null && string.isNotBlank(intFlag.Value__c) && intFlag.Value__c == 'true')) {
                                String xmlBody = SFAMessenger.createXMLBody(task.Id);
                                Id jobId = SFAMessenger.createAndSendJob(xmlBody);
                                return TriggerHandlerStatus.MESSAGES_SENT;
                            }
                            else {
                                return TriggerHandlerStatus.NO_MESSAGES_SENT;
                            }	
                        }
                    }
                } catch (Exception ex) {
                    String message = 'AFTER_INSERT/AFTER_UPDATE; ' +
                        'Error: ' + ex.getMessage() + '; Stack Trace: ' + ex.getStackTraceString();
                    ApplicationLogger.logMessage('TaskTriggerHandler', 'handleTrigger', message, ApplicationLogger.ERROR);
                    throw ex;
                }
                
                return TriggerHandlerStatus.NO_MESSAGES_SENT;
            }
            when else {
                return TriggerHandlerStatus.NO_MESSAGES_SENT;
            }    
        }
    }
    
    private boolean taskHasRelatedAccount(Task task) {
        if (!String.isBlank(task.WhatId)) {
            return true;
        } else {
            return false;
        }
    }
    
    private boolean taskHasRelatedContact(Task task) {
        // if Task has a Contact
        if (!String.isBlank(task.WhoId)) {
            return true;
        } else {
            return false;
        }
    }
    
    private void insertPrefxInDescription(Task task) {
        if (String.isBlank(task.Description)) {
            task.Description = DESCRIPTION_PREFIX;
        }
        else if (!task.Description.startsWith(DESCRIPTION_PREFIX)) {
            task.Description = DESCRIPTION_PREFIX + task.Description;
        }
    }
}