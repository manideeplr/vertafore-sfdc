@isTest
private class SagittaAgencySettingsTriggerTest {
    static Sagitta_Activity_Action_Type__c activityActionType;
    static Sagitta_Issuing_Carrier__c issuingCarrier;
    static Sagitta_Employee__c employee;
    
    @testSetup static void testSetup() {
        createIntegrationFlag('true');
        createIsInitialSyncFlag('false');
        
        activityActionType = new Sagitta_Activity_Action_Type__c(
            Sagitta_Activity_Action_Id__c = '230456720'
        );
        issuingCarrier = new Sagitta_Issuing_Carrier__c(
            Sagitta_Entity_ID__c= 'ID text', 
            Sagitta_Carrier_Type__c= 'N', 
            Sagitta_Issuing_Carrier_Code__c= 'text'
        );
        employee = new Sagitta_Employee__c(
        	Name = 'vpa',
            Sagitta_Employee_Code__c = '###',	
            Sagitta_Entity_ID__c = '123456'
        );
        insert activityActionType;
        insert issuingCarrier;
        insert employee;
    }
    
    // Insert two Sagitta Agency Settings records - should get error on second one
    @isTest static void insertMultipleRecords() {
        Sagitta_Agency_Settings__c agencySettings1 = new Sagitta_Agency_Settings__c(
            Sagitta_Agency_Number__c='text', 
            Default_Activity_Action_Type__c= [SELECT Id FROM Sagitta_Activity_Action_Type__c LIMIT 1].Id,
            Default_Bill_Method__c= 'A', 
            Default_Issuing_Carrier__c= [SELECT Id FROM Sagitta_Issuing_Carrier__c LIMIT 1].Id, 
            Default_Policy_Number__c='text', 
            Integration_User_Id__c='text', 
            Vertafore_Id__c='text',
            Default_Agency_Admin__c = [SELECT Id FROM Sagitta_Employee__c LIMIT 1].Id
        );
        Sagitta_Agency_Settings__c agencySettings2 = new Sagitta_Agency_Settings__c(
            Sagitta_Agency_Number__c='text', 
            Default_Activity_Action_Type__c= [SELECT Id FROM Sagitta_Activity_Action_Type__c LIMIT 1].Id,
            Default_Bill_Method__c= 'A', 
            Default_Issuing_Carrier__c= [SELECT Id FROM Sagitta_Issuing_Carrier__c LIMIT 1].Id,
            Default_Policy_Number__c='text', 
            Integration_User_Id__c='text', 
            Vertafore_Id__c='text',
            Default_Agency_Admin__c = [SELECT Id FROM Sagitta_Employee__c LIMIT 1].Id
        );
        insert agencySettings1;
        
        boolean insertFailed = false;
        try{
            insert agencySettings2;
            Database.SaveResult result = Database.insert(agencySettings2);
        } catch (Exception e) {
            System.assert(e.getMessage().contains('An Agency Settings record already exists in this organization.'));
            insertFailed = true;
        }
        
        System.assert(insertFailed);
    }
    
    // Insert a record that contains a value for Agency_Number__c - should succeed since the request is coming from API
    @isTest static void insertWithAgencyNumber() {
        Sagitta_Agency_Settings__c agencySettings = new Sagitta_Agency_Settings__c(
            Sagitta_Agency_Number__c='text', 
            Default_Activity_Action_Type__c= [SELECT Id FROM Sagitta_Activity_Action_Type__c LIMIT 1].Id,
            Default_Bill_Method__c= 'A', 
            Default_Issuing_Carrier__c= [SELECT Id FROM Sagitta_Issuing_Carrier__c LIMIT 1].Id, 
            Default_Policy_Number__c='text', 
            Integration_User_Id__c='text', 
            Vertafore_Id__c='text',
            Default_Agency_Admin__c = [SELECT Id FROM Sagitta_Employee__c LIMIT 1].Id
        );
        
        Database.SaveResult result = Database.insert(agencySettings);
        
        System.assert(result.isSuccess());
    }
    
    private static void createIntegrationFlag(String flag) {
        Metadata__c intFlag = new Metadata__c();
        intFlag.Name = 'IntegrationFlag';
        intFlag.Value__c = flag;
        insert intFlag;
    }
    
    private static void createIsInitialSyncFlag(String flag) {
        Metadata__c isInitialSyncFlag = new Metadata__c();
        isInitialSyncFlag.Name = 'IsInitialSync';
        isInitialSyncFlag.Value__c = flag;
        insert isInitialSyncFlag;
    }
}