@isTest
public class RestServiceToUpsertDivRecordsTest {

  	@isTest
	static void test1(){
		List<RestServiceToUpsertDivRecords.SagittaDiv> divsToInsert = new List<RestServiceToUpsertDivRecords.SagittaDiv>();
		RestServiceToUpsertDivRecords.SagittaDiv div = new RestServiceToUpsertDivRecords.SagittaDiv();
        div.ItemCode = '1';
        div.ItemCodeHash = '033041071';
        div.Name = 'Test Div';
        div.Status = 'A';
        div.IsHidden = true;
        div.EntityId = '162';
        div.EntityName = 'SagittaDivisionEntity';
        divsToInsert.add(div);
        String myJSON = JSON.serialize(divsToInsert);
		

        RestRequest request = new RestRequest();
        request.requestUri ='https://test.salesforce.com/services/apexrest/UpsertDivRecords';
        request.httpMethod = 'POST';
		request.requestBody = Blob.valueof(myJSON);
		
        RestContext.request = request;
		RestServiceToUpsertDivRecords.ResponseHandler res = RestServiceToUpsertDivRecords.doPost();
		
        List<Sagitta_Division__c> divs = [SELECT Id FROM Sagitta_Division__c];
        system.assertEquals(1, divs.size());		
	}
    
    @isTest
	static void test2(){
		RestServiceToUpsertDivRecords.SagittaDiv div = new RestServiceToUpsertDivRecords.SagittaDiv();
        div.ItemCode = '1';
        div.ItemCodeHash = '033041071';
        div.Name = 'Test Div';
        div.Status = 'A';
        div.IsHidden = true;
        div.EntityId = '162';
        div.EntityName = 'SagittaDivisionEntity';
        String myJSON = JSON.serialize(div);
		

        RestRequest request = new RestRequest();
        request.requestUri ='https://test.salesforce.com/services/apexrest/UpsertDivRecords';
        request.httpMethod = 'POST';
		request.requestBody = Blob.valueof(myJSON);
		
        RestResponse response = new RestResponse();
        response.statusCode = 400;
        
        RestContext.request = request;
        RestContext.response = response;
		RestServiceToUpsertDivRecords.ResponseHandler res = RestServiceToUpsertDivRecords.doPost();
		
        List<Sagitta_Division__c> divs = [SELECT Id FROM Sagitta_Division__c];
        system.assertEquals(0, divs.size());		
	}
}