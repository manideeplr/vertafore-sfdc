@isTest
public class RestServiceToUpsertActActionRecordsTest {
    @testSetup
    static void setup() {
        createIntegrationFlag('true');
        createIsInitialSyncFlag('false');
    }

  	@isTest
	static void test1(){
		List<RestServiceToUpsertActActionRecords.SagittaAct> actsToInsert = new List<RestServiceToUpsertActActionRecords.SagittaAct>();
		RestServiceToUpsertActActionRecords.SagittaAct act = new RestServiceToUpsertActActionRecords.SagittaAct();
        act.DBAction  = 'Test Div';
        act.EntityId = '162';
        act.EntityName = 'SagittaDivisionEntity';
        actsToInsert.add(act);
        String myJSON = JSON.serialize(actsToInsert);
		

        RestRequest request = new RestRequest();
        request.requestUri ='https://test.salesforce.com/services/apexrest/UpsertActActionRecords';
        request.httpMethod = 'POST';
		request.requestBody = Blob.valueof(myJSON);
		
        RestContext.request = request;
		RestServiceToUpsertActActionRecords.ResponseHandler res = RestServiceToUpsertActActionRecords.doPost();
		
        List<Sagitta_Activity_Action_Type__c> acts = [SELECT Id FROM Sagitta_Activity_Action_Type__c];
        system.assertEquals(1, acts.size());		
	}
    
    @isTest
	static void test2(){
		RestServiceToUpsertActActionRecords.SagittaAct act = new RestServiceToUpsertActActionRecords.SagittaAct();
        act.DBAction  = 'Test Div';
        act.EntityId = '162';
        act.EntityName = 'SagittaDivisionEntity';
        String myJSON = JSON.serialize(act);		

        RestRequest request = new RestRequest();
        request.requestUri ='https://test.salesforce.com/services/apexrest/UpsertActActionRecords';
        request.httpMethod = 'POST';
		request.requestBody = Blob.valueof(myJSON);
        
        RestResponse response = new RestResponse();
        response.statusCode = 400;
		
        RestContext.request = request;
        RestContext.response = response;
		RestServiceToUpsertActActionRecords.ResponseHandler res = RestServiceToUpsertActActionRecords.doPost();
		
        List<Sagitta_Activity_Action_Type__c> acts = [SELECT Id FROM Sagitta_Activity_Action_Type__c];
        system.assertEquals(0, acts.size());		
	}
    
    private static void createIntegrationFlag(String flag) {
        Metadata__c intFlag = new Metadata__c();
        intFlag.Name = 'IntegrationFlag';
        intFlag.Value__c = flag;
        insert intFlag;
    }
    
    private static void createIsInitialSyncFlag(String flag) {
        Metadata__c isInitialSyncFlag = new Metadata__c();
        isInitialSyncFlag.Name = 'IsInitialSync';
        isInitialSyncFlag.Value__c = flag;
        insert isInitialSyncFlag;
    }
}