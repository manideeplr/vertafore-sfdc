@isTest
public class RestServiceToUpsertBusinessEntityTest {
	@isTest
	static void test1(){
		List<RestServiceToUpsertBusinessEntityRecords.SagittaBusEnt> itmToInsert = new List<RestServiceToUpsertBusinessEntityRecords.SagittaBusEnt>();
		RestServiceToUpsertBusinessEntityRecords.SagittaBusEnt itm = new RestServiceToUpsertBusinessEntityRecords.SagittaBusEnt();
        itm.EntityName = 'Test';
        itm.EntityId = 'ABC';
        itm.Value = 'Value';
        itmToInsert.add(itm);
        String myJSON = JSON.serialize(itmToInsert);

        RestRequest request = new RestRequest();
        request.requestUri ='https://test.salesforce.com/services/apexrest/UpsertBusinessEntityRecords';
        request.httpMethod = 'POST';
		request.requestBody = Blob.valueof(myJSON);
		
        RestContext.request = request;
		RestServiceToUpsertBusinessEntityRecords.ResponseHandler res = RestServiceToUpsertBusinessEntityRecords.doPost();
		
        List<Sagitta_Business_Entity__c	> entities = [SELECT Id FROM Sagitta_Business_Entity__c];
        system.assertEquals(1, entities.size());
	}
    
    @isTest
	static void test2(){
		RestServiceToUpsertBusinessEntityRecords.SagittaBusEnt itm = new RestServiceToUpsertBusinessEntityRecords.SagittaBusEnt();
        itm.EntityName = 'Test';
        itm.EntityId = 'ABC';
        itm.Value = 'Value';
        String myJSON = JSON.serialize(itm);

        RestRequest request = new RestRequest();
        request.requestUri ='https://test.salesforce.com/services/apexrest/UpsertBranchRecords';
        request.httpMethod = 'POST';
		request.requestBody = Blob.valueof(myJSON);
        
        RestResponse response = new RestResponse();
        response.statusCode = 400;
		
        RestContext.request = request;
        RestContext.response = response;
		RestServiceToUpsertBusinessEntityRecords.ResponseHandler res = RestServiceToUpsertBusinessEntityRecords.doPost();
		
        List<Sagitta_Business_Entity__c> businessEntities = [SELECT Id FROM Sagitta_Business_Entity__c];
        system.assertEquals(0, businessEntities.size());		
	}
}