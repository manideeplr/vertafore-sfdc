@isTest
public class RestServiceToUpsertGrpRecordsTest {

  	@isTest
	static void test1(){
		List<RestServiceToUpsertGroupRecords.SagittaGrp> itmToInsert = new List<RestServiceToUpsertGroupRecords.SagittaGrp>();
		RestServiceToUpsertGroupRecords.SagittaGrp itm = new RestServiceToUpsertGroupRecords.SagittaGrp();
        itm.ItemCode = '1';
        itm.ItemCodeHash = '033041071';
        itm.Name = 'Test';
        itm.Status = 'A';
        itm.IsHidden = true;
        itm.EntityId = '162';
        itm.EntityName = 'SagittaGroupEntity';
        itmToInsert.add(itm);
        String myJSON = JSON.serialize(itmToInsert);
		

        RestRequest request = new RestRequest();
        request.requestUri ='https://test.salesforce.com/services/apexrest/UpsertGroupRecords';
        request.httpMethod = 'POST';
		request.requestBody = Blob.valueof(myJSON);
		
        RestContext.request = request;
		RestServiceToUpsertGroupRecords.ResponseHandler res = RestServiceToUpsertGroupRecords.doPost();
		
        List<Sagitta_Group__c> divs = [SELECT Id FROM Sagitta_Group__c];
        system.assertEquals(1, divs.size());		
	}
    
    @isTest
	static void test2(){
		RestServiceToUpsertGroupRecords.SagittaGrp itm = new RestServiceToUpsertGroupRecords.SagittaGrp();
        itm.ItemCode = '1';
        itm.ItemCodeHash = '033041071';
        itm.Name = 'Test';
        itm.Status = 'A';
        itm.IsHidden = true;
        itm.EntityId = '162';
        itm.EntityName = 'SagittaGroupEntity';
        String myJSON = JSON.serialize(itm);
		

        RestRequest request = new RestRequest();
        request.requestUri ='https://test.salesforce.com/services/apexrest/UpsertGroupRecords';
        request.httpMethod = 'POST';
		request.requestBody = Blob.valueof(myJSON);
        
        RestResponse response = new RestResponse();
        response.statusCode = 400;
		
        RestContext.request = request;
        RestContext.response = response;
		RestServiceToUpsertGroupRecords.ResponseHandler res = RestServiceToUpsertGroupRecords.doPost();
		
        List<Sagitta_Group__c> divs = [SELECT Id FROM Sagitta_Group__c];
        system.assertEquals(0, divs.size());		
	}
}