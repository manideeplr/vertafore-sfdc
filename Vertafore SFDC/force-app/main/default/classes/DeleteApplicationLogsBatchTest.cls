@isTest 
public class DeleteApplicationLogsBatchTest
{
    static testMethod void testMethod1()
    {
        
        Application_Log__c newLogMessage = new Application_Log__c(
            Class__c = 'ClassName',
            Method__c = 'MethodName',
            Message__c = 'Message',
            Log_Type__c = ApplicationLogger.INFO);
        insert newLogMessage;
        Datetime onemonth = Datetime.now().addDays(-31);
		Test.setCreatedDate(newLogMessage.Id, onemonth);
        Application_Log__c log = [SELECT Id, CreatedDate from Application_Log__c WHERE id =:newLogMessage.Id];
        
        Test.startTest();
        DeleteApplicationLogsBatch obj = new DeleteApplicationLogsBatch();
        DataBase.executeBatch(obj);            
        Test.stopTest();
        
        List<Application_Log__c> logs = [SELECT Id FROM Application_Log__c];
        system.assertEquals(0, logs.size());
    }
}