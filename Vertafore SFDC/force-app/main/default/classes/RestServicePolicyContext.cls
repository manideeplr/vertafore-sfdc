/*
    Author : Ben Baik
    Name : RestServicePolicyContext
	Date : 19 July 2019
	Description : Gets relevant Salesforce objects for Sagitta Policies (LOB's, BU's, Employees, Issuing Carriers, and Policies).
				  Finds object by ID or code in relevant data.
					
	Modification History :
	*************************************************************
	Version               Author             Date                      Description
	1                     Ben Baik           19 July 2019              Original
*/
public class RestServicePolicyContext {
    private String lobsNamesAsString;
    private List<Sagitta_Employee__c> employees;
    private List<Sagitta_Division__c> divisions;
    private List<Sagitta_Branch__c> branches;
    private List<Sagitta_Department__c> depts;
    private List<Sagitta_Group__c> groups;
    private List<Account> accounts;
    private List<Sagitta_Issuing_Carrier__c> issuingCarriers;
    private List<Sagitta_Policy__c> policies;
    
    // Summary:
    // Gets all relevant data for Sagitta Policies
    // 
    // Details:
    // @param List<RestServiceToUpsertPolicyRecords.SagittaPolicy> SagittaPolicies - Apex representation of Sagitta Polices
    // @return RestServicePolicyContext instance w/ relevant data
    public RestServicePolicyContext(List<RestServiceToUpsertPolicyRecords.SagittaPolicy> SagittaPolicies) {
        Set<String> employeeIds = new Set<String>();
        Set<String> divisionIds = new Set<String>();
        Set<String> branchIds = new Set<String>();
        Set<String> deptIds = new Set<String>();
        Set<String> groupIds = new Set<String>();
        Set<String> accountIds = new Set<String>();
        Set<String> issuingCarrierIds = new Set<String>();
        Set<String> policyIds = new Set<String>();
      
        for (RestServiceToUpsertPolicyRecords.SagittaPolicy policy : SagittaPolicies) {              
            employeeIds.add(policy.ExecCode);
            employeeIds.add(policy.CsrCode);
            divisionIds.add(policy.GLDivisionCode);
            branchIds.add(policy.GLBranchCode);
            deptIds.add(policy.GLDepartmentCode);
            groupIds.add(policy.GLGroupCode);
            accountIds.add(policy.CustomerId);
            issuingCarrierIds.add(policy.WritingCompanyCode);
            policyIds.add(policy.PolicyId);
        }
        
        employees = getRelevantEmployees(employeeIds);
        divisions = getRelevantDivisions(divisionIds);
        branches = getRelevantBranches(branchIds);
        depts = getRelevantDepts(deptIds);
        groups = getRelevantGroups(groupIds);
        accounts = getRelevantAccounts(accountIds);
        issuingCarriers = getRelevantIssuingCarriers(issuingCarrierIds);
        policies = getRelevantPolicies(policyIds);
    }
    
    // Summary:
    // Find Sagitta_Employee__c by Sagitta Customer employee code
    // 
    // Details:
    // @param String lobIdString - Comma seperated string od Line of Business Id's
    // @return String - Comma seperated string of Line of Business names or empty string
    public String getLobNamesAsString(String lobIdString) {
        if (String.isBlank(lobIdString))  {
            return '';
        }
        else {
            Set<String> lobIdsForQuery = new Set<String>();
            List<String> lobIdList = lobIdString.split(',');
            for (String lobId : lobIdList) {
                lobIdsForQuery.add(lobId.trim());
            }
            
            List<Sagitta_Line_of_Business__c> relevantLobsForPolicy = getRelevantLobs(lobIdsForQuery);
            String lobNameString = '';
            for (Sagitta_Line_of_Business__c lob : relevantLobsForPolicy) {
                lobNameString = lobNameString + lob.Name + ', ';
            }
            
            return lobNameString.removeEnd(', ');
        }
    }
    
    // Summary:
    // Find Sagitta_Employee__c by Sagitta Customer employee code
    // 
    // Details:
    // @param String customerId - Sagitta employee code
    // @return Sagitta_Employee__c - Sagitta_Employee__c with matching Sagitta employee code
    public Sagitta_Employee__c findEmployee(String employeeCode) {
        for (Sagitta_Employee__c employee : employees) {
            if (employee.Sagitta_Employee_Code__c == employeeCode) {
                return employee;
            }
        }
        return null;
    }
    
    // Summary:
    // Find Sagitta_Division__c by Sagitta division code
    // 
    // Details:
    // @param String divisionCode - Sagitta division code
    // @return Sagitta_Division__c - Sagitta_Division__c with matching Sagitta division code
    public Sagitta_Division__c findDivision(String divisionCode) {
        for (Sagitta_Division__c division : divisions) {
            if (division.ItemCode__c == divisionCode) {
                return division;
            }
        }
        return null;
    }
    
    // Summary:
    // Find Sagitta_Branch__c by Sagitta branch code
    // 
    // Details:
    // @param String branchCode - Sagitta branch code
    // @return Sagitta_Branch__c - Sagitta_Branch__c with matching Sagitta branch code
    public Sagitta_Branch__c findBranch(String branchCode) {
        for (Sagitta_Branch__c branch : branches) {
            if (branch.ItemCode__c == branchCode) {
                return branch;
            }
        }
        return null;
    }
    
    // Summary:
    // Find Sagitta_Department__c by Sagitta department code
    // 
    // Details:
    // @param String departmentCode - Sagitta department code
    // @return Sagitta_Department__c - Sagitta_Department__c with matching Sagitta department code
    public Sagitta_Department__c findDept(String deptCode) {
        for (Sagitta_Department__c dept : depts) {
            if (dept.ItemCode__c == deptCode) {
                return dept;
            }
        }
        return null;
    }
    
    // Summary:
    // Find Sagitta_Group__c by Sagitta group code
    // 
    // Details:
    // @param String groupCode - Sagitta group code
    // @return Sagitta_Group__c - Sagitta_Group__c with matching Sagitta group code
    public Sagitta_Group__c findGroup(String groupCode) {
        for (Sagitta_Group__c grp : groups) {
            if (grp.ItemCode__c == groupCode) {
                return grp;
            }
        }
        return null;
    }
    
    // Summary:
    // Find Salesforce Issuing Carrier by Sagitta Company Code 
    // 
    // Details:
    // @param String companyCode - Sagitta Company Code 
    // @return Sagitta_Issuing_Carrier__c - Issuing Carrier with matching Sagitta Company Code
    public Sagitta_Issuing_Carrier__c findIssuingCarrier(String companyCode) {
        for (Sagitta_Issuing_Carrier__c issuingCarrier : issuingCarriers) {
            if (issuingCarrier.Sagitta_Issuing_Carrier_Code__c == companyCode) {
                return issuingCarrier;
            }
        }
        return null;
    }
    
    // Summary:
    // Find Salesforce Account by Sagitta Customer ID
    // 
    // Details:
    // @param String customerId - Sagitta Customer ID
    // @return Account - Account with matching Sagitta Customer ID
    public Account findAccount(String customerId) {
        for (Account account : accounts) {
            if (account.Sagitta_Customer_Id__c == customerId) {
                return account;
            }
        }
        return null;
    }
    
    // Summary:
    // Find Salesforce Policy by Sagitta Policy ID
    // 
    // Details:
    // @param String policyId - Sagitta Policy ID
    // @return Account - Policy object with matching Sagitta Policy ID
    public Sagitta_Policy__c findPolicy(String policyId) {
        for (Sagitta_Policy__c policy : policies) {
            if (policy.Sagitta_Policy_ID__c == policyId) {
                return policy;
            }
        }
        return new Sagitta_Policy__c();
    }
    
    // Summary:
    // Gets all relevant Sagitta_Line_of_Business__c objects
    // 
    // Details:
    // @param Set<String> lobIds - Set of Sagitta Line of Business ID's used to query for relevant Sagitta_Line_of_Business__c objects
    // @return List<Sagitta_Line_of_Business__c> - List of relevant Sagitta_Line_of_Business__c objects
    private List<Sagitta_Line_of_Business__c> getRelevantLobs(Set<String> lobIds) {
    	return [SELECT Id,
                       Name,
                       Sagitta_Entity_ID__c
                FROM Sagitta_Line_of_Business__c
                WHERE Sagitta_Entity_ID__c
                IN :lobIds];
    }
    
    // Summary:
    // Gets all relevant Sagitta_Employee__c objects
    // 
    // Details:
    // @param Set<String> employeeIds - Set of Sagitta Employee ID's used to query for relevant Sagitta_Employee__c objects
    // @return List<Sagitta_Employee__c> - List of relevant Sagitta_Employee__c objects
    private List<Sagitta_Employee__c> getRelevantEmployees(Set<String> employeeIds) {
        return [SELECT Id,
                       Name,
                       Sagitta_Is_Executive__c,
                       Sagitta_Is_Representative__c,
                       Sagitta_Employee_Code__c,
                       Sagitta_Entity_ID__c
                FROM Sagitta_Employee__c
                WHERE Sagitta_Employee_Code__c
                IN :employeeIds];
    }
    
    // Summary:
    // Gets all relevant Sagitta_Division__c objects
    // 
    // Details:
    // @param Set<String> divisionIds - Set of Sagitta Division ID's used to query for relevant Sagitta_Division__c objects
    // @return List<Sagitta_Division__c> - List of relevant Sagitta_Division__c objects
    private List<Sagitta_Division__c> getRelevantDivisions(Set<String> divisionIds) {
        return [SELECT Id,
                       Name,
                       ItemCode__c,
                       Sagitta_IsHidden__c
                FROM Sagitta_Division__c
                WHERE ItemCode__c
                IN :divisionIds];
    }
    
    // Summary:
    // Gets all relevant Sagitta_Branch__c objects
    // 
    // Details:
    // @param Set<String> branchIds - Set of Sagitta Branch ID's used to query for relevant Sagitta_Branch__c objects
    // @return List<Sagitta_Branch__c> - List of relevant Sagitta_Branch__c objects
    private List<Sagitta_Branch__c> getRelevantBranches(Set<String> branchIds) {
        return [SELECT Id,
                       Name,
                       ItemCode__c,
                       Sagitta_IsHidden__c
                FROM Sagitta_Branch__c
                WHERE ItemCode__c
                IN :branchIds];
    }
    
    // Summary:
    // Gets all relevant Sagitta_Department__c objects
    // 
    // Details:
    // @param Set<String> deptIds - Set of Sagitta Department ID's used to query for relevant Sagitta_Department__c objects
    // @return List<Sagitta_Department__c> - List of relevant Sagitta_Department__c objects
    private List<Sagitta_Department__c> getRelevantDepts(Set<String> deptIds) {
        return [SELECT Id,
                       Name,
                       ItemCode__c,
                       Sagitta_IsHidden__c
                FROM Sagitta_Department__c
                WHERE ItemCode__c
                IN :deptIds];
    }
    
    // Summary:
    // Gets all relevant Sagitta_Group__c objects
    // 
    // Details:
    // @param Set<String> groupIds - Set of Sagitta Group ID's used to query for relevant Sagitta_Group__c objects
    // @return List<Sagitta_Group__c> - List of relevant Sagitta_Group__c objects
    private List<Sagitta_Group__c> getRelevantGroups(Set<String> groupIds) {
        return [SELECT Id,
                       Name,
                       ItemCode__c,
                       Sagitta_IsHidden__c
                FROM Sagitta_Group__c
                WHERE ItemCode__c
                IN :groupIds];
    }
    
    // Summary:
    // Gets all relevant Account objects
    // 
    // Details:
    // @param Set<String> customerIds - Set of Sagitta Customer ID's used to query for relevant Account objects
    // @return List<Account> - List of relevant Account objects
    private List<Account> getRelevantAccounts(Set<String> customerIds) {
        return [SELECT Id,
                       Name,
                       Sagitta_Customer_ID__c
                FROM Account
                WHERE Sagitta_Customer_Id__c
               	IN :customerIds];
    }
    
    // Summary:
    // Gets all relevant Sagitta_Issuing_Carrier__c objects
    // 
    // Details:
    // @param Set<String> carrierIds - Set of Sagitta Carrier ID's used to query for relevant Sagitta_Issuing_Carrier__c objects
    // @return List<Sagitta_Issuing_Carrier__c> - List of relevant Sagitta_Issuing_Carrier__c objects
    private List<Sagitta_Issuing_Carrier__c> getRelevantIssuingCarriers(Set<String> carrierIds) {
        return [SELECT Id,
                       Name,
                       Sagitta_Issuing_Carrier_Code__c
                FROM Sagitta_Issuing_Carrier__c
                WHERE Sagitta_Issuing_Carrier_Code__c
               	IN :carrierIds];
    }
    
    // Summary:
    // Gets all relevant Sagitta_Policy__c objects
    // 
    // Details:
    // @param Set<String> policyIds - Set of Sagitta Policy ID's used to query for relevant Sagitta_Policy__c objects
    // @return List<Sagitta_Policy__c> - List of relevant Sagitta_Policy__c objects
    private List<Sagitta_Policy__c> getRelevantPolicies(Set<String> policyIds) {
        List<Sagitta_Policy__c> policiesWithNullFields;
        
        policiesWithNullFields = [SELECT Id,
                       					 Sagitta_Division__c,
                       					 Sagitta_Branch__c,
                                         Sagitta_Department__c,
                                         Sagitta_Group__c,
                                         Sagitta_Employee_Policy_Exec__c,
                                         Sagitta_Employee_Policy_Rep__c,
                                         Sagitta_Account_Name__c,
                                         Sagitta_Bill_Method__c,
                                         Sagitta_Customer_ID__c,
                                         Sagitta_Description__c,
                                         Sagitta_Effective_Date__c,
                                         Sagitta_Expiration_Date__c,
                                         Sagitta_Policy_ID__c,
                                         Name,
                                         Sagitta_Policy_Status__c,
                                         Sagitta_Type_of_Business__c,
                                  		 Sagitta_LOB__c,
                                         Sagitta_Annual_Premium__c,
                                         Sagitta_Writing_Company__c
                                   FROM Sagitta_Policy__c
                                   WHERE Sagitta_Policy_ID__c
                                   IN :policyIds];
        
        return nullFieldsToEmptyString(policiesWithNullFields);
    }

    // Summary:
    // Convert all string fields that are null to an empty string. This is because null fields in the
    // RestService post body string get deserialized into empty strings which will cause issues when
    // comparing fields in the RestServiceToUpsertPolicyRecords.mapFields method.
    // 
    // Details:
    // @param List<Sagitta_Policy__c> policies - List of relevant Sagitta_Policy__c objects
    // @return List<Sagitta_Group__c> - List of relevant Sagitta_Policy__c objects with null fields set to empty string
    private List<Sagitta_Policy__c> nullFieldsToEmptyString(List<Sagitta_Policy__c> policies) {
		for (Sagitta_Policy__c policy : policies) {
            if (policy.Sagitta_Division__c == null) {
                policy.put('Sagitta_Division__c', '');
            }
            
            if (policy.Sagitta_Branch__c == null) {
                policy.put('Sagitta_Branch__c', '');
            }
            
            if (policy.Sagitta_Department__c == null) {
                policy.put('Sagitta_Department__c', '');
            }
            
            if (policy.Sagitta_Group__c == null) {
                policy.put('Sagitta_Group__c', '');
            }
            
            if (policy.Sagitta_Employee_Policy_Exec__c == null) {
                policy.put('Sagitta_Employee_Policy_Exec__c', '');
            }
            
            if (policy.Sagitta_Employee_Policy_Rep__c == null) {
                policy.put('Sagitta_Employee_Policy_Rep__c', '');
            }
            
            if (policy.Sagitta_Account_Name__c == null) {
                policy.put('Sagitta_Account_Name__c', '');
            }
            
            if (policy.Sagitta_Bill_Method__c == null) {
                policy.Sagitta_Bill_Method__c = '';
            }
            
            if (policy.Sagitta_Customer_ID__c == null) {
                policy.Sagitta_Customer_ID__c = '';
            }
            
            if (policy.Sagitta_Description__c == null) {
                policy.Sagitta_Description__c = '';
            }
            
            if (policy.Sagitta_LOB__c == null) {
                policy.Sagitta_LOB__c = '';
            }
                     
            if (policy.Sagitta_Policy_ID__c == null) {
                policy.Sagitta_Policy_ID__c = '';
            }
            
            if (policy.Name == null) {
                policy.Name = '';
            }
            
            if (policy.Sagitta_Policy_Status__c == null) {
                policy.Sagitta_Policy_Status__c = '';
            }
            
            if (policy.Sagitta_Type_of_Business__c == null) {
                policy.Sagitta_Type_of_Business__c = '';
            }
            
            if (policy.Sagitta_Writing_Company__c == null) {
                policy.put('Sagitta_Writing_Company__c', '');
            }
        }
        
        return policies;
    }
}