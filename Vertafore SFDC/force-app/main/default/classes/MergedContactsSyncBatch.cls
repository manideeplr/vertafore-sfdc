/*
    Author : Vikram
    Name : MergedContactsSyncBatch
    Date : 23 July, 2019
    Description: Used to sync Contacts to Sagitta after the ContactMergeJob is done.
    Modification History :
    *************************************************************
    Version             Author          Date                        Description
*/

global class MergedContactsSyncBatch implements Database.Batchable<sObject>,Database.AllowsCallouts{
    
    //Set of Ids of merged-updated contacts
    
    Set<Id> MainContacts = new Set<Id>();
    
    global MergedContactsSyncBatch(Set<Id> listMainContacts)
    {
        mainContacts = listMainContacts;
    }
    
    global Database.QueryLocator start(Database.BatchableContext BC)
    {
        return DataBase.getQueryLocator([SELECT Id FROM contact WHERE Id IN : mainContacts]);
    }
    
    // Summary:
    // Calls the HttpClient class to send notification or callout to Sagitta for the updates made by user.
    //
    // @param context - 
    // @param scope -
    global void execute(Database.BatchableContext BC, List<sObject> scopeAcc)
    {
        
        for (Contact con : (List<Contact>)scopeAcc){
            try {
				string xmlBody = SFAMessenger.createXMLBody(con.Id);                
                HttpClient client = new HttpClient();
                client.SendXMLRequest(xmlBody);
            }
            catch (Exception ex) {         
                String message = 'Error syncing Contacts. ;' + 
                'Error: ' + ex.getMessage() + '; Stack Trace:' + ex.getStackTraceString();
            	ApplicationLogger.logMessage('MergedContactsSyncBatch', 'execute', message, ApplicationLogger.ERROR);        
            }
        }
    }
    
    global void finish(Database.BatchableContext BC)
    {
    }    
}