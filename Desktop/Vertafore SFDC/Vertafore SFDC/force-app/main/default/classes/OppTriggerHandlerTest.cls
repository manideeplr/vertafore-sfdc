@isTest
public class OppTriggerHandlerTest {
    @testSetup
    public static void setup() {
        createIsInitialSyncFlag('false');
        
        User usr = createUser();
        insert usr;
        Sagitta_Division__c div1 = new Sagitta_Division__c(Name = 'Division 1', Sagitta_IsHidden__c	= false);
        Sagitta_Department__c dept1 = new Sagitta_Department__c(Name = 'Department 1', Sagitta_IsHidden__c = false);
        Sagitta_Branch__c branch1 = new Sagitta_Branch__c(Name = 'Branch 1', Sagitta_IsHidden__c = false);
        Sagitta_Group__c grp1 = new Sagitta_Group__c(Name = 'Group 1', Sagitta_IsHidden__c = false);
        insert div1;
        insert dept1;
        insert branch1;
        insert grp1;
        
        Sagitta_Branch__c branch2 = new Sagitta_Branch__c(Name = 'Branch 2');
        insert branch2;
        
        DivBrJunction__c divBrJunc = new DivBrJunction__c(Sagitta_Division__c = div1.Id, Sagitta_Branch__c = branch1.Id);
        BrDepJunction__c brDepJunc = new BrDepJunction__c(Sagitta_Branch__c = branch1.Id, Sagitta_Department__c = dept1.Id);
        DepGrpJunction__c depGrpJunc = new DepGrpJunction__c(Sagitta_Department__c = dept1.Id, Sagitta_Group__c = grp1.Id);
        insert divBrJunc;
        insert brDepJunc;
        insert depGrpJunc;
        
        Sagitta_Employee__c employee = new Sagitta_Employee__c(
            Name = 'Executive',
            Sagitta_Is_Executive__c = true, 
            Sagitta_Is_Representative__c = true,
            Sagitta_Employee_Code__c = 'ABCDEFG',
            Sagitta_Entity_Id__c = 'cjhcj',
            Sagitta_Employee_Status__c = 'Active'
        );
        insert employee;
        
        Sagitta_Mapping__c mapping = new Sagitta_Mapping__c(
            Name='Test',
            Sagitta_Assigned_To__c = UserInfo.getUserId(),
            Sagitta_Executive__c = employee.Id,
            Sagitta_Representative__c = employee.Id,
            Sagitta_Division__c = div1.Id,
            Sagitta_Branch__c = branch1.Id,
            Sagitta_Department__c = dept1.Id,
            Sagitta_Group__c = grp1.Id
        );
        insert mapping;
        
        Sagitta_Activity_Action_Type__c activityAction = createActivityActionType();
        insert activityAction;
        Sagitta_Issuing_Carrier__c issuingCarrier = createIssuingCarrier();
        insert issuingCarrier;
        // first argument is for integration stage value
        Sagitta_Agency_Settings__c agencySettings = createAgencySettings('Proposal/Price Quote', activityAction, issuingCarrier, employee);
        insert agencySettings;
        
        Account acct1 = new Account(
            Name = 'acct1',
            Sagitta_Client_Type__c = 'P',
                        Sagitta_Division__c = div1.Id,
            Sagitta_Branch__c = branch1.Id,
            Sagitta_Department__c = dept1.Id,
            Sagitta_Group__c = grp1.Id,
            Sagitta_Employee_Account_Exec__c = employee.Id,
            Sagitta_Employee_Account_Rep__c	= employee.Id
        );
        Account acct2 = new Account(
            Name = 'acct2',
            Sagitta_Client_Type__c = 'P',
            Sagitta_Employee_Account_Exec__c = employee.Id,
            Sagitta_Employee_Account_Rep__c	= employee.Id,
                                    Sagitta_Division__c = div1.Id,
            Sagitta_Branch__c = branch1.Id,
            Sagitta_Department__c = dept1.Id,
            Sagitta_Group__c = grp1.Id
        );
        insert acct1;
        insert acct2;
        
        Opportunity notSyncingOpp = new Opportunity(
            Name = 'Not Synced', 
            CloseDate = Date.today(), 
            StageName = 'Prospecting',
            Sagitta_Opportunity_Type__c = 'M',
            Sagitta_Type_of_Business__c = '1',
            AccountId = acct1.Id
        );
        Opportunity syncingOpp = new Opportunity(
            Name = 'Synced', 
            CloseDate = Date.today(), 
            StageName = 'Proposal/Price Quote', 
            AccountId = acct1.Id,
            Sagitta_Opportunity_Type__c = 'M',
            Sagitta_Type_of_Business__c = '1');
        Opportunity oppClosedWon = new Opportunity(
            Name = 'Closed as Won', 
            CloseDate = Date.today(), 
            StageName = 'Closed Won', 
            AccountId = acct2.Id,
            Sagitta_Opportunity_Type__c = 'M',
            Sagitta_Type_of_Business__c = '1');
        Opportunity oppClosedLost = new Opportunity(
            AccountId = acct1.Id,
            Name = 'Closed as Lost', 
            CloseDate = Date.today(), 
            StageName = 'Closed Lost',
            Sagitta_Opportunity_Type__c = 'M',
            Sagitta_Type_of_Business__c = '1');
        Opportunity oppWithBusinessUnits = new Opportunity(
            Name = 'BusinessUnits', 
            CloseDate = Date.today(), 
            StageName = 'Closed Lost',
            Sagitta_Opportunity_Type__c = 'M',
            Sagitta_Type_of_Business__c = '1',
        	Sagitta_Division__c = null,
            Sagitta_Branch__c = null,
            Sagitta_Department__c = null,
            Sagitta_Group__c = null,
            AccountId = acct1.Id
        );
        Opportunity oppClosedWonPersonal = new Opportunity(
            Name = 'Closed as Won Personal', 
            CloseDate = Date.today(), 
            StageName = 'Closed Won', 
            AccountId = acct2.Id,
            Sagitta_Opportunity_Type__c = 'M',
            Sagitta_Type_of_Business__c = '1');
        Opportunity oppClosedWonCommercial = new Opportunity(
            Name = 'Closed as Won Commercial', 
            CloseDate = Date.today(), 
            StageName = 'Closed Won', 
            AccountId = acct2.Id,
            Sagitta_Opportunity_Type__c = 'M',
            Sagitta_Type_of_Business__c = '2');
        Opportunity oppClosedWonNonPC = new Opportunity(
            Name = 'Closed as Won NonPC', 
            CloseDate = Date.today(), 
            StageName = 'Closed Won', 
            AccountId = acct2.Id,
            Sagitta_Opportunity_Type__c = 'M',
            Sagitta_Type_of_Business__c = '3');
        Opportunity oppClosedWonBenefits = new Opportunity(
            Name = 'Closed as Won Benefits', 
            CloseDate = Date.today(), 
            StageName = 'Closed Won', 
            AccountId = acct2.Id,
            Sagitta_Opportunity_Type__c = 'M',
            Sagitta_Type_of_Business__c = '4');
        Opportunity oppClosedWonLife = new Opportunity(
            Name = 'Closed as Won Life', 
            CloseDate = Date.today(), 
            StageName = 'Closed Won', 
            AccountId = acct2.Id,
            Sagitta_Opportunity_Type__c = 'M',
            Sagitta_Type_of_Business__c = '5');
        Opportunity oppClosedWonHealth = new Opportunity(
            Name = 'Closed as Won Health', 
            CloseDate = Date.today(), 
            StageName = 'Closed Won', 
            AccountId = acct2.Id,
            Sagitta_Opportunity_Type__c = 'M',
            Sagitta_Type_of_Business__c = '6');
        Opportunity oppClosedWonFinancial = new Opportunity(
            Name = 'Closed as Won Financial', 
            CloseDate = Date.today(), 
            StageName = 'Closed Won', 
            AccountId = acct2.Id,
            Sagitta_Opportunity_Type__c = 'M',
            Sagitta_Type_of_Business__c = '7');
		insert notSyncingOpp;
        insert syncingOpp;
        insert oppClosedWon;
        insert oppClosedLost;
        insert oppWithBusinessUnits;
        insert oppClosedWonPersonal;
		insert oppClosedWonCommercial;
		insert oppClosedWonNonPC;
		insert oppClosedWonBenefits;
		insert oppClosedWonLife;
		insert oppClosedWonHealth;
		insert oppClosedWonFinancial;
    }
    @isTest
    public static void HandleTrigger_ShouldReturnNO_MESSAGES_SENT_WhenThereAreNoAgencySettings() {
        OppTriggerHandler oppTriggerHandler = new OppTriggerHandler();
        Opportunity opp = [SELECT Name, CloseDate, StageName, AccountId, Sagitta_Type_of_Business__c FROM Opportunity WHERE Name = 'Synced'];
        List<Opportunity> oppList = new List<Opportunity>();
        oppList.add(opp);

        TriggerHandlerStatus actual = oppTriggerHandler.handleTrigger(oppList, null, TriggerOperation.AFTER_UPDATE);        
        TriggerHandlerStatus expected = TriggerHandlerStatus.NO_MESSAGES_SENT;
        System.assertEquals(expected, actual);
    }
    @isTest
    public static void HandleTrigger_ShouldReturnNO_MESSAGES_SENT_WhenThereAreAgencySettings_AndNoIntegrationStage() {
        Test.startTest();
        Test.stopTest();
        
        OppTriggerHandler oppTriggerHandler = new OppTriggerHandler();
        Opportunity opp = [SELECT Name, CloseDate, StageName, AccountId, Sagitta_Type_of_Business__c FROM Opportunity WHERE Name = 'Synced'];
        List<Opportunity> oppList = new List<Opportunity>();
        oppList.add(opp);

        TriggerHandlerStatus actual = oppTriggerHandler.handleTrigger(oppList, null, TriggerOperation.AFTER_UPDATE);
        TriggerHandlerStatus expected = TriggerHandlerStatus.NO_MESSAGES_SENT;
        System.assertEquals(expected, actual);
    }
    @isTest
    public static void HandleTrigger_ShouldReturnNO_MESSAGES_SENT_WhenOppUpdatesInNonTriggeringStage(){
        OppTriggerHandler oppTriggerHandler = new OppTriggerHandler();
        Opportunity opp = [SELECT Name, CloseDate, StageName, AccountId, Sagitta_Type_of_Business__c FROM Opportunity WHERE Name = 'Not Synced'];
        List<Opportunity> oppList = new List<Opportunity>();
        oppList.add(opp);
        
        TriggerHandlerStatus actual = oppTriggerHandler.handleTrigger(oppList, null, TriggerOperation.AFTER_UPDATE);        
        TriggerHandlerStatus expected = TriggerHandlerStatus.NO_MESSAGES_SENT;
        System.assertEquals(expected, actual);
    }
    @isTest
    public static void HandleTrigger_ShouldReturnMESSAGE_SENT_WhenOppUpdatesInTriggeringStage(){
        Test.startTest();
        createIntegrationFlag('true');
        Test.stopTest();
        
        OppTriggerHandler oppTriggerHandler = new OppTriggerHandler();
        Opportunity opp = [SELECT Name, CloseDate, StageName, AccountId, Sagitta_Type_of_Business__c FROM Opportunity WHERE Name = 'Synced'];
        List<Opportunity> oppList = new List<Opportunity>();
        oppList.add(opp);
        
        TriggerHandlerStatus actual = oppTriggerHandler.handleTrigger(oppList, null, TriggerOperation.AFTER_UPDATE);        
        TriggerHandlerStatus expected = TriggerHandlerStatus.MESSAGES_SENT;
        System.assertEquals(expected, actual);
    }
    @isTest
    public static void HandleTrigger_ShouldReturnMESSAGE_SENT_WhenOppClosedAsWon(){
        Test.startTest();
        createIntegrationFlag('true');
        Test.stopTest();
        
        OppTriggerHandler oppTriggerHandler = new OppTriggerHandler();
        Opportunity opp = [SELECT Name, CloseDate, StageName, AccountId, Sagitta_Type_of_Business__c FROM Opportunity WHERE Name = 'Closed as Won'];
        List<Opportunity> oppList = new List<Opportunity>();
        oppList.add(opp);
        
        TriggerHandlerStatus actual = oppTriggerHandler.handleTrigger(oppList, null, TriggerOperation.AFTER_UPDATE);        
        TriggerHandlerStatus expected = TriggerHandlerStatus.MESSAGES_SENT;
        System.assertEquals(expected, actual);
    }
    @isTest
    public static void HandleTrigger_ShouldReturnMESSAGE_SENT_WhenOppClosedAsLost(){
        Test.startTest();
        createIntegrationFlag('true');
        Test.stopTest();
        
        OppTriggerHandler oppTriggerHandler = new OppTriggerHandler();
        Opportunity opp = [SELECT Name, CloseDate, StageName, AccountId, Sagitta_Type_of_Business__c FROM Opportunity WHERE Name = 'Closed as Lost'];
        List<Opportunity> oppList = new List<Opportunity>();
        oppList.add(opp);
        
        TriggerHandlerStatus actual = oppTriggerHandler.handleTrigger(oppList, null, TriggerOperation.AFTER_UPDATE);        
        TriggerHandlerStatus expected = TriggerHandlerStatus.MESSAGES_SENT;
        System.assertEquals(expected, actual);
    }
    @isTest
    public static void HandleTrigger_ShouldReturnNO_MESSAGE_SENT_WhenIntegrationFlagIsFlase(){
        Test.startTest();
        createIntegrationFlag('false');
        Test.stopTest();
        
        OppTriggerHandler oppTriggerHandler = new OppTriggerHandler();
        Opportunity opp = [SELECT Name, CloseDate, StageName, AccountId, Sagitta_Type_of_Business__c FROM Opportunity WHERE Name = 'Synced'];
        List<Opportunity> oppList = new List<Opportunity>();
        oppList.add(opp);
        
        TriggerHandlerStatus actual = oppTriggerHandler.handleTrigger(oppList, null, TriggerOperation.AFTER_UPDATE);        
        TriggerHandlerStatus expected = TriggerHandlerStatus.NO_MESSAGES_SENT;
        System.assertEquals(expected, actual);
    }
    @isTest
    public static void HandleTrigger_ShouldReturnNO_MESSAGE_SENT_WhenIntegrationFlagIsEmpty(){
        Test.startTest();
        createIntegrationFlag('');
        Test.stopTest();
        
        OppTriggerHandler oppTriggerHandler = new OppTriggerHandler();
        Opportunity opp = [SELECT Name, CloseDate, StageName, AccountId, Sagitta_Type_of_Business__c FROM Opportunity WHERE Name = 'Synced'];
        List<Opportunity> oppList = new List<Opportunity>();
        oppList.add(opp);
        
        TriggerHandlerStatus actual = oppTriggerHandler.handleTrigger(oppList, null, TriggerOperation.AFTER_UPDATE);        
        TriggerHandlerStatus expected = TriggerHandlerStatus.NO_MESSAGES_SENT;
        System.assertEquals(expected, actual);
    }
    
    @isTest
    public static void HandleTrigger_ShouldReturnNO_MESSAGE_SENT_WhenNoCustomMetadata(){
        OppTriggerHandler oppTriggerHandler = new OppTriggerHandler();
        Opportunity opp = [SELECT Name, CloseDate, StageName, AccountId, Sagitta_Type_of_Business__c FROM Opportunity WHERE Name = 'Synced'];
        List<Opportunity> oppList = new List<Opportunity>();
        oppList.add(opp);
        
        TriggerHandlerStatus actual = oppTriggerHandler.handleTrigger(oppList, null, TriggerOperation.AFTER_UPDATE);        
        TriggerHandlerStatus expected = TriggerHandlerStatus.NO_MESSAGES_SENT;
        System.assertEquals(expected, actual);
    }
    
    @isTest
    public static void HandleTrigger_ShouldReturnMESSAGE_SENT_WhenOppClosedAsWonPersonal(){
        Test.startTest();
        createIntegrationFlag('true');
        Test.stopTest();
        
        OppTriggerHandler oppTriggerHandler = new OppTriggerHandler();
        Opportunity opp = [SELECT Name, CloseDate, StageName, AccountId, Sagitta_Type_of_Business__c FROM Opportunity WHERE Name = 'Closed as Won Personal'];
        List<Opportunity> oppList = new List<Opportunity>();
        oppList.add(opp);
        
        TriggerHandlerStatus actual = oppTriggerHandler.handleTrigger(oppList, null, TriggerOperation.AFTER_UPDATE);        
        TriggerHandlerStatus expected = TriggerHandlerStatus.MESSAGES_SENT;
        System.assertEquals(expected, actual);
    }
	
	@isTest
    public static void HandleTrigger_ShouldReturnMESSAGE_SENT_WhenOppClosedAsWonCommercial(){
        Test.startTest();
        createIntegrationFlag('true');
        Test.stopTest();
        
        OppTriggerHandler oppTriggerHandler = new OppTriggerHandler();
        Opportunity opp = [SELECT Name, CloseDate, StageName, AccountId, Sagitta_Type_of_Business__c FROM Opportunity WHERE Name = 'Closed as Won Commercial'];
        List<Opportunity> oppList = new List<Opportunity>();
        oppList.add(opp);
        
        TriggerHandlerStatus actual = oppTriggerHandler.handleTrigger(oppList, null, TriggerOperation.AFTER_UPDATE);        
        TriggerHandlerStatus expected = TriggerHandlerStatus.MESSAGES_SENT;
        System.assertEquals(expected, actual);
    }
    
    @isTest
    public static void HandleTrigger_ShouldReturnMESSAGE_SENT_WhenOppClosedAsWonNonPC(){
        Test.startTest();
        createIntegrationFlag('true');
        Test.stopTest();
        
        OppTriggerHandler oppTriggerHandler = new OppTriggerHandler();
        Opportunity opp = [SELECT Name, CloseDate, StageName, AccountId, Sagitta_Type_of_Business__c FROM Opportunity WHERE Name = 'Closed as Won NonPC'];
        List<Opportunity> oppList = new List<Opportunity>();
        oppList.add(opp);
        
        TriggerHandlerStatus actual = oppTriggerHandler.handleTrigger(oppList, null, TriggerOperation.AFTER_UPDATE);        
        TriggerHandlerStatus expected = TriggerHandlerStatus.MESSAGES_SENT;
        System.assertEquals(expected, actual);
    }
    
    @isTest
    public static void HandleTrigger_ShouldReturnMESSAGE_SENT_WhenOppClosedAsWonBenefits(){
        Test.startTest();
        createIntegrationFlag('true');
        Test.stopTest();
        
        OppTriggerHandler oppTriggerHandler = new OppTriggerHandler();
        Opportunity opp = [SELECT Name, CloseDate, StageName, AccountId, Sagitta_Type_of_Business__c FROM Opportunity WHERE Name = 'Closed as Won Benefits'];
        List<Opportunity> oppList = new List<Opportunity>();
        oppList.add(opp);
        
        TriggerHandlerStatus actual = oppTriggerHandler.handleTrigger(oppList, null, TriggerOperation.AFTER_UPDATE);        
        TriggerHandlerStatus expected = TriggerHandlerStatus.MESSAGES_SENT;
        System.assertEquals(expected, actual);
    }
    
    @isTest
    public static void HandleTrigger_ShouldReturnMESSAGE_SENT_WhenOppClosedAsWonLife(){
        Test.startTest();
        createIntegrationFlag('true');
        Test.stopTest();
        
        OppTriggerHandler oppTriggerHandler = new OppTriggerHandler();
        Opportunity opp = [SELECT Name, CloseDate, StageName, AccountId, Sagitta_Type_of_Business__c FROM Opportunity WHERE Name = 'Closed as Won Life'];
        List<Opportunity> oppList = new List<Opportunity>();
        oppList.add(opp);
        
        TriggerHandlerStatus actual = oppTriggerHandler.handleTrigger(oppList, null, TriggerOperation.AFTER_UPDATE);        
        TriggerHandlerStatus expected = TriggerHandlerStatus.MESSAGES_SENT;
        System.assertEquals(expected, actual);
    }
	
	@isTest
    public static void HandleTrigger_ShouldReturnMESSAGE_SENT_WhenOppClosedAsWonHealth(){
        Test.startTest();
        createIntegrationFlag('true');
        Test.stopTest();
        
        OppTriggerHandler oppTriggerHandler = new OppTriggerHandler();
        Opportunity opp = [SELECT Name, CloseDate, StageName, AccountId, Sagitta_Type_of_Business__c FROM Opportunity WHERE Name = 'Closed as Won Health'];
        List<Opportunity> oppList = new List<Opportunity>();
        oppList.add(opp);
        
        TriggerHandlerStatus actual = oppTriggerHandler.handleTrigger(oppList, null, TriggerOperation.AFTER_UPDATE);        
        TriggerHandlerStatus expected = TriggerHandlerStatus.MESSAGES_SENT;
        System.assertEquals(expected, actual);
    }
	
	@isTest
    public static void HandleTrigger_ShouldReturnMESSAGE_SENT_WhenOppClosedAsWonFinancial(){
        Test.startTest();
        createIntegrationFlag('true');
        Test.stopTest();
        
        OppTriggerHandler oppTriggerHandler = new OppTriggerHandler();
        Opportunity opp = [SELECT Name, CloseDate, StageName, AccountId, Sagitta_Type_of_Business__c FROM Opportunity WHERE Name = 'Closed as Won Financial'];
        List<Opportunity> oppList = new List<Opportunity>();
        oppList.add(opp);
        
        TriggerHandlerStatus actual = oppTriggerHandler.handleTrigger(oppList, null, TriggerOperation.AFTER_UPDATE);        
        TriggerHandlerStatus expected = TriggerHandlerStatus.MESSAGES_SENT;
        System.assertEquals(expected, actual);
    }

    @isTest
    public static void HandleTrigger_ShouldReturnNO_MESSAGE_SENT_WhenNoCustomMetadataBeforeUpdate(){
        OppTriggerHandler oppTriggerHandler = new OppTriggerHandler();
        Opportunity opp = [SELECT Name, CloseDate, StageName, AccountId, Sagitta_Division__c, Sagitta_Branch__c, 
                           Sagitta_Department__c, Sagitta_Group__c, Sagitta_Opportunity_Exec__c,
                           Sagitta_Opportunity_Rep__c, Sagitta_Bill_Method__c FROM Opportunity WHERE Name = 'BusinessUnits'];
        List<Opportunity> oppList = new List<Opportunity>();
        oppList.add(opp);
        
        TriggerHandlerStatus actual = oppTriggerHandler.handleTrigger(oppList, null, TriggerOperation.BEFORE_UPDATE);        
        TriggerHandlerStatus expected = TriggerHandlerStatus.NO_MESSAGES_SENT;
        System.assertEquals(expected, actual);
    }
    
    private static User createUser() {
        Profile p = [SELECT Id FROM Profile WHERE Name='Standard User']; 
        User user = new User(
            Alias = 'standt',
            Email='standarduser@testorg.com', 
            EmailEncodingKey='UTF-8',
            LastName='Testing',
            LanguageLocaleKey='en_US', 
            LocaleSidKey='en_US',
            ProfileId = p.Id, 
            TimeZoneSidKey='America/Los_Angeles',
            UserName='vert-test@vertafore.com'
        );
        return user;
    }
    
    private static Sagitta_Activity_Action_Type__c createActivityActionType() {
        Sagitta_Activity_Action_Type__c activityActionType = new Sagitta_Activity_Action_Type__c(
            Sagitta_Activity_Action_Id__c = '1234',
            Name = 'Acord'
        );
        return activityActionType;
    }
    
    private static Sagitta_Issuing_Carrier__c createIssuingCarrier() {
        Sagitta_Issuing_Carrier__c issuingCarrier = new Sagitta_Issuing_Carrier__c(
            Sagitta_Entity_ID__c= 'ID text', 
            Sagitta_Carrier_Type__c= 'N',
            Sagitta_IsHidden__c = false,
            Sagitta_Issuing_Carrier_Code__c= 'text',
            Name = 'Issuing Carrier'
        );
        return issuingCarrier;
    }
    
    private static Sagitta_Agency_Settings__c createAgencySettings(string integrationStage,
                                                                       Sagitta_Activity_Action_Type__c activityAction,
                                                                       Sagitta_Issuing_Carrier__c issuingCarrier,
                                                                       Sagitta_Employee__c employee) {
        User usr = [SELECT Id FROM User WHERE UserName = 'vert-test@vertafore.com'];
        Sagitta_Agency_Settings__c agencySettings = new Sagitta_Agency_Settings__c(
            Integration_Stage__c = integrationStage,
            Sagitta_Agency_Number__c = '18R1',
            Benefits_Policy_Status__c = 'A',
            Benefits_Policy_Type__c = 'S',
            Commercial_Policy_Status__c = 'A',
            Commercial_Policy_Type__c = 'S',
            Default_Activity_Action_Type__c = activityAction.Id,
            Default_Agency_Admin__c = employee.Id,
            Default_Bill_Method__c = 'A',
            Default_Issuing_Carrier__c = issuingCarrier.Id,
            Default_Policy_Number__c = '123456789',
            Financial_Policy_Status__c = 'A',
            Financial_Policy_Type__c = 'S',
            Health_Policy_Status__c = 'A',
            Health_Policy_Type__c = 'S',
            Integration_User_Id__c = '555555',
            Life_Policy_Status__c = 'A',
            Life_Policy_Type__c = 'S',
            Non_Property_and_Casualty_Policy_Status__c = 'A',
            Non_Property_and_Casualty_Policy_Type__c = 'S',
            Personal_Policy_Status__c = 'A',
            Personal_Policy_Type__c = 'S',
            Vertafore_Id__c = '54321'
        );
        return agencySettings;
    }
    
    private static void createIntegrationFlag(String flag) {
        Metadata__c intFlag = new Metadata__c();
        intFlag.Name = 'IntegrationFlag';
        intFlag.Value__c = flag;
        insert intFlag;
    }
    
    private static void createIsInitialSyncFlag(String flag) {
        Metadata__c isInitialSyncFlag = new Metadata__c();
        isInitialSyncFlag.Name = 'IsInitialSync';
        isInitialSyncFlag.Value__c = flag;
        insert isInitialSyncFlag;
    }
}