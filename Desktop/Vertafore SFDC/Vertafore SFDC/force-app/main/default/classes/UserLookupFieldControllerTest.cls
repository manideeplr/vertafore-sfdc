@isTest
public class UserLookupFieldControllerTest {
	
    @isTest
    public static void searchForUserTest() {
        User usr = TestDataFactoryUtil.createStandardUser();
        Test.startTest();
        List<String> userNames = UserLookupFieldController.searchForUser('Testing');
        Test.stopTest();
        system.assertEquals('Testing', userNames[0]);
    }
}