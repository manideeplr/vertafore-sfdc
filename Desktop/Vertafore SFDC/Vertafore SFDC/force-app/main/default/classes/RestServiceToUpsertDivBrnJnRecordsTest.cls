@isTest
public class RestServiceToUpsertDivBrnJnRecordsTest {

  	@isTest
	static void test1(){
        Sagitta_Division__c div = new Sagitta_Division__c();
        div.ItemCode__c = '1';
        div.Name = 'Test Div';
        insert div;
        
        Sagitta_Branch__c brn = new Sagitta_Branch__c();
        brn.ItemCode__c = '1';
        brn.Name = 'Test Branch';
        insert brn;
        
        List<RestServiceToUpsertDivBrnJnRecords.SagittaDiv> divsToInsert = new List<RestServiceToUpsertDivBrnJnRecords.SagittaDiv>();
		List<RestServiceToUpsertDivBrnJnRecords.SagittaBrn> brnsToInsert = new List<RestServiceToUpsertDivBrnJnRecords.SagittaBrn>();
        RestServiceToUpsertDivBrnJnRecords.SagittaBrn brnJn = new RestServiceToUpsertDivBrnJnRecords.SagittaBrn();
        brnJn.ItemCodeHash = '1';
        brnsToInsert.add(brnJn);
		RestServiceToUpsertDivBrnJnRecords.SagittaDiv divJn = new RestServiceToUpsertDivBrnJnRecords.SagittaDiv();
        divJn.ItemCodeHash = '1';
        divJn.Branches = brnsToInsert;
        divsToInsert.add(divJn);
        String myJSON = JSON.serialize(divsToInsert);
		

        RestRequest request = new RestRequest();
        request.requestUri ='https://test.salesforce.com/services/apexrest/UpsertDivBrnJnRecords';
        request.httpMethod = 'POST';
		request.requestBody = Blob.valueof(myJSON);
		
        RestContext.request = request;
		RestServiceToUpsertDivBrnJnRecords.ResponseHandler res = RestServiceToUpsertDivBrnJnRecords.doPost();
		
        List<DivBrJunction__c> divs = [SELECT Id FROM DivBrJunction__c];
        system.assertEquals(1, divs.size());		
	}
    
    @isTest
	static void test2(){
        Sagitta_Division__c div = new Sagitta_Division__c();
        div.ItemCode__c = '1';
        div.Name = 'Test Div';
        insert div;
        
        Sagitta_Branch__c brn = new Sagitta_Branch__c();
        brn.ItemCode__c = '1';
        brn.Name = 'Test Branch';
        insert brn;
        
        Sagitta_Branch__c brn2 = new Sagitta_Branch__c();
        brn2.ItemCode__c = '2';
        brn2.Name = 'Test Branch';
        insert brn2;
        
        DivBrJunction__c divBrn = new DivBrJunction__c();
        divBrn.Sagitta_Division__c = div.Id;
        divBrn.Sagitta_Branch__c = brn.Id;
        insert divBrn;
        
        List<RestServiceToUpsertDivBrnJnRecords.SagittaDiv> divsToInsert = new List<RestServiceToUpsertDivBrnJnRecords.SagittaDiv>();
		List<RestServiceToUpsertDivBrnJnRecords.SagittaBrn> brnsToInsert = new List<RestServiceToUpsertDivBrnJnRecords.SagittaBrn>();
        RestServiceToUpsertDivBrnJnRecords.SagittaBrn brnJn = new RestServiceToUpsertDivBrnJnRecords.SagittaBrn();
        brnJn.ItemCodeHash = '1';
        brnsToInsert.add(brnJn);
        RestServiceToUpsertDivBrnJnRecords.SagittaBrn brnJn2 = new RestServiceToUpsertDivBrnJnRecords.SagittaBrn();
        brnJn2.ItemCodeHash = '2';
        brnsToInsert.add(brnJn2);
		RestServiceToUpsertDivBrnJnRecords.SagittaDiv divJn = new RestServiceToUpsertDivBrnJnRecords.SagittaDiv();
        divJn.ItemCodeHash = '1';
        divJn.Branches = brnsToInsert;
        divsToInsert.add(divJn);
        String myJSON = JSON.serialize(divsToInsert);
		

        RestRequest request = new RestRequest();
        request.requestUri ='https://test.salesforce.com/services/apexrest/UpsertDivBrnJnRecords';
        request.httpMethod = 'POST';
		request.requestBody = Blob.valueof(myJSON);
		
        RestContext.request = request;
		RestServiceToUpsertDivBrnJnRecords.ResponseHandler res = RestServiceToUpsertDivBrnJnRecords.doPost();
		
        List<DivBrJunction__c> divs = [SELECT Id FROM DivBrJunction__c];
        system.assertEquals(2, divs.size());		
	}
    
    @isTest
    static void test3(){
        
		List<RestServiceToUpsertDivBrnJnRecords.SagittaBrn> brnsToInsert = new List<RestServiceToUpsertDivBrnJnRecords.SagittaBrn>();
        RestServiceToUpsertDivBrnJnRecords.SagittaBrn brnJn = new RestServiceToUpsertDivBrnJnRecords.SagittaBrn();
        brnJn.ItemCodeHash = '1';
        brnsToInsert.add(brnJn);
		RestServiceToUpsertDivBrnJnRecords.SagittaDiv divJn = new RestServiceToUpsertDivBrnJnRecords.SagittaDiv();
        divJn.ItemCodeHash = '1';
        divJn.Branches = brnsToInsert;
        String myJSON = JSON.serialize(divJn);
		

        RestRequest request = new RestRequest();
        request.requestUri ='https://test.salesforce.com/services/apexrest/UpsertDivBrnJnRecords';
        request.httpMethod = 'POST';
		request.requestBody = Blob.valueof(myJSON);
        
        RestResponse response = new RestResponse();
        response.statusCode = 400;
		
        RestContext.request = request;
        RestContext.response = response;
		RestServiceToUpsertDivBrnJnRecords.ResponseHandler res = RestServiceToUpsertDivBrnJnRecords.doPost();
		
        List<DivBrJunction__c> divs = [SELECT Id FROM DivBrJunction__c];
        system.assertEquals(0, divs.size());		
	}
}