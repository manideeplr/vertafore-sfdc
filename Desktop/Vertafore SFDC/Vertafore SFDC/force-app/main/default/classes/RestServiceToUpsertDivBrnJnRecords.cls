/*
    Author : Vikram
    Name : RestServiceToUpsertDivBrnJnRecords
    Date : 27th March, 2019
    Description: Rest service to upsert Division Branch Junction object records
    Modification History :
    *************************************************************
    Vesion               Author             Date                      Description
	1.1					 Vikram				18 June 2019			  Added logging functionality and few comments where necessary
*/

@RestResource(urlMapping='/UpsertDivBrnJnRecords/*')
global with sharing class RestServiceToUpsertDivBrnJnRecords{
    
    // class that represents SagittaDivision model
    global class SagittaDiv {        
        global String ItemCodeHash { get; set; }
        global List<SagittaBrn> Branches { get; set; }        
    }
    
    // class that represents SagittaBranch model
    global class SagittaBrn {
        global String ItemCodeHash { get; set; }
    }
    
    // class that represents response to caller
    global class ResponseHandler {
        public Boolean Success {get; set;}
        public String Status {get; set;}
        public String Message {get;set;}
    }
    
    // Deserialize the request body and insert the data
	// @param - No Param
	// @return - ResponseHandler
    @HttpPost
    global static ResponseHandler doPost() {
        ResponseHandler obj = new ResponseHandler();
        RestRequest req = RestContext.request;
        String failedRecordIds = '';
        try {
            // Delete all existing DivBr junctions before upserting new ones
            List<DivBrJunction__c> oldJunctions = [SELECT Id FROM DivBrJunction__c];
            delete oldJunctions;
            
            String postBody = req.requestBody.toString();
            // Deserialize the request body and converts into List<SagittaDiv>
            List<SagittaDiv> Divs = (List<SagittaDiv>)JSON.deserialize(postBody, List<SagittaDiv>.class);
            List<Sagitta_Division__c> divIds = new List<Sagitta_Division__c>();
            Set<String> divItemCodes = new Set<String>();
            Set<String> brnItemCodes = new Set<String>();
            Set<String> brnValuesSet = new Set<String>();
            Map<String, String> divIdItemCodesMap = new Map<String, String>();
            Map<String, String> brnIdItemCodesMap = new Map<String, String>();
            Map<String, String> divBrnJnMap = new Map<String, String>();
            List<DivBrJunction__c> newJnRecs = new List<DivBrJunction__c>();
            // For loop to iterate through all the incoming Division records
            for (SagittaDiv div : Divs) {
                divItemCodes.add(div.ItemCodeHash);
                // For loop to iterate through all the incoming Branch records related to Division record
                for (SagittaBrn brn : div.Branches) {
                    brnItemCodes.add(brn.ItemCodeHash);
                }
            }
            
            // Query the existing Division records and build a map to store the itemcode and id pairs
            for(Sagitta_Division__c div : [SELECT Id, ItemCode__c FROM Sagitta_Division__c 
                                             WHERE ItemCode__c IN : divItemCodes]) {
                divIdItemCodesMap.put(div.ItemCode__c, div.Id);
            }
            // Query the existing Branch records and build a map to store the itemcode and id pairs
            for(Sagitta_Branch__c brn : [SELECT Id, ItemCode__c FROM Sagitta_Branch__c 
                                             WHERE ItemCode__c IN : brnItemCodes]) {
                brnIdItemCodesMap.put(brn.ItemCode__c, brn.Id);
            }
            
            // Create junctions to upsert:
            // For each AMS Div
            for (SagittaDiv div : Divs) {
                // Get the ID of the corresponding SF Div
                String dId = divIdItemCodesMap.get(div.ItemCodeHash);
                // For each AMS Branch associated with the AMS Div
                for (SagittaBrn brn : div.Branches) {
                    // Get the ID of the SF Branch
                    String bId = brnIdItemCodesMap.get(brn.ItemCodeHash);
                    if(bId != null) {
                        // Create a new junction
                        DivBrJunction__c newJn = new DivBrJunction__c();
                        newJn.Sagitta_Division__c = dId;
                        newJn.Sagitta_Branch__c = bId;
                        newJnRecs.add(newJn);
                    }                       
                }
            }
               
            // To insert the list of records
            Database.UpsertResult[] results = Database.upsert(newJnRecs, false);
            for (Database.UpsertResult result : results) {
                if (!result.isSuccess()) {
                    Database.Error[] upsertErrors = result.getErrors();
                    for (Database.Error upsertError : upsertErrors) {
                        failedRecordIds = failedRecordIds + upsertError.getMessage() + upsertError.getFields();
                    }
                }
            }
            // Response handler initialization once the insert is successful/failure
            // Insert log into Application Log object for request and response json
            if (String.isNotBlank(failedRecordIds)) {
                obj.success = false;
                obj.status ='ERROR';
                obj.message = failedRecordIds;
                ApplicationLogger.logWebServiceMessage('RestServiceToUpsertDivBrnJnRecords', 'doPost', failedRecordIds, 
                                                   ApplicationLogger.ERROR, req.requestBody.toString(), JSON.serialize(obj));
            } else {
                obj.success = true;
                obj.status ='SUCCESS';
                obj.message = 'SUCCESS';
                ApplicationLogger.logWebServiceMessage('RestServiceToUpsertDivBrnJnRecords', 'doPost', 'SUCCESS', 
                                                   ApplicationLogger.INFO, req.requestBody.toString(), JSON.serialize(obj));
            }
            
            return obj;
        } catch(Exception ex) {
            RestContext.response.statusCode = 400;
            obj.success = false;
            obj.status='Error';
            obj.message=ex.getMessage();
            String message = 'Error: ' + ex.getMessage() + '; Stack Trace:' + ex.getStackTraceString();            
            // Insert log into Application Log object for request, response and error json
            ApplicationLogger.logWebServiceMessage('RestServiceToUpsertDivBrnJnRecords', 'doPost', message, 
                                                   ApplicationLogger.ERROR, req.requestBody.toString(), JSON.serialize(obj));
            return obj;
        }        
    }

}