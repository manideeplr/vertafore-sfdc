@isTest
public class AccountMergeBatchTest {

    @isTest
    public static void myTest() {
        createIntegrationFlag('true');
        createIsInitialSyncFlag('false');
        
        Sagitta_Employee__c rep = new Sagitta_Employee__c();
        rep.Sagitta_Employee_Code__c = '1234';
        rep.Sagitta_Entity_ID__c = '3214';
        rep.Sagitta_Is_Executive__c = false;
        rep.Sagitta_Is_Representative__c = true;
        rep.Sagitta_Employee_Status__c = 'Active';
        rep.Name = 'EmpRep';
        insert rep;
        
        Sagitta_Employee__c exe = new Sagitta_Employee__c();
        exe.Sagitta_Employee_Code__c = '2547';
        exe.Sagitta_Entity_ID__c = '2447';
        exe.Sagitta_Is_Executive__c = true;
        exe.Sagitta_Is_Representative__c = false;
        exe.Sagitta_Employee_Status__c = 'Active';
        exe.Name = 'Empexe';
        insert exe;
        
		Sagitta_Division__c SagittaDivision = new Sagitta_Division__c(
        	Name = 'Division One',
            ItemCode__c = '$$$',
            Sagitta_IsHidden__c	= false
        );
        insert SagittaDivision;
        
        Sagitta_Branch__c SagittaBranch = new Sagitta_Branch__c(
        	Name = 'Branch One',
            ItemCode__c = '***',
            Sagitta_IsHidden__c	= false
        );
        insert SagittaBranch;
        
        Sagitta_Department__c SagittaDepartment = new Sagitta_Department__c(
        	Name = 'Department One',
            ItemCode__c = '+++',
            Sagitta_IsHidden__c	= false
        );
        insert SagittaDepartment;
        
        Sagitta_Group__c SagittaGroup = new Sagitta_Group__c(
        	Name = 'Group One',
            ItemCode__c = '&&&',
            Sagitta_IsHidden__c	= false
        );
        insert SagittaGroup;
        
        DivBrJunction__c divBrJunc = new DivBrJunction__c(Sagitta_Division__c = SagittaDivision.Id, Sagitta_Branch__c = SagittaBranch.Id);
        BrDepJunction__c brDepJunc = new BrDepJunction__c(Sagitta_Branch__c = SagittaBranch.Id, Sagitta_Department__c = SagittaDepartment.Id);
        DepGrpJunction__c depGrpJunc = new DepGrpJunction__c(Sagitta_Department__c = SagittaDepartment.Id, Sagitta_Group__c = SagittaGroup.Id);
        insert divBrJunc;
        insert brDepJunc;
        insert depGrpJunc;
        
        Sagitta_Mapping__c mapping = new Sagitta_Mapping__c(
        	Name = 'MyMapping',
            Sagitta_Assigned_To__c = UserInfo.getUserId(),
            Sagitta_Division__c = SagittaDivision.Id,
            Sagitta_Branch__c = SagittaBranch.Id,
            Sagitta_Department__c = SagittaDepartment.Id,
            Sagitta_Group__c = SagittaGroup.Id,
            Sagitta_Executive__c = exe.Id,
            Sagitta_Representative__c = rep.Id
        );
        insert mapping;
        
        Account acc = new Account();
        acc.Sagitta_Employee_Account_Rep__c = rep.Id;
        acc.Sagitta_Employee_Account_Exec__c = exe.Id;
        acc.Name = 'Test1';
        acc.Phone = '123456';
        acc.Industry = 'Agriculture';
        acc.Website = 'www.vertafore.com';
        acc.BillingCity = 'Denver';
        acc.BillingStreet = 'My Street';
        acc.BillingCountry = 'USA';
        acc.BillingPostalCode = '20051';
        acc.ShippingCity = 'Denver';
        acc.ShippingStreet = 'My Street';
        acc.ShippingCountry = 'USA';
        acc.ShippingPostalCode = '20051';
        acc.NumberOfEmployees = 15;
        acc.Sagitta_First_Name__c = 'FirstName';
        acc.Sagitta_Last_Name__c = 'LastName';
        acc.Sagitta_Middle_Name__c = 'MiddleName';
        acc.Sagitta_Client_Type__c = 'C';
        acc.Sagitta_Company_Email_Address__c = 'myemail@vert.com';
        acc.Sagitta_Customer_ID__c = 'CustID1234';
        acc.Sagitta_Customer_Number__c = 'CustNo1234';
        acc.Sagitta_Customer_Priority__c = 'High';
        acc.Sagitta_DBA__c = 'DBA';
        acc.Sagitta_Type_of_Business__c = '1';
        acc.Sagitta_Division__c = SagittaDivision.Id;
        acc.Sagitta_Branch__c = SagittaBranch.Id;
        acc.Sagitta_Department__c = SagittaDepartment.Id;
        acc.Sagitta_Group__c = SagittaGroup.Id;
        insert acc;
        
        Account acc2 = new Account();
        acc2.Sagitta_Employee_Account_Rep__c = rep.Id;
        acc2.Sagitta_Employee_Account_Exec__c = exe.Id;
        acc2.Name = 'Test2';
        acc2.Phone = '123456';
        acc2.Sagitta_First_Name__c = 'FirstName';
        acc2.Sagitta_Last_Name__c = 'LastName';
        acc2.BillingCity = 'Denver';
        acc2.BillingStreet = 'My Street';
        acc2.BillingCountry = 'USA';
        acc2.BillingPostalCode = '20051';
        acc2.ShippingCity = 'Denver';
        acc2.ShippingStreet = 'My Street';
        acc2.ShippingCountry = 'USA';
        acc2.ShippingPostalCode = '20051';
		acc2.Sagitta_Division__c = SagittaDivision.Id;
        acc2.Sagitta_Branch__c = SagittaBranch.Id;
        acc2.Sagitta_Department__c = SagittaDepartment.Id;
        acc2.Sagitta_Group__c = SagittaGroup.Id;
        insert acc2;
        
        Sagitta_Activity_Action_Type__c activityActionType = new Sagitta_Activity_Action_Type__c(
        	Sagitta_Activity_Action_Id__c = '26544'
        );

        Sagitta_Issuing_Carrier__c issuingCarrier = new Sagitta_Issuing_Carrier__c(
            Sagitta_Entity_ID__c= 'ID text', 
            Sagitta_Carrier_Type__c= 'N', 
            Sagitta_Issuing_Carrier_Code__c= 'text',
        	Sagitta_IsHidden__c = false
        );
        insert activityActionType;
        insert issuingCarrier;
        
        Sagitta_Agency_Settings__c agencySettings1 = new Sagitta_Agency_Settings__c(
            Sagitta_Agency_Number__c='text', 
            Default_Activity_Action_Type__c= [SELECT Id FROM Sagitta_Activity_Action_Type__c LIMIT 1].Id,
            Default_Bill_Method__c= 'A', 
            Default_Issuing_Carrier__c= [SELECT Id FROM Sagitta_Issuing_Carrier__c LIMIT 1].Id, 
            Default_Policy_Number__c='text', 
            Integration_User_Id__c='text', 
            Vertafore_Id__c='text',
            Default_Agency_Admin__c = [SELECT Id FROM Sagitta_Employee__c WHERE Name =: exe.Name].Id
        );
        insert agencySettings1;
        
        // cannot insert with same name because of duplicate error
        Account account = [SELECT Id, Name FROM Account WHERE Name = 'Test2' LIMIT 1];
        account.Name = 'Test1';
        update account;
        
        Test.startTest();
        Database.executeBatch(new AccountMergeBatch(), 10);
        Test.stopTest();
        
        List<Account> accts = [SELECT Id FROM Account];
        system.assertEquals(1, accts.size());
    }

    private static void createIntegrationFlag(String flag) {
        Metadata__c intFlag = new Metadata__c();
        intFlag.Name = 'IntegrationFlag';
        intFlag.Value__c = flag;
        insert intFlag;
    }
    
    private static void createIsInitialSyncFlag(String flag) {
        Metadata__c isInitialSyncFlag = new Metadata__c();
        isInitialSyncFlag.Name = 'IsInitialSync';
        isInitialSyncFlag.Value__c = flag;
        insert isInitialSyncFlag;
    }

}