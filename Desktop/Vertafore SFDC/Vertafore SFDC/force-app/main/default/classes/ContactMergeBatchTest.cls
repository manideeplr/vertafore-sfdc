@isTest
public class ContactMergeBatchTest {

    @isTest
    public static void myTest() {
        createIntegrationFlag('true');
        createIsInitialSyncFlag('false');
        
        Contact con = new Contact();
        con.LastName = 'LastName';
        con.Phone = '1234';
        con.Email = 'mumail@vert.com';
        con.MailingPostalCode = '50021';
        insert con;
        
        Contact con2 = new Contact();
        con2.LastName = 'LastName';
        con2.Phone = '1234';
        con2.Email = 'mumail2@vert.com';
        con2.MailingPostalCode = '50021';
        con2.Sagitta_Customer_Contact_ID__c = 'ContactID321';
        con2.Sagitta_Customer_ID__c = 'CustomerID321';
        insert con2;
        
        // cannot insert with same LastName because of duplicate error
        Contact contact = [SELECT Id, Email FROM Contact WHERE Email = 'mumail2@vert.com' LIMIT 1];
        contact.Email = 'mumail@vert.com';
        update contact;
        
        Test.startTest();
        Database.executeBatch(new ContactMergeBatch(), 10);
        Test.stopTest();
        
        List<Contact> conts = [SELECT Id FROM Contact];
        system.assertEquals(1, conts.size());
    }

    private static void createIntegrationFlag(String flag) {
        Metadata__c intFlag = new Metadata__c();
        intFlag.Name = 'IntegrationFlag';
        intFlag.Value__c = flag;
        insert intFlag;
    }
    
    private static void createIsInitialSyncFlag(String flag) {
        Metadata__c isInitialSyncFlag = new Metadata__c();
        isInitialSyncFlag.Name = 'IsInitialSync';
        isInitialSyncFlag.Value__c = flag;
        insert isInitialSyncFlag;
    }

}