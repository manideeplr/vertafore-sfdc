/*
    Author : Vikram
    Name : ContactMergeBatch
    Date : Prior to 18 June 2019
    Description: Used to merge Contacts in a manageable batch size
    Modification History :
    *************************************************************
    Version             Author          Date                        Description
    1                   Brad            18 June 2019                Refactoring, logging
	1.1 				Vikram			10 July 2019				Code changes to make cont with ContCustId as masterContact
	1.2                 Vikram          19 July 2019				Code changes to fix continues looping of batch class
	1.3					Brad			22 July 2019				Merge fields from Child to Master if necessary
	1.4                 Vikram          24 July 2019                Removed code changes version 1.2
	1.5 				Vikram			31 July 2019 				code changes to fix limit on findDuplicates method and SyncFlag check
*/
global class ContactMergeBatch implements Database.Batchable<SObject> {
    
    global Set<Id> mergedIds = new Set<id>();
    global Set<String> mergedRecordIds = new Set<String>();
    
    global Database.QueryLocator start(Database.BatchableContext context) {
        return Database.getQueryLocator('SELECT Id FROM Contact ORDER BY Name');
    }
    
    // Summary:
    // Checks for duplicate Contact records, collates all data between them and merges the two Contacts in Salesforce
    //
    // @param context - 
    // @param scope - 
    global void execute(Database.BatchableContext context, List<Contact> scope) {
        try {
            // Set IntegrationFlag to false during merge to stop multiple Tasks and Onject updates back to Sagitta
            Metadata__c intflag = Metadata__c.getInstance('IntegrationFlag');
            intflag.Value__c = 'false';
            update intflag;
            Metadata__c syncFlag = Metadata__c.getInstance('IsInitialSync');
            syncFlag.Value__c = 'true';
            update syncFlag;
            
            DescribeSObjectResult describeResult = Contact.getSObjectType().getDescribe();
            List<String> fieldNames = new List<String>(describeResult.fields.getMap().keySet());
            String query ='SELECT ' + String.join( fieldNames, ',' ) + ' FROM ' + describeResult.getName();
            
            Map<String, Contact> conMap = new Map<String, Contact>();
            for(Sobject sobjCon : Database.query(query)) {
                Contact con = (Contact)sobjCon;
                conMap.put(con.Id, con);
            }
            //v1.5 start
            List<Contact> contListMain = scope;
            List<List<Contact>> wrapList = new List<List<Contact>>();
            List<Contact> tempList;
            for (integer i=0; i < (contListMain.size()/50)+1; i++){
                tempList = new List<Contact>();
                for (integer j=(i*50); (j<(i*50)+50) && j<contListMain.size() ; j++) {
                    tempList.add(contListMain[j]);
                }
                wrapList.add(tempList);
            }
            Map<String, List<Contact>> conKeyMap = new Map<String, List<Contact>>();
            for (integer i=0;i<wrapList.size();i++) {
                if(wrapList[i].size() > 0) {
                    Datacloud.FindDuplicatesResult[] results = Datacloud.FindDuplicates.findDuplicates(wrapList[i]);
                    Set<Id> conIds = new Set<Id>();                
                    for (Datacloud.FindDuplicatesResult findDupeResult : results) {
                        for (Datacloud.DuplicateResult dupeResult : findDupeResult.getDuplicateResults()) {
                            for (Datacloud.MatchResult matchResult : dupeResult.getMatchResults()) {
                                for (Datacloud.MatchRecord matchRecord : matchResult.getMatchRecords()) {
                                    List<String> keys = new List<String>();
                                    List<Datacloud.FieldDiff> fieldDiffs = matchRecord.getFieldDiffs();
                                    for (Datacloud.FieldDiff diff : fieldDiffs) {
                                        if(diff.getDifference() == 'Same') {
                                            keys.add(diff.getName());
                                        }
                                    }
                                    Contact con = (Contact)matchRecord.getRecord();
                                    Contact conToCheck = conMap.get(con.Id);
                                    if (!conIds.contains(con.Id)) {
                                        conIds.add(con.Id);
                                        String key = '';
                                        for (String keyname : keys){
                                            if(keyname.equals('Account'))
                                                keyname = 'AccountId';
                                            key = key + conToCheck.get(keyname);
                                        }
                                        if (conKeyMap.containsKey(key)) {
                                            conKeyMap.get(key).add(conToCheck);
                                        } else {
                                            List<Contact> conList = new List<Contact>();
                                            conList.add(conToCheck);
                                            conKeyMap.put(key, conList);
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
            
            for(List<Contact> conList : conKeyMap.Values()) {
                List<Contact> childConts = new List<Contact>();
                Contact masterCont;
                for(Contact conToMerge : conList) {
                    if(String.isNotBlank(conToMerge.Sagitta_Customer_Contact_ID__c)){
                    	masterCont = conToMerge;
                    } else {
                        childConts.add(conToMerge);
                    }
                }
                if(masterCont != null) {
                    for (Contact childCont : childConts) {
                        if (!mergedRecordIds.contains(childCont.Id) && masterCont.Id != childCont.Id) {
                            mapContactFields(masterCont, childCont);
                            Database.MergeResult res = Database.Merge(new Contact(Id=masterCont.Id), new Contact(Id=childCont.Id));   
                            if (res.isSuccess()) {
                                mergedIds.add(res.getId());
                                List<String> deletedRecordIds = res.getMergedRecordIds();
                                mergedRecordIds.add(deletedRecordIds[0]);
                            }
                        }
                    }
                } else {
                    masterCont = childConts[0];
                    for (integer i=1; i<childConts.size(); i++) {
                        Contact childCont = childConts[i];
                        if (!mergedRecordIds.contains(childCont.Id) && masterCont.Id != childCont.Id) {
                            mapContactFields(masterCont, childCont);
                            Database.MergeResult res = Database.Merge(new Contact(Id=masterCont.Id), new Contact(Id=childConts[i].Id));
                            if (res.isSuccess()) {
                                mergedIds.add(res.getId());
                                List<String> deletedRecordIds = res.getMergedRecordIds();
                                mergedRecordIds.add(deletedRecordIds[0]);
                            }
                        }
                    }
                }
            }
        } Catch (Exception ex) {
            String message = 'Custom Message: Error merging contacts. ;' + 
                'Error: ' + ex.getMessage() + '; Stack Trace:' + ex.getStackTraceString();
            ApplicationLogger.logMessage('ContactMergeBatch', 'execute', message, ApplicationLogger.ERROR);
        }
        finally{
            // Reset IntegrationFlag back to true
            Metadata__c intflag = Metadata__c.getInstance('IntegrationFlag');
            intflag.Value__c = 'true';
            update intflag;
            Metadata__c syncFlag = Metadata__c.getInstance('IsInitialSync');
            syncFlag.Value__c = 'false';
            update syncFlag;
        }
    }
    
    global void finish(Database.BatchableContext context) {
        // Call the batch to udpate merged contacts
        // TODO Future feature may require this call to be overridden
    	Database.executeBatch(new MergedContactsSyncBatch(mergedIds));
    }

    private void mapContactFields(Contact master, Contact child){
        if (master.AssistantName == null && child.AssistantName != null) {
            master.AssistantName = child.AssistantName;
        }
        if (master.AssistantPhone == null && child.AssistantPhone != null) {
            master.AssistantPhone = child.AssistantPhone;
        }
        if (master.Birthdate == null && child.Birthdate != null) {
            master.Birthdate = child.Birthdate;
        }
        /*
        if (master.CleanStatus == null && child.CleanStatus != null) {
            master.CleanStatus = child.CleanStatus;
        }
        */
        if (master.Jigsaw == null && child.Jigsaw != null) {
            master.Jigsaw = child.Jigsaw;
        }
        if (master.Department == null && child.Department != null) {
            master.Department = child.Department;
        }
        if (master.Description == null && child.Description != null) {
            master.Description = child.Description;
        }
        if (master.DoNotCall == null && child.DoNotCall != null) {
            master.DoNotCall = child.DoNotCall;
        }
        if (master.Email == null && child.Email != null) {
            master.Email = child.Email;
        }
        if (master.HasOptedOutOfEmail == null && child.HasOptedOutOfEmail != null) {
            master.HasOptedOutOfEmail = child.HasOptedOutOfEmail;
        }
        if (master.Fax == null && child.Fax != null) {
            master.Fax = child.Fax;
        }
        if (master.HasOptedOutOfFax == null && child.HasOptedOutOfFax != null) {
            master.HasOptedOutOfFax = child.HasOptedOutOfFax;
        }
        if (master.HomePhone == null && child.HomePhone != null) {
            master.HomePhone = child.HomePhone;
        }
        if (master.LeadSource == null && child.LeadSource != null) {
            master.LeadSource = child.LeadSource;
        }
        if (master.MailingStreet == null && child.MailingStreet != null) {
            master.MailingStreet = child.MailingStreet;
        }
        if (master.MailingCity == null && child.MailingCity != null) {
            master.MailingCity = child.MailingCity;
        }
        if (master.MailingState == null && child.MailingState != null) {
            master.MailingState = child.MailingState;
        }
        if (master.MailingPostalCode == null && child.MailingPostalCode != null) {
            master.MailingPostalCode = child.MailingPostalCode;
        }
        if (master.MobilePhone == null && child.MobilePhone != null) {
            master.MobilePhone = child.MobilePhone;
        }
        if (master.OtherStreet == null && child.OtherStreet != null) {
            master.OtherStreet = child.OtherStreet;
        }
        if (master.OtherCity == null && child.OtherCity != null) {
            master.OtherCity = child.OtherCity;
        }
        if (master.OtherState == null && child.OtherState != null) {
            master.OtherState = child.OtherState;
        }
        if (master.OtherPostalCode == null && child.OtherPostalCode != null) {
            master.OtherPostalCode = child.OtherPostalCode;
        }
        if (master.OtherPhone == null && child.OtherPhone != null) {
            master.OtherPhone = child.OtherPhone;
        }
        if (master.Phone == null && child.Phone != null) {
            master.Phone = child.Phone;
        }
        if (master.ReportsToId == null && child.ReportsToId != null) {
            master.ReportsToId = child.ReportsToId;
        }
        if (master.Title == null && child.Title != null) {
            master.Title = child.Title;
        }
        
        update master;
    }
}