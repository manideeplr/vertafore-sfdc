/*
    Author : Vikram
    Name : Job
    Date : 12 June 2019
    Description: Application logging framework.
    Modification History :
    *************************************************************
    Version               Author             Date                      Description
	1					  Vikram			 12 June 2019     		   Original
*/
public class ApplicationLogger {
    
    public static String INFO = 'INFO';
    public static String DEBUG = 'DEBUG';
    public static String WARNING = 'WARNING';
    public static String ERROR = 'ERROR';
    
    // Summary:
    // Creates Application Logs and stores them in DB
    // 
    // Details:
    // @param String className - Name of class where log is taking place
    // @param String methodName - Name of method where log is taking place
    // @param String message - Log message
    // @param String logType - Type of log
    // @return N/A
    public static void logMessage(String className, String methodName, String message, String logType){
        if (Label.Enable_logging == 'TRUE') {
            Application_Log__c newLogMessage = new Application_Log__c (
                Class__c = className,
                Method__c = methodName,
                Message__c = message,
                Log_Type__c = logType);
            try {
                Database.insert(newLogMessage);
            }
            catch (Exception ex) {
                System.debug(
                    'Failed to INSERT the Application Log record. ' +
                    'Error: ' + ex.getMessage()
                );
            }
        }
        /*
        else {
            String errorTemplate = 'Error occured at class: {0} method: {1} and the error is: {2}';
            List<Object> parameters = new List<Object> {className, methodName, message};
            String completeErrorMessage = String.format(errorTemplate, parameters);
            System.debug(completeErrorMessage);
        }
        */
    }
    
    // Summary:
    // Creates Application Logs for web service calls and stores them in DB
    // 
    // Details:
    // @param String className - Name of class where log is taking place
    // @param String methodName - Name of method where log is taking place
    // @param String message - Log message
    // @param String logType - Type of log
    // @param String reqMessage - Request message
    // @param String respMessage - Response message
    // @return N/A
    public static void logWebServiceMessage(String className, String methodName, String message, String logType, String reqMessage, String respMessage){
        if (Label.Enable_logging == 'TRUE') {
            Application_Log__c newLogMessage = new Application_Log__c(
                Class__c = className,
                Method__c = methodName,
                Message__c = message,
                Log_Type__c = logType,
            	Request_Message__c = reqMessage,
            	Response_Message__c = respMessage);
            try {
                Database.insert(newLogMessage);
            }
            catch(Exception ex) {
                System.debug(
                    'Failed to INSERT the Application Log record. ' +
                    'Error: ' + ex.getMessage()
                );
            }
        }
        /*
        else {
            String errorTemplate = 'Error occured at class: {0} method: {1} and the error is: {2}';
            List<Object> parameters = new List<Object> {className, methodName, message};
            String completeErrorMessage = String.format(errorTemplate, parameters);
            System.debug(completeErrorMessage);
        }
        */
    }
}