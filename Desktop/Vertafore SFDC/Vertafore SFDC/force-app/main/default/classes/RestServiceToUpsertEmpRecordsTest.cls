@isTest
public class RestServiceToUpsertEmpRecordsTest {
    
    @isTest
    static void test1(){
        List<RestServiceToUpsertEmpRecords.SagittaEmp> itmToInsert = new List<RestServiceToUpsertEmpRecords.SagittaEmp>();
        RestServiceToUpsertEmpRecords.SagittaEmp itm = new RestServiceToUpsertEmpRecords.SagittaEmp();
        itm.EmployeeCode = '1';
        itm.EmployeeCodeHash = '033041071';
        itm.FirstName  = 'First';
        itm.LastName = 'Last';
        itm.Executive = true;
        itm.Representative = true;
        itm.EntityId = '162';
        itm.EntityName = 'SagittaBranchEntity';
        itm.EmployeeStatus = 'A';
        itmToInsert.add(itm);
        String myJSON = JSON.serialize(itmToInsert);
        
        
        RestRequest request = new RestRequest();
        request.requestUri ='https://test.salesforce.com/services/apexrest/UpsertBranchRecords';
        request.httpMethod = 'POST';
        request.requestBody = Blob.valueof(myJSON);
        
        RestContext.request = request;
        RestServiceToUpsertEmpRecords.ResponseHandler res = RestServiceToUpsertEmpRecords.doPost();
        
        List<Sagitta_Employee__c> emps = [SELECT Id FROM Sagitta_Employee__c];
        system.assertEquals(1, emps.size());    
    }
    
    @isTest
    static void test2(){
        RestServiceToUpsertEmpRecords.SagittaEmp itm = new RestServiceToUpsertEmpRecords.SagittaEmp();
        itm.EmployeeCode = '1';
        itm.EmployeeCodeHash = '033041071';
        itm.FirstName  = 'First';
        itm.MiddleName = 'Middle';
        itm.LastName = 'Last';
        itm.Executive = true;
        itm.Representative = true;
        itm.EntityId = '162';
        itm.EntityName = 'SagittaEmployeeEntity';
        itm.EmployeeStatus = 'I';
        String myJSON = JSON.serialize(itm);
        
        
        RestRequest request = new RestRequest();
        request.requestUri ='https://test.salesforce.com/services/apexrest/UpsertBranchRecords';
        request.httpMethod = 'POST';
        request.requestBody = Blob.valueof(myJSON);
        
        RestResponse response = new RestResponse();
        response.statusCode = 400;
        
        RestContext.request = request;
        RestContext.response = response;
        RestServiceToUpsertEmpRecords.ResponseHandler res = RestServiceToUpsertEmpRecords.doPost();
        
        List<Sagitta_Employee__c> divs = [SELECT Id FROM Sagitta_Employee__c];
        system.assertEquals(0, divs.size());    
    }
}