/*
Author : 
    Name : LineOfBusinessUtil
    Date : 
    Description: Utility class used to screen each Line of Business for viewing or updating
                 Gets a list of ALL LOBs from Sagitta
                 Validates LOB relationship to Type os Business
                 Updates SF Junction relationships
    Modification History :
    *************************************************************
    Version             Author          Date                        Description
     
*/
public class LineOfBusinessUtil {
    @AuraEnabled
    public static List<Sagitta_Line_of_Business__c> getAllLobs(Id oppId) {
        try {
            List<Opportunity> opptys = [SELECT Id, Sagitta_Type_of_Business__c FROM Opportunity WHERE Id =: oppId];
            List<Sagitta_Line_of_Business__c> lobs = new List<Sagitta_Line_of_Business__c>();
            if(!opptys.isEmpty()) {
                for(Sagitta_Line_of_Business__c lob : [SELECT Id, Name 
                                                        FROM Sagitta_Line_of_Business__c 
                                                        WHERE Sagitta_Type_of_Business__c =: opptys[0].Sagitta_Type_of_Business__c AND Sagitta_IsHidden__c = false ORDER BY Sagitta_Line_of_Business__c.Name]) {
                    lobs.add(lob);
                }
            }
            return lobs;
        } catch (Exception ex) {
            String message = 'Error getting LOB List; ' +
                'Error: ' + ex.getMessage() + '; Stack Trace: ' + ex.getStackTraceString();
          /*  ApplicationLogger.logMessage('LineOfBusinessUtil', 'getAllLobs', message, ApplicationLogger.ERROR);
            throw new AuraHandledException('Error getting LOB List: ' + ex.getMessage());*/
        } 
        return null;
    }
    
    @AuraEnabled
    public static List<String> getAllLobFromJn(Id oppId) {
        try {
            List<Opportunity> opptys = [SELECT Id, Sagitta_Type_of_Business__c FROM Opportunity WHERE Id =: oppId];
            List<String> lobs = new List<String>();
            if(!opptys.isEmpty()) {
                for(Sagitta_Opportunity_LOB_Junction__c lob : [SELECT Sagitta_LOB__r.Id 
                                                        FROM Sagitta_Opportunity_LOB_Junction__c 
                                                        WHERE Sagitta_Opportunity__c =: opptys[0].Id]) {
                    lobs.add(lob.Sagitta_LOB__r.id);
                }
            }
            return lobs;
        } catch (Exception ex) {
            String message = 'Error getting LOBs from junction; ' +
                'Error: ' + ex.getMessage() + '; Stack Trace: ' + ex.getStackTraceString();
            /*ApplicationLogger.logMessage('LineOfBusinessUtil', 'getAllLobFromJn', message, ApplicationLogger.ERROR);
            throw new AuraHandledException('Error getting LOBs from junction: ' + ex.getMessage());*/
        }
        return null;
    }
    
    @AuraEnabled
    public static void updateLOBs(Id oppId, List<Id> lobsToAdd, List<Id> lobsToDelete) {
        try {
            List<Sagitta_Opportunity_LOB_Junction__c> lobJnToAdd = new List<Sagitta_Opportunity_LOB_Junction__c>();
            List<Sagitta_Opportunity_LOB_Junction__c> lobJnToDelete = new List<Sagitta_Opportunity_LOB_Junction__c>();
            for(Id addId : lobsToAdd) {
                Sagitta_Opportunity_LOB_Junction__c lobJn = new Sagitta_Opportunity_LOB_Junction__c(Sagitta_Opportunity__c = oppId, Sagitta_LOB__c = addId);
                lobJnToAdd.add(lobJn);
            }
            insert lobJnToAdd;
            
            lobJnToDelete = [SELECT Id FROM Sagitta_Opportunity_LOB_Junction__c WHERE Sagitta_LOB__c IN :lobsToDelete];
            delete lobJnToDelete;
        } catch (Exception ex) {
            String message = 'Error updating LOBs; ' +
                'Error: ' + ex.getMessage() + '; Stack Trace: ' + ex.getStackTraceString();
            /*ApplicationLogger.logMessage('LineOfBusinessUtil', 'updateLOBs', message, ApplicationLogger.ERROR);
            throw new AuraHandledException('Error updating LOBs: ' + ex.getMessage());*/
        }
    }
}