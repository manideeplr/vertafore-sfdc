@isTest
public class RestServiceToUpsertPolicyRecordsTest {
	@testSetup 
    private static void TestSetup() {
        createIntegrationFlag('true');
        createIsInitialSyncFlag('false');
        
		Sagitta_Employee__c exec = new Sagitta_Employee__c(
        	Name = 'Jim Bond',
            Sagitta_Employee_Code__c = '033033052',
            Sagitta_Is_Executive__c = true,
            Sagitta_Is_Representative__c = false,
            Sagitta_Entity_ID__c = '54321',
            Sagitta_Employee_Status__c = 'Active'
        );
        insert exec;
        
        Sagitta_Employee__c rep = new Sagitta_Employee__c(
        	Name = 'Nichelle Foster',
            Sagitta_Employee_Code__c = '033033050',
            Sagitta_Is_Executive__c = false,
            Sagitta_Is_Representative__c = true,
            Sagitta_Entity_ID__c = '12345',
            Sagitta_Employee_Status__c = 'Active'
        );
        insert rep;
        
		Sagitta_Division__c SagittaDivision = new Sagitta_Division__c(
        	Name = 'Division One',
            ItemCode__c = '036036036'
        );
        insert SagittaDivision;
        
        Sagitta_Branch__c SagittaBranch = new Sagitta_Branch__c(
        	Name = 'Branch One',
            ItemCode__c = '042042042'
        );
        insert SagittaBranch;
        
        Sagitta_Department__c SagittaDepartment = new Sagitta_Department__c(
        	Name = 'Department One',
            ItemCode__c = '043043043'
        );
        insert SagittaDepartment;
        
        Sagitta_Group__c SagittaGroup = new Sagitta_Group__c(
        	Name = 'Group One',
            ItemCode__c = '038038038'
        );
        insert SagittaGroup;

        DivBrJunction__c divBrJunc = new DivBrJunction__c(Sagitta_Division__c = SagittaDivision.Id, Sagitta_Branch__c = SagittaBranch.Id);
        BrDepJunction__c brDepJunc = new BrDepJunction__c(Sagitta_Branch__c = SagittaBranch.Id, Sagitta_Department__c = SagittaDepartment.Id);
        DepGrpJunction__c depGrpJunc = new DepGrpJunction__c(Sagitta_Department__c = SagittaDepartment.Id, Sagitta_Group__c = SagittaGroup.Id);
        insert divBrJunc;
        insert brDepJunc;
        insert depGrpJunc;
        
        Sagitta_Mapping__c mapping = new Sagitta_Mapping__c(
        	Name = 'MyMapping',
            Sagitta_Assigned_To__c = UserInfo.getUserId(),
            Sagitta_Division__c = SagittaDivision.Id,
            Sagitta_Branch__c = SagittaBranch.Id,
            Sagitta_Department__c = SagittaDepartment.Id,
            Sagitta_Group__c = SagittaGroup.Id,
            Sagitta_Executive__c = exec.Id,
            Sagitta_Representative__c = rep.Id
        );
        insert mapping;
        
        Account account = new Account(
            Name = 'Test Account',
        	Sagitta_Customer_ID__c = '16721680-c7bb-4f17-9353-00003028dffe',
            Sagitta_Employee_Account_Exec__c = exec.Id,
            Sagitta_Employee_Account_Rep__c = rep.Id,
            Sagitta_Account_Status__c = 'Active'
        );
        insert account;
        
        Sagitta_Line_of_Business__c lob = new Sagitta_Line_of_Business__c(
        	Name = 'Auto',
            Sagitta_LOB_Code__c = '037037037',
            Sagitta_Type_of_Business__c = '2',
            Sagitta_Entity_ID__c = '1234-1234-1234'
        );
        insert lob;
        
        Sagitta_Business_Entity__c businessEntity = new Sagitta_Business_Entity__c(
        	Name = 'Association',
            Sagitta_Business_Entity_Id__c = '1111'
        );
        insert businessEntity;
        
        Sagitta_Issuing_Carrier__c issuingCarrier = new Sagitta_Issuing_Carrier__c(
        	Name = 'Parent Test Co',
            Sagitta_Carrier_Type__c = 'mytype',
            Sagitta_Issuing_Carrier_Code__c = '033056044',
            Sagitta_Entity_ID__c = '1234'
        );
        insert issuingCarrier;
    }
    
    @isTest
    private static void doPost_ShouldUpsertPolicies_WhenDeliveredInJsonList(){
        List<RestServiceToUpsertPolicyRecords.SagittaPolicy> policiesToInsert = new List<RestServiceToUpsertPolicyRecords.SagittaPolicy>();
        RestServiceToUpsertPolicyRecords.SagittaPolicy policy = new RestServiceToUpsertPolicyRecords.SagittaPolicy();
        policy.BillMethod = 'P';
        policy.CsrCode = '033033050';
        policy.CustomerId = '16721680-c7bb-4f17-9353-00003028dffe';
        policy.Description = 'This place is awesome - said no one ever';
        policy.EffectiveDate = '2019-09-28T00:00:00Z';
        policy.ExecCode = '033033052';
        policy.ExpiryDate = '2035-02-28T00:00:00Z';
        policy.FullTermPremium = 0;
        policy.GLBranchCode = '042042042';
        policy.GLDepartmentCode = '043043043';
        policy.GLDivisionCode = '036036036';
        policy.GLGroupCode = '038038038';
        policy.LineOfBusinessIDs = '';
        policy.PolicyId = 'daa0634f-f902-478f-8b75-c851eaf878ce';
        policy.PolicyNumber = 'POL N0659 VALP';
		policy.RenewalReportFlag = 'A';
        policy.TypeOfBusiness = '2';
        policy.WritingCompanyCode = '033056044';		
        
        policiesToInsert.add(policy);
        String myJSON = JSON.serialize(policiesToInsert);
        
        RestRequest request = new RestRequest();
        request.requestUri ='https://test.salesforce.com/services/apexrest/SIV/UpsertPolicyRecords';
        request.httpMethod = 'POST';
        request.requestBody = Blob.valueof(myJSON);
        RestResponse response = new RestResponse();
        
        RestContext.request = request;
        RestContext.response = response;
        RestServiceToUpsertPolicyRecords.ResponseHandler res = RestServiceToUpsertPolicyRecords.doPost();
        
        List<Sagitta_Policy__c> policies = [SELECT Id, Sagitta_Policy_ID__c FROM Sagitta_Policy__c];
        system.assertEquals(1, policies.size()); 
    }
    
    @isTest
    private static void doPost_ShouldUpsertTwoPolicies_WhenThereAreTwoPolicies(){
        List<RestServiceToUpsertPolicyRecords.SagittaPolicy> policiesToInsert = new List<RestServiceToUpsertPolicyRecords.SagittaPolicy>();
        RestServiceToUpsertPolicyRecords.SagittaPolicy policyOne = new RestServiceToUpsertPolicyRecords.SagittaPolicy();
        policyOne.BillMethod = 'P';
        policyOne.CsrCode = '033033050';
        policyOne.CustomerId = '16721680-c7bb-4f17-9353-00003028dffe';
        policyOne.Description = 'This place is awesome - said no one ever';
        policyOne.EffectiveDate = '2019-09-28T00:00:00Z';
        policyOne.ExecCode = '033033052';
        policyOne.ExpiryDate = '2035-02-28T00:00:00Z';
        policyOne.FullTermPremium = 0;
        policyOne.GLBranchCode = '042042042';
        policyOne.GLDepartmentCode = '043043043';
        policyOne.GLDivisionCode = '036036036';
        policyOne.GLGroupCode = '038038038';
        policyOne.LineOfBusinessIDs = '';
        policyOne.PolicyId = 'daa0634f-f902-478f-8b75-c851eaf878ce';
        policyOne.PolicyNumber = 'POL N0659 VALP';
		policyOne.RenewalReportFlag = 'A';
        policyOne.TypeOfBusiness = '2';
        policyOne.WritingCompanyCode = '033056044';
        
        RestServiceToUpsertPolicyRecords.SagittaPolicy policyTwo = new RestServiceToUpsertPolicyRecords.SagittaPolicy();
        policyTwo.BillMethod = 'P';
        policyTwo.CsrCode = '033033050';
        policyTwo.CustomerId = '16721680-c7bb-4f17-9353-00003028dffe';
        policyTwo.Description = 'This place is awesome - fo realz!';
        policyTwo.EffectiveDate = '2019-09-28T00:00:00Z';
        policyTwo.ExecCode = '033033052';
        policyTwo.ExpiryDate = '2035-02-28T00:00:00Z';
        policyTwo.FullTermPremium = 0;
        policyTwo.GLBranchCode = '042042042';
        policyTwo.GLDepartmentCode = '043043043';
        policyTwo.GLDivisionCode = '036036036';
        policyTwo.GLGroupCode = '038038038';
        policyTwo.LineOfBusinessIDs = '';
        policyTwo.PolicyId = 'daa0634f-f902-478f-8b75-666666666666';
        policyTwo.PolicyNumber = 'POL NANA NANA';
		policyTwo.RenewalReportFlag = 'A';
        policyTwo.TypeOfBusiness = '2';
        policyTwo.WritingCompanyCode = '033056044';
        
        policiesToInsert.add(policyOne);
        policiesToInsert.add(policyTwo);
        String myJSON = JSON.serialize(policiesToInsert);
        
        RestRequest request = new RestRequest();
        request.requestUri ='https://test.salesforce.com/services/apexrest/SIV/UpsertPolicyRecords';
        request.httpMethod = 'POST';
        request.requestBody = Blob.valueof(myJSON);
        RestResponse response = new RestResponse();
        
        RestContext.request = request;
        RestContext.response = response;
        RestServiceToUpsertPolicyRecords.ResponseHandler res = RestServiceToUpsertPolicyRecords.doPost();
        
        List<Sagitta_Policy__c> policies = [SELECT Id, Sagitta_Policy_ID__c FROM Sagitta_Policy__c];
        system.assertEquals(2, policies.size()); 
    }
    
    @isTest
    private static void doPost_ShouldReturnError_WithPolicyIdsThatFailed_WhenPassedInvalidBUCodes(){
        List<RestServiceToUpsertPolicyRecords.SagittaPolicy> policiesToInsert = new List<RestServiceToUpsertPolicyRecords.SagittaPolicy>();
        RestServiceToUpsertPolicyRecords.SagittaPolicy policyOne = new RestServiceToUpsertPolicyRecords.SagittaPolicy();
        policyOne.BillMethod = 'P';
        policyOne.CsrCode = '033033050';
        policyOne.CustomerId = '16721680-c7bb-4f17-9353-00003028dffe';
        policyOne.Description = 'This place is awesome - said no one ever';
        policyOne.EffectiveDate = '2019-09-28T00:00:00Z';
        policyOne.ExecCode = '033033052';
        policyOne.ExpiryDate = '2035-02-28T00:00:00Z';
        policyOne.FullTermPremium = 0;
        // -----Invalid BU codes-----
        policyOne.GLBranchCode = '---';
        policyOne.GLDepartmentCode = '---';
        policyOne.GLDivisionCode = '---';
        policyOne.GLGroupCode = '---';
        // --------------------------
        policyOne.LineOfBusinessIDs = '';
        policyOne.PolicyId = 'daa0634f-f902-478f-8b75-c851eaf878ce';
        policyOne.PolicyNumber = 'POL N0659 VALP';
		policyOne.RenewalReportFlag = 'A';
        policyOne.TypeOfBusiness = '2';
        policyOne.WritingCompanyCode = '100037036';

        policiesToInsert.add(policyOne);
        String myJSON = JSON.serialize(policiesToInsert);
        
        RestRequest request = new RestRequest();
        request.requestUri ='https://test.salesforce.com/services/apexrest/SIV/UpsertPolicyRecords';
        request.httpMethod = 'POST';
        request.requestBody = Blob.valueof(myJSON);
        RestContext.request = request;
        
        RestResponse response = new RestResponse();
        response.statusCode = 400;
		RestContext.response = response;
        
        RestServiceToUpsertPolicyRecords.ResponseHandler res = RestServiceToUpsertPolicyRecords.doPost();
        
        List<Sagitta_Policy__c> policies = [SELECT Id, Sagitta_Policy_ID__c FROM Sagitta_Policy__c];
        boolean expectedSuccess = false;
        String expectedStatus = 'ERROR';
        String expectedMessage = 'Could not process Sagitta Policies: daa0634f-f902-478f-8b75-c851eaf878ce in Salesforce.';
        System.assertEquals(0, policies.size());
        System.assertEquals(expectedSuccess, res.success);
        System.assertEquals(expectedStatus, res.status);
        System.assertEquals(expectedMessage, res.message);
    }
    
    @isTest
    private static void doPost_ShouldReturnError_WithPolicyIdsThatFailed_WhenPassedInvalidEmployeeCodes(){
        List<RestServiceToUpsertPolicyRecords.SagittaPolicy> policiesToInsert = new List<RestServiceToUpsertPolicyRecords.SagittaPolicy>();
        RestServiceToUpsertPolicyRecords.SagittaPolicy policyOne = new RestServiceToUpsertPolicyRecords.SagittaPolicy();
        policyOne.BillMethod = 'P';
        // -----Invalid Employee code-----
        policyOne.CsrCode = '---';
        // --------------------------------
        policyOne.CustomerId = '16721680-c7bb-4f17-9353-00003028dffe';
        policyOne.Description = 'This place is awesome - said no one ever';
        policyOne.EffectiveDate = '2019-09-28T00:00:00Z';
        // -----Invalid Employee code-----
        policyOne.ExecCode = '---';
        // --------------------------------
        policyOne.ExpiryDate = '2035-02-28T00:00:00Z';
        policyOne.FullTermPremium = 0;
        policyOne.GLBranchCode = '042042042';
        policyOne.GLDepartmentCode = '043043043';
        policyOne.GLDivisionCode = '036036036';
        policyOne.GLGroupCode = '038038038';
        policyOne.LineOfBusinessIDs = '';
        policyOne.PolicyId = 'daa0634f-f902-478f-8b75-c851eaf878ce';
        policyOne.PolicyNumber = 'POL N0659 VALP';
		policyOne.RenewalReportFlag = 'A';
        policyOne.TypeOfBusiness = '2';
        policyOne.WritingCompanyCode = '036104040';

        policiesToInsert.add(policyOne);
        String myJSON = JSON.serialize(policiesToInsert);
        
        RestRequest request = new RestRequest();
        request.requestUri ='https://test.salesforce.com/services/apexrest/SIV/UpsertPolicyRecords';
        request.httpMethod = 'POST';
        request.requestBody = Blob.valueof(myJSON);
        RestContext.request = request;
        
        RestResponse response = new RestResponse();
        response.statusCode = 400;
		RestContext.response = response;
        
        RestServiceToUpsertPolicyRecords.ResponseHandler res = RestServiceToUpsertPolicyRecords.doPost();
        
        List<Sagitta_Policy__c> policies = [SELECT Id, Sagitta_Policy_ID__c FROM Sagitta_Policy__c];
        boolean expectedSuccess = false;
        String expectedStatus = 'ERROR';
        String expectedMessage = 'Could not process Sagitta Policies: daa0634f-f902-478f-8b75-c851eaf878ce in Salesforce.';
        System.assertEquals(0, policies.size());
        System.assertEquals(expectedSuccess, res.success);
        System.assertEquals(expectedStatus, res.status);
        System.assertEquals(expectedMessage, res.message);
    }
    
    @isTest
    private static void doPost_ShouldReturnError_WhenPassedInvalidDateFormats(){
        String myJSON = '[{' +
            '"BillMethod": "A",' +
            '"CustomerId": "16721680-c7bb-4f17-9353-00003028dffe",' +
            '"Description": "First policy.",' +
            '"EffectiveDate": "11/18/2019",' +
            '"ExpiryDate": "11/30/2019",' +
            '"FullTermPremium": "0",' +
            '"LineOfBusinessIDs": "c9830642-feb2-442f-a2f4-c0de99, c9830642-feb2-442f-a2f4-666666",' +
            '"PolicyId": "daa0634f-f902-478f-8b75-c851eaf878ce",' +
            '"PolicyNumber": "POL N0659 VALP",' +
            '"RenewalReportFlag": "A",' +
            '"TypeOfBusiness": "2",' +
            '"WritingCompanyName": "Updated Test Co",' +
            '"ExecCode": "033033052",' +
            '"CsrCode": "033033050",' +
            '"GLDivisionCode": "036036036",' +
            '"GLBranchCode": "042042042",' +
            '"GLDepartmentCode": "043043043",' +
            '"GLGroupCode": "038038038"' +
		'}]';
        
        RestRequest request = new RestRequest();
        request.requestUri ='https://test.salesforce.com/services/apexrest/SIV/UpsertPolicyRecords';
        request.httpMethod = 'POST';
        request.requestBody = Blob.valueof(myJSON);
        RestContext.request = request;
        
        RestResponse response = new RestResponse();
        response.statusCode = 400;
		RestContext.response = response;
        
        RestServiceToUpsertPolicyRecords.ResponseHandler res = RestServiceToUpsertPolicyRecords.doPost();
        
        List<Sagitta_Policy__c> policies = [SELECT Id, Sagitta_Policy_ID__c FROM Sagitta_Policy__c];
        boolean expectedSuccess = false;
        String expectedStatus = 'ERROR';
        String expectedMessage = 'Could not process Sagitta Policies: daa0634f-f902-478f-8b75-c851eaf878ce in Salesforce.';
        System.assertEquals(0, policies.size());
        System.assertEquals(expectedSuccess, res.success);
        System.assertEquals(expectedStatus, res.status);
        System.assertEquals(expectedMessage, res.message);
    }
    
    private static void createIntegrationFlag(String flag) {
        Metadata__c intFlag = new Metadata__c();
        intFlag.Name = 'IntegrationFlag';
        intFlag.Value__c = flag;
        insert intFlag;
    }
    
    private static void createIsInitialSyncFlag(String flag) {
        Metadata__c isInitialSyncFlag = new Metadata__c();
        isInitialSyncFlag.Name = 'IsInitialSync';
        isInitialSyncFlag.Value__c = flag;
        insert isInitialSyncFlag;
    }
}