/*
    Author : Ben Baik
    Name : RestServiceAccountContext
	Date : 19 July 2019
	Description : Gets relevant Salesforce objects for Sagitta Customers (Division, Branch, Department, Group, Employee, and Account).
				  Finds object by ID or code in relevant data.
					
	Modification History :
	*************************************************************
	Version               Author             Date                      Description
	1                     Ben Baik           19 July 2019              Original
*/
public class RestServiceAccountContext {
    private List<Account> accounts;
    private List<Sagitta_Employee__c> employees;
    private List<Sagitta_Division__c> divisions;
    private List<Sagitta_Branch__c> branches;
    private List<Sagitta_Department__c> depts;
    private List<Sagitta_Group__c> groups;
    
    // Summary:
    // Gets all relevant data for Sagitta Customers
    // 
    // Details:
    // @param List<RestServiceToUpsertAcctRecords.SagittaCustomer> customers - Apex representation of Sagitta Customers
    // @return RestServiceAccountContext instance w/ relevant data
    public RestServiceAccountContext(List<RestServiceToUpsertAcctRecords.SagittaCustomer> customers) {
        Set<String> customerIdSet = new Set<String>();
        Set<String> employeeCodeSet = new Set<String>();
        Set<String> divisionCodeSet = new Set<String>();
        Set<String> branchCodeSet = new Set<String>();
        Set<String> deptCodeSet = new Set<String>();
        Set<String> groupCodeSet = new Set<String>();
        
        for (RestServiceToUpsertAcctRecords.SagittaCustomer customer : customers) {
            customerIdSet.add(customer.CustomerId);
            employeeCodeSet.add(customer.AccountExecCode);
            employeeCodeSet.add(customer.AccountRepCode);
			divisionCodeSet.add(customer.GLDivisionCode);
			branchCodeSet.add(customer.GLBranchCode);
			deptCodeSet.add(customer.GLDepartmentCode);
			groupCodeSet.add(customer.GLGroupCode);            
        }
        
		accounts = getRelevantAccounts(customerIdSet);
        employees = getRelevantEmployees(employeeCodeSet);
        divisions = getRelevantDivisions(divisionCodeSet);
        branches = getRelevantBranches(branchCodeSet);
        depts = getRelevantDepts(deptCodeSet);
        groups = getRelevantGroups(groupCodeSet);
    }
    
    // Summary:
    // Find Salesforce Account by Sagitta Customer ID
    // 
    // Details:
    // @param String customerId - Sagitta Customer ID
    // @return Account - Account with matching Sagitta Customer ID
    public Account findAccount(String customerId) {
        for (Account account : accounts) {
            if (account.Sagitta_Customer_Id__c == customerId) {
                return account;
            }
        }
        return new Account();
    }
    
    // Summary:
    // Find Sagitta_Employee__c by Sagitta Customer employee code
    // 
    // Details:
    // @param String customerId - Sagitta employee code
    // @return Sagitta_Employee__c - Sagitta_Employee__c with matching Sagitta employee code
    public Sagitta_Employee__c findEmployee(String employeeCode) {
        for (Sagitta_Employee__c employee : employees) {
            if (employee.Sagitta_Employee_Code__c == employeeCode) {
                return employee;
            }
        }
        return null;
    }
    
    // Summary:
    // Find Sagitta_Division__c by Sagitta division code
    // 
    // Details:
    // @param String divisionCode - Sagitta division code
    // @return Sagitta_Division__c - Sagitta_Division__c with matching Sagitta division code
    public Sagitta_Division__c findDivision(String divisionCode) {
        for (Sagitta_Division__c division : divisions) {
            if (division.ItemCode__c == divisionCode) {
                return division;
            }
        }
        return null;
    }
    
    // Summary:
    // Find Sagitta_Branch__c by Sagitta branch code
    // 
    // Details:
    // @param String branchCode - Sagitta branch code
    // @return Sagitta_Branch__c - Sagitta_Branch__c with matching Sagitta branch code
    public Sagitta_Branch__c findBranch(String branchCode) {
        for (Sagitta_Branch__c branch : branches) {
            if (branch.ItemCode__c == branchCode) {
                return branch;
            }
        }
        return null;
    }
    
    // Summary:
    // Find Sagitta_Department__c by Sagitta department code
    // 
    // Details:
    // @param String departmentCode - Sagitta department code
    // @return Sagitta_Department__c - Sagitta_Department__c with matching Sagitta department code
    public Sagitta_Department__c findDept(String deptCode) {
        for (Sagitta_Department__c dept : depts) {
            if (dept.ItemCode__c == deptCode) {
                return dept;
            }
        }
        return null;
    }
    
    // Summary:
    // Find Sagitta_Group__c by Sagitta group code
    // 
    // Details:
    // @param String groupCode - Sagitta group code
    // @return Sagitta_Group__c - Sagitta_Group__c with matching Sagitta group code
    public Sagitta_Group__c findGroup(String groupCode) {
        for (Sagitta_Group__c grp : groups) {
            if (grp.ItemCode__c == groupCode) {
                return grp;
            }
        }
        return null;
    }
    
    // Summary:
    // Gets all relevant Salesforce Accounts objects
    // 
    // Details:
    // @param Set<String> customerIds - Set of Sagitta Customer ID's used to query for relevant Account objects
    // @return List<Account> - List of relevant Salesforce Account objects
    private List<Account> getRelevantAccounts(Set<String> customerIds) {
        List<Account> accountsWithNullFields;
		accountsWithNullFields = [SELECT Id, 
                            	   		 Sagitta_Customer_ID__c,
                            	   		 Sagitta_Customer_Number__c,
                            	   		 Sagitta_Client_Type__c,
                            	   		 Sagitta_Division__c,
                            	   		 Sagitta_Branch__c,
                            	   		 Sagitta_Department__c,
                            	   		 Sagitta_Group__c,
                             	   		 Sagitta_Employee_Account_Exec__c,
                            	   		 Sagitta_Employee_Account_Rep__c,
                            	   		 Sagitta_Account_Status__c,
                            	   		 Sagitta_Company_Email_Address__c,
                            	   		 Sagitta_DBA__c,
                            	   		 Sagitta_First_Name__c,
                            	   		 Sagitta_Middle_Name__c,
                            	   		 Sagitta_Last_Name__c,
                                         Website,
                                         BillingStreet,
                                         BillingCity,
                                         BillingState,
                                         BillingPostalCode,
                                         Phone,
                                         Sagitta_TOB_IsPersonal__c,
                                         Sagitta_TOB_IsCommercial__c,
                                         Sagitta_TOB_IsLife__c,
                                         Sagitta_TOB_IsBenefits__c,
                                         Sagitta_TOB_IsHealth__c,
                                         Sagitta_TOB_IsNon_P_C__c,
                                         Sagitta_TOB_IsFinancial__c,
                            	   		 Name
                             	FROM Account
                            	WHERE Sagitta_Customer_ID__c
                            	IN :customerIds];
        
        return nullFieldsToEmptyString(accountsWithNullFields);
    }
   
    // Summary:
    // Gets all relevant Sagitta_Employee__c objects
    // 
    // Details:
    // @param Set<String> employeeCodes - Set of Sagitta employee codes used to query for relevant Sagitta_Employee__c objects
    // @return List<Sagitta_Employee__c> - List of relevant Sagitta_Employee__c objects
    private List<Sagitta_Employee__c> getRelevantEmployees(Set<String> employeeCodes) {
        return [SELECT Id,
                       Name,
                       Sagitta_Is_Executive__c,
                       Sagitta_Is_Representative__c,
                       Sagitta_Employee_Code__c,
                       Sagitta_Entity_ID__c
                 FROM Sagitta_Employee__c
                 WHERE Sagitta_Employee_Code__c
                 IN :employeeCodes];
    }
    
    // Summary:
    // Gets all relevant Sagitta_Division__c objects
    // 
    // Details:
    // @param Set<String> divisionCodes - Set of Sagitta division codes used to query for relevant Sagitta_Division__c objects
    // @return List<Sagitta_Division__c> - List of relevant Sagitta_Division__c objects
    private List<Sagitta_Division__c> getRelevantDivisions(Set<String> divisionCodes) {
        return [SELECT Id,
                       Name,
                       ItemCode__c,
                       Sagitta_IsHidden__c
                FROM Sagitta_Division__c
                WHERE ItemCode__c
                IN :divisionCodes];
    }
    
    // Summary:
    // Gets all relevant Sagitta_Branch__c objects
    // 
    // Details:
    // @param Set<String> branchCodes - Set of Sagitta branch codes used to query for relevant Sagitta_Branch__c objects
    // @return List<Sagitta_Branch__c> - List of relevant Sagitta_Branch__c objects
    private List<Sagitta_Branch__c> getRelevantBranches(Set<String> branchCodes) {
        return [SELECT Id,
                       Name,
                       ItemCode__c, 
                       Sagitta_IsHidden__c
                FROM Sagitta_Branch__c
                WHERE ItemCode__c
                IN :branchCodes];
    }
    
    // Summary:
    // Gets all relevant Sagitta_Department__c objects
    // 
    // Details:
    // @param Set<String> deptCodes - Set of Sagitta department codes used to query for relevant Sagitta_Department__c objects
    // @return List<Sagitta_Department__c> - List of relevant Sagitta_Department__c objects
    private List<Sagitta_Department__c> getRelevantDepts(Set<String> deptCodes) {
        return [SELECT Id,
                       Name,
                       ItemCode__c,
                       Sagitta_IsHidden__c
                FROM Sagitta_Department__c
                WHERE ItemCode__c
                IN :deptCodes];
    }
    
    // Summary:
    // Gets all relevant Sagitta_Group__c objects
    // 
    // Details:
    // @param Set<String> groupCodes - Set of Sagitta group codes used to query for relevant Sagitta_Group__c objects
    // @return List<Sagitta_Group__c> - List of relevant Sagitta_Group__c objects
    private List<Sagitta_Group__c> getRelevantGroups(Set<String> groupCodes) {
        return [SELECT Id,
                       Name,
                       ItemCode__c,
                       Sagitta_IsHidden__c
                FROM Sagitta_Group__c
                WHERE ItemCode__c
                IN :groupCodes];
    }

    // Summary:
    // Convert all string fields that are null to an empty string. This is because null fields in the
    // RestService post body string get deserialized into empty strings which will cause issues when
    // comparing fields in the RestServiceToUpsertAcctRecords.mapFields method.
    // 
    // Details:
    // @param List<Account> accounts - List of Account objects to convert null fields to string
    // @return List<Account> - Account objects with null fields now set to ''
    private List<Account> nullFieldsToEmptyString(List<Account> accounts) {
		for (Account account : accounts) {
            if (account.Sagitta_Customer_ID__c == null) {
                account.Sagitta_Customer_ID__c = '';
            }
            
            if (account.Sagitta_Customer_Number__c == null) {
                account.Sagitta_Customer_Number__c = '';
            }
            
            if (account.Sagitta_Client_Type__c == null) {
                account.Sagitta_Client_Type__c = '';
            }
            
            if (account.Sagitta_Division__c == null) {
                account.Sagitta_Division__c = '';
            }
            
            if (account.Sagitta_Branch__c == null) {
                account.Sagitta_Branch__c = '';
            }
            
            if (account.Sagitta_Department__c == null) {
                account.Sagitta_Department__c = '';
            }
            
            if (account.Sagitta_Group__c == null) {
                account.Sagitta_Group__c = '';
            }
            
            if (account.Sagitta_Employee_Account_Exec__c == null) {
                account.Sagitta_Employee_Account_Exec__c = '';
            }
            
            if (account.Sagitta_Employee_Account_Rep__c == null) {
                account.Sagitta_Employee_Account_Rep__c = '';
            }
            
            if (account.Sagitta_Account_Status__c == null) {
                account.Sagitta_Account_Status__c = '';
            }
            
            if (account.Sagitta_Company_Email_Address__c == null) {
                account.Sagitta_Company_Email_Address__c = '';
            }
            
            if (account.Sagitta_DBA__c == null) {
                account.Sagitta_DBA__c = '';
            }
            
            if (account.Sagitta_First_Name__c == null) {
                account.Sagitta_First_Name__c = '';
            }
            
            if (account.Sagitta_Middle_Name__c == null) {
                account.Sagitta_Middle_Name__c = '';
            }
            
            if (account.Sagitta_Last_Name__c == null) {
                account.Sagitta_Last_Name__c = '';
            }
            
            if (account.Website == null) {
                account.Website = '';
            }
            
            if (account.BillingStreet == null) {
                account.BillingStreet = '';
            }
            
            if (account.BillingCity == null) {
                account.BillingCity = '';
            }
            
            if (account.BillingState == null) {
                account.BillingState = '';
            }
            
            if (account.BillingPostalCode == null) {
                account.BillingPostalCode = '';
            }
            
            if (account.Phone == null) {
                account.Phone = '';
            }
            
            if (account.Name == null) {
                account.Name = '';
            }
        }
        
        return accounts;
    }
}