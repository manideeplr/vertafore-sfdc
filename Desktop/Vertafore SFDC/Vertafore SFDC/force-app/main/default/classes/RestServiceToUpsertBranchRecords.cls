/*
    Author : Vikram
    Name : RestServiceToUpsertBranchRecords
    Date : 26th March, 2019
    Description: Rest service to upsert branch records
    Modification History :
    *************************************************************
    Vesion               Author             Date                      Description
	1.1					 Vikram				18 June 2019			  Added logging functionality and few comments where necessary
*/

@RestResource(urlMapping='/UpsertBranchRecords/*')
global with sharing class RestServiceToUpsertBranchRecords{
    
    // class that represents SagittaBranch model
    global class SagittaBrn {		
        global String ItemCode { get; set; }
        global String ItemCodeHash { get; set; }
        global String Name { get; set; }
        global String Status { get; set; }
        global Boolean IsHidden { get; set; }
        global String EntityId { get; set; }
        global String EntityName { get; set; }        
    }
    
    // class represents response to caller
    global class ResponseHandler {
        public Boolean success {get; set;}
        public String status {get; set;}
        public String message {get;set;}
    }
    
    // Deserialize the request body and insert the data
	// @param - No Param
	// @return - ResponseHandler
    @HttpPost
    global static ResponseHandler doPost() {
        ResponseHandler obj = new ResponseHandler();
        RestRequest req = RestContext.request;
        String failedRecordIds = '';
        try {
            String postBody = req.requestBody.toString();
            // Deserialize the request body and converts into List<SagittaBrn>
            List<SagittaBrn> Brns = (List<SagittaBrn>)JSON.deserialize(postBody, List<SagittaBrn>.class);
            List<Sagitta_Branch__c> brnsToUpsert = new List<Sagitta_Branch__c>();
            // For loop to iterate through all the incoming Branch records
            for (SagittaBrn brn : Brns) {
                Sagitta_Branch__c newBrn = new Sagitta_Branch__c();
                newBrn.Name = brn.Name;
                newBrn.ItemCode__c = brn.ItemCodeHash;
                newBrn.Sagitta_isHidden__c = brn.IsHidden;
                brnsToUpsert.add(newBrn);
            }
            Schema.SObjectField extIdField = Sagitta_Branch__c.Fields.ItemCode__c;
            // To insert the list of records
            Database.UpsertResult[] results = Database.upsert(brnsToUpsert, extIdField, false);
            for (Database.UpsertResult result : results) {
                if (!result.isSuccess()) {
                    Database.Error[] upsertErrors = result.getErrors();
                    for (Database.Error upsertError : upsertErrors) {
                        failedRecordIds = failedRecordIds + upsertError.getMessage() + upsertError.getFields();
                    }
                }
            }
            // Response handler initialization once the insert is successful/failure
            // Insert log into Application Log object for request and response json
            if (String.isNotBlank(failedRecordIds)) {
                obj.success = false;
                obj.status='ERROR';
                obj.message= failedRecordIds;
                ApplicationLogger.logWebServiceMessage('RestServiceToUpsertBranchRecords', 'doPost', failedRecordIds, 
                                                   ApplicationLogger.ERROR, req.requestBody.toString(), JSON.serialize(obj));
            } else {
                obj.success = true;
                obj.status='SUCCESS';
                obj.message= 'SUCCESS';
                ApplicationLogger.logWebServiceMessage('RestServiceToUpsertBranchRecords', 'doPost', 'SUCCESS', 
                                                   ApplicationLogger.INFO, req.requestBody.toString(), JSON.serialize(obj));
            }
            
            return obj;
        } catch(Exception ex) {
            // Response handler initialization on exception
            RestContext.response.statusCode = 400;
            obj.success = false;
            obj.status='Error';
            obj.message=ex.getMessage();            
            String message = 'Error: ' + ex.getMessage() + '; Stack Trace:' + ex.getStackTraceString();            
            // Insert log into Application Log object for request, response and error json
            ApplicationLogger.logWebServiceMessage('RestServiceToUpsertBranchRecords', 'doPost', message, 
                                                   ApplicationLogger.ERROR, req.requestBody.toString(), JSON.serialize(obj));
            return obj;
        }        
    }

}