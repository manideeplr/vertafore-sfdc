public class MergeUtility {

    @AuraEnabled
    public static void executeAccountMergeBatch() {
        Database.executeBatch(new AccountMergeBatch(), 100);
    }
    
    @AuraEnabled
    public static void executeContactMergeBatch() {
        Database.executeBatch(new ContactMergeBatch(), 100);
    }
}