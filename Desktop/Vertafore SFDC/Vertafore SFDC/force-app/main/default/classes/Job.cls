/*
    Author : Ben Baik
    Name : Job
    Date : 14 June 2019
    Description: Class that implements Queuebale interface for enqueueing jobs that are asynchronously processed.
    Modification History :
    *************************************************************
    Version               Author             Date                      Description
	1					  Ben			 	 14 June 2019     		   Original
*/
public class Job implements Queueable, Database.AllowsCallouts {
    private String xmlBody;
    
    public Job(String xmlBody) {
        this.xmlBody = xmlBody;
    }
    
    public void execute(QueueableContext context) {
        HttpClient client = new HttpClient();
        client.SendXMLRequest(this.xmlBody);
    }
}