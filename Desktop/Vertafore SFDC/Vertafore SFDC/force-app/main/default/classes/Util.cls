public class Util {
    // When updating a setup object (User in this case) you cannot update any other kind of objects. To work around this we use a @future method on the Util class to update the employee.
	@future
    public static void setEmployees(String userId, String executiveIds) {
        //Get employees currently assigned to this user
        List<Sagitta_Employee__c> employees = [SELECT Id, Sagitta_Assign_To__c FROM Sagitta_Employee__c WHERE Sagitta_Assign_To__c =: userId];
        
        if(employees.size() > 0){
        	//Unassign them from this user so that you dont have to compare 2 lists
        	for(Sagitta_Employee__c employee : employees){
            	employee.Sagitta_Assign_To__c = null;
        	}
        	update employees;            
        }

        //Get employees that were passed in to be assigned to this user
        Set<String> ids = new Set<String>();
        String[] splitIds = executiveIds.split(',');
        for(String anId: splitIds){
            ids.add(anId);
        }

        if(ids.size() > 0){
        	employees = [SELECT Id, Sagitta_Assign_To__c FROM Sagitta_Employee__c WHERE Id IN :ids];

        	for(Sagitta_Employee__c employee : employees){
            	employee.Sagitta_Assign_To__c = Id.valueOf(userId);                
        	}
        	update employees; 
        }       
    }
}