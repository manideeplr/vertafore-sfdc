@isTest
public class RestServiceToUpsertLOBRecordsTest {
    
    @isTest
    static void test1(){
        List<RestServiceToUpsertLOBRecords.SagittaLOB> itmToInsert = new List<RestServiceToUpsertLOBRecords.SagittaLOB>();
        RestServiceToUpsertLOBRecords.SagittaLOB itm = new RestServiceToUpsertLOBRecords.SagittaLOB();
        itm.DescriptionLOBS = '1';
        itm.IdLOBS = '72627';
        itm.PermanentFlagLOBS = 1;
        itm.TypeOfBusinessLOBS = '1';
        itm.EntityId = '162';
        itm.EntityName = 'SagittaBranchEntity';
        itm.IsHide = true;
        itmToInsert.add(itm);
        String myJSON = JSON.serialize(itmToInsert);
        
        
        RestRequest request = new RestRequest();
        request.requestUri ='https://test.salesforce.com/services/apexrest/UpsertLOBRecords';
        request.httpMethod = 'POST';
        request.requestBody = Blob.valueof(myJSON);
        
        RestContext.request = request;
        RestServiceToUpsertLOBRecords.ResponseHandler res = RestServiceToUpsertLOBRecords.doPost();
        
        List<Sagitta_Line_of_Business__c> lobs = [SELECT Id FROM Sagitta_Line_of_Business__c];
        system.assertEquals(1, lobs.size());    
    }
    
    @isTest
    static void test2(){
        List<RestServiceToUpsertLOBRecords.SagittaLOB> itmToInsert = new List<RestServiceToUpsertLOBRecords.SagittaLOB>();
        RestServiceToUpsertLOBRecords.SagittaLOB itm = new RestServiceToUpsertLOBRecords.SagittaLOB();
        itm.DescriptionLOBS = '1';
        itm.IdLOBS = '72627';
        itm.PermanentFlagLOBS = 1;
        itm.TypeOfBusinessLOBS = '1';
        itm.EntityId = '162';
        itm.EntityName = 'SagittaBranchEntity';
        itm.IsHide = false;
        itmToInsert.add(itm);
        String myJSON = JSON.serialize(itmToInsert);
        
        RestRequest request = new RestRequest();
        request.requestUri ='https://test.salesforce.com/services/apexrest/UpsertLOBRecords';
        request.httpMethod = 'POST';
        request.requestBody = Blob.valueof(myJSON);
        
        RestContext.request = request;
        RestServiceToUpsertLOBRecords.ResponseHandler res = RestServiceToUpsertLOBRecords.doPost();
        
        List<Sagitta_Line_of_Business__c> lobs = [SELECT Id FROM Sagitta_Line_of_Business__c];
        system.assertEquals(1, lobs.size());    
    }
    
    @isTest
    static void test3(){
        RestServiceToUpsertLOBRecords.SagittaLOB itm = new RestServiceToUpsertLOBRecords.SagittaLOB();
        itm.DescriptionLOBS = '1';
        itm.IdLOBS = '72627';
        itm.PermanentFlagLOBS = 1;
        itm.TypeOfBusinessLOBS = '1';
        itm.EntityId = '162';
        itm.EntityName = 'SagittaLineOfBusinessEntity';
        String myJSON = JSON.serialize(itm);
        
        
        RestRequest request = new RestRequest();
        request.requestUri ='https://test.salesforce.com/services/apexrest/UpsertLOBRecords';
        request.httpMethod = 'POST';
        request.requestBody = Blob.valueof(myJSON);
        
        RestResponse response = new RestResponse();
        response.statusCode = 400;
        
        RestContext.request = request;
        RestContext.response = response;
        RestServiceToUpsertLOBRecords.ResponseHandler res = RestServiceToUpsertLOBRecords.doPost();
        
        List<Sagitta_Line_of_Business__c> lobs = [SELECT Id FROM Sagitta_Line_of_Business__c];
        system.assertEquals(0, lobs.size());    
    }
}