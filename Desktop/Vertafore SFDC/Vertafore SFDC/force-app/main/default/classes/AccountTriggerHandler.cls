/*
    Author : Ian Horner
    Name : AccountTriggerHandler
	Date : 6 June 2019
	Description :   Trigger for Account. Performs the following:
					Prevents user from creating an account if they don't have a Mapping record assigned to them.
					Prevents user from changing Client Type.
					Populates Business Units, Executive, and Representative on insert.
					Populates Type of Business checkboxes on insert.
					Prevents user from changing Exec or Rep to be blank on update.
					Creates task for account after update.
					
	Modification History :
	*************************************************************
	Version               Author             Date                      Description
	1                     Ian                7 June 2019               Initial refactor
    1.1                   Vikram			 27 June 2019 			   Added WHERE condition for soqls of Brn, Div, Grp and Dept
																	   Changed the 
*/
public class AccountTriggerHandler {
    public AccountTriggerHandler() {}
    public List<Sagitta_Division__c> divisions;
    public List<Sagitta_Branch__c> branches;
    public List<Sagitta_Department__c> departments;
    public List<Sagitta_Group__c> groups;
    public List<Sagitta_Employee__c> employees;
    
    // Summary:
    // Handles all Account trigger behavior. (See description at top of file for full summary of behavior.)
    // 
    // Details:
    // @param workingRecords - List of all Account records that are being inserted or updated.
    // @param triggerEvent - BEFORE_INSERT, BEFORE_UPDATE, or AFTER_UPDATE
    // @return TriggerHandlerStatus - MESSAGES_SENT or NO_MESSAGES_SENT
    public TriggerHandlerStatus handleTrigger(List<Account> workingRecords, TriggerOperation triggerEvent) {
        Sagitta_Mapping__c mapping;
        boolean comingFromAPI = false;
        
        // Get default Mapping for user
        try {
            List<Sagitta_Mapping__c> mappingList = 
                [SELECT Id, Sagitta_Division__c, Sagitta_Branch__c, Sagitta_Department__c, Sagitta_Group__c, Sagitta_Executive__c, Sagitta_Representative__c
                 FROM Sagitta_Mapping__c 
                 WHERE Sagitta_Assigned_To__c =: UserInfo.getUserId() LIMIT 1];
            if (mappingList.size() > 0) {
                mapping = mappingList[0];
            }
        } catch (Exception e) {
            string message = 'Error retrieving Sagitta Mapping: ' + e.getMessage() + '; Stack Trace: ' + e.getStackTraceString();
         //   ApplicationLogger.logMessage('AccountTriggerHandler', 'handleTrigger', message, ApplicationLogger.ERROR);
            throw e;
        }
        
        // See if request is coming from the API.
        if (String.valueOf(URL.getCurrentRequestUrl()).toLowerCase().contains('services/data') || 
            String.valueOf(URL.getCurrentRequestUrl()).toLowerCase().contains('services/apexrest')) {
                comingFromAPI = true;
        }
        
        Metadata__c intFlag = Metadata__c.getValues('IntegrationFlag');
        switch on triggerEvent {
            when BEFORE_INSERT {
                if (comingFromAPI) {
                    return TriggerHandlerStatus.NO_MESSAGES_SENT;
                }
                
                // Prevent the user from creating an account if they don't have a default Mapping
                if (mapping == null) {
                    for (Account account : workingRecords) {
                        account.addError('No Sagitta Mapping record found. An Sagitta Mapping record must be assigned to this user in order to create an Account.');
                    }
                    return TriggerHandlerStatus.NO_MESSAGES_SENT;
                }
                
                // Get all Business Units so that we can later validate each account's Business Unit selection.
                getAllBusinessUnits();
                
                for (Account account : workingRecords) {
                    // Populate the Business Unit fields if they aren't already populated
                    if (account.Sagitta_Division__c == null) { // we only bother to check one field for efficiency
                        account.Sagitta_Division__c = mapping.Sagitta_Division__c;
                        account.Sagitta_Branch__c = mapping.Sagitta_Branch__c;
                        account.Sagitta_Department__c = mapping.Sagitta_Department__c;
                        account.Sagitta_Group__c = mapping.Sagitta_Group__c;
                    }
                    
                    // Populate Exec and Rep
                    if (account.Sagitta_Employee_Account_Exec__c == null) {
                        account.Sagitta_Employee_Account_Exec__c = mapping.Sagitta_Executive__c;
                    }
                    if (account.Sagitta_Employee_Account_Rep__c == null) {
                    	account.Sagitta_Employee_Account_Rep__c = mapping.Sagitta_Representative__c;
                    }
                    
                    // Validate Business Units
                    checkValidBusinessUnits(account);
                    
                    // Prevent user from changing Client Type
                    account.Sagitta_Client_Type__c = 'P';
                    
                    // Map the Type of Business Picklist to appropriate Checkbox field
                    switch on account.Sagitta_Type_of_Business__c{
                        when '1' {
                            account.Sagitta_TOB_IsPersonal__c = true;
                        }
                        when '2' {
                            account.Sagitta_TOB_IsCommercial__c = true;
                        }
                        when '3' {
                            account.Sagitta_TOB_IsNon_P_C__c = true; 
                        }
                        when '4' {
                            account.Sagitta_TOB_IsBenefits__c = true;
                        }
                        when '5' {
                            account.Sagitta_TOB_IsLife__c = true;
                        }
                        when '6' {
                            account.Sagitta_TOB_IsHealth__c = true;
                        }
                        when '7' {
                            account.Sagitta_TOB_IsFinancial__c = true;
                        }
                        when else {
                            account.Sagitta_TOB_IsCommercial__c = true;
                        }
                    }
                }
                
                return TriggerHandlerStatus.NO_MESSAGES_SENT;
            }
            when BEFORE_UPDATE {
                if (mapping == null) {
                    return TriggerHandlerStatus.NO_MESSAGES_SENT;
                }
                
                // Get all Business Units so that we can validate each account's Business Unit selection.
                getAllBusinessUnits();
                
                for (Account account : workingRecords){
                    checkValidBusinessUnits(account);
                    
                    // Get old record
                    Account oldRecord = (Account)Trigger.oldMap.get(account.ID);
                    
                    // If old record and new record have different Customer ID values
                    if (account.Sagitta_Customer_ID__c != oldRecord.Sagitta_Customer_ID__c && !comingFromAPI) {
                        account.addError('The Customer ID field is read-only.');
                    }

                    // If old record and new record have different Client Type values
                    if (account.Sagitta_Client_Type__c != oldRecord.Sagitta_Client_Type__c && !comingFromAPI) {
                        account.addError('The Client Type field is read-only.');
                    }
                    
                    // If Exec and Rep haven't been filled in
                    if (String.isBlank(account.Sagitta_Employee_Account_Exec__c)){
                        account.Sagitta_Employee_Account_Exec__c = mapping.Sagitta_Executive__c;
                    }
                    if (String.isBlank(account.Sagitta_Employee_Account_Rep__c)){
                        account.Sagitta_Employee_Account_Rep__c = mapping.Sagitta_Representative__c;
                    }
                }
                return TriggerHandlerStatus.NO_MESSAGES_SENT;
            }
            when AFTER_UPDATE {
                createTaskForAccountAfterUpdate(workingRecords);
                for (Account account : workingRecords) {
                    // Send update request to Sagitta and create a Task for the updated Account
                    if (accountIsSynced(account) && (intFlag != null && string.isNotBlank(intFlag.Value__c) && intFlag.Value__c == 'true')) {
                        try {
                            String xmlBody = SFAMessenger.createXMLBody(account.Id);
                            Id jobId = SFAMessenger.createAndSendJob(xmlBody); 
                        } catch (Exception e) {
                            string message = 'Error sending update message: ' + e.getMessage() + '; Stack Trace: ' + e.getStackTraceString();
                     //       ApplicationLogger.logMessage('AccountTriggerHandler', 'handleTrigger', message, ApplicationLogger.ERROR);
                            throw e;
                        }
                    }
                    else {
                        return TriggerHandlerStatus.NO_MESSAGES_SENT;
                    }
                }
				return TriggerHandlerStatus.MESSAGES_SENT;
            }
            when else {
            	return TriggerHandlerStatus.NO_MESSAGES_SENT;
            }    
        }
    }
    
    // Summary:
    // Retrieves all Divisions, Branches, Departments, Groups, and Employees
    private void getAllBusinessUnits() {
        try {
            divisions = [SELECT Id, Sagitta_isHidden__c FROM Sagitta_Division__c WHERE Sagitta_IsHidden__c = false];
            branches = [SELECT Id, Sagitta_isHidden__c FROM Sagitta_Branch__c WHERE Sagitta_IsHidden__c = false];
            departments = [SELECT Id, Sagitta_isHidden__c FROM Sagitta_Department__c WHERE Sagitta_IsHidden__c = false];
            groups = [SELECT Id, Sagitta_isHidden__c FROM Sagitta_Group__c WHERE Sagitta_IsHidden__c = false];
            employees = [SELECT Id, Sagitta_Employee_Status__c FROM Sagitta_Employee__c WHERE Sagitta_Employee_Status__c != null];
        } catch (Exception e) {
            string message = 'Error retrieving Business Units: ' + e.getMessage() + '; Stack Trace: ' + e.getStackTraceString();
       //     ApplicationLogger.logMessage('AccountTriggerHandler', 'getAllBusinessUnits', message, ApplicationLogger.ERROR);
            throw e;
        }
    }
    
    // Summary:
    // Checks if account is synced with Sagitta.
    // 
    // Details:
    // @param account - The account that we're checking.
    // @return boolean - Returns true if account is synced.
    private boolean accountIsSynced(Account account) {
        return !String.isBlank(account.Sagitta_Customer_ID__c);
    }
    
    // Summary:
    // Creates a Task for the specified account.
    // 
    // Details:
    // @param account - The account that the Task will be assigned to.
    private void createTaskForAccountAfterUpdate(List<Account> accountList) {
        try {
            // Get default activity action type from Agency Settings record, then insert task
            Metadata__c intFlag = Metadata__c.getValues('IntegrationFlag');
            List<Sagitta_Agency_Settings__c> agencySettingsList = [SELECT Default_Activity_Action_Type__c FROM Sagitta_Agency_Settings__c LIMIT 1];
            List<Task> taskList = new List<Task>();
            if (agencySettingsList.Size() > 0) {
                for (Account acc : accountList) {
                    if (accountIsSynced(acc) && (intFlag != null && string.isNotBlank(intFlag.Value__c) && intFlag.Value__c == 'true')) {
                        Task tsk = new Task();
                        tsk.OwnerId = acc.get('OwnerId').toString();
                        tsk.Subject = 'Account updated by Salesforce user ' + UserInfo.getName();
                        tsk.Status = 'Completed';
                        tsk.Priority = 'Normal';
                        tsk.WhatId = acc.get('Id').toString();
                        tsk.Sagitta_Action__c = agencySettingsList[0].Default_Activity_Action_Type__c; 
                        taskList.add(tsk);
                    }                
                }
            } else {
                accountList[0].addError('No Agency Settings record found. An Agency Settings record must exist in order to create this task.');
            }
            insert taskList;
        } catch (Exception e) {
            string message = 'Error: ' + e.getMessage() + '; Stack Trace: ' + e.getStackTraceString();
         //   ApplicationLogger.logMessage('AccountTriggerHandler', 'createTaskForAccountAfterUpdate', message, ApplicationLogger.ERROR);
            throw e;
        }        
    }
    
    // Summary:
    // Verifies that there are no inactive records referenced in the following Account fields:
    //	 Division, Branch, Department, Group, Executive, Representative.
    // If an inactive record is found, an error is added to the record. Otherwise, nothing happens. 
    // 
    // Details:
    // @param account - The account whose fields we're checking.
    private void checkValidBusinessUnits(Account account){
        for (Sagitta_Division__c div : divisions) {
            if (div.Id == account.Sagitta_Division__c && div.Sagitta_isHidden__c == true) {
                addErrorForInvalidBusinessUnits(account, 'Division');
                return;
            }
        }
        for (Sagitta_Branch__c branch : branches) {
            if (branch.Id == account.Sagitta_Branch__c && branch.Sagitta_isHidden__c == true) {
                addErrorForInvalidBusinessUnits(account, 'Branch');
                return;
            }
        }
        for (Sagitta_Department__c dept : departments) {
            if (dept.Id == account.Sagitta_Department__c && dept.Sagitta_isHidden__c == true) {
                addErrorForInvalidBusinessUnits(account, 'Department');
                return;
            }
        }
        for (Sagitta_Group__c grp : groups) {
            if (grp.Id == account.Sagitta_Group__c && grp.Sagitta_isHidden__c == true) {
                addErrorForInvalidBusinessUnits(account, 'Group');
                return;
            }
        }
        for (Sagitta_Employee__c emp : employees) {
            if (emp.Id == account.Sagitta_Employee_Account_Exec__c && emp.Sagitta_Employee_Status__c != 'Active') {
                addErrorForInvalidBusinessUnits(account, 'Executive');
                return;
            }
            if (emp.Id == account.Sagitta_Employee_Account_Rep__c && emp.Sagitta_Employee_Status__c != 'Active') {
                addErrorForInvalidBusinessUnits(account, 'Representative');
                return;
            }
        }
    }
    
    // Summary:
    // Adds an error to the provided account stating that the user's Business Unit selection is invalid.
    // 
    // Details:
    // @param account - The account that we'll add the error to.
    private void addErrorForInvalidBusinessUnits(Account account, String entity) {
        string error = 'Error: Invalid selection for the ' + entity + ' field';
        account.addError(error);
    }
}