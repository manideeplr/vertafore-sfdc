@isTest
public class RestServiceContactContextTest {
	@testSetup
    private static void testSetup() {
        createIntegrationFlag('false');
		createIsInitialSyncFlag('true');
        
        Account relevantAccountOne = new Account();
        relevantAccountOne.Name = 'Relevant Account One';
        relevantAccountOne.Sagitta_Customer_ID__c = 'c5b25a06-f102-47b2-98cb-0f5c2a30c653';

        Account relevantAccountTwo = new Account();
        relevantAccountTwo.Name = 'Relevant Account Two';
        relevantAccountTwo.Sagitta_Customer_ID__c = '64271042-7f57-445d-8f50-c7f02c0511c6';
        
		Contact relevantContactOne = new Contact();
        relevantContactOne.FirstName = 'Contact';
		relevantContactOne.LastName = 'Relevant Contact One';
        relevantContactOne.Sagitta_Customer_ID__c = 'c5b25a06-f102-47b2-98cb-0f5c2a30c653';
        relevantContactOne.Sagitta_Customer_Contact_ID__C = 'f05aef40-bca8-4cc4-bd03-984c28eec080';
        
		Contact relevantContactTwo = new Contact();
        relevantContactTwo.FirstName = 'Contact';
		relevantContactTwo.LastName = 'Relevant Contact Two';
        relevantContactTwo.Sagitta_Customer_ID__c = '64271042-7f57-445d-8f50-c7f02c0511c6';
        relevantContactTwo.Sagitta_Customer_Contact_ID__C = '307c55c7-5127-4d97-9d0c-42269e45f731';
        
        insert relevantAccountOne;
        insert relevantAccountTwo;
        insert relevantContactOne;
        insert relevantContactTwo;
        
        Account irrelevantAccount = new Account();
        irrelevantAccount.Name = 'Irrelevant Account';
        irrelevantAccount.Sagitta_Customer_ID__c = 'feee13bf-5aa0-458a-912d-041b4401db22';
        
		Contact irrelevantContact = new Contact();
        irrelevantContact.FirstName = 'Contact';
		irrelevantContact.LastName = 'Irrelevant';
        irrelevantContact.Sagitta_Customer_ID__c = 'feee13bf-5aa0-458a-912d-041b4401db22';
        irrelevantContact.Sagitta_Customer_Contact_ID__C = '47e43f28-075f-4926-bf90-c79b9704b11c';
        
        insert irrelevantAccount;
        insert irrelevantContact;
    }
    
    @isTest
    private static void findContacts_ShouldOnlyGetRelevantContacts() {
        Contact irrelevantContact = [SELECT Id, FirstName, LastName, Sagitta_Customer_ID__c, Sagitta_Customer_Contact_ID__C
                                     FROM Contact
                                     WHERE LastName = 'Irrelevant'
                                     AND FirstName = 'Contact'];
        
        List<RestServiceToUpsertContactRecords.SagittaContact> amsContacts = createAmsContacts();
        RestServiceToUpsertContactRecords.SagittaContact contactOne = amsContacts[0];
        RestServiceToUpsertContactRecords.SagittaContact contactTwo = amsContacts[1];
        RestServiceContactContext contactContext = new RestServiceContactContext(amsContacts);
        
        Contact relevantOne = contactContext.findContact(contactOne.CCntId);
        Contact relevantTwo = contactContext.findContact(contactTwo.CCntId);
        Contact irrelevant = contactContext.findContact(irrelevantContact.Sagitta_Customer_Contact_ID__C);
        
        System.assertEquals('Relevant Contact One', relevantOne.LastName);
        System.assertEquals('Relevant Contact Two', relevantTwo.LastName);
        System.assertEquals(new Contact(), irrelevant);
    }
    
	@isTest
    private static void findAccounts_ShouldOnlyGetRelevantAccounts() {
        Account irrelevantAccount = [SELECT Id, Name, Sagitta_Customer_ID__c
                                     FROM Account
                                     WHERE Name = 'Irrelevant Account'];
        
        List<RestServiceToUpsertContactRecords.SagittaContact> amsContacts = createAmsContacts();
        RestServiceToUpsertContactRecords.SagittaContact contactOne = amsContacts[0];
        RestServiceToUpsertContactRecords.SagittaContact contactTwo = amsContacts[1];
        RestServiceContactContext contactContext = new RestServiceContactContext(amsContacts);
        
        Account relevantOne = contactContext.findAccount(contactOne.CustomerId);
        Account relevantTwo = contactContext.findAccount(contactTwo.CustomerId);
        Account irrelevant = contactContext.findAccount(irrelevantAccount.Sagitta_Customer_ID__C);
        
        System.assertEquals('Relevant Account One', relevantOne.Name);
        System.assertEquals('Relevant Account Two', relevantTwo.Name);
        System.assertEquals(null, irrelevant);
    }
    
	@isTest
    private static void findContact_ShouldReturnContacts_WithEmptyStrings_InsteadOfNull() {
        List<RestServiceToUpsertContactRecords.SagittaContact> amsContacts = createAmsContacts();
        RestServiceToUpsertContactRecords.SagittaContact contactOne = amsContacts[0];
        RestServiceToUpsertContactRecords.SagittaContact contactTwo = amsContacts[1];
        RestServiceContactContext contactContext = new RestServiceContactContext(amsContacts);
        
        Contact relevantOne = contactContext.findContact(contactOne.CCntId);
        Contact relevantTwo = contactContext.findContact(contactTwo.CCntId);
        
        System.assertEquals('', relevantOne.Email);
        System.assertEquals('', relevantOne.Phone);
        System.assertEquals('', relevantOne.MobilePhone);
        System.assertEquals('', relevantOne.HomePhone);
        System.assertEquals('', relevantOne.Fax);
        System.assertEquals('', relevantOne.MailingStreet);
        System.assertEquals('', relevantOne.MailingCity);
        System.assertEquals('', relevantOne.MailingState);
        System.assertEquals('', relevantOne.MailingPostalCode);
        
        System.assertEquals('', relevantTwo.Email);
        System.assertEquals('', relevantTwo.Phone);
        System.assertEquals('', relevantTwo.MobilePhone);
        System.assertEquals('', relevantTwo.HomePhone);
        System.assertEquals('', relevantTwo.Fax);
        System.assertEquals('', relevantTwo.MailingStreet);
        System.assertEquals('', relevantTwo.MailingCity);
        System.assertEquals('', relevantTwo.MailingState);
        System.assertEquals('', relevantTwo.MailingPostalCode);
    }
    
    private static void createIntegrationFlag(String flag) {
        Metadata__c intFlag = new Metadata__c();
        intFlag.Name = 'IntegrationFlag';
        intFlag.Value__c = flag;
        insert intFlag;
    }
    
    private static void createIsInitialSyncFlag(String flag) {
        Metadata__c isInitialSyncFlag = new Metadata__c();
        isInitialSyncFlag.Name = 'IsInitialSync';
        isInitialSyncFlag.Value__c = flag;
        insert isInitialSyncFlag;
    }
    
    private static List<RestServiceToUpsertContactRecords.SagittaContact> createAmsContacts() {
        List<RestServiceToUpsertContactRecords.SagittaContact> amsContacts = new List<RestServiceToUpsertContactRecords.SagittaContact>();
        
        RestServiceToUpsertContactRecords.SagittaContact contactOne = new RestServiceToUpsertContactRecords.SagittaContact();
        contactOne.CustomerId = 'c5b25a06-f102-47b2-98cb-0f5c2a30c653';
        contactOne.CCntId = 'f05aef40-bca8-4cc4-bd03-984c28eec080';
        contactOne.ContactName = 'Contact One';
        contactOne.EMail = 'contactOne@example.com';
        contactOne.BusinessAreaCode = '303';
        contactOne.BusinessExtension = '123';
        contactOne.BusinessPhone = '592-1033';
        contactOne.MobileAreaCode = '303';
        contactOne.MobileExtension = '321';
        contactOne.MobilePhone = '822-9105';
        contactOne.ResidenceAreaCode = '303';
        contactOne.ResidenceExtension = '';
        contactOne.ResidencePhone = '449-1002';
        contactOne.FaxAreaCode = '303';
        contactOne.FaxExtension = '444';
        contactOne.FaxPhone = '893-3332';
        contactOne.Address1 = '923 Main St';
        contactOne.Address2 = 'Unit C';
        contactOne.City = 'Denver';
		contactOne.State = 'CO';
		contactOne.ZipCode = '80202';
        
        RestServiceToUpsertContactRecords.SagittaContact contactTwo = new RestServiceToUpsertContactRecords.SagittaContact();
        contactTwo.CustomerId = '64271042-7f57-445d-8f50-c7f02c0511c6';
        contactTwo.CCntId = '307c55c7-5127-4d97-9d0c-42269e45f731';
        contactTwo.ContactName = 'Contact Two';
        contactTwo.EMail = 'contactTwo@example.com';
        contactTwo.BusinessAreaCode = '303';
        contactTwo.BusinessExtension = '987';
        contactTwo.BusinessPhone = '003-1028';
        contactTwo.MobileAreaCode = '303';
        contactTwo.MobileExtension = '789';
        contactTwo.MobilePhone = '220-4239';
        contactTwo.ResidenceAreaCode = '303';
        contactTwo.ResidenceExtension = '';
        contactTwo.ResidencePhone = '021-1448';
        contactTwo.FaxAreaCode = '303';
        contactTwo.FaxExtension = '777';
        contactTwo.FaxPhone = '304-1100';
        contactTwo.Address1 = '10001 Broadway St';
        contactTwo.Address2 = 'Apt H';
        contactTwo.City = 'Denver';
		contactTwo.State = 'CO';
		contactTwo.ZipCode = '80202';
        
        amsContacts.add(contactOne);
        amsContacts.add(contactTwo);
        
        return amsContacts;
    }
}