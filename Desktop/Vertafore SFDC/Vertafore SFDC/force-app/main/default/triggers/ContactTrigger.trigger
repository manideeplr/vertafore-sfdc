/*
    Author : Ben Baik
    Name : ContactTrigger
    Date : Prior to 14 June 2019
    Description: Watches for Contact Events
    Modification History :
    *************************************************************
    Version             Author          Date                        Description
    1                   Brad            17 June 2019                Refactoring best practices
	1.1					  Ben				 28 June 2019			   Added IsInitialSync flag check
*/
trigger ContactTrigger on Contact (before update, after update, after insert) {
    Metadata__c initialSyncFlag = Metadata__c.getValues('IsInitialSync');
    
    if (initialSyncFlag.Value__c == 'false') {
        ContactTriggerHandler contactTriggerHandler = new ContactTriggerHandler();
        contactTriggerHandler.handleTrigger(Trigger.new, Trigger.oldMap, Trigger.operationType);   
    }
}