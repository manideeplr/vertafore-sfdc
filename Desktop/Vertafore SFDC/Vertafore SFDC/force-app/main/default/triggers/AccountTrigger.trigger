/*
    Author : 
	Name : AccountTrigger
	Date : 
	Description : Trigger for Account. Logic is handled in AccountTriggerHandler class.
	Modification History :
	*************************************************************
	Version               Author             Date                      Description
	
*/
trigger AccountTrigger on Account (before insert, after update, before update) {
    Metadata__c initialSyncFlag = Metadata__c.getValues('IsInitialSync');
    
    if (initialSyncFlag.Value__c == 'false') {
        AccountTriggerHandler accountTriggerHandler = new AccountTriggerHandler();
        accountTriggerHandler.handleTrigger(Trigger.new, Trigger.operationType);   
    }
}