/*
    Author : Manideep
    Name : SagittaAgencySettingsTrigger
    Date : 1 November 2019
    Description : Trigger for Agency Settings to prevent the user from editing certain fields or creating a new record.
    Modification History :
    *************************************************************
    Version               Author             Date                      Description
    1                    Manideep         1 November 2019                Original
*/
trigger SagittaAgencySettingsTrigger on Sagitta_Agency_Settings__c (before insert, before update) {
    Metadata__c initialSyncFlag = Metadata__c.getValues('IsInitialSync');
    
    if (initialSyncFlag.Value__c == 'false') {
        boolean agencySettingsRecordExists = false;
        
        try {
            agencySettingsRecordExists = [SELECT COUNT() FROM Sagitta_Agency_Settings__c LIMIT 1] > 0;
        } catch (Exception e) {
            string message = 'Error: ' + e.getMessage() + '; Stack Trace: ' + e.getStackTraceString();
            ApplicationLogger.logMessage('SagittaAgencySettingsTrigger', 'SagittaAgencySettingsTrigger', message, ApplicationLogger.ERROR);
            throw e;
        }
     
        // For each record we're trying to add/update
        for (Sagitta_Agency_Settings__c newRecord : Trigger.New) {
            // If we're inserting a record
            if (Trigger.isInsert) {
                // If an Agency Settings record already exists on current org
                if (agencySettingsRecordExists) {
                    newRecord.addError('An Agency Settings record already exists in this organization.');
                    continue;
                }
            }
        }
    }
}