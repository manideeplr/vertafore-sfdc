/*
    Author : Manideep
    Name : ContactTrigger
    Date : 1 November 2019
    Description: Watches for Contact Events
    Modification History :
    *************************************************************
    Version             Author          Date                        Description
    1                  Manideep       1 November 2019                 Original
*/
trigger ContactTrigger on Contact (before update, after update, after insert) {
    Metadata__c initialSyncFlag = Metadata__c.getValues('IsInitialSync');
    
    if (initialSyncFlag.Value__c == 'false') {
        ContactTriggerHandler contactTriggerHandler = new ContactTriggerHandler();
        contactTriggerHandler.handleTrigger(Trigger.new, Trigger.oldMap, Trigger.operationType);   
    }
}