/*
    Author : Manideep
    Name : OppTrigger
    Date : 1 November 2019
    Description: Watches for Opportunity Events
    Modification History :
    *************************************************************
    Version             Author          Date                        Description
    1                  Manideep       1 November 2019                 Original
*/
trigger OppTrigger on Opportunity (before update, after update, before insert, after insert) {
    Metadata__c initialSyncFlag = Metadata__c.getValues('IsInitialSync');
    
    if (initialSyncFlag != null && initialSyncFlag.Value__c == 'false') {
        OppTriggerHandler oppTriggerHandler = new OppTriggerHandler();
        oppTriggerHandler.handleTrigger(Trigger.new, Trigger.oldMap, Trigger.operationType);   
    }
}