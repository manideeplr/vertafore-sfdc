/*
    Author : Manideep
    Name : SagittaActivityActionTypeTrigger
    Date : 1 November 2019
    Description : Trigger for Sagitta Activity Action Type to prevent the user from updating fields or creating a new record.
    Modification History :
    *************************************************************
    Version               Author             Date                      Description
    1                    Manideep             1 November 2019                 Original
*/

trigger SagittaActivityActionTypeTrigger on Sagitta_Activity_Action_Type__c (before insert, before update) {
    Metadata__c initialSyncFlag = Metadata__c.getValues('IsInitialSync');
    
    if (initialSyncFlag.Value__c == 'false') {
        boolean valuesHaveChanged = false;
        boolean comingFromAPI = false;
        
        // See if our request is coming from the API
        if (String.valueOf(URL.getCurrentRequestUrl()).toLowerCase().contains('services/data') || 
            String.valueOf(URL.getCurrentRequestUrl()).toLowerCase().contains('services/apexrest') ||
            Test.isRunningTest()) {
            comingFromAPI = true;
        }
     
        // For each record we're trying to add/update
        for (Sagitta_Activity_Action_Type__c newRecord : Trigger.New) {
            // If we're inserting a record
            if (Trigger.isInsert) {
                if (!comingFromAPI) {
                    newRecord.addError('User cannot manually create Activity Action Type object.');
                    continue;
                }
            }
            // else if we're updating a record
            else {
                // Get old record
                Sagitta_Activity_Action_Type__c oldRecord = Trigger.oldMap.get(newRecord.ID);
                
                // If old record and new record have different field values
                if (newRecord.Name != oldRecord.Name) {
                    valuesHaveChanged = true;
                }
            }
            
            if (valuesHaveChanged && !comingFromAPI) {
                newRecord.addError('The following fields cannot be changed: Name');
            }
        } 
    }
}