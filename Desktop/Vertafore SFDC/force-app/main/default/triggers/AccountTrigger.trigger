/*
    Author : Manideep
	Name : AccountTrigger
	Date : 1 November
	Description : Trigger for Account. Logic is handled in AccountTriggerHandler class.
	Modification History :
	*************************************************************
	Version               Author             Date                      Description
	   1                 Manideep         1 November 2019               Original
*/
trigger AccountTrigger on Account (before insert, after update, before update) {
    Metadata__c initialSyncFlag = Metadata__c.getValues('IsInitialSync');
    
    if (initialSyncFlag.Value__c == 'false') {
        AccountTriggerHandler accountTriggerHandler = new AccountTriggerHandler();
        accountTriggerHandler.handleTrigger(Trigger.new, Trigger.operationType);   
    }
}