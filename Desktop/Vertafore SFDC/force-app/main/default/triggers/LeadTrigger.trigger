/*
    Author : Manideep
    Name : TaskTrigger
    Date : 1 November 2019
    Description: Watches for Lead Events
    Modification History :
    *************************************************************
    Version             Author          Date                        Description
    1                  Manideep      1 November 2019                  Original
*/
trigger LeadTrigger on Lead (before update, before insert) {
    Metadata__c initialSyncFlag = Metadata__c.getValues('IsInitialSync');
    
    if (initialSyncFlag.Value__c == 'false') {
        Sagitta_Mapping__c mapping;
        boolean comingFromAPI = false;
        
        // Get default Mapping for user
        List<Sagitta_Mapping__c> mappingList = 
            [SELECT Id, Sagitta_Division__c, Sagitta_Branch__c, Sagitta_Department__c, Sagitta_Group__c, Sagitta_Executive__c, Sagitta_Representative__c
             FROM Sagitta_Mapping__c 
             WHERE Sagitta_Assigned_To__c =: UserInfo.getUserId()];
        
        if (mappingList.size() > 0) {
            mapping = mappingList[0];
        }
        
        // See if request is coming from the API.
        if (String.valueOf(URL.getCurrentRequestUrl()).toLowerCase().contains('services/data') || 
            String.valueOf(URL.getCurrentRequestUrl()).toLowerCase().contains('services/apexrest')) {
                comingFromAPI = true;
        }
        
        // Handle insert
        if (Trigger.isInsert) {
            try {
                // Prevent the user from creating a lead if they don't have a default Mapping
                if (mapping == null && !comingFromAPI) {
                    for (Lead newRecord : Trigger.New) {
                        newRecord.addError('No Sagitta Mapping record found. An Sagitta Mapping record must be assigned to this user in order to create a Lead.');
                    }
                return;
                }
            
                // For each record we're inserting
                for (Lead newRecord : Trigger.New) {
                    // Populate Exec and Rep if the user didn't specify them
                    if (Trigger.isInsert && mapping != null) {
                        if (String.isBlank(newRecord.Sagitta_Employee_Account_Exec__c)){
                            newRecord.Sagitta_Employee_Account_Exec__c = mapping.Sagitta_Executive__c;
                        }
                        if (String.isBlank(newRecord.Sagitta_Employee_Account_Rep__c)){
                            newRecord.Sagitta_Employee_Account_Rep__c = mapping.Sagitta_Representative__c;
                        }
                    }
                }
                return;
            } catch (Exception ex) {
                String message = 'Lead Insert Error; ' +
                    'Error inserting Lead record ' + ex.getMessage() + '; Stack Trace: ' + ex.getStackTraceString();
             //   ApplicationLogger.logMessage('LeadTrigger ', 'isInsert ', message, ApplicationLogger.ERROR);
                throw ex;
            }
        }
        
        // Handle update
        if (Trigger.isUpdate) {
            try {
                // For each record we're updating
                for (Lead newRecord : Trigger.New) {
                    // If user does not have a default Mapping
                    if (mapping == null) {
                        newRecord.addError('User does not have an Sagitta Mapping record. Please navigate to the Business Units tab to set the values for this user\'s default Sagitta Mapping record.');
                        return;
                    }
                    // else if the user has a default Mapping record
                    else {
                        // If no values have been copied over to our hidden Business Unit Lead fields
                        if (String.isBlank(newRecord.Sagitta_Division__c)) { // (we only bother to check one field for efficiency)
                            newRecord.Sagitta_Division__c = mapping.Sagitta_Division__c;
                            newRecord.Sagitta_Branch__c = mapping.Sagitta_Branch__c;
                            newRecord.Sagitta_Department__c = mapping.Sagitta_Department__c;
                            newRecord.Sagitta_Group__c = mapping.Sagitta_Group__c;
                        }
                        
                        // If Exec and Rep haven't been filled in
                        if (String.isBlank(newRecord.Sagitta_Employee_Account_Exec__c)){
                            newRecord.Sagitta_Employee_Account_Exec__c = mapping.Sagitta_Executive__c;
                        }
                        if (String.isBlank(newRecord.Sagitta_Employee_Account_Rep__c)){
                            newRecord.Sagitta_Employee_Account_Rep__c = mapping.Sagitta_Representative__c;
                        }
                    }
                }
            } catch (Exception ex) {
                String message = 'Lead Update Error; ' +
                    'Error updating Lead record.' + ex.getMessage() + '; Stack Trace: ' + ex.getStackTraceString();
             //   ApplicationLogger.logMessage('LeadTrigger', 'isUpdate', message, ApplicationLogger.ERROR);
                throw ex;
            }
    	}   
    }
}