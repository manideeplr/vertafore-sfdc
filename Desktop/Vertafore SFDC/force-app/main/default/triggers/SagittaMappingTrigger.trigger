/*
    Author : Manideep
	Name : SagittaMappingTrigger
	Date : 1 November 2019
	Description : Trigger for Sagitta Mapping. When a Mapping record is created, this trigger will set the owner of the
				  new record to the same user as the "Assign To" field. This trigger will also prevent the user from creating
				  a Mapping record for a user who already has one assigned to them.
	Modification History :
	*************************************************************
	Version               Author             Date                      Description
	1                    Manideep         1 November 2019             Original
*/
trigger SagittaMappingTrigger on Sagitta_Mapping__c (before insert, before update) {
    Metadata__c initialSyncFlag = Metadata__c.getValues('IsInitialSync');
    
    if (initialSyncFlag.Value__c == 'false') {
        boolean duplicateFound = false;
        Sagitta_Mapping__c[] mappings;
        
        try {
            mappings = [SELECT Sagitta_Assigned_To__c FROM Sagitta_Mapping__c WHERE Name != null];
        } catch (Exception e) {
            string message = 'Error checking AssignedTo on Mapping record' + e.getMessage() + '; Stack Trace: ' + e.getStackTraceString();
            ApplicationLogger.logMessage('SagittaMappingTrigger', 'SagittaMappingTrigger', message, ApplicationLogger.ERROR);
            throw e;
        }
        
        // For each Sagitta Mapping we're trying to add/update
		for (Sagitta_Mapping__c newMapping : Trigger.New) {
            // Continue if we're not changing Assigned_To__c
            if (Trigger.isUpdate && Trigger.isBefore && newMapping.Sagitta_Assigned_To__c == newMapping.OwnerId) {
                continue;
            }
        
            // Search for duplicates
            for (Sagitta_Mapping__c mapping : mappings) {
                if (mapping.Sagitta_Assigned_To__c == newMapping.Sagitta_Assigned_To__c){
                    duplicateFound = true;
                }
            }
            
            // If we found a duplicate, print an error
            if (duplicateFound) {
                newMapping.addError('Mapping already exists for specified user.');
            }
            
            // Set the new owner
            newMapping.OwnerId = newMapping.Sagitta_Assigned_To__c;
        }   
    }
}