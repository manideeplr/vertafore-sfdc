({
	doInit : function(component, event, helper) {
		
        var action = component.get("c.getAllLobs");
        var action2 = component.get("c.getAllLobFromJn");
        action.setParams({
            "oppId" : component.get("v.recordId")
        });
        action2.setParams({
            "oppId" : component.get("v.recordId")
        });
        action.setCallback(this, function(response){
           	var state = response.getState();
             
            if(state == "SUCCESS") {
                component.set("v.lobsNew", response.getReturnValue());
            }
        });
        action2.setCallback(this, function(response){
           	var state = response.getState();
          
            if(state == "SUCCESS") {
                var jnList = response.getReturnValue();
                var lobNew = component.get("v.lobsNew");
                var lobExisting = component.get("v.lobsExisting");
                for (var i = 0; i < lobNew.length; i++) {
                    if(jnList.indexOf(lobNew[i].Id) > -1) {
                        lobExisting.push(lobNew[i]);
                    }
                }
                component.set("v.lobsExisting", lobExisting);
                for (var i = 0; i < lobNew.length; i++) {
                    if(jnList.indexOf(lobNew[i].Id) > -1) {
                        lobNew.splice(i, 1);
                    }
                }
                component.set("v.lobsNew", lobNew);
            }
        });
        
        $A.enqueueAction(action);
        $A.enqueueAction(action2);
	},
    
    onCheck : function(component, event, helper) {
        var lobsToAdd = component.get("v.lobsToAdd");
        var lobsToDelete = component.get("v.lobsToDelete");
        
        if(event.getSource().get("v.checked")) {
            var item = event.getSource().get("v.value");
            lobsToAdd.push(item);
            if(lobsToDelete.indexOf(item) > -1) {
                lobsToDelete.splice(lobsToDelete.indexOf(item), 1)
            }
        } else {
            var item = event.getSource().get("v.value");
            lobsToDelete.push(item);
            if(lobsToAdd.indexOf(item) > -1) {
                lobsToAdd.splice(lobsToAdd.indexOf(item), 1)
            }
        }
        var toastEvent = $A.get("e.force:showToast");
        toastEvent.setParams({
            title : 'Info Message',
            message: 'Please cick Save button after selection',
            type: 'info',
            mode: 'dismissible'
        });
        toastEvent.fire();
        component.set("v.lobsToAdd", lobsToAdd);
        component.set("v.lobsToDelete", lobsToDelete);
    },
    
    onSave : function(component, event, helper) {
        var action = component.get("c.updateLOBs");
        action.setParams({
            "oppId" : component.get("v.recordId"),
            "lobsToAdd" : component.get("v.lobsToAdd"),
            "lobsToDelete" : component.get("v.lobsToDelete")
        });
        action.setCallback(this, function(response){
           	var state = response.getState();
            
            if(state == "SUCCESS") {
                component.set("v.lobsToAdd", []);
                component.set("v.lobsToDelete", []);
                var toastEvent = $A.get("e.force:showToast");
                toastEvent.setParams({
                    title : 'Success!',
                    message: 'Save successful',
                    type: 'success',
                    mode: 'dismissible'
                });
                toastEvent.fire();
            }
        });
        
        $A.enqueueAction(action);
    }
})