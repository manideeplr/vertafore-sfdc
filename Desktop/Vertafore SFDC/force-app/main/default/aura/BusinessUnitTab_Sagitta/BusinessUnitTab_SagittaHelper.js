({
    getBusinessUnits : function(cmp, args) {
        var action = cmp.get(args.apexMethod);
        var callbackFunction = function(response) {
            switch(response.getState()) {
                case "SUCCESS":
                    var res = response.getReturnValue();
                    var businessUnits = [];
                    for (var i = 0; i < res.length; i++) {
                        var bu = {
                            "label": res[i],
                            "value": res[i]
                        };
                        businessUnits.push(bu);
                    }
                    
                    cmp.set(args.auraAttrList, businessUnits);
                 
                    break;
                case "INCOMPLETE":
                    console.log("Incomplete");
                    break;
                case "ERROR":
                    console.log("Error");
                    break;
                default:
                    console.log("Default");
            }
        };
        if (args.params) {
            action.setParams(args.params);
        }
        action.setCallback(this, callbackFunction);
        $A.enqueueAction(action);
    },
    clearFields: function(cmp, args) {
        var divPicklist = "Sagitta_Division__r";
        var branchPicklist = "Sagitta_Branch__r";
        var deptPicklist = "Sagitta_Department__r";
        
        var auraAttrDivInput = "v.divInput";
        var auraAttrBranchInput = "v.branchInput";
        var auraAttrDeptInput = "v.deptInput";
        var auraAttrGroupInput = "v.groupInput";
        
        if (args.params.topLevel === divPicklist) {
            cmp.set(auraAttrBranchInput, null);
            cmp.set(auraAttrDeptInput, null);
            cmp.set(auraAttrGroupInput, null);
        } else if (args.params.topLevel === branchPicklist) {
            cmp.set(auraAttrDeptInput, null);
            cmp.set(auraAttrGroupInput, null);
        } else if (args.params.topLevel === deptPicklist) {
            cmp.set(auraAttrGroupInput, null);
        }
    },
    getSavedMapping: function(cmp, args) {
        var url = decodeURIComponent(window.location);
        var type;
        var id;
        if (url.includes("Lead/")) {
        	var split = url.split("Lead/");
            id = split[1].substring(0, split[1].indexOf("/"));
            type = "lead";
            cmp.set("v.recordType", type);
        } else if (url.includes("Account/")) {
        	var split = url.split("Account/");
            id = split[1].substring(0, split[1].indexOf("/"));
            type = "account";
            cmp.set("v.recordType", type);
        }  else if (url.includes("Opportunity/")) {
        	var split = url.split("Opportunity/");
            id = split[1].substring(0, split[1].indexOf("/"));
            type = "opportunity";
			cmp.set("v.recordType", type);
        } else if (url.includes("Sagitta_Mapping__c/")) {
            var split = url.split("Sagitta_Mapping__c/");
            id = split[1].substring(0, split[1].indexOf("/"));
            type = "Sagitta_Mapping__c";
			cmp.set("v.recordType", type);
        }
        cmp.set("v.recordId", id);
        var action = cmp.get("c.getRecord");
        action.setParams({
            "recordId": id,
            "type": type
        })
        action.setCallback(this, function(response){  
            var state = response.getState();
           // alert(JSON.stringify(response.getReturnValue()));
           // alert(state);
            if (state === "SUCCESS") {
                var businessUnitNames = response.getReturnValue();
                cmp.set("v.divPrepopulate", businessUnitNames[0]);
                cmp.set("v.branchPrepopulate", businessUnitNames[1]);
                cmp.set("v.deptPrepopulate", businessUnitNames[2]);
                cmp.set("v.groupPrepopulate", businessUnitNames[3]);
            } else {
                console.log("getMapping failed");
            }
        });
        $A.enqueueAction(action);
    },
    getSObjectId: function(url){
        var url = decodeURIComponent(window.location);
    	var id;
        if (url.includes("Lead/")) {
        	var split = url.split("Lead/");
            id = split[1].substring(0, split[1].indexOf("/"));
        } else if (url.includes("Account/")) {
        	var split = url.split("Account/");
            id = split[1].substring(0, split[1].indexOf("/"));
        }  else if (url.includes("Opportunity/")) {
        	var split = url.split("Opportunity/");
            id = split[1].substring(0, split[1].indexOf("/"));
        }  else if (url.includes("Sagitta_Mapping__c/")) {
            var split = url.split("Sagitta_Mapping__c/");
            id = split[1].substring(0, split[1].indexOf("/"));
        }  else {
    		id = null;
		}
    	return id;
	}
})