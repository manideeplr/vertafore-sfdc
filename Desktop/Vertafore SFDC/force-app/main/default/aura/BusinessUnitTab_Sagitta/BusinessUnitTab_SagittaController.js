({
    init: function(cmp, event, helper) {
        var divArgs = {
            apexMethod: "c.initDivisionList",
            auraAttrList: "v.divisions",
            auraAttrInput: "v.divInput",
            params: {
                selectInput: "v.divPrepopulate"
            }
        }
        helper.getBusinessUnits(cmp, divArgs);
        var args = {
            apexMethod: "c.getRecord",
            auraDivPopulate: "v.divPrepopulate",
            auraBranchPopulate: "v.branchPopulate",
            auraDeptPopulate: "v.deptPopulate",
            auraGroupPopulate: "v.groupPopulate",
            params: null,
        };
        helper.getSavedMapping(cmp, args);
    },
    prepopulateDivision: function(cmp, event, helper) {
        var div = cmp.get("v.divPrepopulate");
        cmp.set("v.divInput", div);
        var args = {
            apexMethod: "c.getBUList",
            auraAttrList: "v.branches",
            auraAttrInput: "v.divInput",
            params: {
            	selectInput : event.getParam("value"),
                topLevel : 'Sagitta_Division__r',
                lowLevel : 'Sagitta_Branch__c',
                junction : 'DivBrJunction__c'
            }
        };
        helper.getBusinessUnits(cmp, args);
        helper.clearFields(cmp, args);
    },
    prepopulateBranch: function(cmp, event, helper) {
        var branch = cmp.get("v.branchPrepopulate");
        cmp.set("v.branchInput", branch);
        var args = {
            apexMethod: "c.getBUList",
            auraAttrList: "v.departments",
            auraAttrInput: "v.branchInput",
            params: {
                selectInput : event.getParam("value"),
                topLevel : 'Sagitta_Branch__r',
                lowLevel : 'Sagitta_Department__c',
                junction : 'BrDepJunction__c'
            }
        };
        helper.getBusinessUnits(cmp, args);
        helper.clearFields(cmp, args);
    },
    prepopulateDept: function(cmp, event, helper) {
        var dept = cmp.get("v.deptPrepopulate");
        cmp.set("v.deptInput", dept);
        var args = {
            apexMethod: "c.getBUList",
            auraAttrList: "v.groups",
            auraAttrInput: "v.deptInput",
            params: {
                selectInput : event.getParam("value"),
                topLevel : 'Sagitta_Department__r',
                lowLevel : 'Sagitta_Group__c',
                junction : 'DepGrpJunction__c'
            }
        };
        helper.getBusinessUnits(cmp, args);
        helper.clearFields(cmp, args);
    },
    prepopulateGroup: function(cmp, event) {
        var group = cmp.get("v.groupPrepopulate");
        cmp.set("v.groupInput", group);
    },
    handleDivision: function(cmp, event, helper) {
        var args = {
            apexMethod: "c.getBUList",
            auraAttrList: "v.branches",
            auraAttrInput: "v.divInput",
            params: {
            	selectInput : event.getParam("value"),
                topLevel : 'Sagitta_Division__r',
                lowLevel : 'Sagitta_Branch__c',
                junction : 'DivBrJunction__c'
            }
        };
        helper.getBusinessUnits(cmp, args);
        helper.clearFields(cmp, args);
    },
    handleBranch: function(cmp, event, helper) {
        var args = {
            apexMethod: "c.getBUList",
            auraAttrList: "v.departments",
            auraAttrInput: "v.branchInput",
            params: {
                selectInput : event.getParam("value"),
                topLevel : 'Sagitta_Branch__r',
                lowLevel : 'Sagitta_Department__c',
                junction : 'BrDepJunction__c'
            }
        };
        helper.getBusinessUnits(cmp, args);
        helper.clearFields(cmp, args);
    },
    handleDept: function(cmp, event, helper) {
        var args = {
            apexMethod: "c.getBUList",
            auraAttrList: "v.groups",
            auraAttrInput: "v.deptInput",
            params: {
                selectInput : event.getParam("value"),
                topLevel : 'Sagitta_Department__r',
                lowLevel : 'Sagitta_Group__c',
                junction : 'DepGrpJunction__c'
            }
        };
        helper.getBusinessUnits(cmp, args);
        helper.clearFields(cmp, args);
    },
    handleGroup: function(cmp, event) {
        var selectInput = event.getParam("value");
        var auraAttrInput = "v.groupInput";
        cmp.set(auraAttrInput, selectInput);
    },
    save: function(cmp, event, helper) {
        var auraAttrDivInput = "v.divInput";
        var auraAttrBranchInput = "v.branchInput";
        var auraAttrDeptInput = "v.deptInput";
        var auraAttrGroupInput = "v.groupInput";
        var appEvent = $A.get("e.vertsagitta:MessageEvent");
        var sPageURL = decodeURIComponent(window.location);
		var id = helper.getSObjectId(sPageURL);
        var action = cmp.get("c.saveBusinessUnits");
        action.setParams({
            "divName": cmp.get(auraAttrDivInput),
            "branchName": cmp.get(auraAttrBranchInput),
            "deptName": cmp.get(auraAttrDeptInput),
            "grpName": cmp.get(auraAttrGroupInput),
            "sObjectId": id,
            "recordType": cmp.get("v.recordType")
        });
        
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                var idx = response.getReturnValue();
                var toastEvent = $A.get("e.force:showToast");
                toastEvent.setParams({
                    title : 'Success!',
                    message: 'Save successful',
                    type: 'success',
                    mode: 'dismissible'
                });
                toastEvent.fire();
            } else if (state === "INCOMPLETE") {
                console.log("Incomplete");
            } else if (state === "ERROR") {
                var errors = response.getError();
                var msg = 'Unknown Error';
                if(errors) {
                    alert("Error: " + errors[0].message)
                    if (errors[0] && errors[0].message) {
                        msg = errors[0].message;//Fetching Custom Message.
                    }
                }
                appEvent.setParams({
                    "appMessageType":"error",
                    "appMessage": msg,
                    "appMsgDisplay":"display: block;"
                });
                appEvent.fire();
            } else {
                console.log("Default");
            }
        });
        
        $A.enqueueAction(action);
    }
    
})