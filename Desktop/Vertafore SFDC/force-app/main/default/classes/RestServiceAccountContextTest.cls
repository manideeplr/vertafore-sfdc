@isTest
public class RestServiceAccountContextTest {
	@testSetup
    private static void testSetup() {
        createIntegrationFlag('false');
        createIsInitialSyncFlag('true');
        
        Sagitta_Employee__c relevantEmployee = new Sagitta_Employee__c();
        relevantEmployee.Name = 'Relevant Employee';
		relevantEmployee.Sagitta_Is_Executive__c = true;
        relevantEmployee.Sagitta_Is_Representative__c = true;
		relevantEmployee.Sagitta_Employee_Code__c = '064033034';
		relevantEmployee.Sagitta_Entity_ID__c = '@!"';
		relevantEmployee.Sagitta_Employee_Status__c = 'Active';
        
        Sagitta_Division__c relevantDivision = new Sagitta_Division__c();
        relevantDivision.Name = 'Relevant Division';
        relevantDivision.Sagitta_IsHidden__c = false;
        relevantDivision.ItemCode__c = '041041042';
        
        Sagitta_Branch__c relevantBranch = new Sagitta_Branch__c();
        relevantBranch.Name = 'Relevant Branch';
        relevantBranch.Sagitta_IsHidden__c = false;
        relevantBranch.ItemCode__c = '119033104';
        
        Sagitta_Department__c relevantDept = new Sagitta_Department__c();
        relevantDept.Name = 'Relevant Dept';
        relevantDept.Sagitta_IsHidden__c = false;
        relevantDept.ItemCode__c = '051120034';
        
        Sagitta_Group__c relevantGroup = new Sagitta_Group__c();
        relevantGroup.Name = 'Relevant Group';
        relevantGroup.Sagitta_IsHidden__c = false;
        relevantGroup.ItemCode__c = '111043036';
        
        insert relevantEmployee;
        insert relevantDivision;
        insert relevantBranch;
        insert relevantDept;
        insert relevantGroup;
        
        Sagitta_Employee__c irrelevantEmployee = new Sagitta_Employee__c();
        irrelevantEmployee.Name = 'Irrelevant Employee';
		irrelevantEmployee.Sagitta_Is_Executive__c = true;
        irrelevantEmployee.Sagitta_Is_Representative__c = true;
		irrelevantEmployee.Sagitta_Employee_Code__c = '040040037';
		irrelevantEmployee.Sagitta_Entity_ID__c = '((%';
		irrelevantEmployee.Sagitta_Employee_Status__c = 'Active';
        
        Sagitta_Division__c irrelevantDivision = new Sagitta_Division__c();
        irrelevantDivision.Name = 'Irrelevant Division';
        irrelevantDivision.Sagitta_IsHidden__c = false;
        irrelevantDivision.ItemCode__c = '047042033';
        
        Sagitta_Branch__c irrelevantBranch = new Sagitta_Branch__c();
        irrelevantBranch.Name = 'Irrelevant Branch';
        irrelevantBranch.Sagitta_IsHidden__c = false;
        irrelevantBranch.ItemCode__c = '034421047';
        
        Sagitta_Department__c irrelevantDept = new Sagitta_Department__c();
        irrelevantDept.Name = 'Irrelevant Dept';
        irrelevantDept.Sagitta_IsHidden__c = false;
        irrelevantDept.ItemCode__c = '042033098';
        
		Sagitta_Group__c irrelevantGroup = new Sagitta_Group__c();
        irrelevantGroup.Name = 'Irrelevant Group';
        irrelevantGroup.Sagitta_IsHidden__c = false;
        irrelevantGroup.ItemCode__c = '045110034';
        
        insert irrelevantEmployee;
        insert irrelevantDivision;
        insert irrelevantBranch;
        insert irrelevantDept;
        insert irrelevantGroup;
        
        // accounts must be inserted last because id's do not generate until after insert
        Account relevantAccountOne = new Account();
        relevantAccountOne.Name = 'Relevant Account One';
        relevantAccountOne.Sagitta_Customer_ID__c = 'c5b25a06-f102-47b2-98cb-0f5c2a30c653';
        relevantAccountOne.Sagitta_Employee_Account_Rep__c = relevantEmployee.Id;
        relevantAccountOne.Sagitta_Employee_Account_Exec__c = relevantEmployee.Id;
        relevantAccountOne.Sagitta_Division__c = relevantDivision.Id;
        relevantAccountOne.Sagitta_Branch__c = relevantBranch.Id;
        relevantAccountOne.Sagitta_Department__c = relevantDept.Id;
        relevantAccountOne.Sagitta_Group__c = relevantGroup.Id;
        
        Account relevantAccountTwo = new Account();
        relevantAccountTwo.Name = 'Relevant Account Two';
        relevantAccountTwo.Sagitta_Customer_ID__c = 'feee13bf-5aa0-458a-912d-041b4401db22';
        relevantAccountTwo.Sagitta_Employee_Account_Rep__c = relevantEmployee.Id;
        relevantAccountTwo.Sagitta_Employee_Account_Exec__c = relevantEmployee.Id;
        relevantAccountTwo.Sagitta_Division__c = relevantDivision.Id;
        relevantAccountTwo.Sagitta_Branch__c = relevantBranch.Id;
        relevantAccountTwo.Sagitta_Department__c = relevantDept.Id;
        relevantAccountTwo.Sagitta_Group__c = relevantGroup.Id;
        
        Account irrelevantAccount = new Account();
        irrelevantAccount.Name = 'Irrelevant Account';
        irrelevantAccount.Sagitta_Customer_ID__c = '4ca613kq-2nx8-00dk-aa0f-af9m0kjajm0z';
        irrelevantAccount.Sagitta_Employee_Account_Rep__c = irrelevantEmployee.Id;
        irrelevantAccount.Sagitta_Employee_Account_Exec__c = irrelevantEmployee.Id;
        irrelevantAccount.Sagitta_Division__c = irrelevantDivision.Id;
        irrelevantAccount.Sagitta_Branch__c = irrelevantBranch.Id;
        irrelevantAccount.Sagitta_Department__c = irrelevantDept.Id;
        irrelevantAccount.Sagitta_Group__c = irrelevantGroup.Id;
        
        insert relevantAccountOne;
        insert relevantAccountTwo;
        insert irrelevantAccount;
    }
    
    @isTest
    private static void findEmployee_ShouldOnlyGetRelevantEmployees() {
        Sagitta_Employee__c irrelevantEmployee = [SELECT Id, Name, Sagitta_Employee_Code__c
                                                 FROM Sagitta_Employee__c
                                                 WHERE Name = 'Irrelevant Employee'];
        
        List<RestServiceToUpsertAcctRecords.SagittaCustomer> customers = createCustomers();
        RestServiceToUpsertAcctRecords.SagittaCustomer customerOne = customers[0];
        RestServiceToUpsertAcctRecords.SagittaCustomer customerTwo = customers[1];
        RestServiceAccountContext accountContext = new RestServiceAccountContext(customers);
        
        Sagitta_Employee__c relevantOne = accountContext.findEmployee(customerOne.AccountExecCode);
        Sagitta_Employee__c relevantTwo = accountContext.findEmployee(customerTwo.AccountRepCode);
        Sagitta_Employee__c irrelevant = accountContext.findEmployee(irrelevantEmployee.Sagitta_Employee_Code__c);
        
        System.assertEquals('Relevant Employee', relevantOne.Name);
        System.assertEquals('Relevant Employee', relevantTwo.Name);
        System.assertEquals(null, irrelevant);
    }
    
    @isTest
    private static void findDivision_ShouldOnlyGetRelevantDivisions() {        
        Sagitta_Division__c irrelevantDivision = [SELECT Id, Name, ItemCode__c
                                                 FROM Sagitta_Division__c
                                                 WHERE Name = 'Irrelevant Division'];
        
        List<RestServiceToUpsertAcctRecords.SagittaCustomer> customers = createCustomers();
        RestServiceToUpsertAcctRecords.SagittaCustomer customerOne = customers[0];
        RestServiceToUpsertAcctRecords.SagittaCustomer customerTwo = customers[1];
        RestServiceAccountContext accountContext = new RestServiceAccountContext(customers);
        
        Sagitta_Division__c relevantOne = accountContext.findDivision(customerOne.GLDivisionCode);
        Sagitta_Division__c relevantTwo = accountContext.findDivision(customerTwo.GLDivisionCode);
        Sagitta_Division__c irrelevant = accountContext.findDivision(irrelevantDivision.ItemCode__c);
        
        System.assertEquals('Relevant Division', relevantOne.Name);
        System.assertEquals('Relevant Division', relevantTwo.Name);
        System.assertEquals(null, irrelevant);
    }
    
    @isTest
    private static void findBranch_ShouldOnlyGetRelevantBranches() {
        Sagitta_Branch__c irrelevantBranch = [SELECT Id, Name, ItemCode__c
                                             FROM Sagitta_Branch__c
                                             WHERE Name = 'Irrelevant Branch'];
        
        List<RestServiceToUpsertAcctRecords.SagittaCustomer> customers = createCustomers();
        RestServiceToUpsertAcctRecords.SagittaCustomer customerOne = customers[0];
        RestServiceToUpsertAcctRecords.SagittaCustomer customerTwo = customers[1];
        RestServiceAccountContext accountContext = new RestServiceAccountContext(customers);
        
        Sagitta_Branch__c relevantOne = accountContext.findBranch(customerOne.GLBranchCode);
        Sagitta_Branch__c relevantTwo = accountContext.findBranch(customerTwo.GLBranchCode);
        Sagitta_Branch__c irrelevant = accountContext.findBranch(irrelevantBranch.ItemCode__c);
        
        System.assertEquals('Relevant Branch', relevantOne.Name);
        System.assertEquals('Relevant Branch', relevantTwo.Name);
        System.assertEquals(null, irrelevant);
    }
    
    @isTest
    private static void findDept_ShouldOnlyGetRelevantDepts() {
        Sagitta_Department__c irrelevantDept = [SELECT Id, Name, ItemCode__c
                                               FROM Sagitta_Department__c
                                               WHERE Name = 'Irrelevant Dept'];
        
        List<RestServiceToUpsertAcctRecords.SagittaCustomer> customers = createCustomers();
        RestServiceToUpsertAcctRecords.SagittaCustomer customerOne = customers[0];
        RestServiceToUpsertAcctRecords.SagittaCustomer customerTwo = customers[1];
        RestServiceAccountContext accountContext = new RestServiceAccountContext(customers);
        
        Sagitta_Department__c relevantOne = accountContext.findDept(customerOne.GLDepartmentCode);
        Sagitta_Department__c relevantTwo = accountContext.findDept(customerTwo.GLDepartmentCode);
        Sagitta_Department__c irrelevant = accountContext.findDept(irrelevantDept.ItemCode__c);
        
        System.assertEquals('Relevant Dept', relevantOne.Name);
        System.assertEquals('Relevant Dept', relevantTwo.Name);
        System.assertEquals(null, irrelevant);
    }
    
    @isTest
    private static void findGroup_ShouldOnlyGetRelevantGroups() {
        Sagitta_Group__c irrelevantGroup = [SELECT Id, Name, ItemCode__c
                                           FROM Sagitta_Group__c
                                           WHERE Name = 'Irrelevant Group'];
        
        List<RestServiceToUpsertAcctRecords.SagittaCustomer> customers = createCustomers();
		RestServiceToUpsertAcctRecords.SagittaCustomer customerOne = customers[0];
        RestServiceToUpsertAcctRecords.SagittaCustomer customerTwo = customers[1];
        RestServiceAccountContext accountContext = new RestServiceAccountContext(customers);
        
        Sagitta_Group__c relevantOne = accountContext.findGroup(customerOne.GLGroupCode);
        Sagitta_Group__c relevantTwo = accountContext.findGroup(customerTwo.GLGroupCode);
        Sagitta_Group__c irrelevant = accountContext.findGroup(irrelevantGroup.ItemCode__c);
        
        System.assertEquals('Relevant Group', relevantOne.Name);
        System.assertEquals('Relevant Group', relevantTwo.Name);
        System.assertEquals(null, irrelevant);
    }
    
    @isTest
    private static void findAccount_ShouldOnlyGetRelevantAccounts() {
        Account irrelevantAccount = [SELECT Id, Name, Sagitta_Customer_ID__c
                                     FROM Account
                                     WHERE Name = 'Irrelevant Account'];
        
        List<RestServiceToUpsertAcctRecords.SagittaCustomer> customers = createCustomers();
        RestServiceToUpsertAcctRecords.SagittaCustomer customerOne = customers[0];
        RestServiceToUpsertAcctRecords.SagittaCustomer customerTwo = customers[1];
        RestServiceAccountContext accountContext = new RestServiceAccountContext(customers);
        
        Account relevantOne = accountContext.findAccount(customerOne.CustomerId);
        Account relevantTwo = accountContext.findAccount(customerTwo.CustomerId);
        Account irrelevant = accountContext.findAccount(irrelevantAccount.Sagitta_Customer_ID__c);
        
        System.assertEquals('Relevant Account One', relevantOne.Name);
        System.assertEquals('Relevant Account Two', relevantTwo.Name);
        System.assertEquals(new Account(), irrelevant);
    }
    
    @isTest
    private static void findAccount_ShouldReturnAccounts_WithEmptyStrings_InsteadOfNull() {
        List<RestServiceToUpsertAcctRecords.SagittaCustomer> customers = createCustomers();
        RestServiceToUpsertAcctRecords.SagittaCustomer customerOne = customers[0];
        RestServiceToUpsertAcctRecords.SagittaCustomer customerTwo = customers[1];
        RestServiceAccountContext accountContext = new RestServiceAccountContext(customers);
        
        Account relevantOne = accountContext.findAccount(customerOne.CustomerId);
        Account relevantTwo = accountContext.findAccount(customerTwo.CustomerId);
        
        // Sagitta_Client_Type__c gets set to 'P' BEFORE_UPDATE so it is not needed in test assertions
        System.assertEquals('', relevantOne.Sagitta_Customer_Number__c);
        System.assertEquals('', relevantOne.Sagitta_Account_Status__c);
        System.assertEquals('', relevantOne.Sagitta_Company_Email_Address__c);
        System.assertEquals('', relevantOne.Sagitta_DBA__c);
        System.assertEquals('', relevantOne.Sagitta_First_Name__c);
        System.assertEquals('', relevantOne.Sagitta_Middle_Name__c);
        System.assertEquals('', relevantOne.Sagitta_Last_Name__c);
        System.assertEquals('', relevantOne.Website);
        System.assertEquals('', relevantOne.BillingStreet);
        System.assertEquals('', relevantOne.BillingCity);
        System.assertEquals('', relevantOne.BillingState);
        System.assertEquals('', relevantOne.BillingPostalCode);
        System.assertEquals('', relevantOne.Phone);
        
        System.assertEquals('', relevantTwo.Sagitta_Customer_Number__c);
        System.assertEquals('', relevantTwo.Sagitta_Account_Status__c);
        System.assertEquals('', relevantTwo.Sagitta_Company_Email_Address__c);
        System.assertEquals('', relevantTwo.Sagitta_DBA__c);
        System.assertEquals('', relevantTwo.Sagitta_First_Name__c);
        System.assertEquals('', relevantTwo.Sagitta_Middle_Name__c);
        System.assertEquals('', relevantTwo.Sagitta_Last_Name__c);
        System.assertEquals('', relevantTwo.Website);
        System.assertEquals('', relevantTwo.BillingStreet);
        System.assertEquals('', relevantTwo.BillingCity);
        System.assertEquals('', relevantTwo.BillingState);
        System.assertEquals('', relevantTwo.BillingPostalCode);
        System.assertEquals('', relevantTwo.Phone);
    }

    private static void createIntegrationFlag(String flag) {
        Metadata__c intFlag = new Metadata__c();
        intFlag.Name = 'IntegrationFlag';
        intFlag.Value__c = flag;
        insert intFlag;
    }
    
    private static void createIsInitialSyncFlag(String flag) {
        Metadata__c isInitialSyncFlag = new Metadata__c();
        isInitialSyncFlag.Name = 'IsInitialSync';
        isInitialSyncFlag.Value__c = flag;
        insert isInitialSyncFlag;
    }
    
    private static List<RestServiceToUpsertAcctRecords.SagittaCustomer> createCustomers() {
        List<RestServiceToUpsertAcctRecords.SagittaCustomer> customers = new List<RestServiceToUpsertAcctRecords.SagittaCustomer>();
        
		RestServiceToUpsertAcctRecords.SagittaCustomer customerOne = new RestServiceToUpsertAcctRecords.SagittaCustomer();
		customerOne.CustomerId = 'c5b25a06-f102-47b2-98cb-0f5c2a30c653';
        customerOne.CustomerNumber = '11';
        customerOne.CustomerType = 'P';
        customerOne.FirmName = '';
        customerOne.IsActive = 'Inactive';
        customerOne.BusinessEntity = '';
        customerOne.EMail = 'email@example.com';
        customerOne.DoingBusinessAs = '';
        customerOne.FirstName = 'Craig';
        customerOne.MiddleName = '';
        customerOne.Last = 'Taylor';
        customerOne.CustomerWebAddress = '';
        customerOne.AddressLine1 = '555 S Main St';
        customerOne.AddressLine2 = 'Ste 500';
        customerOne.City = 'Denver';
        customerOne.State = 'CO';
        customerOne.ZipCode = '80202';
        customerOne.HomeAreaCode = '303';
        customerOne.HomePhone = '555-5555';
        customerOne.HomeExtension = '';
        customerOne.BusinessAreaCode = '';
        customerOne.BusinessPhone = '';
        customerOne.BusinessExtension = '';
        customerOne.IsPersonalCust = true;
        customerOne.IsCommercialCustomer = false;
        customerOne.IsBenefitCustomer = false;
        customerOne.IsFinancialCustomer = false;
        customerOne.IsHealthCustomer = false;
        customerOne.IsLifeCustomer = false;
        customerOne.IsNonPropertyAndCasualtyCustomer = false;
        customerOne.GLDivisionCode = '041041042';
        customerOne.GLBranchCode = '119033104';
        customerOne.GLDepartmentCode = '051120034';
        customerOne.GLGroupCode = '111043036';
        customerOne.AccountExecCode = '064033034';
        customerOne.AccountRepCode = '064033034';
        
        RestServiceToUpsertAcctRecords.SagittaCustomer customerTwo = new RestServiceToUpsertAcctRecords.SagittaCustomer();
		customerTwo.CustomerId = 'feee13bf-5aa0-458a-912d-041b4401db22';
        customerTwo.CustomerNumber = '22';
        customerTwo.CustomerType = 'C';
        customerTwo.FirmName = 'Vertafore';
        customerTwo.IsActive = 'Active';
        customerTwo.BusinessEntity = 'Association';
        customerTwo.EMail = 'email@aol.com';
        customerTwo.DoingBusinessAs = 'Vertaforceful';
        customerTwo.FirstName = '';
        customerTwo.MiddleName = '';
        customerTwo.Last = '';
        customerTwo.CustomerWebAddress = '';
        customerTwo.AddressLine1 = '123 Big St';
        customerTwo.AddressLine2 = 'Ste 1000';
        customerTwo.City = 'Denver';
        customerTwo.State = 'CO';
        customerTwo.ZipCode = '80202';
        customerTwo.HomeAreaCode = '';
        customerTwo.HomePhone = '';
        customerTwo.HomeExtension = '';
        customerTwo.BusinessAreaCode = '303';
        customerTwo.BusinessPhone = '841-4420';
        customerTwo.BusinessExtension = '321';
        customerTwo.IsPersonalCust = false;
        customerTwo.IsCommercialCustomer = true;
        customerTwo.IsBenefitCustomer = false;
        customerTwo.IsFinancialCustomer = false;
        customerTwo.IsHealthCustomer = false;
        customerTwo.IsLifeCustomer = false;
        customerTwo.IsNonPropertyAndCasualtyCustomer = false;
        customerTwo.GLDivisionCode = '041041042';
        customerTwo.GLBranchCode = '119033104';
        customerTwo.GLDepartmentCode = '051120034';
        customerTwo.GLGroupCode = '111043036';
        customerTwo.AccountExecCode = '064033034';
        customerTwo.AccountRepCode = '064033034';
        
        customers.add(customerOne);
        customers.add(customerTwo);
        
        return customers;
    }
}