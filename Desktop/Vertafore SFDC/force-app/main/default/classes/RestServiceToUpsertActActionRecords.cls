/*
    Author : Manideep
    Name : RestServiceToUpsertActActionRecords
    Date : 1 November 2019
    Description: Rest Service to upsert Activity Action Records
    Modification History :
    *************************************************************
    Version               Author             Date                      Description
	  1				     Manideep		1 November 2019		             Original
*/

@RestResource(urlMapping='/UpsertActActionRecords/*')
global with sharing class RestServiceToUpsertActActionRecords{
    
    // class that represents SagittaActivityAction model
    global class SagittaAct {
        global String ActionId { get; set; }
        global String DBAction { get; set; }
        global String EntityId { get; set; }
        global String EntityName { get; set; }
    }
    
    // class represents response to caller
    global class ResponseHandler {
        public Boolean Success {get; set;}
        public String Status {get; set;}
        public String Message {get;set;}
    }
    
    // Deserialize the request body and insert the data
	// @param - No Param
	// @return - ResponseHandler
    @HttpPost
    global static ResponseHandler doPost() {
        ResponseHandler obj = new ResponseHandler();
        RestRequest req = RestContext.request;
        String failedRecordIds = '';
        try {
            String postBody = req.requestBody.toString();
            // Deserialize the request body and converts into List<SagittaAct>
            List<SagittaAct> acts = (List<SagittaAct>)JSON.deserialize(postBody, List<SagittaAct>.class);
            List<Sagitta_Activity_Action_Type__c> actsToUpsert = new List<Sagitta_Activity_Action_Type__c>();
            
            // For loop to iterate through all the incoming activity action records
            for (SagittaAct act : acts) {
                Sagitta_Activity_Action_Type__c newAct = new Sagitta_Activity_Action_Type__c();
                newAct.Sagitta_Activity_Action_Id__c = act.EntityId;
                newAct.Name = act.DBAction;
                actsToUpsert.add(newAct);
            }
            Schema.SObjectField extIdField = Sagitta_Activity_Action_Type__c.Fields.Sagitta_Activity_Action_Id__c;
            // To insert the list of records
            Database.UpsertResult[] results = Database.upsert(actsToUpsert, extIdField, false);
            for (Database.UpsertResult result : results) {
                if (!result.isSuccess()) {
                    Database.Error[] upsertErrors = result.getErrors();
                    for (Database.Error upsertError : upsertErrors) {
                        failedRecordIds = failedRecordIds + upsertError.getMessage() + upsertError.getFields();
                    }
                }
            }
            // Response handler initialization once the insert is successful/failure
            // Insert log into Application Log object for request and response json
            if (String.isNotBlank(failedRecordIds)) {
                obj.success = false;
                obj.status='ERROR';
                obj.message= failedRecordIds;
                ApplicationLogger.logWebServiceMessage('RestServiceToUpsertActActionRecords', 'doPost', failedRecordIds, 
                                                   ApplicationLogger.ERROR, req.requestBody.toString(), JSON.serialize(obj));
            } else {
                obj.success = true;
                obj.status='SUCCESS';
                obj.message= 'SUCCESS';
                ApplicationLogger.logWebServiceMessage('RestServiceToUpsertActActionRecords', 'doPost', 'SUCCESS', 
                                                   ApplicationLogger.INFO, req.requestBody.toString(), JSON.serialize(obj));
            }
            
            return obj;
        } catch(Exception ex) {
            // Response handler initialization on exception
            RestContext.response.statusCode = 400;
            obj.success = false;
            obj.status='Error';
            obj.message=ex.getMessage();
            String message = 'Error: ' + ex.getMessage() + '; Stack Trace:' + ex.getStackTraceString();
            
            // Insert log into Application Log object for request, response and error json
            ApplicationLogger.logWebServiceMessage('RestServiceToUpsertActActionRecords', 'doPost', message, 
                                                   ApplicationLogger.ERROR, req.requestBody.toString(), JSON.serialize(obj));
            return obj;
        }        
    }

}