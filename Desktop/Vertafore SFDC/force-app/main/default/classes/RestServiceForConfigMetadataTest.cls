@isTest
public class RestServiceForConfigMetadataTest {
    
    @isTest
    static void doPost_ShouldInsertAllMetadaInRequest(){
        Map<String, String> metaMap = new Map<String, String>();
        metaMap.put('key1', 'value1');
        metaMap.put('key2', 'value2');
        
        String myJSON = JSON.serialize(metaMap);
        
        RestRequest request = new RestRequest();
        request.requestUri ='https://test.salesforce.com/services/apexrest/ConfigMetadata';
        request.httpMethod = 'POST';
        request.requestBody = Blob.valueof(myJSON);
        
        RestContext.request = request;
        RestServiceForConfigMetadata.ResponseHandler res = RestServiceForConfigMetadata.doPost();
        
        List<Metadata__c> met = [SELECT id, Name FROM Metadata__c];
        system.assertEquals(2, met.size());    
    }
    
    @isTest
    static void doPost_ShouldNotInsertMetadata_IfThereAreNoValues(){
        List<String> metaMap = new List<String>();
        metaMap.add('key1');
        metaMap.add('key2');
        
        String myJSON = JSON.serialize(metaMap);
        
        RestRequest request = new RestRequest();
        request.requestUri ='https://test.salesforce.com/services/apexrest/ConfigMetadata';
        request.httpMethod = 'POST';
        request.requestBody = Blob.valueof(myJSON);
        
        RestResponse response = new RestResponse();
        response.statusCode = 400;
        
        RestContext.request = request;
        RestContext.response = response;
        
        RestServiceForConfigMetadata.ResponseHandler res = RestServiceForConfigMetadata.doPost();
        
        List<Metadata__c> met = [SELECT id, Name FROM Metadata__c];
        system.assertEquals(0, met.size());    
    }
    
    @isTest
    static void doPost_ShouldNotInsertAllMetadaInRequest_ErrorResult(){
        Map<String, String> metaMap = new Map<String, String>();
        metaMap.put('', 'value1');
        metaMap.put('', 'value2');
        
        String myJSON = JSON.serialize(metaMap);
        
        RestRequest request = new RestRequest();
        request.requestUri ='https://test.salesforce.com/services/apexrest/ConfigMetadata';
        request.httpMethod = 'POST';
        request.requestBody = Blob.valueof(myJSON);
        
        RestContext.request = request;
        RestServiceForConfigMetadata.ResponseHandler res = RestServiceForConfigMetadata.doPost();
        
        List<Metadata__c> met = [SELECT id, Name FROM Metadata__c];
        system.assertEquals(0, met.size());    
    }
    
    @isTest
    static void doPost_ShouldNotInsertAllMetadaInRequest_ErrorResult2(){
        Map<String, String> metaMap = new Map<String, String>();
        metaMap.put('key1', 'value1');
        metaMap.put('key2', 'value2');
        
        String myJSON = JSON.serialize(metaMap);
        
        RestRequest request = new RestRequest();
        request.requestUri ='https://test.salesforce.com/services/apexrest/ConfigMetadata';
        request.httpMethod = 'POST';
        request.requestBody = Blob.valueof(myJSON);
        
        RestContext.request = request;
        RestServiceForConfigMetadata.ResponseHandler res = RestServiceForConfigMetadata.doPost();
        
        Map<String, String> metaMap2 = new Map<String, String>();
        metaMap2.put('key1', 'value1');
        metaMap2.put('key2', 'value2');
        
        String myJSON2 = JSON.serialize(metaMap2);
        
        RestRequest request2 = new RestRequest();
        request2.requestUri ='https://test.salesforce.com/services/apexrest/ConfigMetadata';
        request2.httpMethod = 'POST';
        request2.requestBody = Blob.valueof(myJSON2);
        
        RestContext.request = request2;
        RestServiceForConfigMetadata.ResponseHandler res2 = RestServiceForConfigMetadata.doPost();
        
        List<Metadata__c> met = [SELECT id, Name FROM Metadata__c];
        system.assertEquals(2, met.size());    
    }
    
    @isTest
    static void doPost_ShouldNotInsertAllMetadaInRequest_ErrorResult3(){
        Map<String, String> metaMap = new Map<String, String>();
        metaMap.put('key1', 'value1');
        metaMap.put('key2', 'value2');
        
        String myJSON = JSON.serialize(metaMap);
        
        RestRequest request = new RestRequest();
        request.requestUri ='https://test.salesforce.com/services/apexrest/ConfigMetadata';
        request.httpMethod = 'POST';
        request.requestBody = Blob.valueof(myJSON);
        
        RestContext.request = request;
        RestServiceForConfigMetadata.ResponseHandler res = RestServiceForConfigMetadata.doPost();
        
        Map<String, String> metaMap2 = new Map<String, String>();
        metaMap2.put('key1', 'value1');
        metaMap2.put('key1', 'value2');
        
        String myJSON2 = JSON.serialize(metaMap2);
        
        RestRequest request2 = new RestRequest();
        request2.requestUri ='https://test.salesforce.com/services/apexrest/ConfigMetadata';
        request2.httpMethod = 'POST';
        request2.requestBody = Blob.valueof(myJSON2);
        
        RestContext.request = request2;
        RestServiceForConfigMetadata.ResponseHandler res2 = RestServiceForConfigMetadata.doPost();
        
        List<Metadata__c> met = [SELECT id, Name FROM Metadata__c];
        system.assertEquals(2, met.size());    
    }
}