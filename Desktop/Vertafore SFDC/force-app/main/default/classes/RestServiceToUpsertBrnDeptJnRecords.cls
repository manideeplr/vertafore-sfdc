/*
    Author : Manideep
    Name : RestServiceToUpsertBrnDeptJnRecords
    Date : 1 November 2019
    Description: Rest service to upsert Branch Department Junction object records
    Modification History :
    *************************************************************
    Vesion               Author             Date                      Description
	1				    Manideep		1 November 2019			       Original
*/

@RestResource(urlMapping='/UpsertBranchDeptJnRecords/*')
global with sharing class RestServiceToUpsertBrnDeptJnRecords{
    
    // class that represents SagittaBranch model
    global class SagittaBrn {        
        global String ItemCodeHash { get; set; }
        global List<SagittaDept> Departments { get; set; }        
    }
    
    // class that represents SagittaDepartment model
    global class SagittaDept {
        global String ItemCodeHash { get; set; }
    }
    
    // class represents response to caller
    global class ResponseHandler {
        public Boolean Success {get; set;}
        public String Status {get; set;}
        public String Message {get;set;}
    }
    
    // Deserialize the request body and insert the data
	// @param - No Param
	// @return - ResponseHandler
    @HttpPost
    global static ResponseHandler doPost() {
		// Delete all existing BrDep junctions before upserting new ones
        List<BrDepJunction__c> oldJunctions = [SELECT Id FROM BrDepJunction__c];
        delete oldJunctions;
        
        ResponseHandler obj = new ResponseHandler();
        RestRequest req = RestContext.request;
        String failedRecordIds = '';
        try {
            String postBody = req.requestBody.toString();
            // Deserialize the request body and converts into List<SagittaBrn>
            List<SagittaBrn> Brns = (List<SagittaBrn>)JSON.deserialize(postBody, List<SagittaBrn>.class);
            List<Sagitta_Branch__c> brnIds = new List<Sagitta_Branch__c>();
            Set<String> brnItemCodes = new Set<String>();
            Set<String> deptItemCodes = new Set<String>();
            Set<String> deptValuesSet = new Set<String>();
            Map<String, String> brnIdItemCodesMap = new Map<String, String>();
            Map<String, String> deptIdItemCodesMap = new Map<String, String>();
            Map<String, String> brnDeptJnMap = new Map<String, String>();
            List<BrDepJunction__c> newJnRecs = new List<BrDepJunction__c>();
            // For loop to iterate through all the incoming Branch records
            for (SagittaBrn brn : Brns) {
                brnItemCodes.add(brn.ItemCodeHash);
                // For loop to iterate through all the incoming Department records related to branch record
                for (SagittaDept dept : brn.Departments) {
                    deptItemCodes.add(dept.ItemCodeHash);
                }
            }
            // Query the existing branch records and build a map to store the itemcode and id pairs
            for(Sagitta_Branch__c brn : [SELECT Id, ItemCode__c FROM Sagitta_Branch__c 
                                             WHERE ItemCode__c IN : brnItemCodes]) {
                brnIdItemCodesMap.put(brn.ItemCode__c, brn.Id);
            }
            // Query the existing department records and build a map to store the itemcode and id pairs
            for(Sagitta_Department__c dept : [SELECT Id, ItemCode__c FROM Sagitta_Department__c 
                                             WHERE ItemCode__c IN : deptItemCodes]) {
                deptIdItemCodesMap.put(dept.ItemCode__c, dept.Id);
            }

            // Create junctions to upsert
            for (SagittaBrn brn : Brns) {
                String dId = brnIdItemCodesMap.get(brn.ItemCodeHash);
                for (SagittaDept dept : brn.Departments) {
                    String bId = deptIdItemCodesMap.get(dept.ItemCodeHash);
                    if(bId != null) {
                        BrDepJunction__c newJn = new BrDepJunction__c();
                        newJn.Sagitta_Branch__c = dId;
                        newJn.Sagitta_Department__c = bId;
                        newJnRecs.add(newJn);
                    }                       
                }
            }
            
            // To insert the list of records
            Database.UpsertResult[] results = Database.upsert(newJnRecs, false);
            for (Database.UpsertResult result : results) {
                if (!result.isSuccess()) {
                    Database.Error[] upsertErrors = result.getErrors();
                    for (Database.Error upsertError : upsertErrors) {
                        failedRecordIds = failedRecordIds + upsertError.getMessage() + upsertError.getFields();
                    }
                }
            }
            // Response handler initialization once the insert is successful/failure
            // Insert log into Application Log object for request and response json
            if (String.isNotBlank(failedRecordIds)) {
                obj.success = false;
                obj.status='ERROR';
                obj.message= failedRecordIds;
                ApplicationLogger.logWebServiceMessage('RestServiceToUpsertBrnDeptJnRecords', 'doPost', failedRecordIds, 
                                                   ApplicationLogger.ERROR, req.requestBody.toString(), JSON.serialize(obj));
            } else {
                obj.success = true;
                obj.status='SUCCESS';
                obj.message= 'SUCCESS';
                ApplicationLogger.logWebServiceMessage('RestServiceToUpsertBrnDeptJnRecords', 'doPost', 'SUCCESS', 
                                                   ApplicationLogger.INFO, req.requestBody.toString(), JSON.serialize(obj));
            }
            
            return obj;
        } catch(Exception ex) {
            // Response handler initialization on exception
            RestContext.response.statusCode = 400;
            obj.success = false;
            obj.status='Error';
            obj.message=ex.getMessage();
            String message = 'Error: ' + ex.getMessage() + '; Stack Trace:' + ex.getStackTraceString();            
            // Insert log into Application Log object for request, response and error json
            ApplicationLogger.logWebServiceMessage('RestServiceToUpsertBrnDeptJnRecords', 'doPost', message, 
                                                   ApplicationLogger.ERROR, req.requestBody.toString(), JSON.serialize(obj));
            return obj;
        }        
    }

}