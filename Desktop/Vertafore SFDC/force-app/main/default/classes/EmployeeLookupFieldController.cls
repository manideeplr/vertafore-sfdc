/*
    Author : Manideep
    Name : EmployeeLookupFieldController
    Date : 1 November 2019
    Description: 
    Modification History :
    *************************************************************
    Version             Author          Date                        Description
    1                  Manideep      1 November 2019                  Original
*/
public class EmployeeLookupFieldController {
	@AuraEnabled
    public static List<String> searchForExecs(String searchString) {
        try {
            List<String> result = new List<String>();
            if (!String.isBlank(searchString)) {
                searchString = '%' + searchString + '%';
            } else {
                return result;
            }
            List<Sagitta_Employee__c> employees = [SELECT Name
                                                       FROM Sagitta_Employee__c 
                                                       WHERE Sagitta_Is_Executive__c = true 
                                                       AND Sagitta_Employee_Status__c = 'Active'
                                                       AND Name 
                                                       LIKE: searchString ORDER 
                                                       BY Name DESC LIMIT 5];
            for(Sagitta_Employee__c employee : employees)
            {
                result.add(employee.Name);
            }
            result.sort();
            return result;
        } catch (Exception ex) {
            String message = 'Error looking up Exec; ' +
                'Error: ' + ex.getMessage() + '; Stack Trace: ' + ex.getStackTraceString();
            ApplicationLogger.logMessage('EmployeeLookupFieldController', 'searchForExecs', message, ApplicationLogger.ERROR);
            throw new AuraHandledException('There was an error looking up the Executive: ' + ex.getMessage());
        }
    }
    @AuraEnabled
    public static List<String> searchForReps(String searchString) {
        try {
            List<String> result = new List<String>();
            if (!String.isBlank(searchString)) {
                searchString = '%' + searchString + '%';
            } else {
                return result;
            }
            List<Sagitta_Employee__c> employees = [SELECT Name
                                                       FROM Sagitta_Employee__c
                                                       WHERE Sagitta_Is_Representative__c = true
                                                       AND Sagitta_Employee_Status__c = 'Active'
                                                       AND Name
                                                       LIKE: searchString ORDER
                                                       BY Name DESC LIMIT 5];
            
            for(Sagitta_Employee__c employee : employees)
            {
                result.add(employee.Name);
            }
            result.sort();
            return result;
        } catch (Exception ex) {
            String message = 'Error looking up Exec; ' +
                'Error: ' + ex.getMessage() + '; Stack Trace: ' + ex.getStackTraceString();
            ApplicationLogger.logMessage('EmployeeLookupFieldController', 'searchForReps', message, ApplicationLogger.ERROR);
            throw new AuraHandledException('There was an error looking up the Representative: ' + ex.getMessage());
        }
    }
}