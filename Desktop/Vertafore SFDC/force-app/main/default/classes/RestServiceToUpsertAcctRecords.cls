/*
    Author : Manideep
    Name : RestServiceToUpsertAccountRecords
    Date : 1 November 2019
    Description:
    Modification History :
    *************************************************************
    Version               Author             Date                      Description
	  1					 Manideep		 1 November 2019			    Original
*/

@RestResource(urlMapping='/UpsertAcctRecords/*')
global with sharing class RestServiceToUpsertAcctRecords{
    // class that represents Sagitta Customer model
    global class SagittaCustomer {
        global String CustomerId { get; set; }
        global String CustomerNumber { get; set; }
        global String CustomerType { get; set; }
        global String FirmName { get; set; }
        global String IsActive { get; set; }
        global String BusinessEntity { get; set; }
        global String EMail { get; set; }
        global String DoingBusinessAs { get; set; }
        global String FirstName { get; set; }
        global String MiddleName { get; set; }
        global String Last { get; set; }
        global String CustomerWebAddress { get; set; }
        global String AddressLine1 { get; set; }
        global String AddressLine2 { get; set; }
        global String City { get; set; }
        global String State { get; set; }
        global String ZipCode { get; set; }
        global String HomeAreaCode { get; set; }
        global String HomePhone { get; set; }
        global String HomeExtension { get; set; }
        global String BusinessAreaCode { get; set; }
        global String BusinessPhone { get; set; }
        global String BusinessExtension { get; set; }
        global boolean IsPersonalCust = false;
        global boolean IsCommercialCustomer { get; set; }
		global boolean IsLifeCustomer { get; set; }
		global boolean IsBenefitCustomer { get; set; }
		global boolean IsHealthCustomer { get; set; }
		global boolean IsNonPropertyAndCasualtyCustomer { get; set; }
		global boolean IsFinancialCustomer { get; set; }
        global string GLBranchCode { get; set; }
        global string GLDepartmentCode { get; set; }
        global string GLDivisionCode { get; set; }
        global string GLGroupCode { get; set; }
        global String AccountExecCode { get; set; }
        global String AccountRepCode { get; set; }
        // Field always defaults to "Main" in mappers, doesn't seems necessary in SF
        // global string StatePrintGroup { get; set; }
    }
    
    global class ResponseHandler {
        public Boolean Success { get; set; }
        public String Status { get; set; }
        public String Message { get; set; }
    }
    
    private static RestServiceAccountContext accountContext;
    
    // only gets Accounts for Customers sent in request
    private static List<Account> relevantAccounts;
    private static List<Sagitta_Employee__c> allEmployees;
    private static List<Sagitta_Division__c> allDivisions;
    private static List<Sagitta_Branch__c> allBranches;
    private static List<Sagitta_Department__c> allDepartments;
    private static List<Sagitta_Group__c> allGroups;

    @HttpGet
    global static String doget(){
        return '1';
    }
    
    // Deserialize the request body and insert the data
    // @param - No Param
    // @return - ResponseHandler
    @HttpPost
    global static ResponseHandler doPost() {
        ResponseHandler obj = new ResponseHandler();
        String failedCustomers = 'FAILURES: ';
        String upsertErrors = 'UPSERT ERROR: ';
        RestRequest req = RestContext.request;
        String postBody;

        try {
            postBody = req.requestBody.toString();
            system.debug(postBody);
            Schema.SObjectField extIdfield = Account.Fields.Sagitta_Customer_ID__c;            
            List<SagittaCustomer> customers = (List<SagittaCustomer>)JSON.deserialize(postBody, List<SagittaCustomer>.class);
            
            accountContext = new RestServiceAccountContext(customers);
            
			List<Account> accountsToUpsert = new List<Account>();         
            for (SagittaCustomer customer : customers) {
                try {
                    Account accountToMap = accountContext.findAccount(customer.CustomerId);
                    
                    if (mapFields(accountToMap, customer)) {
                        accountsToUpsert.add(accountToMap);
                    }
                }
                catch(Exception ex) {
                    failedCustomers = failedCustomers + customer.CustomerId + '-' + ex.getMessage() + ' at line '+ ex.getStackTraceString();
                    obj.message = ex.getMessage() + ' at line '+ ex.getStackTraceString();
                    //system.debug();
                }
            }
            
            if (accountsToUpsert.size() > 0) {
                Database.UpsertResult [] res = Database.upsert(accountsToUpsert, extIdfield, false);
                        
                for(Database.UpsertResult result : res){
                    if(result.getErrors().size() > 0){
                        for(Database.Error err : result.getErrors()){
                            upsertErrors = upsertErrors + err.getMessage() + err.getFields() +'; ';
                        }
                    }
                }
            }

            if(upsertErrors != 'UPSERT ERROR: '){
                RestContext.response.statusCode = 400;
                obj.success = false;
                obj.status ='ERROR';
                obj.message = upsertErrors;
                ApplicationLogger.logWebServiceMessage('RestServiceToUpsertAcctRecords', 'doPost', obj.message,
                                                        ApplicationLogger.ERROR, postBody, JSON.serialize(obj));   
            }    
            
            else if(failedCustomers != 'FAILURES: '){ 
                RestContext.response.statusCode = 400;
                obj.success = false;
                obj.status ='ERROR';
                obj.message = failedCustomers;
                ApplicationLogger.logWebServiceMessage('RestServiceToUpsertAcctRecords', 'doPost', obj.message,
                                                        ApplicationLogger.ERROR, postBody, JSON.serialize(obj));     
            }
            else {
                obj.success = true;
                obj.status ='SUCCESS';
                obj.message = 'SUCCESS';
                ApplicationLogger.logWebServiceMessage('RestServiceToUpsertAcctRecords', 'doPost', obj.message,
                                                        ApplicationLogger.INFO, postBody, JSON.serialize(obj));   
            }
            
		    return obj;
        } catch(Exception ex) {
            RestContext.response.statusCode = 400;
            obj.success = false;
            obj.status ='ERROR';
            obj.message = ex.getMessage() + ' at line '+ ex.getStackTraceString();
            ApplicationLogger.logWebServiceMessage('RestServiceToUpsertAcctRecords', 'doPost', obj.message,
                                                    ApplicationLogger.ERROR, postBody, JSON.serialize(obj));   
            return obj;
        }        
    }
    
    // Map Sagitta Customer fields to Salesforce Account fields
    // @param - Account account, SagittaCustomer customer
    // @return - Boolean
    private static boolean mapFields(Account account, SagittaCustomer customer) {
        boolean fieldsUpdated = false;
        
        Id division = accountContext.findDivision(customer.GLDivisionCode).Id;
        Id branch = accountContext.findBranch(customer.GLBranchCode).Id;
        Id department = accountContext.findDept(customer.GLDepartmentCode).Id;
        // group is a keyword in apex
        Id grp = accountContext.findGroup(customer.GLGroupCode).Id;
        
        if (account.Sagitta_Division__c != division) {
            account.Sagitta_Division__c = division;
            fieldsUpdated = true;
        }
        
        if (account.Sagitta_Branch__c != branch) {
            account.Sagitta_Branch__c = branch;
            fieldsUpdated = true;
        }
        
        if (account.Sagitta_Department__c != department) {
            account.Sagitta_Department__c = department;
            fieldsUpdated = true;
        }
        
        if (account.Sagitta_Group__c != grp) {
            account.Sagitta_Group__c = grp;
            fieldsUpdated = true;
        }
        
        Id exec = accountContext.findEmployee(customer.AccountExecCode).Id;
        Id rep = accountContext.findEmployee(customer.AccountRepCode).Id;
        
        if (account.Sagitta_Employee_Account_Exec__c != exec) {
            account.Sagitta_Employee_Account_Exec__c = exec;
            fieldsUpdated = true;
        }
        
        if (account.Sagitta_Employee_Account_Rep__c != rep) {
            account.Sagitta_Employee_Account_Rep__c = rep;
            fieldsUpdated = true;
        }
        
        if (account.Sagitta_Customer_Id__c != customer.CustomerId) {
            account.Sagitta_Customer_Id__c = customer.CustomerId;
            fieldsUpdated = true;
        }
        
        if (account.Sagitta_Customer_Number__c != customer.CustomerNumber) {
            account.Sagitta_Customer_Number__c = customer.CustomerNumber;
            fieldsUpdated = true;
        }
        
        if (account.Sagitta_Client_Type__c != customer.CustomerType) {
            account.Sagitta_Client_Type__c = customer.CustomerType;
            fieldsUpdated = true;
        }
        
        string isActive = (String.isNotBlank(customer.IsActive) && customer.IsActive.toLowerCase() == 'true') ? 'Active' : 'Inactive';
        
        if (account.Sagitta_Account_Status__c != isActive) {
            account.Sagitta_Account_Status__c = isActive;
            fieldsUpdated = true;
        }
        
        if (account.Sagitta_Company_Email_Address__c != customer.EMail) {
            account.Sagitta_Company_Email_Address__c = customer.EMail;
            fieldsUpdated = true;
        }
        
        if (account.Sagitta_DBA__c != customer.DoingBusinessAs) {
            account.Sagitta_DBA__c = customer.DoingBusinessAs;
            fieldsUpdated = true;
        }
        
        if (account.Sagitta_First_Name__c != customer.FirstName) {
            account.Sagitta_First_Name__c = customer.FirstName;
            fieldsUpdated = true;
        }
        
        if (account.Sagitta_Middle_Name__c != customer.MiddleName) {
            account.Sagitta_Middle_Name__c = customer.MiddleName;
            fieldsUpdated = true;
        }
        
        if (account.Sagitta_Last_Name__c != customer.Last) {
            account.Sagitta_Last_Name__c = customer.Last;
            fieldsUpdated = true;
        }
        
        if (account.Website != customer.CustomerWebAddress) {
            account.Website = customer.CustomerWebAddress;
            fieldsUpdated = true;
        }

        String billingStreet = mergeStreetAddress(customer.AddressLine1, customer.AddressLine2);
        
        if (account.BillingStreet != billingStreet) {
            account.BillingStreet = billingStreet;
            fieldsUpdated = true;
        }
        
        if (account.BillingCity != customer.City) {
            account.BillingCity = customer.City;
            fieldsUpdated = true;
        }
        
        if (account.BillingState != customer.State) {
            account.BillingState = customer.State;
            fieldsUpdated = true;
        }
        
        if (account.BillingPostalCode != customer.ZipCode) {
            account.BillingPostalCode = customer.ZipCode;
            fieldsUpdated = true;
        }
        if(customer.IsPersonalCust == null) customer.IsPersonalCust = false;
        
        String phone = customer.IsPersonalCust ? mergePhoneNumber(customer.HomeAreaCode, customer.HomePhone, customer.HomeExtension) : mergePhoneNumber(customer.BusinessAreaCode, customer.BusinessPhone, customer.BusinessExtension);
        
        if (account.Phone != phone) {
            account.Phone = phone;
            fieldsUpdated = true;
        }
        
        if (account.BillingState != customer.State) {
            account.BillingState = customer.State;
            fieldsUpdated = true;
        }
        
        if (account.Sagitta_TOB_IsPersonal__c != customer.IsPersonalCust) {
            account.Sagitta_TOB_IsPersonal__c = customer.IsPersonalCust;
            fieldsUpdated = true;
        }
        
        if (account.Sagitta_TOB_IsCommercial__c != customer.IsCommercialCustomer) {
            account.Sagitta_TOB_IsCommercial__c = customer.IsCommercialCustomer;
            fieldsUpdated = true;
        }
        
        if (account.Sagitta_TOB_IsLife__c != customer.IsLifeCustomer) {
            account.Sagitta_TOB_IsLife__c = customer.IsLifeCustomer;
            fieldsUpdated = true;
        }
        
        if (account.Sagitta_TOB_IsBenefits__c != customer.IsBenefitCustomer) {
            account.Sagitta_TOB_IsBenefits__c = customer.IsBenefitCustomer;
            fieldsUpdated = true;
        }
        
        if (account.Sagitta_TOB_IsHealth__c != customer.IsHealthCustomer) {
            account.Sagitta_TOB_IsHealth__c = customer.IsHealthCustomer;
            fieldsUpdated = true;
        }
        
        if (account.Sagitta_TOB_IsNon_P_C__c != customer.IsNonPropertyAndCasualtyCustomer) {
            account.Sagitta_TOB_IsNon_P_C__c = customer.IsNonPropertyAndCasualtyCustomer;
            fieldsUpdated = true;
        }
        
        if (account.Sagitta_TOB_IsFinancial__c != customer.IsFinancialCustomer) {
            account.Sagitta_TOB_IsFinancial__c = customer.IsFinancialCustomer;
            fieldsUpdated = true;
        }
        
        string name = String.IsNotBlank(customer.FirmName) ? customer.FirmName : customer.FirstName + ' ' + customer.MiddleName + ' ' + customer.Last;
        
        if (account.Name != name) {
            account.Name = name;
            fieldsUpdated = true;
        }
        
        return fieldsUpdated;
    }
    
    private static String mergeStreetAddress(String addressLineOne, String addressLineTwo) {
        String template = '{0}\n{1}';
        List<Object> parameters = new List<Object> { addressLineOne, addressLineTwo };
        
        return  !String.isBlank(addressLineTwo) ? String.Format(template, parameters) : addressLineOne;
    }
    
    private static string mergePhoneNumber(String areaCode, String phoneNumber, string extension){
        String phone = '';
        
        if(String.isNotBlank(areaCode)){
            phone = '(' + areaCode + ') ';
        }
        
        if(String.isNotBlank(phoneNumber) && !phoneNumber.contains('-')){
            phone = phone + phoneNumber.substring(0,3) + '-' + phoneNumber.substring(3);    
        }
        else {
            phone = phone + phoneNumber;
        }
        
        if(String.isNotBlank(extension)){
            phone = phone + ' ext ' + extension;
        }
        
        return phone;
    }
}