/*
    Author : Manideep
    Name : RestServiceToUpsertLOBRecords
    Date : 1 November 2019
    Description: Rest service to upsert Line Of Business records
    Modification History :
    *************************************************************
    Version               Author             Date                      Description
	  1				     Manideep		 1 November 2019			     Original
*/

@RestResource(urlMapping='/UpsertLOBRecords/*')
global with sharing class RestServiceToUpsertLOBRecords{
    
    // class that represents SagittaLOB model
    global class SagittaLOB {        
        global String DescriptionLOBS { get; set; }
        global String IdLOBS { get; set; }
        global String NameLOBS { get; set; }
        global Integer PermanentFlagLOBS { get; set; }
        global String TypeOfBusinessLOBS { get; set; }
        global Boolean IsHide { get; set; }
        global String EntityId { get; set; }
        global String EntityName { get; set; }        
    }
    
    // class represents response to caller
    global class ResponseHandler {
        public Boolean Success {get; set;}
        public String Status {get; set;}
        public String Message {get;set;}
    }
    
    // Deserialize the request body and insert the data
    // @param - No Param
    // @return - ResponseHandler
    @HttpPost
    global static ResponseHandler doPost() {
        ResponseHandler obj = new ResponseHandler();
        RestRequest req = RestContext.request;
        String failedRecordIds = '';
       try {
            String postBody = req.requestBody.toString();
            List<SagittaLOB> lobs = (List<SagittaLOB>)JSON.deserialize(postBody, List<SagittaLOB>.class);
            List<Sagitta_Line_Of_Business__c> lobsToUpsert = new List<Sagitta_Line_Of_Business__c>();
            for (SagittaLOB lob : lobs) {
                Sagitta_Line_Of_Business__c newlob = new Sagitta_Line_Of_Business__c();
                newlob.Name = lob.DescriptionLOBS;
                newlob.Sagitta_Entity_ID__c = lob.IdLOBS;
                newlob.Sagitta_LOB_Code__c = lob.EntityId;
                newlob.Sagitta_LOB_Permanent_Flag__c = lob.PermanentFlagLOBS == 1 ? true : false;
                newlob.Sagitta_Type_of_Business__c = lob.TypeOfBusinessLOBS;
                newlob.Sagitta_IsHidden__c = lob.IsHide;

                lobsToUpsert.add(newlob);
            }
            Schema.SObjectField extIdfield = Sagitta_Line_Of_Business__c.Fields.Sagitta_Entity_ID__c;
            // To insert the list of records
            Database.UpsertResult[] results = Database.upsert(lobsToUpsert, extIdfield, false);
            for (Database.UpsertResult result : results) {
                if (!result.isSuccess()) {
                    Database.Error[] upsertErrors = result.getErrors();
                    for (Database.Error upsertError : upsertErrors) {
                        failedRecordIds = failedRecordIds + upsertError.getMessage() + upsertError.getFields();
                    }
                }
            }
            // Response handler initialization once the insert is successful/failure
            // Insert log into Application Log object for request and response json
            if (String.isNotBlank(failedRecordIds)) {
                obj.success = false;
                obj.status='ERROR';
                obj.message= failedRecordIds;
                ApplicationLogger.logWebServiceMessage('RestServiceToUpsertLOBRecords', 'doPost', failedRecordIds, 
                                                   ApplicationLogger.ERROR, req.requestBody.toString(), JSON.serialize(obj));
            } else {
                obj.success = true;
                obj.status='SUCCESS';
                obj.message= 'SUCCESS';
                ApplicationLogger.logWebServiceMessage('RestServiceToUpsertLOBRecords', 'doPost', 'SUCCESS', 
                                                   ApplicationLogger.INFO, req.requestBody.toString(), JSON.serialize(obj));
            }
            
            return obj;
        } catch(Exception ex) {
            RestContext.response.statusCode = 400;
            obj.success = false;
            obj.status='Error';
            obj.message=ex.getMessage();
            String message = 'Error: ' + ex.getMessage() + '; Stack Trace:' + ex.getStackTraceString();            
            // Insert log into Application Log object for request, response and error json
            ApplicationLogger.logWebServiceMessage('RestServiceToUpsertLOBRecords', 'doPost', message, 
                                                   ApplicationLogger.ERROR, req.requestBody.toString(), JSON.serialize(obj));
            return obj;
        }        
    }

}