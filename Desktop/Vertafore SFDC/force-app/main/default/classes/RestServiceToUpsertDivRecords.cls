/*
    Author : Manideep
    Name : RestServiceToUpsertDivRecords
    Date : 1 November 2019
    Description: Rest Service to upsert Division records
    Modification History :
    *************************************************************
    Version               Author             Date                      Description
	 1			         Manideep		 1 November 2019			     Original
*/

@RestResource(urlMapping='/UpsertDivRecords/*')
global with sharing class RestServiceToUpsertDivRecords{
    
    // class that represents SagittaBranch model
    global class SagittaDiv {
        global String ItemCode { get; set; }
        global String ItemCodeHash { get; set; }
        global String Name { get; set; }
        global String Status { get; set; }
        global Boolean IsHidden { get; set; }
        global String EntityId { get; set; }
        global String EntityName { get; set; }        
    }
    
    // class represents response to caller
    global class ResponseHandler {
        public Boolean Success {get; set;}
        public String Status {get; set;}
        public String Message {get;set;}
    }
    
    // Deserialize the request body and insert the data
	// @param - No Param
	// @return - ResponseHandler
    @HttpPost
    global static ResponseHandler doPost() {
        ResponseHandler obj = new ResponseHandler();
        RestRequest req = RestContext.request;
        String failedRecordIds = '';
        try {
            String postBody = req.requestBody.toString();
            // Deserialize the request body and converts into List<SagittaDiv>
            List<SagittaDiv> Divs = (List<SagittaDiv>)JSON.deserialize(postBody, List<SagittaDiv>.class);
            List<Sagitta_Division__c> divsToUpsert = new List<Sagitta_Division__c>();
            // For loop to iterate through all the incoming Division records
            for (SagittaDiv div : Divs) {
                Sagitta_Division__c newDiv = new Sagitta_Division__c();
                newDiv.Name = div.Name;
                newDiv.ItemCode__c = div.ItemCodeHash;
                newDiv.Sagitta_isHidden__c = div.IsHidden;
                divsToUpsert.add(newDiv);
            }
            Schema.SObjectField extIdField = Sagitta_Division__c.Fields.ItemCode__c;
            // To insert the list of records
            Database.UpsertResult[] results = Database.upsert(divsToUpsert, extIdField, false);
            for (Database.UpsertResult result : results) {
                if (!result.isSuccess()) {
                    Database.Error[] upsertErrors = result.getErrors();
                    for (Database.Error upsertError : upsertErrors) {
                        failedRecordIds = failedRecordIds + upsertError.getMessage() + upsertError.getFields();
                    }
                }
            }
            // Response handler initialization once the insert is successful/failure
            // Insert log into Application Log object for request and response json
            if (String.isNotBlank(failedRecordIds)) {
                obj.success = false;
                obj.status='ERROR';
                obj.message= failedRecordIds;
                ApplicationLogger.logWebServiceMessage('RestServiceToUpsertDivRecords', 'doPost', failedRecordIds, 
                                                   ApplicationLogger.ERROR, req.requestBody.toString(), JSON.serialize(obj));
            } else {
                obj.success = true;
                obj.status='SUCCESS';
                obj.message= 'SUCCESS';
                ApplicationLogger.logWebServiceMessage('RestServiceToUpsertDivRecords', 'doPost', 'SUCCESS', 
                                                   ApplicationLogger.INFO, req.requestBody.toString(), JSON.serialize(obj));
            }
            
            return obj;
        } catch(Exception ex) {
            // Response handler initialization on exception
            RestContext.response.statusCode = 400;
            obj.success = false;
            obj.status='Error';
            obj.message=ex.getMessage();
            String message = 'Error: ' + ex.getMessage() + '; Stack Trace:' + ex.getStackTraceString();            
            // Insert log into Application Log object for request, response and error json
            ApplicationLogger.logWebServiceMessage('RestServiceToUpsertDivRecords', 'doPost', message, 
                                                   ApplicationLogger.ERROR, req.requestBody.toString(), JSON.serialize(obj));
            return obj;
        }        
    }

}