/*
    Author : Manideep
    Name : OppTriggerHandler
    Date : 1 November 2019
    Description: Validates conditions to send message when Opportunity reaches
					Prevents message processing if no Mapping record exists
					Prevents user from changing Client Type.
					Populates Business Units, Executive, and Representative on insert.
					Populates Type of Business checkboxes on insert.
					Prevents user from changing Exec or Rep to be blank on update.
					Creates task for account after update. 
    Modification History :
    *************************************************************
    Version             Author          Date                        Description
      1                Manideep       1 November 2019                 Original
*/

public class OppTriggerHandler {
    private static List<Sagitta_Employee__c> allEmployees = [SELECT Sagitta_Employee_Status__c, Id FROM Sagitta_Employee__c WHERE Sagitta_Employee_Status__c != null];
    private static List<Sagitta_Division__c> allDivisions = [SELECT Sagitta_isHidden__c, Id FROM Sagitta_Division__c WHERE Sagitta_IsHidden__c = false];
    private static List<Sagitta_Department__c> allDepartments = [SELECT Sagitta_isHidden__c, Id FROM Sagitta_Department__c WHERE Sagitta_IsHidden__c = false];
    private static List<Sagitta_Branch__c> allBranches = [SELECT Sagitta_isHidden__c, Id FROM Sagitta_Branch__c WHERE Sagitta_IsHidden__c = false];
    private static List<Sagitta_Group__c> allGroups = [SELECT Sagitta_isHidden__c, Id FROM Sagitta_Group__c WHERE Sagitta_IsHidden__c = false];
    private static List<Sagitta_Agency_Settings__c> agencySettings = [SELECT Name, Integration_Stage__c, Default_Bill_Method__c FROM Sagitta_Agency_Settings__c LIMIT 1];
    private static List<Sagitta_Mapping__c> mappingList = 
            [SELECT Id, Sagitta_Division__c, Sagitta_Branch__c, Sagitta_Department__c, Sagitta_Group__c, Sagitta_Executive__c, Sagitta_Representative__c
             FROM Sagitta_Mapping__c
             WHERE Sagitta_Assigned_To__c =: UserInfo.getUserId() LIMIT 1];
    
    // Summary:
    // Handles all Opportunity trigger behavior
    // 
    // Details:
    // @param workingRecords - List of all Account records that are being inserted or updated.
    // @param triggerEvent - BEFORE_INSERT, BEFORE_UPDATE, or AFTER_UPDATE
    // @return TriggerHandlerStatus - MESSAGES_SENT or NO_MESSAGES_SENT
    public TriggerHandlerStatus handleTrigger(List<Opportunity> workingRecords, Map<Id, Opportunity> oldRecords, System.TriggerOperation triggerEvent) {
        Sagitta_Mapping__c mapping;
        boolean comingFromAPI = false;
        
        if (mappingList.size() > 0) {
            mapping = mappingList[0];
        }
        
        // See if request is coming from the API.
        if (String.valueOf(URL.getCurrentRequestUrl()).toLowerCase().contains('services/data') || 
            String.valueOf(URL.getCurrentRequestUrl()).toLowerCase().contains('services/apexrest')) {
                comingFromAPI = true;
        }
        
        switch on triggerEvent {
            when BEFORE_INSERT{
                // Prevent the user from creating an opportunity if they don't have a default Mapping
                if (mapping == null) {
                    for (Opportunity opp: workingRecords) {
                        opp.addError('No Sagitta Mapping record found. An Sagitta Mapping record must be assigned to this user in order to create an Opportunity.');
                    }
                    return TriggerHandlerStatus.NO_MESSAGES_SENT;
                }

                // If user has a default Mapping
                if (mapping != null) {
                   	//confirm business units are all active and not hidden
                   	try {
                        //ensure business unit junctions are valid
            			DivBrJunction__c divBrJunc = [SELECT Id FROM DivBrJunction__c WHERE Sagitta_Division__c =: mapping.Sagitta_Division__c AND Sagitta_Branch__c =: mapping.Sagitta_Branch__c][0];
          				BrDepJunction__c branchDepJunc = [SELECT Id FROM BrDepJunction__c WHERE Sagitta_Branch__c =: mapping.Sagitta_Branch__c AND Sagitta_Department__c =: mapping.Sagitta_Department__c][0];
            			DepGrpJunction__c depGrpJunc = [SELECT Id FROM DepGrpJunction__c WHERE Sagitta_Department__c =: mapping.Sagitta_Department__c AND Sagitta_Group__c =: mapping.Sagitta_Group__c][0];
       					} catch (Exception ex) {
                            String message = 'Invalid combination of Division, Branch, Department, and Group. ;' + 
                                'Error: ' + ex.getMessage() + '; Stack Trace:' + ex.getStackTraceString();
                 //               ApplicationLogger.logMessage('OppTriggerHandler', 'HandleTrigger', message, ApplicationLogger.ERROR);
        				}
                    }
                
                for (Opportunity opp: workingRecords){
  					// Populate Bill Method
                    if (agencySettings.size() == 0) {
                        opp.addError('Could not find Agency Settings record.');
                        continue;
                    }
                    
                    // If Bill Method not set to a value when creating a new Opportunity, use agency default setting
                    if(String.isBlank(opp.Sagitta_Bill_Method__c)){
                        opp.Sagitta_Bill_Method__c = agencySettings[0].Default_Bill_Method__c;
                    }
                    
                    // If we don't already have values for Domain Settings
                    if (opp.Sagitta_Division__c == null) { // we only bother to check one field for efficiency
                        opp.Sagitta_Division__c = mapping.Sagitta_Division__c;
                        opp.Sagitta_Branch__c = mapping.Sagitta_Branch__c;
                        opp.Sagitta_Department__c = mapping.Sagitta_Department__c;
                        opp.Sagitta_Group__c = mapping.Sagitta_Group__c;
                    }
                    
                    // If Exec and Rep haven't been filled in
                    if (String.isBlank(opp.Sagitta_Opportunity_Exec__c)){
                        opp.Sagitta_Opportunity_Exec__c = mapping.Sagitta_Executive__c;
                    }
                    if (String.isBlank(opp.Sagitta_Opportunity_Rep__c)){
                        opp.Sagitta_Opportunity_Rep__c = mapping.Sagitta_Representative__c;
                    }
                    
                    // Validate Business Units
                    try {
                         checkValidBusinessUnits(opp);
                    } catch (Exception ex) {
                        String message = 'Business Units are invalid. ; ' +
                            'Error: ' + ex.getMessage() + '; Stack Trace: ' + ex.getStackTraceString();
                   //     ApplicationLogger.logMessage('AccountTriggerHandler', 'checkValidBusinessUnits', message, ApplicationLogger.ERROR);
                        throw ex;
                    }
                }
            }
            when BEFORE_UPDATE{
                for (Opportunity opp: workingRecords){
                    // If user does not have a default Mapping
                    if (mapping == null) {
                        opp.addError('User does not have an Sagitta Mapping record. Please navigate to the Business Units tab to set the values for this user\'s default Sagitta Mapping record.');
                        return TriggerHandlerStatus.NO_MESSAGES_SENT;
                    }
                    // else if the user has a default Mapping record
                    else {
                        try {
                            if (oldRecords != null && opp.StageName != oldRecords.get(opp.Id).StageName) {
                                checkValidBusinessUnits(opp);
                            }
                        } catch (Exception ex) {
                            String message = 'Custom Message: Business Units are invalid. ; ' +
                                'Error: ' + ex.getMessage() + '; Stack Trace: ' + ex.getStackTraceString();
                     //       ApplicationLogger.logMessage('AccountTriggerHandler', 'checkValidBusinessUnits', message, ApplicationLogger.ERROR);
                            throw ex;
                        }

                        // If no values have been copied over to our hidden Business Unit Lead fields
                        if (String.isBlank(opp.Sagitta_Division__c)) { // (we only bother to check one field for efficiency)
                            opp.Sagitta_Division__c = mapping.Sagitta_Division__c;
                            opp.Sagitta_Branch__c = mapping.Sagitta_Branch__c;
                            opp.Sagitta_Department__c = mapping.Sagitta_Department__c;
                            opp.Sagitta_Group__c = mapping.Sagitta_Group__c;
                        }
                        
                        // If Exec and Rep haven't been filled in
                        if (String.isBlank(opp.Sagitta_Opportunity_Exec__c)){
                            opp.Sagitta_Opportunity_Exec__c = mapping.Sagitta_Executive__c;
                        }
                        if (String.isBlank(opp.Sagitta_Opportunity_Rep__c)){
                            opp.Sagitta_Opportunity_Rep__c = mapping.Sagitta_Representative__c;
                        }
                    }
                    
                    // Update Bill Method if necessary
                    if(String.isBlank(opp.Sagitta_Bill_Method__c)){
                        opp.Sagitta_Bill_Method__c = agencySettings[0].Default_Bill_Method__c;
                    }
                }
            }
            when AFTER_UPDATE{
                Metadata__c intFlag = Metadata__c.getValues('IntegrationFlag');
                List<Id> oppIdsToDeleteFromOppLobJn = new List<Id>();

                for (Opportunity opp: workingRecords){
                    //Check if field Type of Business is changes or not
                    //If changed then delete the mappings of LOB and Opporunity from Jnc object
                    if(oldRecords != null && opp.Sagitta_Type_of_Business__c != oldRecords.get(opp.Id).Sagitta_Type_of_Business__c){
                        oppIdsToDeleteFromOppLobJn.add(opp.Id);
                    }
                    // Update Type of Business check boxes on Parent Account with TOB from Closed Won Opportunity
                    if(opp.StageName == 'Closed Won'){
                        // Get Parent Account
                      	List <Account> account = new List<Account>([SELECT Id FROM Account WHERE ID =:opp.AccountId LIMIT 1]);
                        for (Account acct:account){
                            // Check box for Opportunity Type of Business - does not change previous selections on Account
                            switch on opp.Sagitta_Type_of_Business__c {
                                when '1' {
                                    acct.Sagitta_TOB_IsPersonal__c = true;
                               }
                                when '2' {
                                    acct.Sagitta_TOB_IsCommercial__c = true;
                                }
                                when '3' {
                                    acct.Sagitta_TOB_IsNon_P_C__c = true;
                                }
                                when '4' {
                                    acct.Sagitta_TOB_IsBenefits__c = true;
                                }
                                when '5' {
                                    acct.Sagitta_TOB_IsLife__c = true;
                                }
                                when '6' {
                                    acct.Sagitta_TOB_IsHealth__c = true;
                                }
                                when '7' {
                                    acct.Sagitta_TOB_IsFinancial__c = true;
                                }
                                when else {
                                    acct.Sagitta_TOB_IsCommercial__c = true;
                                }
                            }
                       	}
                        
                        update(account);
                    }
                    
                    // Sagitta_Agency_Settings__c.Name is being autonumbered to '100000' when created in Apex
                    string agencySettingsName;
                    if (Test.isRunningTest()) {
                        agencySettingsName = '100000';
                    }
                    else {
                        agencySettingsName = '0';
                    }
                    string integrationStage;
                    if (agencySettings.size() > 0){
                        integrationStage = agencySettings.get(0).Integration_Stage__c;
                    }
                    if (((string.isNotBlank(integrationStage) && opp.StageName == integrationStage) || opp.StageName == 'Closed Won' || opp.StageName == 'Closed Lost') 
                        && (intFlag != null && string.isNotBlank(intFlag.Value__c) && intFlag.Value__c == 'true'))
                    {
                        String xmlBody = SFAMessenger.createXMLBody(opp.Id);
                        Id jobId = SFAMessenger.createAndSendJob(xmlBody);
                        return TriggerHandlerStatus.MESSAGES_SENT;
                    }
                }
                deleteOppLobJnRecds(oppIdsToDeleteFromOppLobJn);
            }
        }
        
        return TriggerHandlerStatus.NO_MESSAGES_SENT;
    }
    
    // Summary:
    // Verifies that there are no inactive/hidden records referenced in the following fields:
    //	 Division, Branch, Department, Group, Executive, Representative
    // If an inactive record is found, an error is added to the record
    // 
    // Details:
    // @param opp - The Opportunity being processed
    private void checkValidBusinessUnits(Opportunity opp){
        String errorField = '';
        String acctId = opp.AccountId;
        
        // Check validity of account
        string acctErrorField = getInvalidAccountFields(acctId);
        if (string.isNotBlank(acctErrorField)) {
            opp.addError('Error: The account associated with this opportunity has an invalid selection for ' + acctErrorField + '. Please fix the selection before updating this opportunity.');
        	return;
        }
        
        // Check validity of opportunity
        if (isDivHidden(opp.Sagitta_Division__c)) {
            errorField = 'Division';
        }
        else if (isBranchHidden(opp.Sagitta_Branch__c)) {
            errorField = 'Branch';
        }
        else if (isDeptHidden(opp.Sagitta_Department__c)) {
            errorField = 'Department';
        }
        else if (isGroupHidden(opp.Sagitta_Group__c)) {
            errorField = 'Group';
        }
        else if (!isEmployeeActive(opp.Sagitta_Opportunity_Exec__c)) {
            errorField = 'Executive';
        }
        else if (!isEmployeeActive(opp.Sagitta_Opportunity_Rep__c)) {
            errorField = 'Representative';
        }
        
        if (string.isNotBlank(errorField)) {
            opp.addError('Error: Invalid selection for ' + errorField);
        }
    }
    
    // Summary:
    // Verifies that there are no inactive records referenced in Account:
    //	 Division, Branch, Department, Group, Executive, Representative
    // If an inactive record is found, returns false 
    // 
    // Details:
    // @param account - The account whose fields we're checking
    // @return Boolean
    private string getInvalidAccountFields(string accountId){
		List<Account> account = [SELECT Sagitta_Division__c, Sagitta_Branch__c, Sagitta_Department__c, Sagitta_Group__c, Sagitta_Employee_Account_Exec__c, Sagitta_Employee_Account_Rep__c FROM Account WHERE ID =: accountId LIMIT 1];
        for (Account acct:account){
            if (isDivHidden(acct.Sagitta_Division__c)) {
                return 'Division';
            }
            else if (isBranchHidden(acct.Sagitta_Branch__c)) {
                return 'Branch';
            }
            else if (isDeptHidden(acct.Sagitta_Department__c)) {
                return 'Department';
            }
            else if (isGroupHidden(acct.Sagitta_Group__c)) {
                return 'Group';
            }
            else if (!isEmployeeActive(acct.Sagitta_Employee_Account_Exec__c)) {
                return 'Executive';
            }
            else if (!isEmployeeActive(acct.Sagitta_Employee_Account_Rep__c)) {
                return 'Representative';
            }
        }
        return '';
    }
    
    // Summary: Next five private methods
    // Validate if entities are Inactive or Hidden
    //
    // @param Id - Applicable Id of the entity being checked
    // @return Boolean - returns null if there are no records
    private static boolean isEmployeeActive(string employeeId) {
        for (Sagitta_Employee__c emp : allEmployees) {
            if (employeeId == emp.Id) {
                string status = emp.Sagitta_Employee_Status__c;
                boolean isActive = (status == 'Active');
                return isActive;
            } 
        }
        return true;
    }
    private static boolean isDivHidden(string divId) {
        for (Sagitta_Division__c div : allDivisions) {
            if (divId == div.Id) {
                return div.Sagitta_isHidden__c;
            }
        }
        return true;
    }
    private static boolean isDeptHidden(string deptId) {
        for (Sagitta_Department__c dept : allDepartments) {
            if (deptId == dept.Id) {
                return dept.Sagitta_isHidden__c;
            }
        }
        return true;
    }
    private static boolean isBranchHidden(string branchId) {
        for (Sagitta_Branch__c branch : allBranches) {
            if (branchId == branch.Id) {
                return branch.Sagitta_isHidden__c;
            }
        }
        return true;
    }
    private static boolean isGroupHidden(string groupId) {
        for (Sagitta_Group__c grp : allGroups) {
            if (groupId == grp.Id) {
                return grp.Sagitta_isHidden__c;
            }
        }
        return true;
    }
    private void deleteOppLobJnRecds(List<Id> oppIdsToDelete) {
        List<Sagitta_Opportunity_LOB_Junction__c> jnRecords = [SELECT Id FROM Sagitta_Opportunity_LOB_Junction__c 
                                                                   WHERE Sagitta_Opportunity__c IN :oppIdsToDelete];
        Database.delete(jnRecords);
    }
}