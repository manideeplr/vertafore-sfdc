@isTest
public class SFAMessengerTest {
    @testSetup
    public static void testSetup() {
        createIntegrationFlag('true');
        createIsInitialSyncFlag('false');
        
        Sagitta_Employee__c employee = new Sagitta_Employee__c(
            Name = 'ExecAndRep',
            Sagitta_Is_Executive__c = true,
            Sagitta_Is_Representative__c = true,
            Sagitta_Employee_Code__c = 'ABCDEFG',
            Sagitta_Entity_Id__c = 'cjhcj',
            Sagitta_Employee_Status__c = 'Active'
        );
        insert employee;
        
        Sagitta_Division__c SagittaDivision = new Sagitta_Division__c(
        	Name = 'Division One',
            ItemCode__c = '$$$'
        );
        insert SagittaDivision;
        
        Sagitta_Branch__c SagittaBranch = new Sagitta_Branch__c(
        	Name = 'Branch One',
            ItemCode__c = '***'
        );
        insert SagittaBranch;
        
        Sagitta_Department__c SagittaDepartment = new Sagitta_Department__c(
        	Name = 'Department One',
            ItemCode__c = '+++'
        );
        insert SagittaDepartment;
        
        Sagitta_Group__c SagittaGroup = new Sagitta_Group__c(
        	Name = 'Group One',
            ItemCode__c = '&&&'
        );
        insert SagittaGroup;

        DivBrJunction__c divBrJunc = new DivBrJunction__c(Sagitta_Division__c = SagittaDivision.Id, Sagitta_Branch__c = SagittaBranch.Id);
        BrDepJunction__c brDepJunc = new BrDepJunction__c(Sagitta_Branch__c = SagittaBranch.Id, Sagitta_Department__c = SagittaDepartment.Id);
        DepGrpJunction__c depGrpJunc = new DepGrpJunction__c(Sagitta_Department__c = SagittaDepartment.Id, Sagitta_Group__c = SagittaGroup.Id);
        insert divBrJunc;
        insert brDepJunc;
        insert depGrpJunc;
        
        Sagitta_Mapping__c mapping = new Sagitta_Mapping__c(
        	Name = 'MyMapping',
            Sagitta_Assigned_To__c = UserInfo.getUserId(),
            Sagitta_Division__c = SagittaDivision.Id,
            Sagitta_Branch__c = SagittaBranch.Id,
            Sagitta_Department__c = SagittaDepartment.Id,
            Sagitta_Group__c = SagittaGroup.Id,
            Sagitta_Executive__c = employee.Id,
            Sagitta_Representative__c = employee.Id
        );
        insert mapping;
        
        Account account = new Account(
            Name = 'Acct One',
            Sagitta_Client_Type__c = 'P',
            Sagitta_Employee_Account_Exec__c = employee.Id,
            Sagitta_Employee_Account_Rep__c = employee.Id
        );
        insert account;
    }
    
    @isTest
    public static void createXMLBody_ShouldReturnXMLString_WhenPassedEntityId() {
        Account account = [SELECT Id FROM Account WHERE Name = 'Acct One'];
        
        String actual = SFAMessenger.createXMLBody(account.Id);
        
        String expected = 
        '<?xml version="1.0"?>' +
        '<i:Envelope xmlns:i="http://schemas.xmlsoap.org/soap/envelope/">' +
            '<i:Body xmlns:j="http://soap.sforce.com/2005/09/outbound">' +
                '<j:notifications>' +
					'<j:OrganizationId>' + UserInfo.getOrganizationId() + '</j:OrganizationId>' +
                    '<j:Notification xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">' +
						'<j:sObject xsi:type="sf:Account" xmlns:sf="urn:sobject.enterprise.soap.sforce.com">' +
							'<sf:Id>' + account.Id + '</sf:Id>' +
                      	'</j:sObject>' +
                    '</j:Notification>' +
                '</j:notifications>' +
            '</i:Body>' +
        '</i:Envelope>';
        
        System.assertEquals(expected, actual);
    }
    
    @isTest
    public static void createAndSendJob_ShouldReturnJobId_WhenPassedXMLBody() {
        Account account = [SELECT Id FROM Account WHERE Name = 'Acct One'];
        String xmlBody = SFAMessenger.createXMLBody(account.Id);
        
        Id actual = SFAMessenger.createAndSendJob(xmlBody);
        
        String expected = '001xa0000011111';
        System.assertEquals(expected, actual);
    }
    
    private static void createIntegrationFlag(String flag) {
        Metadata__c intFlag = new Metadata__c();
        intFlag.Name = 'IntegrationFlag';
        intFlag.Value__c = flag;
        insert intFlag;
    }
    
    private static void createIsInitialSyncFlag(String flag) {
        Metadata__c isInitialSyncFlag = new Metadata__c();
        isInitialSyncFlag.Name = 'IsInitialSync';
        isInitialSyncFlag.Value__c = flag;
        insert isInitialSyncFlag;
    }
}