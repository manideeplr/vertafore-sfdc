/*
    Author : Manideep
    Name : SagittaMappingQuickCreateFooterController
    Date : 1 November 2019
    Description: Checks status of required Mapping fields before Map record is saved,
                 returns a message to user if any field is empty
    Modification History :
    *************************************************************
    Version             Author          Date                        Description
       1               Manideep    1 November 2019                    Original
*/
public class SgtMappingQuickCreateFooterController {
    // Summary:
    // Checks for null/empty Map fields, saves Map if all fields are filled
    //
    // @param name - Name field of Map record
    // @param assignedTo - assignedTo field of Map record
    // @param exec - executive field of Map record
    // @param rep - representative field of Map record
    // @param div - division field of Map record
    // @param branch - branch field of Map record
    // @param dept - department field of Map record
    // @param grp - group field of Map record
    // @return String - Salesforce ID of the new Map object
    @AuraEnabled
    public static String saveMapping(string name, string assignedTo, string exec,string rep, 
                                     string div, string branch, string dept, string grp) { // group is a keyword
        
        try{
            boolean nullCheck = false;
            String nullCheckMsg = 'These Required fields must be completed :';
            if(String.isBlank(name)) {
                nullCheck = true;
                nullCheckMsg += ' Sagitta Mapping Name,';
            }
            if(String.isBlank(assignedTo)) {
                nullCheck = true;
                nullCheckMsg += ' Assigned To,';
            }
            if(String.isBlank(exec)) {
                nullCheck = true;
                nullCheckMsg += ' Executive,';
            }
            if(String.isBlank(rep)) {
                nullCheck = true;
                nullCheckMsg += ' Representative,';
            }
            if(String.isBlank(div)) {
               nullCheck = true;
               nullCheckMsg += ' Division,';
            }
            if(String.isBlank(branch)) {
               nullCheck = true;
               nullCheckMsg += ' Branch,';
            }
            if(String.isBlank(dept)) {
                nullCheck = true;
                nullCheckMsg += ' Department,';
            }
            if(String.isBlank(grp)) {
                nullCheck = true;
                nullCheckMsg += ' Group,';
            } 
            if(nullCheck){
                nullCheckMsg = nullCheckMsg.removeEnd(',');
                AuraHandledException ex = new AuraHandledException(nullCheckMsg);
                ex.setMessage(nullCheckMsg);
                ApplicationLogger.logMessage('SagittaMappingQuickCreateFooterController', 'SaveMapping.nullCheck', nullCheckMsg, ApplicationLogger.ERROR);
                throw ex;
            }else {
                   Sagitta_Mapping__c mapping = new Sagitta_Mapping__c(
                       Name=name,
                       Sagitta_Assigned_To__c = [SELECT Id FROM User WHERE Name =: assignedTo][0].Id,
                       Sagitta_Executive__c = [SELECT Id FROM Sagitta_Employee__c WHERE Name =: exec][0].Id,
                       Sagitta_Representative__c = [SELECT Id FROM Sagitta_Employee__c WHERE Name =: rep][0].Id,
                       Sagitta_Division__c = [SELECT Id FROM Sagitta_Division__c WHERE Name =: div][0].Id,
                       Sagitta_Branch__c = [SELECT Id FROM Sagitta_Branch__c WHERE Name =: branch][0].Id,
                       Sagitta_Department__c = [SELECT Id FROM Sagitta_Department__c WHERE Name =: dept][0].Id,
                       Sagitta_Group__c = [SELECT Id FROM Sagitta_Group__c WHERE Name =: grp][0].Id
                   );
                   insert mapping;
                   return mapping.Id; 
               }
        }catch(Exception ex) {
            String message = 'Error saving Sagitta Mapping; ' +
                'Error: ' + ex.getMessage() + '; Stack Trace: ' + ex.getStackTraceString();
            ApplicationLogger.logMessage('SagittaMappingQuickCreateFooterController', 'SaveMapping', message, ApplicationLogger.ERROR);
            throw new AuraHandledException('Error saving the Sagitta Mapping file: ' + ex.getMessage());
        }
    }
}