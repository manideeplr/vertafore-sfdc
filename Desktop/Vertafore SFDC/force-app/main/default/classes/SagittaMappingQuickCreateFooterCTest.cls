@isTest
public class SagittaMappingQuickCreateFooterCTest {
	@testSetup
    public static void setup() {
        Profile p = [SELECT Id FROM Profile WHERE Name='Standard User']; 
        User user = new User(Alias = 'standt', 
                             Email='standarduser@testorg.com', 
            				 EmailEncodingKey='UTF-8',
                             LastName='Testing',
                             LanguageLocaleKey='en_US', 
            				 LocaleSidKey='en_US',
                             ProfileId = p.Id, 
            				 TimeZoneSidKey='America/Los_Angeles',
                             UserName='vert-test@vertafore.com');
        insert user;
        
        createIntegrationFlag('true');
        createIsInitialSyncFlag('false');
        
    	Sagitta_Activity_Action_Type__c activityActionType = new Sagitta_Activity_Action_Type__c();
        activityActionType.Sagitta_Activity_Action_Id__c = '26544';
        Sagitta_Issuing_Carrier__c issuingCarrier = new Sagitta_Issuing_Carrier__c(
            Sagitta_Entity_ID__c= 'ID text', 
            Sagitta_Carrier_Type__c= 'N',
            Sagitta_IsHidden__c = false,
            Sagitta_Issuing_Carrier_Code__c= 'text');
        insert activityActionType;
        insert issuingCarrier;
        
        Sagitta_Employee__c amsExecRep = new Sagitta_Employee__c(
            Name = 'ExecRep',
            Sagitta_Is_Representative__c = true,
            Sagitta_Is_Executive__c = true,
            Sagitta_Employee_Code__c = '123456',
            Sagitta_Entity_ID__c = '2203450',
            Sagitta_Employee_Status__c = 'Active'
        );
        insert amsExecRep;
        
        Sagitta_Agency_Settings__c agencySettings1 = new Sagitta_Agency_Settings__c(
            Sagitta_Agency_Number__c='text', 
            Default_Activity_Action_Type__c= [SELECT Id FROM Sagitta_Activity_Action_Type__c LIMIT 1].Id,
            Default_Bill_Method__c= 'A', 
            Default_Issuing_Carrier__c= [SELECT Id FROM Sagitta_Issuing_Carrier__c LIMIT 1].Id, 
            Default_Policy_Number__c='text', 
            Integration_User_Id__c='text',
            Default_Agency_Admin__c = [SELECT Id FROM Sagitta_Employee__c LIMIT 1].Id,
            Vertafore_Id__c='text');
        insert agencySettings1; 

		Sagitta_Division__c div1 = new Sagitta_Division__c(Name = 'Division 1');
        Sagitta_Department__c dept1 = new Sagitta_Department__c(Name = 'Department 1');
        Sagitta_Branch__c branch1 = new Sagitta_Branch__c(Name = 'Branch 1');
        Sagitta_Group__c grp1 = new Sagitta_Group__c(Name = 'Group 1');
        insert div1;
        insert dept1;
        insert branch1;
        insert grp1;
        
        DivBrJunction__c divBrJunc = new DivBrJunction__c(Sagitta_Division__c = div1.Id, Sagitta_Branch__c = branch1.Id);
        BrDepJunction__c brDepJunc = new BrDepJunction__c(Sagitta_Branch__c = branch1.Id, Sagitta_Department__c = dept1.Id);
        DepGrpJunction__c depGrpJunc = new DepGrpJunction__c(Sagitta_Department__c = dept1.Id, Sagitta_Group__c = grp1.Id);
        insert divBrJunc;
        insert brDepJunc;
        insert depGrpJunc;
    }
    
    @isTest
    public static void InsertValidMapping() {
        string name = 'MyMapping';
        string assignedTo = UserInfo.getName();
        string exec = [SELECT Name FROM Sagitta_Employee__c][0].Name;
        string rep = exec;
        string div = [SELECT Name FROM Sagitta_Division__c][0].Name;
        string branch = [SELECT Name FROM Sagitta_Branch__c][0].Name;
        string dept = [SELECT Name FROM Sagitta_Department__c][0].Name;
        string grp = [SELECT Name FROM Sagitta_Group__c][0].Name;
        
        string mappingId = SgtMappingQuickCreateFooterController.saveMapping(name, assignedTo, exec, rep, div, branch, dept, grp);
        
        System.assert(!String.isEmpty(mappingId));
    }
    
    @isTest
    public static void InsertIncompleteMapping() {
        try {
            string mappingId = SgtMappingQuickCreateFooterController.saveMapping(null, null, null, null, null, null, null, null);
        }
        catch (Exception e) {
            System.assert(String.isNotBlank(e.getMessage()));
        }
    }
    
    private static void createIntegrationFlag(String flag) {
        Metadata__c intFlag = new Metadata__c();
        intFlag.Name = 'IntegrationFlag';
        intFlag.Value__c = flag;
        insert intFlag;
    }
    
    private static void createIsInitialSyncFlag(String flag) {
        Metadata__c isInitialSyncFlag = new Metadata__c();
        isInitialSyncFlag.Name = 'IsInitialSync';
        isInitialSyncFlag.Value__c = flag;
        insert isInitialSyncFlag;
    }
}