/*
    Author : Manideep
    Name : RestServiceToUpsertContactRecords
    Date : 1 November 2019
    Description: Rest Service to upsert the contact object records
    Modification History :
    *************************************************************
    Version               Author             Date                      Description
    1					 Manideep		 1 November 2019			     Original
*/
@RestResource(urlMapping='/UpsertContactRecords/*')
global with sharing class RestServiceToUpsertContactRecords {
    global class SagittaContact {
        global String CustomerId { get; set; }
        global String CCntId { get; set; } 
        global String ContactName { get; set; }
        global String EMail { get; set; }
        
        global String BusinessAreaCode { get; set; }
        global String BusinessExtension { get; set; }
        global String BusinessPhone { get; set; }
        
        global String MobileAreaCode { get; set; }
        global String MobileExtension { get; set; }
        global String MobilePhone { get; set; }
        
        global String ResidenceAreaCode { get; set; }
        global String ResidenceExtension { get; set; }
        global String ResidencePhone { get; set; }
        global String FaxAreaCode { get; set; }
        global String FaxExtension { get; set; }
        global String FaxPhone { get; set; }
        
        global String Address1 { get; set; }
        global String Address2 { get; set; }
        global String City { get; set; }
        global String State { get; set; }
        global String ZipCode { get; set; }
    }
    
    global class NameParts {
        global String FirstName { get; set; }
        global String LastName { get; set; }
    }
    
    global class ResponseHandler {        
        public Boolean Success { get; set; }
        public String Status { get; set; }
        public String Message { get; set; }
    }
    
    private static List<Contact> existingContacts;
    private static List<Account> accounts;
    
    private static RestServiceContactContext contactContext;
    
    // Summary:
    // Deserialize the request body and insert the data
    // @param - No Param
    // @return - ResponseHandler
    @HttpPost
    global static ResponseHandler doPost() {
        ResponseHandler obj = new ResponseHandler();        
        RestRequest req = RestContext.request;
        String postBody;
        String failedRecordIds = '';

          try{
            postBody = req.requestBody.toString();
            
            Schema.SObjectField extIdfield = Contact.Fields.Sagitta_Customer_Contact_ID__c;
            List<SagittaContact> contacts = (List<SagittaContact>)JSON.deserialize(postBody, List<SagittaContact>.class);
            
            contactContext = new RestServiceContactContext(contacts);

            List<Contact> contactsToUpsert = new List<Contact>();
            for (SagittaContact contact : contacts) {
                 Contact currentContact = contactContext.findContact(contact.CCntId);
                if(String.isBlank(currentContact.AccountId)){
                    currentContact.AccountId = contactContext.findAccount(contact.CustomerId).Id;
                    if(String.isBlank(currentContact.AccountId)){
                        // couldnt find an Account which shouldn't happen
                        List<Object> parameters = new List<Object>();
                        parameters.add(contact.CustomerId);
                        parameters.add(contact.CCntId);
                        String formatted = String.format('Couldnt find a corresponding Account for Customer Id: {0} Contact Id: {1}', parameters);
                        
                        NoDataFoundException ex = new NoDataFoundException();
                        ex.setMessage(formatted);
                        throw ex;
                    }
                }                
                //currentContact = mapFields(currentContact, contact);
                if(mapFields(currentContact, contact)){
                    contactsToUpsert.add(currentContact);
                }
            }

            if(contactsToUpsert.size() > 0){
                Database.UpsertResult [] results = Database.upsert(contactsToUpsert, extIdfield, false);
            	for (Database.UpsertResult result : results) {
                	if (!result.isSuccess()) {
                    	Database.Error[] upsertErrors = result.getErrors();
                    	for (Database.Error upsertError : upsertErrors) {
                        	failedRecordIds = failedRecordIds + upsertError.getMessage() + upsertError.getFields();
                    	}
                	}
            	}
            }

            if (String.isNotBlank(failedRecordIds)) {
                RestContext.response.statusCode = 400;
                obj.success = false;
                obj.status='ERROR';
                obj.message= failedRecordIds;
                ApplicationLogger.logWebServiceMessage('RestServiceToUpsertContactRecords', 'doPost', failedRecordIds, 
                                                   ApplicationLogger.ERROR, postBody, JSON.serialize(obj));
            } else {
                obj.success = true;
                obj.status='SUCCESS';
                obj.message= 'SUCCESS';
                ApplicationLogger.logWebServiceMessage('RestServiceToUpsertContactRecords', 'doPost', 'SUCCESS', 
                                                   ApplicationLogger.INFO, postBody, JSON.serialize(obj));
            }
            
            return obj;
        } catch (Exception ex) {
            RestContext.response.statusCode = 400;
            obj.success = false;
            obj.status ='ERROR';
            obj.message = ex.getMessage();
            String message = 'ERROR: ' + ex.getMessage() + '; Stack Trace:' + ex.getStackTraceString();  
            obj.message = Message;  
            system.debug('ERROR: ' + ex.getMessage() + '; Stack Trace:' + ex.getStackTraceString());        
            ApplicationLogger.logWebServiceMessage('RestServiceToUpsertContactRecords', 'doPost', message, 
                                                   ApplicationLogger.ERROR, postBody, JSON.serialize(obj));
            return obj;
        }
    }
    
    // Map fields if there are updates
    // @param - Account account, SagittaCustomer customer
    // @return - boolean
    private static boolean mapFields(Contact existingContact, SagittaContact SagittaContact){
        boolean fieldsUpdated = false;
        
        if(existingContact.Sagitta_Customer_ID__c != SagittaContact.CustomerId){
        	existingContact.Sagitta_Customer_ID__c = SagittaContact.CustomerId;
            fieldsUpdated = true;
        }
        
        if(existingContact.Sagitta_Customer_Contact_ID__c != SagittaContact.CCntId){
        	existingContact.Sagitta_Customer_Contact_ID__c = SagittaContact.CCntId;
            fieldsUpdated = true;
        }

        NameParts names = splitName(SagittaContact.ContactName);
        
        if(existingContact.LastName != names.LastName){
        	existingContact.LastName = names.LastName;
            fieldsUpdated = true;
        }

        if(existingContact.FirstName != names.FirstName){
        	existingContact.FirstName = names.FirstName;
            fieldsUpdated = true;
        }

        if(existingContact.Email != SagittaContact.Email){
            existingContact.Email = SagittaContact.Email;
            fieldsUpdated = true;
        }

        string phone = formatPhoneNumber(SagittaContact.BusinessAreaCode, SagittaContact.BusinessPhone, SagittaContact.BusinessExtension);  
        string mobilePhone = formatPhoneNumber(SagittaContact.MobileAreaCode, SagittaContact.MobilePhone, SagittaContact.MobileExtension);      
        string homePhone = formatPhoneNumber(SagittaContact.ResidenceAreaCode, SagittaContact.ResidencePhone, SagittaContact.ResidenceExtension);    
        string fax = formatPhoneNumber(SagittaContact.FaxAreaCode, SagittaContact.FaxPhone, SagittaContact.FaxExtension);
        
        if(existingContact.Phone != phone){
        	existingContact.Phone = phone;
            fieldsUpdated = true;
        }  
        
        if(existingContact.MobilePhone != mobilePhone){
        	existingContact.MobilePhone = mobilePhone;
            fieldsUpdated = true;
        }  
        
        if(existingContact.HomePhone != homePhone){
        	existingContact.HomePhone = homePhone;
            fieldsUpdated = true;
        }

        if(existingContact.Fax != fax){
        	existingContact.Fax = fax;
            fieldsUpdated = true;
        }          

        string address = SagittaContact.Address1;
        if(SagittaContact.Address2 != ''){
            address = address + '\n' + SagittaContact.Address2;
        }
        
        if(existingContact.MailingStreet != address){
        	existingContact.MailingStreet = address;
            fieldsUpdated = true;
        }
        
        if(existingContact.MailingCity != SagittaContact.City){
        	existingContact.MailingCity = SagittaContact.City;
            fieldsUpdated = true;
        }
		
        if(existingContact.MailingState != SagittaContact.State){
        	existingContact.MailingState = SagittaContact.State;
            fieldsUpdated = true;
        }

        string zipCode = '';
        if(string.isNotBlank(SagittaContact.ZipCode))
        {
            formatZipCode(SagittaContact.ZipCode);
        }
        
        if(existingContact.MailingPostalCode != zipCode){
        	existingContact.MailingPostalCode = zipCode;
            fieldsUpdated = true;
        }        
        
        return fieldsUpdated;
    }
    
    private static string formatPhoneNumber(String areaCode, String phoneNumber, string extenstion){
        String phone = '';
        
        if(areaCode != ''){
            phone = '(' + areaCode + ') ';
        }
        
        if(string.isNotBlank(phoneNumber)  && !phoneNumber.contains('-')){
            phone = phone + phoneNumber.substring(0,3) + '-' + phoneNumber.substring(3);    
        }
        else {
            phone = phone + phoneNumber;
        }
        
        if(extenstion != ''){
            phone = phone + ' ext ' + extenstion;
        }
        
        return phone;
    }
    
    private static String formatZipCode(String zipCode){
        zipCode = zipCode.remove('-');
        zipCode = zipCode.remove(' ');
        
        if(zipCode.length() == 5){
            return zipCode;
        }
        
        if(zipCode.length() == 9){
            return zipCode.substring(0, 5) + '-' + zipCode.substring(5);
        }
        
        if(zipCode.length() == 6){
            return zipCode.substring(0, 3) + '-' + zipCode.substring(3);
        }
        
        return zipCode;
    }
    
    private static NameParts splitName(string contactName){
        string firstName = '';
        string lastName = '';
        
        List<string> parts = new List<string>();
        // Split on period, commas or spaces, but don't remove from results.
        
        string currentPart = '';
        
        if(string.isNotBlank(contactName)){

        
            for(integer i = 0; i < contactName.length(); i++){
                
                String currentLetter = contactName.substring(i, i + 1);
                
                currentPart = currentPart + currentLetter;
                
                if(currentLetter == '.' || currentLetter == ',' || currentLetter == ' '){                
                    parts.add(currentPart);
                    currentPart = '';                
                }
            }
        }   
        
        if(currentPart != ''){
            parts.add(currentPart);
        }
        //contains comma or not
        boolean containsComma = false;
        if(string.isNotBlank(contactName))
        {
            containsComma = contactName.contains(',');
        }
        // Remove any empty parts
        /*
        for (integer a = parts.size() - 1; a >= 0; a--){
            if (parts[a].Trim() == ''){
                parts.remove(a);
            }            	
        }
        */
        
        // If only one part 
        if (parts.size() == 1){
            lastName = parts[0].Replace(',', '').Trim();
        }
        // If name contains a comma and contains at least two parts, assume format:
        //   Last [...Last...], First [...First...] Middle
        else if (containsComma){
            //All parts before first comma are last name
            integer x;
            for (x = 0; (x < parts.size()) && (parts[x].IndexOf(',') == -1); x++){
                lastName += parts[x].Trim() + ' ';
            }
            
            lastName += parts[x].Replace(',', '').Trim();
            x++;
            //assign first name
            if (parts.size() - x > 1){
                for (; x < parts.size() - 1; x++){
                    firstName += parts[x].Replace(',', '').Trim() + ' ';
                }                    
                firstName = firstName.Trim();
            }
            else//no middle name
            {
                for (; x < parts.size(); x++){
                    firstName += parts[x].Replace(',', '').Trim() + ' ';
                }                    
                firstName = firstName.Trim();
            }
        }
        
        // If two parts, First Last
        else if (parts.size() == 2){
            firstName = parts[0].Trim();
            lastName = parts[1].Trim();
        }
        // Three or more parts, assume format:
        // First Middle Last [...Last...]
        else if(parts.size()>0){
            firstName = parts[0].Trim();            
            for (integer y = 2; y < parts.size(); y++) {
                lastName += parts[y].Trim() + ' ';
            }
            lastName = lastName.Trim();
        }
        
        NameParts names = new NameParts();
        names.FirstName = firstName;
        names.LastName = lastName;
        
        return names;
    }
    
}