/*
    Author : Manideep
    Name : RestServiceToUpsertVssoUserConfig
    Date : 1 November 2019
    Description:
    Modification History :
    *************************************************************
    Version               Author             Date                      Description
       1                 Manideep         1 November 2019                Original
*/

@RestResource(urlMapping='/UpsertVssoUserConfig/*')
global with sharing class RestServiceToUpsertVssoUserConfig {
    
    global class VssoUserConfig {
        global String UserId { get; set; }
        global String VimId { get; set; }
        global String ExecutiveIds { get; set; }
    }
    
    global class ResponseHandler {
        public Boolean Success { get; set; }
        public String Status { get; set; }
        public String Message { get; set; }
    }
    
    @HttpPost
    global static ResponseHandler doPost() {
        ResponseHandler obj = new ResponseHandler();
        
        RestRequest req = RestContext.request;
        String postBody = req.requestBody.toString();

        try {            
            VssoUserConfig config = (VssoUserConfig)JSON.deserialize(postBody, VssoUserConfig.class);   
            
            List<User> users = [SELECT Id, Vim_ID__c FROM User WHERE Vim_ID__c =: config.VimId LIMIT 1];
            
            User currentSetUser = null;
            if(users.size() > 0){
                currentSetUser = users[0];
            }

        	if(currentSetUser != null){
            	if(currentSetUser.Id != config.UserId){
                	//Switching VIM to a different user
                	currentSetUser.Vim_ID__c = null;
                	update currentSetUser;                
            	}
        	}
        
        	//Set the current desired user to the specified VIM
        	currentSetUser = [SELECT Id, Vim_ID__c FROM User WHERE Id =: config.UserId LIMIT 1];
        	currentSetUser.Vim_ID__c = config.VimId;
        	update currentSetUser;     

        	//Set assigned executives
            // When updating a setup object (User in this case) you cannot update any other kind of objects. To work around this we use a @future method on the Util class to update the employee.
        	Util.setEmployees(config.UserId, config.ExecutiveIds);
            
            obj.success = true;
            obj.status ='success';
            obj.message = 'success';
		    return obj;
        }
        catch(Exception ex) {
            ApplicationLogger.logMessage('RestServiceToUpsertVssoUserConfig', 'doPost', null, ApplicationLogger.INFO);      
            RestContext.response.statusCode = 400;
            obj.success = false;
            obj.status ='Error';
            obj.message = ex.getMessage();
            return obj;
        }
    }
}