/*
    Author : Manideep
    Name : SFAMessenger
    Date : 1 November 2019
    Description: Creates and enqueues Jobs for processing
    Modification History :
    *************************************************************
    Version               Author             Date                      Description
	   1			     Manideep		  1 November 2019               Original
*/

public class SFAMessenger {
    // Summary:
    // Creates XML request body string for SalesforceApp Adapter
    // 
    // Details:
    // @param Id entityId - SF Id of entity to be synced
    // @return String - representation of the XML request body
    //         
	
       /* @return:
        <?xml version="1.0"?>
        <i:Envelope xmlns:i="http://schemas.xmlsoap.org/soap/envelope/">
            <i:Body xmlns:j="http://soap.sforce.com/2005/09/outbound">
                <j:notifications>
					<j:OrganizationId>{{ UserInfo.getOrganizationId() }}</j:OrganizationId>
                    <j:Notification xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
						<j:sObject xsi:type="{{ entityTypeTag }}" xmlns:sf="urn:sobject.enterprise.soap.sforce.com">
							<sf:Id>{{ entityId }}</sf:Id>
                      	</j:sObject>
                    </j:Notification>
                </j:notifications>
            </i:Body>
        </i:Envelope> */
	
	public static String createXMLBody(Id entityId) {
        Schema.SObjectType entityType = entityId.getSobjectType();
        String entityTypeTag = 'sf:' + String.valueOf(entityType);
        XmlStreamWriter writer = new XmlStreamWriter();
        
        writer.writeStartDocument(null, '1.0');
        
            writer.writeStartElement('i', 'Envelope', 'http://schemas.xmlsoap.org/soap/envelope/');
            writer.writeNamespace('i', 'http://schemas.xmlsoap.org/soap/envelope/');
        
                writer.writeStartElement('i', 'Body', 'http://schemas.xmlsoap.org/soap/envelope/');
                writer.writeNamespace('j', 'http://soap.sforce.com/2005/09/outbound');
        
                    writer.writeStartElement('j', 'notifications', 'http://soap.sforce.com/2005/09/outbound');
                    writer.writeStartElement('j', 'OrganizationId', 'http://soap.sforce.com/2005/09/outbound');
                    writer.writeCharacters(UserInfo.getOrganizationId());
        			writer.writeEndElement(); // end OrganizationId
        
                        writer.writeStartElement(null , 'Notification', 'http://soap.sforce.com/2005/09/outbound');
                        writer.writeNamespace('xsi', 'http://www.w3.org/2001/XMLSchema-instance');
        
                            writer.writeStartElement(null , 'sObject', 'http://soap.sforce.com/2005/09/outbound');
                            writer.writeAttribute('xsi', 'Entity', 'type', entityTypeTag);
                            writer.writeNamespace('sf', 'urn:sobject.enterprise.soap.sforce.com');
        
                                writer.writeStartElement(null, 'sf:Id', null);
                                writer.writeCharacters(entityId);
        
                                writer.writeEndElement(); // end end sf:Id
                            writer.writeEndElement(); // end sObject
                        writer.writeEndElement(); // end Notification
                    writer.writeEndElement(); // end notifications
                writer.writeEndElement(); // end Body
            writer.writeEndElement(); // end Envelope
        writer.writeEndDocument(); // end Document
        system.debug('SFA  Messenger ' + writer.getXmlString());

        string body;
        body = '<?xml version="1.0"?>'+
'<i:Envelope xmlns:i="http://schemas.xmlsoap.org/soap/envelope/">'+
   '<i:Body xmlns:j="http://soap.sforce.com/2005/09/outbound">'+
      '<j:notifications>'+
       '<j:OrganizationId>'+UserInfo.getOrganizationId()+'</j:OrganizationId>'+
            '<j:Notification xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:j="http://soap.sforce.com/2005/09/outbound">'+
                 '<j:sObject xsi:type="sf:'+entityType+'" xmlns:sf="urn:sobject.enterprise.soap.sforce.com">'+
                     '<sf:Id>'+entityId+'</sf:Id>'+
                 '</j:sObject>'+
            '</j:Notification>'+
      '</j:notifications>'+
  '</i:Body>'+
'</i:Envelope>';
        Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
        String[] toAddresses = new String[] {'manideep.lankireddy@verys.com'}; 
        mail.setToAddresses(toAddresses);
        mail.setSubject('SFA Messenger Debug');
        mail.setPlainTextBody(body);
      //  Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });
        //return writer.getXmlString();
        return body;
    }
    
    // Summary:
    // Creates a Job instance which implements Queueuable interface and enqueues Job for processing
    // 
    // Details:
    // @param String xmlBody - String representation of XML request body
    // @return Id - Id of the Job that was sent, can be used to get the status of the Job
    public static Id createAndSendJob(String xmlBody) {
        // Because we can't mock the System.enququeJob method
        if (Test.isRunningTest()) {
            String mockId = '001xa0000011111';
			return Id.valueOf(mockId);
        } 
        else {
			Job job = new Job(xmlBody);
           ApplicationLogger.logMessage('SFAMessenger', 'createAndSendJob', 'Queueing Job for processing. XML body: ' + xmlBody, ApplicationLogger.INFO);
        	return System.enqueueJob(job);
        }
    }
}