/*
    Author : Manideep
    Name : RestServiceForConfigMetadata
    Date : 1 November 2019
    Description: Rest Service for inserting Custom Settings(Metadata)
    Modification History :
    *************************************************************
    Version               Author             Date                      Description
	1					 Manideep	   	1 November 2019				    Original
*/

@RestResource(urlMapping='/ConfigMetadata/*')
global with sharing class RestServiceForConfigMetadata {
    global class ResponseHandler {
        public Boolean Success { get; set; }
        public String Status { get; set; }
        public String Message { get; set; }
    }
    
    // Summary:
    // Inserts Custom Settings(Metadata) into Salesforce
    // 
    // @param N/A
    // @return ResponseHandler - object that stores response details
    @HttpPost
    global static ResponseHandler doPost() {
        ResponseHandler obj = new ResponseHandler();
        RestRequest req = RestContext.request;
        String failedRecordIds = '';
        try {
            // loop through all metadata in request body and inserts metadata
            String postBody = req.requestBody.toString();
            Map<String, String> mapReq = (Map<String, String>)JSON.deserialize(postBody, Map<String, String>.class);
            Map<String, Metadata__c> mdtList = Metadata__c.getAll();
            List<Metadata__c> mdtUpsert = new List<Metadata__c>();
            List<Metadata__c> mdtUpdate = new List<Metadata__c>();
            Metadata__c metaDataToInsert;
            Metadata__c metaDataToUpdate;
            for (String key : mapReq.keySet()) {
                if (mdtList.get(key) == null){
                    // Build Upsert list
                    metaDataToInsert = new Metadata__c();
                	metaDataToInsert.Name = key;
                	metaDataToInsert.Value__c = mapReq.get(key);
                	mdtUpsert.add(metaDataToInsert);
                	System.debug('mdtUpsert = ' + metaDataToInsert);
                	System.debug('mdtUpsert = ' + mdtUpsert);
                }
                else {
                    // Build Update list
                    metaDataToUpdate = mdtList.get(key);
                	metaDataToUpdate.Value__c = mapReq.get(key);
                	mdtUpdate.add(metaDataToUpdate);
                }
            }
            
            // To insert the list of Metadata__c records
            if (mdtUpsert.size() > 0){
                Database.UpsertResult[] results = Database.upsert(mdtUpsert, false);
            	System.debug('UpsertResults = ' + results);
            	for (Database.UpsertResult result : results) {
                	if (!result.isSuccess()) {
                    	Database.Error[] upsertErrors = result.getErrors();
                    	for (Database.Error upsertError : upsertErrors) {
                        	failedRecordIds = failedRecordIds + upsertError.getMessage() + upsertError.getFields();
                    	}
                	}
            	}
            }
            
            // To update the list of Metadata__c records
            if (mdtUpdate.size() > 0) {
                Database.SaveResult[] results = Database.update(mdtUpdate, false);
                System.debug('UpdateResults = ' + results);
            	for (Database.SaveResult result : results) {
                	if (!result.isSuccess()) {
                    	Database.Error[] updateErrors = result.getErrors();
                    	for (Database.Error updateError : updateErrors) {
                        	failedRecordIds = failedRecordIds + updateError.getMessage() + updateError.getFields();
                    	}
                	}
            	}
            }
            
            // Response handler initialization once the insert is successful/failure
            // Insert log into Application Log object for request and response json
            if (String.isNotBlank(failedRecordIds)) {
                obj.success = false;
                obj.status='ERROR';
                obj.message= failedRecordIds;
                ApplicationLogger.logWebServiceMessage('RestServiceForConfigMetadata', 'doPost', failedRecordIds, 
                                                   ApplicationLogger.ERROR, req.requestBody.toString(), JSON.serialize(obj));
            } else {
                obj.success = true;
                obj.status='SUCCESS';
                obj.message= 'SUCCESS';
                ApplicationLogger.logWebServiceMessage('RestServiceForConfigMetadata', 'doPost', 'SUCCESS', 
                                                   ApplicationLogger.INFO, req.requestBody.toString(), JSON.serialize(obj));
            }
            return obj;
        } 
        catch(Exception ex) {
            RestContext.response.statusCode = 400;
            obj.success = false;
            obj.status='Error';
            obj.message=ex.getMessage();
            String message = 'Error: ' + ex.getMessage() + '; Stack Trace:' + ex.getStackTraceString();
            ApplicationLogger.logWebServiceMessage('RestServiceForConfigMetadata', 'doPost', message, 
                                                   ApplicationLogger.ERROR, req.requestBody.toString(), JSON.serialize(obj));
            return obj;
        }        
    }
}