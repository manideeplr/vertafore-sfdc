@isTest
public class BusinessUnitSaver_Test {
	@testSetup
    public static void setup() {
        createIntegrationFlag('true');
        createIsInitialSyncFlag('false');
        
        Sagitta_Employee__c employee = new Sagitta_Employee__c(
            Name = 'Employee',
            Sagitta_Is_Executive__c = true,
            Sagitta_Is_Representative__c = true,
            Sagitta_Employee_Code__c = 'ABCDEFG',
            Sagitta_Entity_Id__c = 'cjhcj',
            Sagitta_Employee_Status__c = 'Active'
        );
        insert employee;
        
        Sagitta_Division__c div1 = new Sagitta_Division__c(Name = 'Division 1');
        Sagitta_Department__c dept1 = new Sagitta_Department__c(Name = 'Department 1');
        Sagitta_Branch__c branch1 = new Sagitta_Branch__c(Name = 'Branch 1');
        Sagitta_Group__c grp1 = new Sagitta_Group__c(Name = 'Group 1');
        insert div1;
        insert dept1;
        insert branch1;
        insert grp1;
        
        Sagitta_Branch__c branch2 = new Sagitta_Branch__c(Name = 'Branch 2');
        insert branch2;
        
        DivBrJunction__c divBrJunc = new DivBrJunction__c(Sagitta_Division__c = div1.Id, Sagitta_Branch__c = branch1.Id);
        BrDepJunction__c brDepJunc = new BrDepJunction__c(Sagitta_Branch__c = branch1.Id, Sagitta_Department__c = dept1.Id);
        DepGrpJunction__c depGrpJunc = new DepGrpJunction__c(Sagitta_Department__c = dept1.Id, Sagitta_Group__c = grp1.Id);
        insert divBrJunc;
        insert brDepJunc;
        insert depGrpJunc;
        
        Sagitta_Mapping__c mapping = new Sagitta_Mapping__c(
        	Name = 'MyMapping',
            Sagitta_Assigned_To__c = UserInfo.getUserId(),
            Sagitta_Division__c = div1.Id,
            Sagitta_Branch__c = branch1.Id,
            Sagitta_Department__c = dept1.Id,
            Sagitta_Group__c = grp1.Id,
            Sagitta_Executive__c = employee.Id,
            Sagitta_Representative__c = employee.Id
        );
        insert mapping;
        
        Sagitta_Activity_Action_Type__c activityActionType = new Sagitta_Activity_Action_Type__c(
            Sagitta_Activity_Action_Id__c = '26544'
        );
        Sagitta_Issuing_Carrier__c issuingCarrier = new Sagitta_Issuing_Carrier__c(
            Sagitta_Entity_ID__c= 'ID text', 
            Sagitta_Carrier_Type__c= 'N',
            Sagitta_IsHidden__c = false,
            Sagitta_Issuing_Carrier_Code__c= 'text'
        );
        insert activityActionType;
        insert issuingCarrier;
        
        Sagitta_Agency_Settings__c agencySettings1 = new Sagitta_Agency_Settings__c(
            Sagitta_Agency_Number__c='text', 
            Default_Activity_Action_Type__c= [SELECT Id FROM Sagitta_Activity_Action_Type__c LIMIT 1].Id,
            Default_Bill_Method__c= 'A', 
            Default_Issuing_Carrier__c= [SELECT Id FROM Sagitta_Issuing_Carrier__c LIMIT 1].Id, 
            Default_Policy_Number__c='text', 
            Integration_User_Id__c='text', 
            Vertafore_Id__c='text',
            Default_Agency_Admin__c = [SELECT Id FROM Sagitta_Employee__c LIMIT 1].Id
        );
        insert agencySettings1;
        
        Sagitta_Employee__c amsExecutive = new Sagitta_Employee__c(
            Name = 'Executive',
            Sagitta_Is_Executive__c = true,
            Sagitta_Employee_Code__c = 'ABCDEFGH',
            Sagitta_Entity_ID__c = '5235633',
            Sagitta_Employee_Status__c = 'Active'
        );
        Sagitta_Employee__c amsRepresentative = new Sagitta_Employee__c(
            Name = 'Representative',
            Sagitta_Is_Representative__c = true,
            Sagitta_Employee_Code__c = '123456',
            Sagitta_Entity_ID__c = '2203450',
            Sagitta_Employee_Status__c = 'Active'
        );
        insert amsExecutive;
       	insert amsRepresentative;
        
        Account account = new Account(
            Name = 'Test Account',
            Sagitta_Customer_ID__c = 'SagittaCUSTOMERID',
            Sagitta_Employee_Account_Exec__c = amsExecutive.Id,
            Sagitta_Employee_Account_Rep__c = amsRepresentative.Id,
            Sagitta_Client_Type__c = 'P',
            Sagitta_Division__c = div1.Id,
            Sagitta_Branch__c = branch1.Id,
            Sagitta_Department__c = dept1.Id,
            Sagitta_Group__c = grp1.Id,
            Sagitta_Type_of_Business__c = '7'
        );
        insert account;
        
        Opportunity notSyncingOpp = new Opportunity(
            Name = 'Not Synced', 
            CloseDate = Date.today(), 
            StageName = 'Prospecting',
            Sagitta_Opportunity_Type__c = 'M',
            Sagitta_Type_of_Business__c = '7',
            AccountId = account.Id
        );
        insert notSyncingOpp;
        
        Lead lead = new Lead(LastName = 'Horner', Company = 'MyCompany', Sagitta_Employee_Account_Exec__c = employee.Id, 
                             Sagitta_Employee_Account_Rep__c = employee.Id,
                            Sagitta_Division__c = div1.Id,
                            Sagitta_Branch__c = branch1.Id,
                            Sagitta_Department__c = dept1.Id,
                            Sagitta_Group__c = grp1.Id);
        insert lead;
    }
    
    @isTest
    public static void Lead_SaveBusinessUnits_FieldsAreValid_NoMappingExists_ShouldSucceed() {
        // Assert that fields are copied over
        Lead lead = [SELECT Id FROM Lead][0];
        boolean succeeded = BusinessUnitSaver.saveBusinessUnits('Division 1', 'Branch 1', 'Department 1', 'Group 1', (string)lead.Id, 'Lead');
		System.assertEquals(true, succeeded);
        
        // Assert that mapping was created
        List<Sagitta_Mapping__c> mappingList = [SELECT Id FROM Sagitta_Mapping__c];
        System.assertEquals(true, (mappingList.size() > 0));
    }
    
    @isTest
    public static void Lead_SaveBusinessUnits_BusinessUnitsNotFound_ShouldFail() {
        Lead lead = [SELECT Id FROM Lead][0];
        try {
            boolean succeeded = BusinessUnitSaver.saveBusinessUnits('Division G', 'Branch G', 'Department G', 'Group G', (string)lead.Id, 'Lead');
        }
        catch (Exception e) {
            System.assertEquals('Could not find Division, Branch, Department, or Group.', e.getMessage());
        }
    }
    
    @isTest
    public static void Lead_SaveBusinessUnits_InvalidCombination_ShouldFail() {
        Lead lead = [SELECT Id FROM Lead][0];
        try {
            boolean succeeded = BusinessUnitSaver.saveBusinessUnits('Division 1', 'Branch 2', 'Department 1', 'Group 1', (string)lead.Id, 'Lead');
        }
        catch (Exception e) {
            System.assertEquals('Invalid combination of Division, Branch, Department, or Group.', e.getMessage());
        }
    }
    
    @isTest
    public static void Lead_SaveBusinessUnits_LeadNotFound_ShouldFail() {
        try {
            boolean succeeded = BusinessUnitSaver.saveBusinessUnits('Division 1', 'Branch 1', 'Department 1', 'Group 1', 'LeadName', 'Lead');
        }
        catch (Exception e) {
            System.assertEquals('Could not find Lead.', e.getMessage());
        }
    }
    
    @isTest
    public static void Account_SaveBusinessUnits_FieldsAreValid_NoMappingExists_ShouldSucceed() {
        // Assert that fields are copied over
        Account acct = [SELECT Id FROM Account][0];
        boolean succeeded = BusinessUnitSaver.saveBusinessUnits('Division 1', 'Branch 1', 'Department 1', 'Group 1', (string)acct.Id, 'Account');
		System.assertEquals(true, succeeded);
        
        // Assert that mapping was created
        List<Sagitta_Mapping__c> mappingList = [SELECT Id FROM Sagitta_Mapping__c];
        System.assertEquals(true, (mappingList.size() > 0));
    }
    
    @isTest
    public static void Account_SaveBusinessUnits_AccountNotFound_ShouldFail() {
        try {
            boolean succeeded = BusinessUnitSaver.saveBusinessUnits('Division 1', 'Branch 1', 'Department 1', 'Group 1', 'Test Account', 'Account');
        }
        catch (Exception e) {
            System.assertEquals('Could not find Account.', e.getMessage());
        }
    }
    
    @isTest
    public static void Oppty_SaveBusinessUnits_FieldsAreValid_NoMappingExists_ShouldSucceed() {
        // Assert that fields are copied over
        Opportunity notSyncingOpp = [SELECT Id FROM Opportunity][0];
        boolean succeeded = BusinessUnitSaver.saveBusinessUnits('Division 1', 'Branch 1', 'Department 1', 'Group 1', (string)notSyncingOpp.Id, 'opportunity');
		System.assertEquals(true, succeeded);
        
        // Assert that mapping was created
        List<Sagitta_Mapping__c> mappingList = [SELECT Id FROM Sagitta_Mapping__c];
        System.assertEquals(true, (mappingList.size() > 0));
    }
    
    @isTest
    public static void Oppty_SaveBusinessUnits_OpptyNotFound_ShouldFail() {
        try {
            boolean succeeded = BusinessUnitSaver.saveBusinessUnits('Division 1', 'Branch 1', 'Department 1', 'Group 1', 'Not Synced', 'opportunity');
        }
        catch (Exception e) {
            System.assertEquals('Could not find Opportunity.', e.getMessage());
        }
    }
    
    @isTest
    public static void mapping_SaveBusinessUnits_FieldsAreValid_NoMappingExists_ShouldSucceed() {
        // Assert that fields are copied over
        Sagitta_Mapping__c mymapping = [SELECT Id FROM Sagitta_Mapping__c][0];
        boolean succeeded = BusinessUnitSaver.saveBusinessUnits('Division 1', 'Branch 1', 'Department 1', 'Group 1', (string)mymapping.Id, 'Sagitta_Mapping__c');
		System.assertEquals(true, succeeded);
        
        // Assert that mapping was created
        List<Sagitta_Mapping__c> mappingList = [SELECT Id FROM Sagitta_Mapping__c];
        System.assertEquals(true, (mappingList.size() > 0));
    }
    
    @isTest
    public static void mapping_SaveBusinessUnits_mappingNotFound_ShouldFail() {
        try {
            boolean succeeded = BusinessUnitSaver.saveBusinessUnits('Division 1', 'Branch 1', 'Department 1', 'Group 1', 'MyMapping', 'Sagitta_Mapping__c');
        }
        catch (Exception e) {
            System.assertEquals('Could not find Mapping.', e.getMessage());
        }
    }
    
    @isTest
    public static void mapping_SaveBusinessUnits_initListMethodCall() {
      	Test.startTest();
      	List<String> strList = BusinessUnitSaver.initDivisionList();
      	Test.stopTest();
      	system.assertEquals(1, strList.size());
    }
    
    @isTest
    public static void mapping_SaveBusinessUnits_getBUListMethodCall() {
      	Test.startTest();
      	List<String> strList = BusinessUnitSaver.getBUList('Branch 1', 'Sagitta_Branch__r', 'Sagitta_Department__c', 'BrDepJunction__c');
      	Test.stopTest();
      	system.assertEquals(1, strList.size());
    }
    
    @isTest
    public static void Oppty_SaveBusinessUnits_InvalidRecordType() {
        try {
            BusinessUnitSaver.saveBusinessUnits('Division 1', 'Branch 1', 'Department 1', 'Group 1', null, 'MyOpp');
        } catch(Exception e) {
            System.assertEquals('Invalid record type', e.getMessage());
        }
    }
    
    @isTest
    public static void Oppty_getRecord_InvalidRecordType() {
        try {
            BusinessUnitSaver.getRecord('Division 1', 'Opp');
        } catch(Exception e) {
            System.assertEquals('Script-thrown exception', e.getMessage());
        }
    }
    
    @isTest
    public static void test_getRecord() {        
        String[] expectedNames = new String[]{'Division 1', 'Branch 1', 'Department 1', 'Group 1'};
        Sagitta_Mapping__c mymapping = [SELECT Id FROM Sagitta_Mapping__c][0];
        Opportunity notSyncingOpp = [SELECT Id FROM Opportunity][0];
        Account acct = [SELECT Id FROM Account][0];
        Lead lead = [SELECT Id FROM Lead][0];
        String[] businessUnitNames1 = BusinessUnitSaver.getRecord((String)mymapping.Id, 'Sagitta_Mapping__c');
        system.assertEquals(expectedNames, businessUnitNames1);
        String[] businessUnitNames2 = BusinessUnitSaver.getRecord(lead.Id, 'lead');
        system.assertEquals(expectedNames, businessUnitNames2);
        String[] businessUnitNames3 = BusinessUnitSaver.getRecord(acct.Id, 'account');
        system.assertEquals(expectedNames, businessUnitNames3);
        String[] businessUnitNames4 = BusinessUnitSaver.getRecord(notSyncingOpp.Id, 'opportunity');
        system.assertEquals(expectedNames, businessUnitNames4);        
    }
    
    private static void createIntegrationFlag(String flag) {
        Metadata__c intFlag = new Metadata__c();
        intFlag.Name = 'IntegrationFlag';
        intFlag.Value__c = flag;
        insert intFlag;
    }
    
    private static void createIsInitialSyncFlag(String flag) {
        Metadata__c isInitialSyncFlag = new Metadata__c();
        isInitialSyncFlag.Name = 'IsInitialSync';
        isInitialSyncFlag.Value__c = flag;
        insert isInitialSyncFlag;
    }
}