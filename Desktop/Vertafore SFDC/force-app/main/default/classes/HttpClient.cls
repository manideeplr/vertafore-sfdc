/*
    Author : Manideep
    Name : HttpClient
    Date : 1 November 2019
    Description: Sends XML requests to SFA URL
    Modification History :
    *************************************************************
    Version               Author             Date                      Description
	1					 Manideep		 1 November 2019     		     Original
*/
public class HttpClient {
    private Http client = new Http();
    private String POST_METHOD = 'POST';
    private String CLIENTCERTBASE64_HEADER = 'ClientCertBase64';
    private Sagitta_Agency_Settings__c agencySettings = [SELECT SFA_URL__c, ClientCertBase64__c
                                                             FROM Sagitta_Agency_Settings__c
                                                             LIMIT 1].get(0);
    
    // Summary:
    // Creates and sends XML requests to SFA URL
    // 
    // Details:
    // @param String xmlBody - String representation of XML request body
    // @return HttpResponse - HttpResponse containing response details
    public HttpResponse sendXMLRequest(String xmlBody) {
        HttpRequest request = new HttpRequest();
		request.setEndpoint(agencySettings.SFA_URL__c);
        request.setHeader(CLIENTCERTBASE64_HEADER, agencySettings.ClientCertBase64__c);
        request.setBody(xmlBody);
        request.setMethod(POST_METHOD);

        system.debug(xmlBody);
        HttpResponse res = client.send(request);
        Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
        String[] toAddresses = new String[] {'manideep.lankireddy@verys.com'}; 
        mail.setToAddresses(toAddresses);
        mail.setSubject('Http Client Debug');
        mail.setPlainTextBody(''+request);
        Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });
        return res;
    }
}