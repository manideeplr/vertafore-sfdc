@isTest
public class LeadTrigger_Test {
	@testSetup
    public static void setup() {
        createIntegrationFlag('true');
        createIsInitialSyncFlag('false');
        
        Sagitta_Employee__c employee = new Sagitta_Employee__c(
            Name = 'Employee',
            Sagitta_Is_Executive__c = true,
            Sagitta_Is_Representative__c = true,
            Sagitta_Employee_Code__c = 'ABCDEFG',
            Sagitta_Entity_Id__c = 'cjhcj',
            Sagitta_Employee_Status__c = 'Active'
        );
        insert employee;
        
        Sagitta_Division__c div1 = new Sagitta_Division__c(Name = 'Division 1');
        Sagitta_Department__c dept1 = new Sagitta_Department__c(Name = 'Department 1');
        Sagitta_Branch__c branch1 = new Sagitta_Branch__c(Name = 'Branch 1');
        Sagitta_Group__c grp1 = new Sagitta_Group__c(Name = 'Group 1');
        insert div1;
        insert dept1;
        insert branch1;
        insert grp1;
        
        Sagitta_Branch__c branch2 = new Sagitta_Branch__c(Name = 'Branch 2');
        insert branch2;
        
        DivBrJunction__c divBrJunc = new DivBrJunction__c(Sagitta_Division__c = div1.Id, Sagitta_Branch__c = branch1.Id);
        BrDepJunction__c brDepJunc = new BrDepJunction__c(Sagitta_Branch__c = branch1.Id, Sagitta_Department__c = dept1.Id);
        DepGrpJunction__c depGrpJunc = new DepGrpJunction__c(Sagitta_Department__c = dept1.Id, Sagitta_Group__c = grp1.Id);
        insert divBrJunc;
        insert brDepJunc;
        insert depGrpJunc;
        
        Profile p = [SELECT Id FROM Profile WHERE Name='System Administrator']; 
        
        User user = new User(
            Alias = 'gardyloo',
            Email='gardyloo@gardyloo.com',
            EmailEncodingKey='UTF-8',
            LastName='gardyloo',
            LanguageLocaleKey='en_US', 
            LocaleSidKey='en_US',
            ProfileId = p.Id, 
            TimeZoneSidKey='America/Los_Angeles',
            UserName='gardyloo@gardyloo1.com');
        insert user;
        
        Sagitta_Mapping__c mapping = new Sagitta_Mapping__c(
        	Name = 'MyMapping',
            Sagitta_Assigned_To__c = UserInfo.getUserId(),
            Sagitta_Division__c = div1.Id,
            Sagitta_Branch__c = branch1.Id,
            Sagitta_Department__c = dept1.Id,
            Sagitta_Group__c = grp1.Id,
            Sagitta_Executive__c = employee.Id,
            Sagitta_Representative__c = employee.Id
        );
        insert mapping;

        Lead lead = new Lead(
            LastName = 'Horner',
            Company = 'MyCompany',
            Sagitta_Employee_Account_Exec__c = employee.Id,
            Sagitta_Employee_Account_Rep__c = employee.Id
        );
        insert lead;
    }
    
    @isTest
    public static void Insert_NoDefaultMappingExists_ShouldFail() {
        String actual;
        // Delete mapping
        Sagitta_Mapping__c mapping = [SELECT Id FROM Sagitta_Mapping__c][0];
        delete mapping;
        
        try {
            Sagitta_Employee__c employee = [SELECT Id FROM Sagitta_Employee__c][0];
            Lead lead = new Lead(LastName = 'Jay', Company = 'MyCompany');
            insert lead;    
        }
        catch(Exception e) {
            actual = e.getMessage();
        }
        
        String expected = 'Insert failed. First exception on row 0; first error: ' + 
            			  'FIELD_CUSTOM_VALIDATION_EXCEPTION, No Sagitta Mapping record found. ' +
            			  'An Sagitta Mapping record must be assigned to this user in order to create a Lead.: []';
        System.assertEquals(expected, actual);
    }

    @isTest
    public static void Insert_DefaultMappingExists_ShouldCopyExecAndRep() {
        Lead lead = new Lead(LastName = 'Jay', Company = 'MyCompany');
        insert lead;
        
        // Retrieve lead
        lead = [SELECT Sagitta_Employee_Account_Exec__c FROM Lead WHERE LastName = 'Jay'][0];
        
        System.assert(lead.Sagitta_Employee_Account_Exec__c != null);
    }
    
    @isTest
    public static void Update_DefaultMappingExists_ShouldCopyBusinessUnits() {
        Lead lead = [SELECT Id, Sagitta_Division__c FROM Lead][0];
        lead.LastName = 'ChangedLastName';
        update lead;
        
        // Retrieve lead
        lead = [SELECT Sagitta_Division__c FROM Lead WHERE LastName = 'ChangedLastName'][0];
        
        System.assert(lead.Sagitta_Division__c != null);
    }
    
    @isTest
    public static void Update_NoDefaultMappingExists_ShouldFail() {
        try {
            Lead lead = [SELECT Id FROM Lead][0];
            lead.LastName = 'ChangedLastName';
            update lead;
        }
        catch (Exception e) {
            System.assert(e != null);
        }
    }
    
    private static void createIntegrationFlag(String flag) {
        Metadata__c intFlag = new Metadata__c();
        intFlag.Name = 'IntegrationFlag';
        intFlag.Value__c = flag;
        insert intFlag;
    }
    
    private static void createIsInitialSyncFlag(String flag) {
        Metadata__c isInitialSyncFlag = new Metadata__c();
        isInitialSyncFlag.Name = 'IsInitialSync';
        isInitialSyncFlag.Value__c = flag;
        insert isInitialSyncFlag;
    }
}