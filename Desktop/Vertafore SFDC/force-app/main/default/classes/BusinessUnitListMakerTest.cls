@isTest
public class BusinessUnitListMakerTest {
	
    @isTest
    public static void initDivisionListTest() {
        List<Sagitta_Division__c> divList = TestDataFactoryUtil.createSagittaDivisions(1);
        Test.startTest();
        List<String> divNames = BusinessUnitListMaker.initDivisionList();
        Test.stopTest();
        system.assertEquals('Division0', divNames[0]);
    }
    
    @isTest
    public static void getBUListTest() {
        List<DivBrJunction__c> divBrList = TestDataFactoryUtil.createSagittaDivBrJunction(1);
        Test.startTest();
        List<String> brNames = BusinessUnitListMaker.getBUList('Division0','Sagitta_Division__r','Sagitta_Branch__c','DivBrJunction__c');
        Test.stopTest();
        system.assertEquals('Branch0', brNames[0]);
    }
}