@isTest
public class SagittaMappingTriggerTest {
    @isTest static void testCreateDuplicateMapping() {
        // Test data setup
        Profile profileId = [SELECT Id FROM Profile WHERE Name = 'Standard User' LIMIT 1];
        User user = new User(LastName = 'LIVESTON',
                     FirstName='JASON',
                     Alias = 'jliv',
                     Email = 'jason.liveston@asdf.com',
                     Username = 'jason.liveston@asdf.com' + System.currentTimeMillis(),
                     ProfileId = profileId.id,
                     TimeZoneSidKey = 'GMT',
                     LanguageLocaleKey = 'en_US',
                     EmailEncodingKey = 'UTF-8',
                     LocaleSidKey = 'en_US'
                     );
        
        createIntegrationFlag('true');
        createIsInitialSyncFlag('false');
        
        Sagitta_Employee__c executive = new Sagitta_Employee__c(Name = 'exec',
                                                              Sagitta_Is_Executive__c = true,
                                                              Sagitta_Employee_Code__c = '1234',
                                                              Sagitta_Entity_Id__c = '11',
                                                             Sagitta_Employee_Status__c = 'Active');
        Sagitta_Employee__c representative = new Sagitta_Employee__c(Name = 'rep',
                                                                   Sagitta_Is_Representative__c = true,
                                                                   Sagitta_Employee_Code__c = '5678',
                                                                   Sagitta_Entity_Id__c = '22',
                                                                   Sagitta_Employee_Status__c = 'Active');
        Sagitta_Division__c divisionOne = new Sagitta_Division__c(Name = 'Division One');
        Sagitta_Branch__c branchOne = new 	Sagitta_Branch__c(Name = 'Branch One');
        Sagitta_Department__c departmentOne = new Sagitta_Department__c(Name = 'Department One');
        Sagitta_Group__c groupOne = new Sagitta_Group__c(Name = 'Group One');
            
        // Perform test
        Test.startTest();
        Database.SaveResult resultExec = Database.insert(executive, false);
        Database.SaveResult resultRep = Database.insert(representative, false);
        Database.SaveResult resultDivision = Database.insert(divisionOne, false);
        Database.SaveResult resultBranch = Database.insert(branchOne, false);
        Database.SaveResult resultDepartment = Database.insert(departmentOne, false);
        Database.SaveResult resultGroup = Database.insert(groupOne, false);
        Database.SaveResult result0 = Database.insert(user, false);
        
        Sagitta_Mapping__c map1 = new Sagitta_Mapping__c(Name ='TestMap1',
                                                       Sagitta_Assigned_To__c = user.Id,
                                                       OwnerId = user.Id,
                                                       Sagitta_Executive__c = executive.Id, 
                                                       Sagitta_Representative__c = representative.Id,
                                                       Sagitta_Division__c = divisionOne.Id,
                                                       Sagitta_Branch__c = branchOne.Id,
                                                       Sagitta_Department__c = departmentOne.Id,
                                                       Sagitta_Group__c = groupOne.Id);
        Sagitta_Mapping__c map2 = new Sagitta_Mapping__c(Name = 'TestMap2',
                                                       Sagitta_Assigned_To__c = user.Id,
                                                       OwnerId = user.Id,
                                                       Sagitta_Executive__c = executive.Id,
                                                       Sagitta_Representative__c = representative.Id,
                                                       Sagitta_Division__c = divisionOne.Id,
                                                       Sagitta_Branch__c = branchOne.Id,
                                                       Sagitta_Department__c = departmentOne.Id,
                                                       Sagitta_Group__c = groupOne.Id);
        
        Database.SaveResult result1 = Database.insert(map1, false);
        Database.SaveResult result2 = Database.insert(map2, false);
        
        Test.stopTest();
        
        // Verify
        System.assert(result0.isSuccess());
        System.assert(result1.isSuccess());
        System.assert(!result2.isSuccess());
        System.assert(result2.getErrors().size() > 0);
        System.assertEquals('Mapping already exists for specified user.',
                             result2.getErrors()[0].getMessage());
    }
    
    private static void createIntegrationFlag(String flag) {
        Metadata__c intFlag = new Metadata__c();
        intFlag.Name = 'IntegrationFlag';
        intFlag.Value__c = flag;
        insert intFlag;
    }
    
    private static void createIsInitialSyncFlag(String flag) {
        Metadata__c isInitialSyncFlag = new Metadata__c();
        isInitialSyncFlag.Name = 'IsInitialSync';
        isInitialSyncFlag.Value__c = flag;
        insert isInitialSyncFlag;
    }
}