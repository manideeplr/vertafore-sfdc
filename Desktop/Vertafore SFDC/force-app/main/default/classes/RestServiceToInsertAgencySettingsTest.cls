@isTest
public class RestServiceToInsertAgencySettingsTest {

    @isTest
    static void test1(){
        createIntegrationFlag('true');
        createIsInitialSyncFlag('false');
        
        Sagitta_Activity_Action_Type__c activityActionType = new Sagitta_Activity_Action_Type__c();
        activityActionType.Sagitta_Activity_Action_Id__c = '26544';
        Sagitta_Issuing_Carrier__c issuingCarrier = new Sagitta_Issuing_Carrier__c(
            Sagitta_Entity_ID__c= 'ID text', 
            Sagitta_Carrier_Type__c= 'N',
            Sagitta_IsHidden__c = false,
            Sagitta_Issuing_Carrier_Code__c= 'text'
        );
		Sagitta_Employee__c employee = new Sagitta_Employee__c(
        	Name = 'vpa@mailinator.com',
            Sagitta_Employee_Code__c = '!!4',
            Sagitta_Is_Executive__c = true,
            Sagitta_Is_Representative__c = true,
            Sagitta_Entity_ID__c = '54321',
            Sagitta_Employee_Status__c = 'Active'
        );
        
        insert employee;
        insert activityActionType;
        insert issuingCarrier;
        
        Map<String, String> metaMap = new Map<String, String>();
        metaMap.put('AgencyId', '895566556-1');
        metaMap.put('VertaforeId', '5489552');
        metaMap.put('DefAgencyAdmin', '54321');
        
        String myJSON = JSON.serialize(metaMap);
        
        RestRequest request = new RestRequest();
        request.requestUri ='https://test.salesforce.com/services/apexrest/InsertAgencySettings';
        request.httpMethod = 'POST';
        request.requestBody = Blob.valueof(myJSON);
        
        RestResponse response = new RestResponse();
        
        RestContext.request = request;
        RestContext.response = response;
        RestServiceToInsertAgencySettings.ResponseHandler res = RestServiceToInsertAgencySettings.doPost();
        
        List<Sagitta_Agency_Settings__c> met = [SELECT Id, Name FROM Sagitta_Agency_Settings__c];
        system.assertEquals(1, met.size());
    }
    
    @isTest
    static void test2(){
        List<String> metaMap = new List<String>();
        metaMap.add('key1');
        metaMap.add('key2');
        
        String myJSON = JSON.serialize(metaMap);
        
        RestRequest request = new RestRequest();
        request.requestUri ='https://test.salesforce.com/services/apexrest/InsertAgencySettings';
        request.httpMethod = 'POST';
        request.requestBody = Blob.valueof(myJSON);
        
        RestResponse response = new RestResponse();
        response.statusCode = 400;
        
        RestContext.request = request;
        RestContext.response = response;
        RestServiceToInsertAgencySettings.ResponseHandler res = RestServiceToInsertAgencySettings.doPost();
        
        List<Sagitta_Agency_Settings__c> met = [SELECT id, Name FROM Sagitta_Agency_Settings__c];
        system.assertEquals(0, met.size());    
    }
    
    private static void createIntegrationFlag(String flag) {
        Metadata__c intFlag = new Metadata__c();
        intFlag.Name = 'IntegrationFlag';
        intFlag.Value__c = flag;
        insert intFlag;
    }
    
    private static void createIsInitialSyncFlag(String flag) {
        Metadata__c isInitialSyncFlag = new Metadata__c();
        isInitialSyncFlag.Name = 'IsInitialSync';
        isInitialSyncFlag.Value__c = flag;
        insert isInitialSyncFlag;
    }
}