/*
    Author : Manideep
    Name : RestServiceToUpsertIssCarrRecords
    Date : 1 November 2019
    Description: Rest service to upsert IssuingCarrier records
    Modification History :
    *************************************************************
    Version               Author             Date                      Description
	  1				     Manideep		1 November 2019			         Original
*/

@RestResource(urlMapping='/UpsertIssCarsRecords/*')
global with sharing class RestServiceToUpsertIssCarrRecords{
    
    // class that represents SagittaIss model
    global class SagittaIss {
        global String CompanyCode { get; set; }
        global String CompanyCodeHash { get; set; }
        global String CompanyName { get; set; }
        global String ParentCompanyCode { get; set; }
        global String CompanyType { get; set; }
        global String EntityId { get; set; }
        global Boolean IsHide { get; set; }
        global String EntityName { get; set; }
    }
    
    // class represents response to caller
    global class ResponseHandler {
        public Boolean Success {get; set;}
        public String Status {get; set;}
        public String Message {get;set;}
    }
    
    private static List<Sagitta_Issuing_Carrier__c> allIssuingCarriers;
    
    // Deserialize the request body and insert the data
    // @param - No Param
    // @return - ResponseHandler
    @HttpPost
    global static ResponseHandler doPost() {
        getAllIssuingCarriers();
        ResponseHandler obj = new ResponseHandler();
        RestRequest req = RestContext.request;
        String failedRecordIds = '';
        try {
            String postBody = req.requestBody.toString();
            
            List<SagittaIss> amsCarriers = (List<SagittaIss>)JSON.deserialize(postBody, List<SagittaIss>.class);
            List<Sagitta_Issuing_Carrier__c> carriersToUpsert = new List<Sagitta_Issuing_Carrier__c>();
            
            // no carriers in request
            if (amsCarriers.isEmpty()) {
                RestContext.response.statusCode = 400;
                obj.success = false;
                obj.status='Error';
                obj.message = 'No Issuing Carriers in request.';
                return obj;
            }
            // no carriers in Salesforce
            else if (allIssuingCarriers.isEmpty()) {
                for (SagittaIss amsCarrier : amsCarriers) {
                    Sagitta_Issuing_Carrier__c issuingCarrier = new Sagitta_Issuing_Carrier__c();
                    issuingCarrier = mapFields(issuingCarrier, amsCarrier);
                    carriersToUpsert.add(issuingCarrier);
                }
            }
            else {
                for (SagittaIss amsCarrier : amsCarriers) {
                    Sagitta_Issuing_Carrier__c issuingCarrier = findIssuingCarrier(amsCarrier.CompanyCode);
                    if (issuingCarrier == null) {
                        issuingCarrier = new Sagitta_Issuing_Carrier__c();
                    }
                    issuingCarrier = mapFields(issuingCarrier, amsCarrier);
                    carriersToUpsert.add(issuingCarrier);
                }
            }
            
            Schema.SObjectField extIdfield = Sagitta_Issuing_Carrier__c.Fields.Sagitta_Entity_ID__c;
            // To insert the list of records
            Database.UpsertResult[] results = Database.upsert(carriersToUpsert, extIdfield, false);
            for (Database.UpsertResult result : results) {
                if (!result.isSuccess()) {
                    Database.Error[] upsertErrors = result.getErrors();
                    for (Database.Error upsertError : upsertErrors) {
                        failedRecordIds = failedRecordIds + upsertError.getMessage() + upsertError.getFields();
                    }
                }
            }
            // Response handler initialization once the insert is successful/failure
            // Insert log into Application Log object for request and response json
            if (String.isNotBlank(failedRecordIds)) {
                obj.success = false;
                obj.status='ERROR';
                obj.message= failedRecordIds;
                ApplicationLogger.logWebServiceMessage('RestServiceToUpsertIssCarrRecords', 'doPost', failedRecordIds, 
                                                   ApplicationLogger.ERROR, req.requestBody.toString(), JSON.serialize(obj));
            } else {
                obj.success = true;
                obj.status='SUCCESS';
                obj.message= 'SUCCESS';
                ApplicationLogger.logWebServiceMessage('RestServiceToUpsertIssCarrRecords', 'doPost', 'SUCCESS', 
                                                   ApplicationLogger.INFO, req.requestBody.toString(), JSON.serialize(obj));
            }
            
            return obj;
        } catch(Exception ex) {
            RestContext.response.statusCode = 400;
            obj.success = false;
            obj.status='Error';
            obj.message = ex.getMessage();
            String message = 'Error: ' + ex.getMessage() + '; Stack Trace:' + ex.getStackTraceString();            
            // Insert log into Application Log object for request, response and error json
            ApplicationLogger.logWebServiceMessage('RestServiceToUpsertIssCarrRecords', 'doPost', message, 
                                                   ApplicationLogger.ERROR, req.requestBody.toString(), JSON.serialize(obj));
            return obj;
        }        
    }
    
    private static void getAllIssuingCarriers() {
        allIssuingCarriers = [SELECT Id,
                              Name,
                              Sagitta_Entity_ID__c,
                              Sagitta_Carrier_Type__c,
                              Sagitta_Issuing_Carrier_Code__c,
                              Sagitta_IsHidden__c
                              FROM Sagitta_Issuing_Carrier__c];
    }
    
    private static Sagitta_Issuing_Carrier__c findIssuingCarrier(String companyCode) {
        for (Sagitta_Issuing_Carrier__c issuingCarrier : allIssuingCarriers) {
            if (issuingCarrier.Sagitta_Issuing_Carrier_Code__c == companyCode) {
                return issuingCarrier;
            }
        }
        return null;
    }
    
    private static Sagitta_Issuing_Carrier__c mapFields(Sagitta_Issuing_Carrier__c issuingCarrier, SagittaIss SagittaCarrier) {
        if (issuingCarrier.Name != SagittaCarrier.CompanyName) {
            issuingCarrier.Name = SagittaCarrier.CompanyName;
        }
        if (issuingCarrier.Sagitta_Entity_ID__c != SagittaCarrier.EntityId) {
            issuingCarrier.Sagitta_Entity_ID__c = SagittaCarrier.EntityId;
        }
        if (issuingCarrier.Sagitta_Carrier_Type__c != SagittaCarrier.CompanyType) {
            issuingCarrier.Sagitta_Carrier_Type__c = SagittaCarrier.CompanyType;    
        }
        if (issuingCarrier.Sagitta_Issuing_Carrier_Code__c != SagittaCarrier.CompanyCodeHash) {
            issuingCarrier.Sagitta_Issuing_Carrier_Code__c = SagittaCarrier.CompanyCodeHash;    
        }
        if (issuingCarrier.Sagitta_IsHidden__c != SagittaCarrier.IsHide) {
            issuingCarrier.Sagitta_IsHidden__c = SagittaCarrier.IsHide;
        }
        return issuingCarrier;
    }
}