/*
    Author : Manideep
    Name : RestServiceToUpsertBusinessEntityRecords
    Date : 1 November 2019
    Description: Rest service to upsert BusinessEntity object records
    Modification History :
    *************************************************************
    Version               Author             Date                      Description
	   1				    Manideep	 1 November 2019			     Original
*/
@RestResource(urlMapping='/UpsertBusinessEntityRecords/*')
global with sharing class RestServiceToUpsertBusinessEntityRecords{
    
    // class that represents SagittaBranch model
    global class SagittaBusEnt {        
        //global String BusinessEntityId { get; set; }
        global String EntityId { get; set; }
        global String EntityName { get; set; }
        global String Value { get; set; }        
    }
    
    // class represents response to caller
    global class ResponseHandler {
        public Boolean Success {get; set;}
        public String Status {get; set;}
        public String Message {get;set;}
    }
    
    // Deserialize the request body and insert the data
	// @param - No Param
	// @return - ResponseHandler
    @HttpPost
    global static ResponseHandler doPost() {
        ResponseHandler obj = new ResponseHandler();
        RestRequest req = RestContext.request;
        String failedRecordIds = '';
        try {
            String postBody = req.requestBody.toString();
            // Deserialize the request body and converts into List<SagittaBusEnt>
            List<SagittaBusEnt> busEnts = (List<SagittaBusEnt>)JSON.deserialize(postBody, List<SagittaBusEnt>.class);
            List<Sagitta_Business_Entity__c> busEntsToUpsert = new List<Sagitta_Business_Entity__c>();
            // For loop to iterate through all the incoming BusinessEntity records
            for (SagittaBusEnt busEnt : busEnts) {
                Sagitta_Business_Entity__c newBusEnt = new Sagitta_Business_Entity__c();
                newBusEnt.Sagitta_Business_Entity_Id__c = busEnt.EntityId;
                newBusEnt.Name = busEnt.Value;
                busEntsToUpsert.add(newBusEnt);
            }
            Schema.SObjectField extIdField = Sagitta_Business_Entity__c.Fields.Sagitta_Business_Entity_Id__c;
            // To insert the list of records
            Database.UpsertResult[] results = Database.upsert(busEntsToUpsert, extIdField, false);
            for (Database.UpsertResult result : results) {
                if (!result.isSuccess()) {
                    Database.Error[] upsertErrors = result.getErrors();
                    for (Database.Error upsertError : upsertErrors) {
                        failedRecordIds = failedRecordIds + upsertError.getMessage() + upsertError.getFields();
                    }
                }
            }
            // Response handler initialization once the insert is successful/failure
            // Insert log into Application Log object for request and response json
            if (String.isNotBlank(failedRecordIds)) {
                obj.success = false;
                obj.status='ERROR';
                obj.message= failedRecordIds;
                ApplicationLogger.logWebServiceMessage('RestServiceToUpsertBusinessEntityRecords', 'doPost', failedRecordIds, 
                                                   ApplicationLogger.ERROR, req.requestBody.toString(), JSON.serialize(obj));
            } else {
                obj.success = true;
                obj.status='SUCCESS';
                obj.message= 'SUCCESS';
                ApplicationLogger.logWebServiceMessage('RestServiceToUpsertBusinessEntityRecords', 'doPost', 'SUCCESS', 
                                                   ApplicationLogger.INFO, req.requestBody.toString(), JSON.serialize(obj));
            }
            
            return obj;
        } catch(Exception ex) {
            RestContext.response.statusCode = 400;
            obj.success = false;
            obj.status='Error';
            obj.message=ex.getMessage();
            String message = 'Error: ' + ex.getMessage() + '; Stack Trace:' + ex.getStackTraceString(); 
            // Insert log into Application Log object for request and response json
            ApplicationLogger.logWebServiceMessage('RestServiceToUpsertBusinessEntityRecords', 'doPost', 'SUCCESS', 
                                                   ApplicationLogger.INFO, req.requestBody.toString(), JSON.serialize(obj));
            return obj;
        }        
    }

}