@isTest
public class RestServiceToUpsertIssCarrRecordsTest {
    @isTest
    static void doPost_ShouldNotUpsertIssuingCarrier_WhenInList(){
        List<RestServiceToUpsertIssCarrRecords.SagittaIss> icToInsert = new List<RestServiceToUpsertIssCarrRecords.SagittaIss>();
        RestServiceToUpsertIssCarrRecords.SagittaIss ic = new RestServiceToUpsertIssCarrRecords.SagittaIss();
        ic.CompanyName = '1';
        ic.CompanyType  = 'N';
        ic.CompanyCodeHash = '033041071';
        ic.EntityId = '162';
        ic.EntityName = 'SagittaIssuingCarrierEntity';
        ic.IsHide = false;
        icToInsert.add(ic);
        String myJSON = JSON.serialize(icToInsert);
        
        RestRequest request = new RestRequest();
        request.requestUri ='https://test.salesforce.com/services/apexrest/UpsertIssCarsRecords';
        request.httpMethod = 'POST';
        request.requestBody = Blob.valueof(myJSON);
        
        RestContext.request = request;
        RestServiceToUpsertIssCarrRecords.ResponseHandler res = RestServiceToUpsertIssCarrRecords.doPost();
        
        List<Sagitta_Issuing_Carrier__c> iss = [SELECT Id,
                                                    	   Name,
                                                    	   Sagitta_Entity_ID__c,
                                                    	   Sagitta_Carrier_Type__c,
                                                    	   Sagitta_Issuing_Carrier_Code__c,
                                                    	   Sagitta_IsHidden__c
                                                    FROM Sagitta_Issuing_Carrier__c];
        if (iss.size() == 1) {
        	System.assertEquals(ic.CompanyName, iss[0].Name);
            System.assertEquals(ic.EntityId, iss[0].Sagitta_Entity_ID__c);
            System.assertEquals(ic.CompanyType, iss[0].Sagitta_Carrier_Type__c);
            System.assertEquals(ic.CompanyCodeHash, iss[0].Sagitta_Issuing_Carrier_Code__c);
            System.assertEquals(ic.IsHide, iss[0].Sagitta_IsHidden__c); 
        }
        else {
            System.assert(false);
        }
    }
    
    @isTest
    static void doPost_ShouldNotUpsertIssuingCarrier_WhenNotInList(){
        RestServiceToUpsertIssCarrRecords.SagittaIss itm = new RestServiceToUpsertIssCarrRecords.SagittaIss();
        itm.CompanyName = '1';
        itm.CompanyType  = 'B';
        itm.CompanyCodeHash = '033041071';
        itm.EntityId = '162';
        itm.EntityName = 'SagittaIssuingCarrierEntity';
        itm.IsHide = false;
        String myJSON = JSON.serialize(itm);        
        
        RestRequest request = new RestRequest();
        request.requestUri ='https://test.salesforce.com/services/apexrest/UpsertIssCarsRecords';
        request.httpMethod = 'POST';
        request.requestBody = Blob.valueof(myJSON);
        
        RestResponse response = new RestResponse();
        response.statusCode = 400;
        
        RestContext.request = request;
        RestContext.response = response;
        RestServiceToUpsertIssCarrRecords.ResponseHandler res = RestServiceToUpsertIssCarrRecords.doPost();
        
        List<Sagitta_Issuing_Carrier__c> iss = [SELECT Id FROM Sagitta_Issuing_Carrier__c];
        System.assertEquals(0, iss.size());
        System.assertEquals(400, RestContext.response.statusCode);
    }
    
    @isTest
    static void doPost_ShouldNotUpsertIssuingCarrier_WhenInList_EmptyRequest(){
        List<RestServiceToUpsertIssCarrRecords.SagittaIss> icToInsert = new List<RestServiceToUpsertIssCarrRecords.SagittaIss>();
        String myJSON = JSON.serialize(icToInsert);
        
        RestRequest request = new RestRequest();
        request.requestUri ='https://test.salesforce.com/services/apexrest/UpsertIssCarsRecords';
        request.httpMethod = 'POST';
        request.requestBody = Blob.valueof(myJSON);
        
        RestResponse response = new RestResponse();
        response.statusCode = 400;
        
        RestContext.request = request;
        RestContext.response = response;
        RestServiceToUpsertIssCarrRecords.ResponseHandler res = RestServiceToUpsertIssCarrRecords.doPost();
        
        List<Sagitta_Issuing_Carrier__c> iss = [SELECT Id FROM Sagitta_Issuing_Carrier__c];
        
        System.assertEquals(0, iss.size()); 
        
    }
    
    @isTest
    static void doPost_ShouldNotUpsertIssuingCarrier_WhenNotInList_FailedRecords(){
        Sagitta_Issuing_Carrier__c issuingCarrier = new Sagitta_Issuing_Carrier__c(
            Sagitta_Entity_ID__c= 'ID text', 
            Sagitta_Carrier_Type__c= 'N',
            Sagitta_IsHidden__c = false,
            Sagitta_Issuing_Carrier_Code__c= 'text'
        );
        insert issuingCarrier;
        List<RestServiceToUpsertIssCarrRecords.SagittaIss> icToInsert = new List<RestServiceToUpsertIssCarrRecords.SagittaIss>();
        RestServiceToUpsertIssCarrRecords.SagittaIss ic = new RestServiceToUpsertIssCarrRecords.SagittaIss();
        ic.CompanyName = '1';
        ic.CompanyType  = 'N';
        ic.CompanyCodeHash = '033041071';
        ic.EntityId = '162';
        ic.EntityName = 'SagittaIssuingCarrierEntity';
        ic.IsHide = false;
        RestServiceToUpsertIssCarrRecords.SagittaIss ic2 = new RestServiceToUpsertIssCarrRecords.SagittaIss();
        ic2.CompanyName = '1';
        ic2.CompanyType  = 'N';
        ic2.CompanyCodeHash = '033041071';
        ic2.EntityId = '';
        ic2.EntityName = 'SagittaIssuingCarrierEntity';
        ic2.IsHide = false;
        icToInsert.add(ic2);
        String myJSON = JSON.serialize(icToInsert);
        
        RestRequest request = new RestRequest();
        request.requestUri ='https://test.salesforce.com/services/apexrest/UpsertIssCarsRecords';
        request.httpMethod = 'POST';
        request.requestBody = Blob.valueof(myJSON);
        
        RestResponse response = new RestResponse();
        response.statusCode = 400;
        
        RestContext.request = request;
        RestContext.response = response;
        RestServiceToUpsertIssCarrRecords.ResponseHandler res = RestServiceToUpsertIssCarrRecords.doPost();
        
        List<Sagitta_Issuing_Carrier__c> iss = [SELECT Id FROM Sagitta_Issuing_Carrier__c];
        System.assertEquals(1, iss.size());
    }
}