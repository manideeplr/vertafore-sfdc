@isTest
public class JobTest {

    @isTest
    public static void myTest() {
        createIntegrationFlag('true');
        createIsInitialSyncFlag('false');
        
        User usr = createUser();
        insert usr;
        Sagitta_Division__c div1 = new Sagitta_Division__c(Name = 'Division 1', Sagitta_IsHidden__c	= false);
        Sagitta_Department__c dept1 = new Sagitta_Department__c(Name = 'Department 1', Sagitta_IsHidden__c = false);
        Sagitta_Branch__c branch1 = new Sagitta_Branch__c(Name = 'Branch 1', Sagitta_IsHidden__c = false);
        Sagitta_Group__c grp1 = new Sagitta_Group__c(Name = 'Group 1', Sagitta_IsHidden__c = false);
        insert div1;
        insert dept1;
        insert branch1;
        insert grp1;
        Sagitta_Employee__c employee = new Sagitta_Employee__c(
            Name = 'Executive',
            Sagitta_Is_Executive__c = true, 
            Sagitta_Is_Representative__c = true,
            Sagitta_Employee_Code__c = 'ABCDEFG',
            Sagitta_Entity_Id__c = 'cjhcj',
            Sagitta_Employee_Status__c = 'Active'
        );
        insert employee;
        Sagitta_Activity_Action_Type__c activityAction = createActivityActionType();
        insert activityAction;
        Sagitta_Issuing_Carrier__c issuingCarrier = createIssuingCarrier();
        insert issuingCarrier;
        // first argument is for integration stage value
        Sagitta_Agency_Settings__c agencySettings = createAgencySettings('Proposal/Price Quote', activityAction, issuingCarrier, employee);
        insert agencySettings;
        Sagitta_Mapping__c mapping = new Sagitta_Mapping__c(
            Name='Test',
            Sagitta_Assigned_To__c = UserInfo.getUserId(),
            Sagitta_Executive__c = employee.Id,
            Sagitta_Representative__c = employee.Id,
            Sagitta_Division__c = div1.Id,
            Sagitta_Branch__c = branch1.Id,
            Sagitta_Department__c = dept1.Id,
            Sagitta_Group__c = grp1.Id
        );
        insert mapping;
        Account account = new Account(
            Name = 'Acct One',
            Sagitta_Client_Type__c = 'P',
            Sagitta_Employee_Account_Exec__c = employee.Id,
            Sagitta_Employee_Account_Rep__c = employee.Id
        );
        insert account;
        
        Test.startTest();
        Test.setMock(HttpCalloutMock.class, new MockHttpResponseGenerator());
        String xmlBody = 
        '<?xml version="1.0"?>' +
        '<i:Envelope xmlns:i="http://schemas.xmlsoap.org/soap/envelope/">' +
            '<i:Body xmlns:j="http://soap.sforce.com/2005/09/outbound">' +
                '<j:notifications>' +
					'<j:OrganizationId>' + UserInfo.getOrganizationId() + '</j:OrganizationId>' +
                    '<j:Notification xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">' +
						'<j:sObject xsi:type="sf:Account" xmlns:sf="urn:sobject.enterprise.soap.sforce.com">' +
							'<sf:Id>' + account.Id + '</sf:Id>' +
                      	'</j:sObject>' +
                    '</j:Notification>' +
                '</j:notifications>' +
            '</i:Body>' +
        '</i:Envelope>';
        
        Job job = new Job(xmlBody);
        ID jobId = System.enqueueJob(job);
        Test.stopTest();
        
        system.assert(jobId != null);
    }
    
    private static Sagitta_Activity_Action_Type__c createActivityActionType() {
        Sagitta_Activity_Action_Type__c activityActionType = new Sagitta_Activity_Action_Type__c(
            Sagitta_Activity_Action_Id__c = '1234',
            Name = 'Acord'
        );
        return activityActionType;
    }
    
    private static Sagitta_Issuing_Carrier__c createIssuingCarrier() {
        Sagitta_Issuing_Carrier__c issuingCarrier = new Sagitta_Issuing_Carrier__c(
            Sagitta_Entity_ID__c= 'ID text', 
            Sagitta_Carrier_Type__c= 'N',
            Sagitta_IsHidden__c = false,
            Sagitta_Issuing_Carrier_Code__c= 'text',
            Name = 'Issuing Carrier'
        );
        return issuingCarrier;
    }
    
    private static Sagitta_Agency_Settings__c createAgencySettings(string integrationStage,
                                                                       Sagitta_Activity_Action_Type__c activityAction,
                                                                       Sagitta_Issuing_Carrier__c issuingCarrier,
                                                                       Sagitta_Employee__c employee) {
        User usr = [SELECT Id FROM User WHERE UserName = 'vert-test@vertafore.com'];
        Sagitta_Agency_Settings__c agencySettings = new Sagitta_Agency_Settings__c(
            Integration_Stage__c = integrationStage,
            Sagitta_Agency_Number__c = '18R1',
            Benefits_Policy_Status__c = 'A',
            Benefits_Policy_Type__c = 'S',
            Commercial_Policy_Status__c = 'A',
            Commercial_Policy_Type__c = 'S',
            Default_Activity_Action_Type__c = activityAction.Id,
            Default_Agency_Admin__c = employee.Id,
            Default_Bill_Method__c = 'A',
            Default_Issuing_Carrier__c = issuingCarrier.Id,
            Default_Policy_Number__c = '123456789',
            Financial_Policy_Status__c = 'A',
            Financial_Policy_Type__c = 'S',
            Health_Policy_Status__c = 'A',
            Health_Policy_Type__c = 'S',
            Integration_User_Id__c = '555555',
            Life_Policy_Status__c = 'A',
            Life_Policy_Type__c = 'S',
            Non_Property_and_Casualty_Policy_Status__c = 'A',
            Non_Property_and_Casualty_Policy_Type__c = 'S',
            Personal_Policy_Status__c = 'A',
            Personal_Policy_Type__c = 'S',
            Vertafore_Id__c = '54321',
            SFA_URL__c = 'http://example.com/example/test',
            ClientCertBase64__c = 'test'
        );
        return agencySettings;
    }
    
    private static User createUser() {
        Profile p = [SELECT Id FROM Profile WHERE Name='Standard User']; 
        User user = new User(
            Alias = 'standt',
            Email='standarduser@testorg.com', 
            EmailEncodingKey='UTF-8',
            LastName='Testing',
            LanguageLocaleKey='en_US', 
            LocaleSidKey='en_US',
            ProfileId = p.Id, 
            TimeZoneSidKey='America/Los_Angeles',
            UserName='vert-test@vertafore.com'
        );
        return user;
    }
    
    private static void createIntegrationFlag(String flag) {
        Metadata__c intFlag = new Metadata__c();
        intFlag.Name = 'IntegrationFlag';
        intFlag.Value__c = flag;
        insert intFlag;
    }
    
    private static void createIsInitialSyncFlag(String flag) {
        Metadata__c isInitialSyncFlag = new Metadata__c();
        isInitialSyncFlag.Name = 'IsInitialSync';
        isInitialSyncFlag.Value__c = flag;
        insert isInitialSyncFlag;
    }
}