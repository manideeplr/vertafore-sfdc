/*
    Author : Manideep
    Name : RestServiceToInsertAgencySettings
    Date : 1 November 2019
    Description: Rest Service to create AgencySettings record
    Modification History :
    *************************************************************
    Version               Author             Date                      Description
	 1				     Manideep		 1 November 2019                Original
*/
@RestResource(urlMapping='/InsertAgencySettings/*')
global with sharing class RestServiceToInsertAgencySettings{
    
    // class represents response to caller
    global class ResponseHandler {
        public Boolean Success {get; set;}
        public String Status {get; set;}
        public String Message {get;set;}
    }

	// Deserialize the request body and insert the data
	// @param - No Param
	// @return - ResponseHandler    
    @HttpPost
    global static ResponseHandler doPost() {
        ResponseHandler obj = new ResponseHandler();
        RestRequest req = RestContext.request;
        String failedRecordIds = '';
        try {
            String postBody = req.requestBody.toString();
            // Deserialize the request body and converts into the map of key value pairs
            Map<String, String> mapReq = (Map<String, String>)JSON.deserialize(postBody, Map<String, String>.class);
            List<Sagitta_Agency_Settings__c> agencySettingsList = new List<Sagitta_Agency_Settings__c>();
            Sagitta_Agency_Settings__c agcySettings = new Sagitta_Agency_Settings__c();
            agcySettings.Sagitta_Agency_Number__c = mapReq.get('AgencyId');
            agcySettings.Vertafore_Id__c = mapReq.get('VertaforeId');
            agcySettings.Integration_User_Id__c = mapReq.get('DefAgencyAdmin');
            
            agcySettings.Personal_Policy_Type__c = 'S';
            agcySettings.Commercial_Policy_Type__c = 'S';
            agcySettings.Non_Property_and_Casualty_Policy_Type__c = 'S';
            agcySettings.Life_Policy_Type__c = 'S';
            agcySettings.Health_Policy_Type__c = 'S';
            agcySettings.Financial_Policy_Type__c = 'S';
            agcySettings.Benefits_Policy_Type__c = 'S';
            agcySettings.Personal_Policy_Status__c = 'A';
            agcySettings.Commercial_Policy_Status__c = 'A';
            agcySettings.Non_Property_and_Casualty_Policy_Status__c = 'A';
            agcySettings.Life_Policy_Status__c = 'A';
            agcySettings.Health_Policy_Status__c = 'A';
            agcySettings.Financial_Policy_Status__c = 'A';
            agcySettings.Benefits_Policy_Status__c = 'A';
            agcySettings.Default_Bill_Method__c = 'A';
            agcySettings.Default_Policy_Number__c = 'DefSub';
            agcySettings.Default_Agency_Admin__c = [SELECT Id FROM Sagitta_Employee__c LIMIT 1].Id;
            agencySettingsList.add(agcySettings);
            
            // To insert the list of Metadata__c records
            Database.UpsertResult[] results = Database.upsert(agencySettingsList, false);
            for (Database.UpsertResult result : results) {
                if (!result.isSuccess()) {
                    Database.Error[] upsertErrors = result.getErrors();
                    for (Database.Error upsertError : upsertErrors) {
                        failedRecordIds = failedRecordIds + upsertError.getMessage();
                    }
                }
            }
            // Response handler initialization once the insert is successful/failure
            // Insert log into Application Log object for request and response json
            if (String.isNotBlank(failedRecordIds)) {
                obj.success = false;
                obj.status='ERROR';
                obj.message= failedRecordIds;
                ApplicationLogger.logWebServiceMessage('RestServiceToInsertAgencySettings', 'doPost', failedRecordIds, 
                                                   ApplicationLogger.ERROR, req.requestBody.toString(), JSON.serialize(obj));
            } else {
                obj.success = true;
                obj.status='SUCCESS';
                obj.message= 'SUCCESS';
                ApplicationLogger.logWebServiceMessage('RestServiceToInsertAgencySettings', 'doPost', 'SUCCESS', 
                                                   ApplicationLogger.INFO, req.requestBody.toString(), JSON.serialize(obj));
            }
            
            return obj;
        } catch(Exception ex) {
            // Response handler initialization on exception
            RestContext.response.statusCode = 400;
            obj.success = false;
            obj.status='Error';
            obj.message=ex.getMessage();
            
            String message = 'Error: ' + ex.getMessage() + '; Stack Trace:' + ex.getStackTraceString();
            
            // Insert log into Application Log object for request, response and error json
            ApplicationLogger.logWebServiceMessage('RestServiceToInsertAgencySettings', 'doPost', message, 
                                                   ApplicationLogger.ERROR, req.requestBody.toString(), JSON.serialize(obj));
            return obj;
        }        
    }
}