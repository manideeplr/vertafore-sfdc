@isTest
public class LineOfBusinessUtilTest {

    @isTest
    public static void setup() {
        createIntegrationFlag('true');
        createIsInitialSyncFlag('false');
        
        Sagitta_Issuing_Carrier__c carrier = new Sagitta_Issuing_Carrier__c(
            Name = 'Carrier Name',
            Sagitta_Carrier_Type__c = 'N',
            Sagitta_Entity_ID__c = 'abc123',
            Sagitta_Issuing_Carrier_Code__c = '!!#'
        );
        Sagitta_Activity_Action_Type__c actionType = new Sagitta_Activity_Action_Type__c(
            Name = 'Application',
            Sagitta_Activity_Action_Id__c = 'jjfjghj4369478'
        );  
        insert carrier;
        insert actionType;
        
        Sagitta_Employee__c employee = new Sagitta_Employee__c(
            Name = 'Employee',
            Sagitta_Is_Executive__c = true,
            Sagitta_Is_Representative__c = true,
            Sagitta_Employee_Code__c = 'ABCDEFG',
            Sagitta_Entity_Id__c = 'cjhcj',
            Sagitta_Employee_Status__c = 'Active'
        );
        insert employee;
        
        Sagitta_Agency_Settings__c agency = new Sagitta_Agency_Settings__c(
            Default_Bill_Method__c = 'A',
            Commercial_Policy_Status__c = 'A',
            Commercial_Policy_Type__c = 'S',
            Sagitta_Agency_Number__c = '1',
            Default_Policy_Number__c = 'Def Sub',
            Integration_User_Id__c = 'IntUserId',
            Default_Activity_Action_Type__c = actionType.Id,
            Default_Issuing_Carrier__c = carrier.Id,
            Vertafore_Id__c= 'VertId',
            Default_Agency_Admin__c = [SELECT Id FROM Sagitta_Employee__c WHERE Name =: employee.Name].Id
        );
        insert agency;
        
        Sagitta_Division__c div1 = new Sagitta_Division__c(
            Name = 'Division 1',
            Sagitta_IsHidden__c = false,
            ItemCode__c = '1234'
        );
        Sagitta_Department__c dept1 = new Sagitta_Department__c(
            Name = 'Department 1',
            Sagitta_IsHidden__c = false,
            ItemCode__c = '1235'
        );
        Sagitta_Branch__c branch1 = new Sagitta_Branch__c(
            Name = 'Branch 1',
            Sagitta_IsHidden__c = false,
            ItemCode__c = '1236'
        );
        Sagitta_Group__c grp1 = new Sagitta_Group__c(
            Name = 'Group 1',
            Sagitta_IsHidden__c = false,
            ItemCode__c = '1237'
        );
        insert div1;
        insert dept1;
        insert branch1;
        insert grp1;
        
        Sagitta_Branch__c branch2 = new Sagitta_Branch__c(Name = 'Branch 2');
        insert branch2;
        
        DivBrJunction__c divBrJunc = new DivBrJunction__c(Sagitta_Division__c = div1.Id, Sagitta_Branch__c = branch1.Id);
        BrDepJunction__c brDepJunc = new BrDepJunction__c(Sagitta_Branch__c = branch1.Id, Sagitta_Department__c = dept1.Id);
        DepGrpJunction__c depGrpJunc = new DepGrpJunction__c(Sagitta_Department__c = dept1.Id, Sagitta_Group__c = grp1.Id);
        insert divBrJunc;
        insert brDepJunc;
        insert depGrpJunc;
        
        Sagitta_Mapping__c mapping = new Sagitta_Mapping__c(
        	Name = 'MyMapping',
            Sagitta_Assigned_To__c = UserInfo.getUserId(),
            Sagitta_Division__c = div1.Id,
            Sagitta_Branch__c = branch1.Id,
            Sagitta_Department__c = dept1.Id,
            Sagitta_Group__c = grp1.Id,
            Sagitta_Executive__c = employee.Id,
            Sagitta_Representative__c = employee.Id
        );
        insert mapping;
        
        Account account = new Account(
            Sagitta_Employee_Account_Rep__c = employee.Id,
            Sagitta_Employee_Account_Exec__c = employee.Id,
            Name = 'Test1',
            Phone = '123456',
            Sagitta_First_Name__c = 'FirstName',
            Sagitta_Last_Name__c = 'LastName',
            BillingCity = 'Denver',
            BillingStreet = 'My Street',
            BillingCountry = 'USA',
            BillingPostalCode = '20051',
            ShippingCity = 'Denver',
            ShippingStreet = 'My Street',
            ShippingCountry = 'USA',
            ShippingPostalCode = '20051',
            Sagitta_Division__c = div1.Id,
            Sagitta_Branch__c = branch1.Id,
            Sagitta_Department__c = dept1.Id,
            Sagitta_Group__c = grp1.Id
        );
        insert account;
        
        // No Sagitta Mapping record found. An Sagitta Mapping record must be assigned to this user in order to create an Opportunity.: []
        Opportunity oppty = new Opportunity(
            Name = 'Not Synced',
            AccountId = account.Id,
            CloseDate = Date.today(), 
            StageName = 'Prospecting', 
            Sagitta_Type_of_Business__c = '2',
            Sagitta_Opportunity_Type__c = 'M',
        	Sagitta_Opportunity_Exec__c = employee.Id,
            Sagitta_Opportunity_Rep__c = employee.Id,
            Sagitta_Division__c = div1.Id,
            Sagitta_Branch__c = branch1.Id,
            Sagitta_Department__c = dept1.Id,
            Sagitta_Group__c = grp1.Id
        );
        Sagitta_Line_of_Business__c lob = new Sagitta_Line_of_Business__c(
            Name = 'Commercial', 
            Sagitta_LOB_Code__c = '1234', 
            Sagitta_Type_of_Business__c = '2', 
            Sagitta_Entity_ID__c = '5235633',
            Sagitta_IsHidden__c = false
        );
        insert oppty;
        insert lob;
        
        List<Sagitta_Line_of_Business__c> listOfLOB = LineOfBusinessUtil.getAllLobs(oppty.Id);
        LineOfBusinessUtil.updateLOBs(oppty.Id, new List<Id>{lob.Id}, new List<Id>{lob.Id});
        List<String> lobJnIds = LineOfBusinessUtil.getAllLobFromJn(oppty.Id);
        system.assertEquals(listOfLOB.size(), 1);
    }
    
    private static void createIntegrationFlag(String flag) {
        Metadata__c intFlag = new Metadata__c();
        intFlag.Name = 'IntegrationFlag';
        intFlag.Value__c = flag;
        insert intFlag;
    }
    
    private static void createIsInitialSyncFlag(String flag) {
        Metadata__c isInitialSyncFlag = new Metadata__c();
        isInitialSyncFlag.Name = 'IsInitialSync';
        isInitialSyncFlag.Value__c = flag;
        insert isInitialSyncFlag;
    }
}