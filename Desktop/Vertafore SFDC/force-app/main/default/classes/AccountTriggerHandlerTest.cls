@isTest
public class AccountTriggerHandlerTest { 
    @testSetup
    public static void setup() {
        Profile p = [SELECT Id FROM Profile WHERE Name='Standard User']; 
        User user = new User(Alias = 'standt', 
                             Email='standarduser@testorg.com', 
            				 EmailEncodingKey='UTF-8',
                             LastName='Testing',
                             LanguageLocaleKey='en_US', 
            				 LocaleSidKey='en_US',
                             ProfileId = p.Id, 
            				 TimeZoneSidKey='America/Los_Angeles',
                             UserName='vert-test@vertafore.com');
        insert user;
        
        createIsInitialSyncFlag('false');
        
        Sagitta_Activity_Action_Type__c activityActionType = new Sagitta_Activity_Action_Type__c(
            Sagitta_Activity_Action_Id__c = '26544'
        );
        Sagitta_Issuing_Carrier__c issuingCarrier = new Sagitta_Issuing_Carrier__c(
            Sagitta_Entity_ID__c= 'ID text', 
            Sagitta_Carrier_Type__c= 'N',
            Sagitta_IsHidden__c = false,
            Sagitta_Issuing_Carrier_Code__c= 'text'
        );
        Sagitta_Employee__c employee = new Sagitta_Employee__c(
        	Name = 'vpa',
            Sagitta_Employee_Code__c = '###',	
            Sagitta_Entity_ID__c = '123456'
        );
        insert activityActionType;
        insert issuingCarrier;
        insert employee;
        
        Sagitta_Agency_Settings__c agencySettings1 = new Sagitta_Agency_Settings__c(
            Sagitta_Agency_Number__c='text', 
            Default_Activity_Action_Type__c= [SELECT Id FROM Sagitta_Activity_Action_Type__c LIMIT 1].Id,
            Default_Bill_Method__c= 'A', 
            Default_Issuing_Carrier__c= [SELECT Id FROM Sagitta_Issuing_Carrier__c LIMIT 1].Id, 
            Default_Policy_Number__c='text', 
            Integration_User_Id__c='text', 
            Vertafore_Id__c='text',
            Default_Agency_Admin__c = [SELECT Id FROM Sagitta_Employee__c LIMIT 1].Id
        );
        insert agencySettings1;
        
        Sagitta_Employee__c amsExecutive = new Sagitta_Employee__c(
            Name = 'Executive',
            Sagitta_Is_Executive__c = true,
            Sagitta_Employee_Code__c = 'ABCDEFG',
            Sagitta_Entity_ID__c = '5235633',
            Sagitta_Employee_Status__c = 'Active'
        );
        Sagitta_Employee__c amsRepresentative = new Sagitta_Employee__c(
            Name = 'Representative',
            Sagitta_Is_Representative__c = true,
            Sagitta_Employee_Code__c = '123456',
            Sagitta_Entity_ID__c = '2203450',
            Sagitta_Employee_Status__c = 'Active'
        );
        insert amsExecutive;
       	insert amsRepresentative;

		Sagitta_Division__c div1 = new Sagitta_Division__c(Name = 'Division 1');
        Sagitta_Department__c dept1 = new Sagitta_Department__c(Name = 'Department 1');
        Sagitta_Branch__c branch1 = new Sagitta_Branch__c(Name = 'Branch 1');
        Sagitta_Group__c grp1 = new Sagitta_Group__c(Name = 'Group 1');
        insert div1;
        insert dept1;
        insert branch1;
        insert grp1;
        
        DivBrJunction__c divBrJunc = new DivBrJunction__c(Sagitta_Division__c = div1.Id, Sagitta_Branch__c = branch1.Id);
        BrDepJunction__c brDepJunc = new BrDepJunction__c(Sagitta_Branch__c = branch1.Id, Sagitta_Department__c = dept1.Id);
        DepGrpJunction__c depGrpJunc = new DepGrpJunction__c(Sagitta_Department__c = dept1.Id, Sagitta_Group__c = grp1.Id);
        insert divBrJunc;
        insert brDepJunc;
        insert depGrpJunc;
        
        Sagitta_Mapping__c mapping = new Sagitta_Mapping__c(
        	Name = 'MyMapping',
            Sagitta_Assigned_To__c = UserInfo.getUserId(),
            Sagitta_Division__c = div1.Id,
            Sagitta_Branch__c = branch1.Id,
            Sagitta_Department__c = dept1.Id,
            Sagitta_Group__c = grp1.Id,
            Sagitta_Executive__c = amsExecutive.Id,
            Sagitta_Representative__c = amsRepresentative.Id
        );
        insert mapping;
        
        Account account = new Account(
            Name = 'Test Account',
            Sagitta_Customer_ID__c = 'SagittaCUSTOMERID',
            Sagitta_Employee_Account_Exec__c = amsExecutive.Id,
            Sagitta_Employee_Account_Rep__c = amsRepresentative.Id
        );
        insert account;
    }
    
    @isTest
    public static void shouldNotSendMessage() {
        AccountTriggerHandler accountTriggerHandler = new AccountTriggerHandler();
        TriggerOperation triggerOperation = TriggerOperation.AFTER_UPDATE;
        TriggerHandlerStatus expected = TriggerHandlerStatus.NO_MESSAGES_SENT;
        Account notSyncedAccount = new Account();
        notSyncedAccount.Id = Id.valueOf('001xa0000000000');
        List<Account> listOfAccounts = new List<Account>();
        listOfAccounts.Add(notSyncedAccount);
        
        TriggerHandlerStatus actual = accountTriggerHandler.HandleTrigger(listofAccounts, triggerOperation);
        
        System.assertEquals(expected, actual);
    }
    
    @isTest
    public static void shouldSendMessage() {
        AccountTriggerHandler accountTriggerHandler = new AccountTriggerHandler();
        TriggerOperation triggerOperation = TriggerOperation.AFTER_UPDATE;
        TriggerHandlerStatus expected = TriggerHandlerStatus.MESSAGES_SENT;
        createIntegrationFlag('true');
        
        user user = [SELECT Id FROM User WHERE LastName = 'Testing'];
        Account account = [SELECT Id, Name, OwnerId, Sagitta_Customer_ID__c FROM Account WHERE Name = 'Test Account'];
        
        List<Account> listOfAccounts = new List<Account>();
        listOfAccounts.Add(account);
        
        TriggerHandlerStatus actual = accountTriggerHandler.HandleTrigger(listofAccounts, triggerOperation);
        
        System.assertEquals(expected, actual);
    }
    
    @isTest
    public static void shouldSendMessage_BeforeUpdate() {
        AccountTriggerHandler accountTriggerHandler = new AccountTriggerHandler();
        TriggerOperation triggerOperation = TriggerOperation.BEFORE_UPDATE;
        TriggerHandlerStatus expected = TriggerHandlerStatus.MESSAGES_SENT;
        createIntegrationFlag('true');
        
        user user = [SELECT Id FROM User WHERE LastName = 'Testing'];
        Account account = [SELECT Id, Name, OwnerId, Sagitta_Customer_ID__c, Sagitta_Division__c, Sagitta_Branch__c, Sagitta_Department__c, Sagitta_Group__c FROM Account WHERE Name = 'Test Account'];
        account.Sagitta_Customer_ID__c = '12345';
        account.Sagitta_Client_Type__c = 'C';
        account.Sagitta_Employee_Account_Exec__c = null;
        account.Sagitta_Employee_Account_Rep__c = null;
        
        try {
            update account;
        } catch (Exception e) {
            System.assertEquals(e.getMessage(), 'Update failed. First exception on row 0 with id '+account.Id+'; first error: FIELD_CUSTOM_VALIDATION_EXCEPTION, The Client Type field is read-only.: []');
        }
    }
    
    @isTest
    public static void shouldNotSendMessageNoMapping() {
        List<Sagitta_Mapping__c> mappingRec = new List<Sagitta_Mapping__c>([SELECT Id FROM Sagitta_Mapping__c]);
        delete mappingRec[0];
        AccountTriggerHandler accountTriggerHandler = new AccountTriggerHandler();
        TriggerOperation triggerOperation = TriggerOperation.BEFORE_INSERT;
        TriggerHandlerStatus expected = TriggerHandlerStatus.NO_MESSAGES_SENT;
        Account notSyncedAccount = new Account();
        notSyncedAccount.Id = Id.valueOf('001xa0000000000');
        List<Account> listOfAccounts = new List<Account>();
        listOfAccounts.Add(notSyncedAccount);
        
        TriggerHandlerStatus actual = accountTriggerHandler.HandleTrigger(listofAccounts, triggerOperation);
        
        System.assertEquals(expected, actual);
    }
    
    private static void createCustomSettingsData() {
        Metadata__c met = new Metadata__c();
        met.Name = 'IntegrationFlag';
        met.Value__c = 'true';
        insert met;
    }
    
    private static void createIntegrationFlag(String flag) {
        Metadata__c intFlag = new Metadata__c();
        intFlag.Name = 'IntegrationFlag';
        intFlag.Value__c = flag;
        insert intFlag;
    }
    
    private static void createIsInitialSyncFlag(String flag) {
        Metadata__c isInitialSyncFlag = new Metadata__c();
        isInitialSyncFlag.Name = 'IsInitialSync';
        isInitialSyncFlag.Value__c = flag;
        insert isInitialSyncFlag;
    }
}