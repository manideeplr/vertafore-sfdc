@isTest
private class SagittaActivityActionTypeTest {
    @testSetup
    static void setup() {
        createIntegrationFlag('true');
        createIsInitialSyncFlag('false');
    }
    
    // Insert record - should succeed since the request is coming from API
    @isTest static void insertTest() {
        Sagitta_Activity_Action_Type__c actionType = new Sagitta_Activity_Action_Type__c(Name='Billing', Sagitta_Activity_Action_Id__c = '29352653');
        
        Database.SaveResult result = Database.insert(actionType);
        
        System.assert(result.isSuccess());
    }
    
    // Update record - should succeed since the request is coming from API
    @isTest static void updateTest() {
        Sagitta_Activity_Action_Type__c actionType = new Sagitta_Activity_Action_Type__c(Name='Billing', Sagitta_Activity_Action_Id__c = '02750345');
        insert actionType;
        
        // Update record
        actionType.Name = 'Application';
        Database.SaveResult result = Database.update(actionType);
        
        System.assert(result.isSuccess());
    }
    
    private static void createIntegrationFlag(String flag) {
        Metadata__c intFlag = new Metadata__c();
        intFlag.Name = 'IntegrationFlag';
        intFlag.Value__c = flag;
        insert intFlag;
    }
    
    private static void createIsInitialSyncFlag(String flag) {
        Metadata__c isInitialSyncFlag = new Metadata__c();
        isInitialSyncFlag.Name = 'IsInitialSync';
        isInitialSyncFlag.Value__c = flag;
        insert isInitialSyncFlag;
    }
}