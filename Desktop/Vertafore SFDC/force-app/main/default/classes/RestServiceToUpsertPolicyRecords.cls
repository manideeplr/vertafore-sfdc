/*
    Author : Manideep
    Name : RestServiceToUpsertPolicyRecords
    Date : 1 November 2019
    Description:
    Modification History :
    *************************************************************
    Version               Author             Date                      Description
    1                    Manideep         1 November 2019               Original
*/

@RestResource(urlMapping='/UpsertPolicyRecords/*')
global with sharing class RestServiceToUpsertPolicyRecords {
    global class SagittaPolicy {    
        global String BillMethod { get; set; }
        global String CsrCode { get; set; }
        global String CustomerId { get; set; }
        global String Description { get; set; }
        global String EffectiveDate { get; set; }
        global String ExecCode { get; set; }
        global String ExpiryDate { get; set; }
        global Decimal FullTermPremium { get; set; }
        global String GLBranchCode { get; set; }
        global String GLDepartmentCode { get; set; }
        global String GLDivisionCode { get; set; }
        global String GLGroupCode { get; set; }
        global String LineOfBusinessIDs { get; set; }
        global String PolicyId { get; set; }
        global String PolicyNumber { get; set; }
		global String RenewalReportFlag { get; set; }
        global String TypeOfBusiness { get; set; }
        global String WritingCompanyCode { get; set; }
    }
    
    global class ResponseHandler {
        public Boolean Success { get; set; }
        public String Status { get; set; }
        public String Message { get; set; }
    }

    private static RestServicePolicyContext policyContext;
    
    @HttpPost
    global static ResponseHandler doPost() {
        ResponseHandler obj = new ResponseHandler();
        RestRequest req = RestContext.request;
        String postBody;
        String failedPolicyIds = '';
        
        try {
            postBody = req.requestBody.toString();
            List<SagittaPolicy> policies;
            Schema.SObjectField extIdfield = Sagitta_Policy__c.Fields.Sagitta_Policy_ID__c;
            policies = (List<SagittaPolicy>)JSON.deserialize(postBody, List<SagittaPolicy>.class);
            policyContext = new RestServicePolicyContext(policies);
                
            for (SagittaPolicy policy : policies) {
                try {
                    Sagitta_Policy__c existingPolicy = policyContext.findPolicy(policy.PolicyId);
                    
                    if (mapFields(existingPolicy, policy)) {
                        Database.UpsertResult policyUpsertResult = Database.upsert(existingPolicy, extIdfield, false);  
                        Database.Error[] policyUpsertErrors = policyUpsertResult.getErrors();
                        if (policyUpsertErrors.size() > 0) {
                            ApplicationLogger.logMessage('RestServiceToUpsertPolicyRecords', 'doPost', 'Policy Upsert Error: ' + policyUpsertErrors, ApplicationLogger.ERROR);
                            failedPolicyIds = failedPolicyIds + policy.PolicyId + ', ';
                        }
                    }
                }
                catch(Exception ex) {
                    ApplicationLogger.logMessage('RestServiceToUpsertPolicyRecords', 'doPost', 'Exception Caught: ' + ex.getMessage() + '; Stace Trace: ' + ex.getStackTraceString(), ApplicationLogger.ERROR);
                    failedPolicyIds = failedPolicyIds + policy.PolicyId + ', ';
                }
            }
            
            if (String.isNotBlank(failedPolicyIds)) {
                List<Object> parameters = new List<Object>();
                parameters.add(failedPolicyIds.removeEnd(', '));
                String formatted = String.format('Could not process Sagitta Policies: {0} in Salesforce.', parameters);
                DmlException ex = new DmlException();
                ex.setMessage(formatted);
                throw ex;
            }
            
            obj.success = true;
            obj.status = 'SUCCESS';
            obj.message = 'SUCCESS';
            ApplicationLogger.logWebServiceMessage('RestServiceToUpsertPolicyRecords', 'doPost', obj.message,
                                                    ApplicationLogger.INFO, postBody, JSON.serialize(obj));
		    return obj;
        }
        catch(Exception ex) {
            RestContext.response.statusCode = 400;
            obj.success = false;
            obj.status ='ERROR';
            obj.message = ex.getMessage();
            ApplicationLogger.logWebServiceMessage('RestServiceToUpsertPolicyRecords', 'doPost', obj.message,
                                                    ApplicationLogger.ERROR, postBody, JSON.serialize(obj));
            return obj;
        }    
        //return null;    
    }
    
    private static boolean mapFields(Sagitta_Policy__c existingPolicy, SagittaPolicy policy) {
        boolean fieldsUpdated = false;
        
        Id division = policyContext.findDivision(policy.GLDivisionCode).Id;
        Id branch = policyContext.findBranch(policy.GLBranchCode).Id;
        Id department = policyContext.findDept(policy.GLDepartmentCode).Id;
        // group is a keyword in apex
        Id grp = policyContext.findGroup(policy.GLGroupCode).Id;
        
        if (existingPolicy.Sagitta_Division__c != division) {
            existingPolicy.Sagitta_Division__c = division;
            fieldsUpdated = true;
        }
        
        if (existingPolicy.Sagitta_Branch__c != branch) {
            existingPolicy.Sagitta_Branch__c = branch;
            fieldsUpdated = true;
        }
        
        if (existingPolicy.Sagitta_Department__c != department) {
            existingPolicy.Sagitta_Department__c = department;
            fieldsUpdated = true;
        }
        
        if (existingPolicy.Sagitta_Group__c != grp) {
            existingPolicy.Sagitta_Group__c = grp;
            fieldsUpdated = true;
        }
        
        Id exec = policyContext.findEmployee(policy.ExecCode).Id;
        Id rep = policyContext.findEmployee(policy.CsrCode).Id;
        
        if (existingPolicy.Sagitta_Employee_Policy_Exec__c != exec) {
            existingPolicy.Sagitta_Employee_Policy_Exec__c = exec;
            fieldsUpdated = true;
        }
        
        if (existingPolicy.Sagitta_Employee_Policy_Rep__c != rep) {
            existingPolicy.Sagitta_Employee_Policy_Rep__c = rep;
            fieldsUpdated = true;
        }
        
        Id account = policyContext.findAccount(policy.CustomerId).Id;
        
        if (existingPolicy.Sagitta_Account_Name__c != account) {
			existingPolicy.Sagitta_Account_Name__c = account;
            fieldsUpdated = true;
        }
        
        if (existingPolicy.Sagitta_Bill_Method__c != policy.BillMethod) {
			existingPolicy.Sagitta_Bill_Method__c = policy.BillMethod;
            fieldsUpdated = true;
        }
        
        if (existingPolicy.Sagitta_Customer_ID__c != policy.CustomerId) {
			existingPolicy.Sagitta_Customer_ID__c = policy.CustomerId;
            fieldsUpdated = true;
        }
        
        if (existingPolicy.Sagitta_Description__c != policy.Description) {
			existingPolicy.Sagitta_Description__c = policy.Description;
            fieldsUpdated = true;
        }
        
        Date effectiveDate = String.isBlank(policy.EffectiveDate) ?  null : iso8601StringToDate(policy.EffectiveDate);
        Date expirationDate = String.isBlank(policy.ExpiryDate) ? null : iso8601StringToDate(policy.ExpiryDate);
        
        if (existingPolicy.Sagitta_Effective_Date__c != effectiveDate) {
            existingPolicy.Sagitta_Effective_Date__c = effectiveDate;
            fieldsUpdated = true;
        }
        
        if (existingPolicy.Sagitta_Expiration_Date__c != expirationDate) {
            existingPolicy.Sagitta_Expiration_Date__c = expirationDate;
            fieldsUpdated = true;
        }
        
        if (existingPolicy.Sagitta_Policy_ID__c != policy.PolicyId) {
            existingPolicy.Sagitta_Policy_ID__c = policy.PolicyId;
            fieldsUpdated = true;
        }
        
        if (existingPolicy.Name != policy.PolicyNumber) {
            existingPolicy.Name = policy.PolicyNumber;
            fieldsUpdated = true;
        }
        
        if (existingPolicy.Sagitta_Policy_Status__c != policy.RenewalReportFlag) {
            existingPolicy.Sagitta_Policy_Status__c = policy.RenewalReportFlag;
            fieldsUpdated = true;
        }
        
        if (existingPolicy.Sagitta_Type_of_Business__c != policy.TypeOfBusiness) {
            existingPolicy.Sagitta_Type_of_Business__c = policy.TypeOfBusiness;
            fieldsUpdated = true;
        }
        
        if (existingPolicy.Sagitta_Annual_Premium__c != policy.FullTermPremium) {
            existingPolicy.Sagitta_Annual_Premium__c = policy.FullTermPremium;
            fieldsUpdated = true;
        }
        
        Id issuingCarrier = policyContext.findIssuingCarrier(policy.WritingCompanyCode).Id;
        
        if (existingPolicy.Sagitta_Writing_Company__c != issuingCarrier) {
            existingPolicy.Sagitta_Writing_Company__c = issuingCarrier;
            fieldsUpdated = true;
        }
        
        // if LineOfBusinessIDs is null split inside createLobString method will not work
        String lobs = String.isBlank(policy.LineOfBusinessIDs) ? '' : policyContext.getLobNamesAsString(policy.LineOfBusinessIDs);
        
		if (existingPolicy.Sagitta_LOB__c != lobs) {
            existingPolicy.Sagitta_LOB__c = lobs;
            fieldsUpdated = true;
        }

        return fieldsUpdated;
    }
    
    // iso8601 is a specific date format
    private static Date iso8601StringToDate(String iso8601String) {
        String dateString = iso8601String.substringBefore('T');
        return Date.valueOf(dateString);
    }
}